#+TITLE: Manpages - ram.4
#+DESCRIPTION: Linux manpage for ram.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ram - ram disk device

* DESCRIPTION
The /ram/ device is a block device to access the ram disk in raw mode.

It is typically created by:

#+begin_example
  mknod -m 660 /dev/ram b 1 1
  chown root:disk /dev/ram
#+end_example

* FILES
//dev/ram/

* SEE ALSO
*chown*(1), *mknod*(1), *mount*(8)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
