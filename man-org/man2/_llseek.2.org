#+TITLE: Manpages - _llseek.2
#+DESCRIPTION: Linux manpage for _llseek.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about _llseek.2 is found in manpage for: [[../man2/llseek.2][man2/llseek.2]]