#+TITLE: Manpages - getgid.2
#+DESCRIPTION: Linux manpage for getgid.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
getgid, getegid - get group identity

* SYNOPSIS
#+begin_example
  #include <unistd.h>

  gid_t getgid(void);
  gid_t getegid(void);
#+end_example

* DESCRIPTION
*getgid*() returns the real group ID of the calling process.

*getegid*() returns the effective group ID of the calling process.

* ERRORS
These functions are always successful and never modify /errno/.

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, 4.3BSD.

* NOTES
The original Linux *getgid*() and *getegid*() system calls supported
only 16-bit group IDs. Subsequently, Linux 2.4 added *getgid32*() and
*getegid32*(), supporting 32-bit IDs. The glibc *getgid*() and
*getegid*() wrapper functions transparently deal with the variations
across kernel versions.

On Alpha, instead of a pair of *getgid*() and *getegid*() system calls,
a single *getxgid*() system call is provided, which returns a pair of
real and effective GIDs. The glibc *getgid*() and *getegid*() wrapper
functions transparently deal with this. See *syscall*(2) for details
regarding register mapping.

* SEE ALSO
*getresgid*(2), *setgid*(2), *setregid*(2), *credentials*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
