#+TITLE: Manpages - vhangup.2
#+DESCRIPTION: Linux manpage for vhangup.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vhangup - virtually hangup the current terminal

* SYNOPSIS
#+begin_example
  #include <unistd.h>

  int vhangup(void);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*vhangup*():

#+begin_example
      Since glibc 2.21:
          _DEFAULT_SOURCE
      In glibc 2.19 and 2.20:
          _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE < 500)
      Up to and including glibc 2.19:
          _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE < 500)
#+end_example

* DESCRIPTION
*vhangup*() simulates a hangup on the current terminal. This call
arranges for other users to have a “clean” terminal at login time.

* RETURN VALUE
On success, zero is returned. On error, -1 is returned, and /errno/ is
set to indicate the error.

* ERRORS
- *EPERM* :: The calling process has insufficient privilege to call
  *vhangup*(); the *CAP_SYS_TTY_CONFIG* capability is required.

* CONFORMING TO
This call is Linux-specific, and should not be used in programs intended
to be portable.

* SEE ALSO
*init*(1), *capabilities*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
