#+TITLE: Manpages - setdomainname.2
#+DESCRIPTION: Linux manpage for setdomainname.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setdomainname.2 is found in manpage for: [[../man2/getdomainname.2][man2/getdomainname.2]]