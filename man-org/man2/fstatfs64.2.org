#+TITLE: Manpages - fstatfs64.2
#+DESCRIPTION: Linux manpage for fstatfs64.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fstatfs64.2 is found in manpage for: [[../man2/fstatfs.2][man2/fstatfs.2]]