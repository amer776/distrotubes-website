#+TITLE: Manpages - truncate64.2
#+DESCRIPTION: Linux manpage for truncate64.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about truncate64.2 is found in manpage for: [[../man2/truncate.2][man2/truncate.2]]