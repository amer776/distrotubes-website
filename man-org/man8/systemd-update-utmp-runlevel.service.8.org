#+TITLE: Manpages - systemd-update-utmp-runlevel.service.8
#+DESCRIPTION: Linux manpage for systemd-update-utmp-runlevel.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-update-utmp-runlevel.service.8 is found in manpage for: [[../systemd-update-utmp.service.8][systemd-update-utmp.service.8]]