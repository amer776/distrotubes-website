#+TITLE: Manpages - NetworkManager-dispatcher.8
#+DESCRIPTION: Linux manpage for NetworkManager-dispatcher.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
NetworkManager-dispatcher - Dispatch user scripts for NetworkManager

* SYNOPSIS
*NetworkManager [OPTIONS...]*

* DESCRIPTION
NetworkManager-dispatcher service is a D-Bus activated service that runs
user provided scripts upon certain changes in NetworkManager.

NetworkManager-dispatcher will execute scripts in the
/{etc,usr/lib}/NetworkManager/dispatcher.d directory or subdirectories
in alphabetical order in response to network events. Each script should
be a regular executable file owned by root. Furthermore, it must not be
writable by group or other, and not setuid.

Each script receives two arguments, the first being the interface name
of the device an operation just happened on, and second the action. For
device actions, the interface is the name of the kernel interface
suitable for IP configuration. Thus it is either VPN_IP_IFACE,
DEVICE_IP_IFACE, or DEVICE_IFACE, as applicable. For the /hostname/
action the device name is always "none" and for /connectivity-change/ it
is empty.

The actions are:

/pre-up/

#+begin_quote
  The interface is connected to the network but is not yet fully
  activated. Scripts acting on this event must be placed or symlinked
  into the /etc/NetworkManager/dispatcher.d/pre-up.d directory, and
  NetworkManager will wait for script execution to complete before
  indicating to applications that the interface is fully activated.
#+end_quote

/up/

#+begin_quote
  The interface has been activated.
#+end_quote

/pre-down/

#+begin_quote
  The interface will be deactivated but has not yet been disconnected
  from the network. Scripts acting on this event must be placed or
  symlinked into the /etc/NetworkManager/dispatcher.d/pre-down.d
  directory, and NetworkManager will wait for script execution to
  complete before disconnecting the interface from its network. Note
  that this event is not emitted for forced disconnections, like when
  carrier is lost or a wireless signal fades. It is only emitted when
  there is an opportunity to cleanly handle a network disconnection
  event.
#+end_quote

/down/

#+begin_quote
  The interface has been deactivated.
#+end_quote

/vpn-pre-up/

#+begin_quote
  The VPN is connected to the network but is not yet fully activated.
  Scripts acting on this event must be placed or symlinked into the
  /etc/NetworkManager/dispatcher.d/pre-up.d directory, and
  NetworkManager will wait for script execution to complete before
  indicating to applications that the VPN is fully activated.
#+end_quote

/vpn-up/

#+begin_quote
  A VPN connection has been activated.
#+end_quote

/vpn-pre-down/

#+begin_quote
  The VPN will be deactivated but has not yet been disconnected from the
  network. Scripts acting on this event must be placed or symlinked into
  the /etc/NetworkManager/dispatcher.d/pre-down.d directory, and
  NetworkManager will wait for script execution to complete before
  disconnecting the VPN from its network. Note that this event is not
  emitted for forced disconnections, like when the VPN terminates
  unexpectedly or general connectivity is lost. It is only emitted when
  there is an opportunity to cleanly handle a VPN disconnection event.
#+end_quote

/vpn-down/

#+begin_quote
  A VPN connection has been deactivated.
#+end_quote

/hostname/

#+begin_quote
  The system hostname has been updated. Use gethostname(2) to retrieve
  it. The interface name (first argument) is empty and no environment
  variable is set for this action.
#+end_quote

/dhcp4-change/

#+begin_quote
  The DHCPv4 lease has changed (renewed, rebound, etc).
#+end_quote

/dhcp6-change/

#+begin_quote
  The DHCPv6 lease has changed (renewed, rebound, etc).
#+end_quote

/connectivity-change/

#+begin_quote
  The network connectivity state has changed (no connectivity, went
  online, etc).
#+end_quote

The environment contains more information about the interface and the
connection. The following variables are available for the use in the
dispatcher scripts:

/NM_DISPATCHER_ACTION/

#+begin_quote
  The dispatcher action like "up" or "dhcp4-change", identical to the
  first command line argument. Since NetworkManager 1.12.0.
#+end_quote

/CONNECTION_UUID/

#+begin_quote
  The UUID of the connection profile.
#+end_quote

/CONNECTION_ID/

#+begin_quote
  The name (ID) of the connection profile.
#+end_quote

/CONNECTION_DBUS_PATH/

#+begin_quote
  The NetworkManager D-Bus path of the connection.
#+end_quote

/CONNECTION_FILENAME/

#+begin_quote
  The backing file name of the connection profile (if any).
#+end_quote

/CONNECTION_EXTERNAL/

#+begin_quote
  If "1", this indicates that the connection describes a network
  configuration created outside of NetworkManager.
#+end_quote

/DEVICE_IFACE/

#+begin_quote
  The interface name of the control interface of the device. Depending
  on the device type, this differs from /DEVICE_IP_IFACE/. For example
  for ADSL devices, this could be atm0 or for WWAN devices it might be
  ttyUSB0.
#+end_quote

/DEVICE_IP_IFACE/

#+begin_quote
  The IP interface name of the device. This is the network interface on
  which IP addresses and routes will be configured.
#+end_quote

/IP4_ADDRESS_N/

#+begin_quote
  The IPv4 address in the format "address/prefix gateway", where N is a
  number from 0 to (# IPv4 addresses - 1). gateway item in this variable
  is deprecated, use IP4_GATEWAY instead.
#+end_quote

/IP4_NUM_ADDRESSES/

#+begin_quote
  The variable contains the number of IPv4 addresses the script may
  expect.
#+end_quote

/IP4_GATEWAY/

#+begin_quote
  The gateway IPv4 address in traditional numbers-and-dots notation.
#+end_quote

/IP4_ROUTE_N/

#+begin_quote
  The IPv4 route in the format "address/prefix next-hop metric", where N
  is a number from 0 to (# IPv4 routes - 1).
#+end_quote

/IP4_NUM_ROUTES/

#+begin_quote
  The variable contains the number of IPv4 routes the script may expect.
#+end_quote

/IP4_NAMESERVERS/

#+begin_quote
  The variable contains a space-separated list of the DNS servers.
#+end_quote

/IP4_DOMAINS/

#+begin_quote
  The variable contains a space-separated list of the search domains.
#+end_quote

/DHCP4_<dhcp-option-name>/

#+begin_quote
  If the connection used DHCP for address configuration, the received
  DHCP configuration is passed in the environment using standard DHCP
  option names, prefixed with "DHCP4_", like "DHCP4_HOST_NAME=foobar".
#+end_quote

/IP6_<name> and DHCP6_<name>/

#+begin_quote
  The same variables as for IPv4 are available for IPv6, but the
  prefixes are IP6_ and DHCP6_ instead.
#+end_quote

/CONNECTIVITY_STATE/

#+begin_quote
  The network connectivity state, which can take the values defined by
  the NMConnectivityState type, from the org.freedesktop.NetworkManager
  D-Bus API: unknown, none, portal, limited or full. Note: this variable
  will only be set for connectivity-change actions.
#+end_quote

In case of VPN, VPN_IP_IFACE is set, and IP4_*, IP6_* variables with VPN
prefix are exported too, like VPN_IP4_ADDRESS_0, VPN_IP4_NUM_ADDRESSES.

Dispatcher scripts are run one at a time, but asynchronously from the
main NetworkManager process, and will be killed if they run for too
long. If your script might take arbitrarily long to complete, you should
spawn a child process and have the parent return immediately. Scripts
that are symbolic links pointing inside the
/etc/NetworkManager/dispatcher.d/no-wait.d/ directory are run
immediately, without waiting for the termination of previous scripts,
and in parallel. Also beware that once a script is queued, it will
always be run, even if a later event renders it obsolete. (Eg, if an
interface goes up, and then back down again quickly, it is possible that
one or more "up" scripts will be run after the interface has gone down.)

* BUGS
Please report any bugs you find in NetworkManager at the *NetworkManager
issue tracker*[1].

* SEE ALSO
*NetworkManager home page*[2], *NetworkManager*(8),

* NOTES
-  1. :: NetworkManager issue tracker

  https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/issues

-  2. :: NetworkManager home page

  https://networkmanager.dev
