#+TITLE: Manpages - devlink-monitor.8
#+DESCRIPTION: Linux manpage for devlink-monitor.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
devlink-monitor - state monitoring

* SYNOPSIS
*devlink monitor* [ *all* | /OBJECT-LIST/ ]

* DESCRIPTION
The *devlink* utility can monitor the state of devlink devices and ports
continuously. This option has a slightly different format. Namely, the
*monitor* command is the first in the command line and then the object
list.

/OBJECT-LIST/ is the list of object types that we want to monitor. It
may contain *dev*, *port*, *health*, *trap*, *trap-group*,
*trap-policer*.

*devlink* opens Devlink Netlink socket, listens on it and dumps state
changes.

* SEE ALSO
*devlink*(8), *devlink-dev*(8), *devlink-sb*(8), *devlink-port*(8),
*devlink-health*(8), *devlink-trap*(8),\\

* AUTHOR
Jiri Pirko <jiri@mellanox.com>
