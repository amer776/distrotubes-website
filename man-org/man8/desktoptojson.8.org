#+TITLE: Manpages - desktoptojson.8
#+DESCRIPTION: Linux manpage for desktoptojson.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
desktoptojson - Converts a .desktop file to a .json file.

* SYNOPSIS
*desktoptojson* --input /DESKTOP-FILE/ --output /JSON-FILE/

*desktoptojson* { | --version | --help }

* DESCRIPTION
The KService framework uses information contained in .desktop files to
locate services, including plugins for Qt5-based applications and
libraries. The Qt5 plugin system, however, uses JSON data embedded in
the plugin itself. *desktoptojson* allows the information contained in a
.desktop file to also be used as the embedded data for a Qt5 plugin by
converting the .desktop file entries into JSON data.

The generated JSON data is a JSON object that maps the entries from the
[Desktop Entry] group of the .desktop file. Any other groups are
ignored. Most entries are just converted to JSON strings, but certain
entries (such as Hidden and X-KDE-PluginInfo-EnabledByDefault) are known
to be boolean values and converted as such, and similarly some (such as
X-KDE-ServiceTypes and X-KDE-PluginInfo-Depends) are always converted to
arrays of strings.

* OPTIONS
*--input */DESKTOP-FILE/

#+begin_quote

  The .desktop file to convert.
#+end_quote

*--output */JSON-FILE/

#+begin_quote
  The file to write the generated JSON data to.
#+end_quote

*--help*

#+begin_quote
  Show a brief help text.
#+end_quote

*--version*

#+begin_quote
  Show version information.
#+end_quote

* USAGE
Most users of this utility will use the CMake macro
*kservice_desktop_to_json* as part of the process of building a plugin.

#+begin_quote
  #+begin_example

    add_library(myplugin MODULE ${myplugin_SRCS})
    kservice_desktop_to_json(myplugin myplugin.desktop)
  #+end_example
#+end_quote

This will produce the JSON file myplugin.json, which can be referenced
from the *K_PLUGIN_FACTORY_WITH_JSON* or *Q_PLUGIN_METADATA* macros.

* BUGS
Please use *KDEs bugtracker*[1] to report bugs.

* AUTHORS
*Scarlett Clark* <scarlett@scarlettgatelyclark.com>\\

#+begin_quote
  Wrote the original documentation.
#+end_quote

*Alex Merry* <alexmerry@kde.org>\\

#+begin_quote
  Edited the documentation.
#+end_quote

* NOTES
-  1. :: KDE's bugtracker

  https://bugs.kde.org
