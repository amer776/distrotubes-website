#+TITLE: Manpages - depmod.8
#+DESCRIPTION: Linux manpage for depmod.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
depmod - Generate modules.dep and map files.

* SYNOPSIS
*depmod* [*-b */basedir/] [*-e*] [*-E */Module.symvers/] [*-F
*/System.map/] [*-n*] [*-v*] [*-A*] [*-P */prefix/] [*-w*] [/version/]

*depmod* [*-e*] [*-E */Module.symvers/] [*-F */System.map/] [*-n*]
[*-v*] [*-P */prefix/] [*-w*] [/version/] [/filename/...]

* DESCRIPTION
Linux kernel modules can provide services (called "symbols") for other
modules to use (using one of the EXPORT_SYMBOL variants in the code). If
a second module uses this symbol, that second module clearly depends on
the first module. These dependencies can get quite complex.

*depmod* creates a list of module dependencies by reading each module
under /lib/modules//version/ and determining what symbols it exports and
what symbols it needs. By default, this list is written to modules.dep,
and a binary hashed version named modules.dep.bin, in the same
directory. If filenames are given on the command line, only those
modules are examined (which is rarely useful unless all modules are
listed). *depmod* also creates a list of symbols provided by modules in
the file named modules.symbols and its binary hashed version,
modules.symbols.bin. Finally, *depmod* will output a file named
modules.devname if modules supply special device names (devname) that
should be populated in /dev on boot (by a utility such as
systemd-tmpfiles).

If a /version/ is provided, then that kernel versions module directory
is used rather than the current kernel version (as returned by *uname
-r*).

* OPTIONS
*-a*, *--all*

#+begin_quote
  Probe all modules. This option is enabled by default if no file names
  are given in the command-line.
#+end_quote

*-A*, *--quick*

#+begin_quote
  This option scans to see if any modules are newer than the modules.dep
  file before any work is done: if not, it silently exits rather than
  regenerating the files.
#+end_quote

*-b */basedir/, *--basedir */basedir/

#+begin_quote
  If your modules are not currently in the (normal) directory
  /lib/modules//version/, but in a staging area, you can specify a
  /basedir/ which is prepended to the directory name. This /basedir/ is
  stripped from the resulting modules.dep file, so it is ready to be
  moved into the normal location. Use this option if you are a
  distribution vendor who needs to pre-generate the meta-data files
  rather than running depmod again later.
#+end_quote

*-C*, *--config */file or directory/

#+begin_quote
  This option overrides the default configuration directory at
  /etc/depmod.d/.
#+end_quote

*-e*, *--errsyms*

#+begin_quote
  When combined with the *-F* option, this reports any symbols which a
  module needs which are not supplied by other modules or the kernel.
  Normally, any symbols not provided by modules are assumed to be
  provided by the kernel (which should be true in a perfect world), but
  this assumption can break especially when additionally updated third
  party drivers are not correctly installed or were built incorrectly.
#+end_quote

*-E*, *--symvers*

#+begin_quote
  When combined with the *-e* option, this reports any symbol versions
  supplied by modules that do not match with the symbol versions
  provided by the kernel in its Module.symvers. This option is mutually
  incompatible with *-F*.
#+end_quote

*-F*, *--filesyms */System.map/

#+begin_quote
  Supplied with the System.map produced when the kernel was built, this
  allows the *-e* option to report unresolved symbols. This option is
  mutually incompatible with *-E*.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print the help message and exit.
#+end_quote

*-n*, *--show*, *--dry-run*

#+begin_quote
  This sends the resulting modules.dep and the various map files to
  standard output rather than writing them into the module directory.
#+end_quote

*-P*

#+begin_quote
  Some architectures prefix symbols with an extraneous character. This
  specifies a prefix character (for example _) to ignore.
#+end_quote

*-v*, *--verbose*

#+begin_quote
  In verbose mode, *depmod* will print (to stdout) all the symbols each
  module depends on and the modules file name which provides that
  symbol.
#+end_quote

*-V*, *--version*

#+begin_quote
  Show version of program and exit. See below for caveats when run on
  older kernels.
#+end_quote

*-w*

#+begin_quote
  Warn on duplicate dependencies, aliases, symbol versions, etc.
#+end_quote

* COPYRIGHT
This manual page originally Copyright 2002, Rusty Russell, IBM
Corporation. Portions Copyright Jon Masters, and others.

* SEE ALSO
*depmod.d*(5), *modprobe*(8), *modules.dep*(5)

* AUTHORS
*Jon Masters* <jcm@jonmasters.org>

#+begin_quote
  Developer
#+end_quote

*Robby Workman* <rworkman@slackware.com>

#+begin_quote
  Developer
#+end_quote

*Lucas De Marchi* <lucas.de.marchi@gmail.com>

#+begin_quote
  Developer
#+end_quote
