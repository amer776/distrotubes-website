#+TITLE: Manpages - atmdiag.8
#+DESCRIPTION: Linux manpage for atmdiag.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
atmdiag - show ATM device driver diagnostics

* SYNOPSIS
*atmdiag* [*-z*] [*/interface/*...*]*

* DESCRIPTION
*atmdiag* shows statistic counters maintained by ATM device drivers. If
one or more interface numbers are specified, the statistics of only
these interfaces are shown. Otherwise, statistics of all interfaces are
displayed.

The following counters are shown for each AAL:

#+begin_quote
  - TX_okay :: number of successfully sent PDUs.

  - TX_err :: number of PDUs not sent because of errors.

  - RX_okay :: number of successfully received PDUs.

  - RX_err :: number of PDUs discarded because of errors.

  - RX_drop :: number of PDUs dropped because of lack of memory.
#+end_quote

* OPTIONS
- -z :: zero all counters after showing them.

* AUTHOR
Werner Almesberger, EPFL ICA <Werner.Almesberger@epfl.ch>

* SEE ALSO
atmdump(8), sonetdiag(8)
