#+TITLE: Manpages - btrfs-select-super.8
#+DESCRIPTION: Linux manpage for btrfs-select-super.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
btrfs-select-super - overwrite primary superblock with a backup copy

* SYNOPSIS
*btrfs-select-super* -s number /<device>/

* DESCRIPTION
Destructively overwrite all copies of the superblock with a specified
copy. This helps in certain cases, for example when write barriers were
disabled during a power failure and not all superblocks were written, or
if the primary superblock is damaged, eg. accidentally overwritten.

The filesystem specified by /device/ must not be mounted.

#+begin_quote
  \\

  *Note*

  \\

  *Prior to overwriting the primary superblock, please make sure that
  the backup copies are valid!*
#+end_quote

To dump a superblock use the *btrfs inspect-internal dump-super*
command.

Then run the check (in the non-repair mode) using the command *btrfs
check -s* where /-s/ specifies the superblock copy to use.

Superblock copies exist in the following offsets on the device:

#+begin_quote
  ·

  primary: /64KiB/ (65536)
#+end_quote

#+begin_quote
  ·

  1st copy: /64MiB/ (67108864)
#+end_quote

#+begin_quote
  ·

  2nd copy: /256GiB/ (274877906944)
#+end_quote

A superblock size is /4KiB/ (4096).

* OPTIONS
-s|--super /<superblock>/

#+begin_quote
  use superblock'th superblock copy, valid values are 0 1 or 2 if the
  respective superblock offset is within the device size
#+end_quote

* SEE ALSO
*btrfs-inspect-internal*(8), *btrfsck check*(8)
