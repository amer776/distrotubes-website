#+TITLE: Manpages - lvmdbusd.8
#+DESCRIPTION: Linux manpage for lvmdbusd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lvmdbusd --- LVM D-Bus daemon

* SYNOPSIS
*lvmdbusd* [*--debug*] [*--udev*]

* DESCRIPTION
lvmdbusd is a service which provides a D-Bus API to the logical volume
manager (LVM). Run *lvmdbusd*(8) as root.

* OPTIONS
- *--debug* :: Enable debug statements

- *--udev* :: Use udev events to trigger updates

* SEE ALSO
*dbus-send*(1), *lvm*(8)
