#+TITLE: Manpages - pppoe-stop.8
#+DESCRIPTION: Linux manpage for pppoe-stop.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pppoe-stop - Shell script to shut down a PPPoE link

* SYNOPSIS
*pppoe-stop [/config_file/]*

* DESCRIPTION
*pppoe-stop* is a shell script which stops the RP-PPPoE user-space PPPoE
client. If you omit /config_file/, the default file
*/etc/ppp/pppoe.conf* is used.

* AUTHOR
*pppoe-stop* was written by Dianne Skoll <dianne@skoll.ca>

The *pppoe* home page is /https://dianne.skoll.ca/projects/rp-pppoe//.

* SEE ALSO
pppoe(8), pppoe-start(8), pppoe-connect(8), pppd(8), pppoe.conf(5),
pppoe-setup(8), pppoe-status(8), pppoe-sniff(8), pppoe-relay(8),
pppoe-server(8)
