#+TITLE: Manpages - udevadm.8
#+DESCRIPTION: Linux manpage for udevadm.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
udevadm - udev management tool

* SYNOPSIS
*udevadm* [*--debug*] [*--version*] [*--help*]

*udevadm info [options] [devpath]*

*udevadm trigger [options] [devpath]*

*udevadm settle [options]*

*udevadm control */option/

*udevadm monitor [options]*

*udevadm test [options] */devpath/

*udevadm test-builtin [options] */command/* */devpath/

* DESCRIPTION
*udevadm* expects a command and command specific options. It controls
the runtime behavior of *systemd-udevd*, requests kernel events, manages
the event queue, and provides simple debugging mechanisms.

* OPTIONS
*-d*, *--debug*

#+begin_quote
  Print debug messages to standard error. This option is implied in
  *udevadm test* and *udevadm test-builtin* commands.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

** udevadm info [/options/] [/devpath/|/file/|/unit/...]
Query the udev database for device information.

Positional arguments should be used to specify one or more devices. Each
one may be a device name (in which case it must start with /dev/), a sys
path (in which case it must start with /sys/), or a systemd device unit
name (in which case it must end with ".device", see
*systemd.device*(5)).

*-q*, *--query=*/TYPE/

#+begin_quote
  Query the database for the specified type of device data. Valid
  /TYPE/s are: *name*, *symlink*, *path*, *property*, *all*.
#+end_quote

*-p*, *--path=*/DEVPATH/

#+begin_quote
  The /sys/ path of the device to query, e.g. [/sys/]/class/block/sda.
  This option is an alternative to the positional argument with a /sys/
  prefix. *udevadm info --path=/class/block/sda* is equivalent to
  *udevadm info /sys/class/block/sda*.
#+end_quote

*-n*, *--name=*/FILE/

#+begin_quote
  The name of the device node or a symlink to query, e.g. [/dev/]/sda.
  This option is an alternative to the positional argument with a /dev/
  prefix. *udevadm info --name=sda* is equivalent to *udevadm info
  /dev/sda*.
#+end_quote

*-r*, *--root*

#+begin_quote
  Print absolute paths in *name* or *symlink* query.
#+end_quote

*-a*, *--attribute-walk*

#+begin_quote
  Print all sysfs properties of the specified device that can be used in
  udev rules to match the specified device. It prints all devices along
  the chain, up to the root of sysfs that can be used in udev rules.
#+end_quote

*-x*, *--export*

#+begin_quote
  Print output as key/value pairs. Values are enclosed in single quotes.
  This takes effects only when *--query=property* or
  *--device-id-of-file=*/FILE/ is specified.
#+end_quote

*-P*, *--export-prefix=*/NAME/

#+begin_quote
  Add a prefix to the key name of exported values. This implies
  *--export*.
#+end_quote

*-d*, *--device-id-of-file=*/FILE/

#+begin_quote
  Print major/minor numbers of the underlying device, where the file
  lives on. If this is specified, all positional arguments are ignored.
#+end_quote

*-e*, *--export-db*

#+begin_quote
  Export the content of the udev database.
#+end_quote

*-c*, *--cleanup-db*

#+begin_quote
  Cleanup the udev database.
#+end_quote

*-w[SECONDS]*, *--wait-for-initialization[=SECONDS]*

#+begin_quote
  Wait for device to be initialized. If argument /SECONDS/ is not
  specified, the default is to wait forever.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

The generated output shows the current device database entry in a terse
format. Each line shown is prefixed with one of the following
characters:

\\
*Table 1. udevadm info output prefixes*

| Prefix | Meaning                      |
| "P:"   | Device path in /sys/         |
| "N:"   | Kernel device node name      |
| "L:"   | Device node symlink priority |
| "S:"   | Device node symlink          |
| "E:"   | Device property              |

** udevadm trigger [/options/] [/devpath/|/file/|/unit/]
Request device events from the kernel. Primarily used to replay events
at system coldplug time.

Takes device specifications as positional arguments. See the description
of *info* above.

*-v*, *--verbose*

#+begin_quote
  Print the list of devices which will be triggered.
#+end_quote

*-n*, *--dry-run*

#+begin_quote
  Do not actually trigger the event.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  Suppress error logging in triggering events.
#+end_quote

*-t*, *--type=*/TYPE/

#+begin_quote
  Trigger a specific type of devices. Valid types are: *devices*,
  *subsystems*. The default value is *devices*.
#+end_quote

*-c*, *--action=*/ACTION/

#+begin_quote
  Type of event to be triggered. Possible actions are "add", "remove",
  "change", "move", "online", "offline", "bind", and "unbind". Also, the
  special value "help" can be used to list the possible actions. The
  default value is "change".
#+end_quote

*-s*, *--subsystem-match=*/SUBSYSTEM/

#+begin_quote
  Trigger events for devices which belong to a matching subsystem. This
  option supports shell style pattern matching. When this option is
  specified more than once, then each matching result is ORed, that is,
  all the devices in each subsystem are triggered.
#+end_quote

*-S*, *--subsystem-nomatch=*/SUBSYSTEM/

#+begin_quote
  Do not trigger events for devices which belong to a matching
  subsystem. This option supports shell style pattern matching. When
  this option is specified more than once, then each matching result is
  ANDed, that is, devices which do not match all specified subsystems
  are triggered.
#+end_quote

*-a*, *--attr-match=*/ATTRIBUTE/*=*/VALUE/

#+begin_quote
  Trigger events for devices with a matching sysfs attribute. If a value
  is specified along with the attribute name, the content of the
  attribute is matched against the given value using shell style pattern
  matching. If no value is specified, the existence of the sysfs
  attribute is checked. When this option is specified multiple times,
  then each matching result is ANDed, that is, only devices which have
  all specified attributes are triggered.
#+end_quote

*-A*, *--attr-nomatch=*/ATTRIBUTE/*=*/VALUE/

#+begin_quote
  Do not trigger events for devices with a matching sysfs attribute. If
  a value is specified along with the attribute name, the content of the
  attribute is matched against the given value using shell style pattern
  matching. If no value is specified, the existence of the sysfs
  attribute is checked. When this option is specified multiple times,
  then each matching result is ANDed, that is, only devices which have
  none of the specified attributes are triggered.
#+end_quote

*-p*, *--property-match=*/PROPERTY/*=*/VALUE/

#+begin_quote
  Trigger events for devices with a matching property value. This option
  supports shell style pattern matching. When this option is specified
  more than once, then each matching result is ORed, that is, devices
  which have one of the specified properties are triggered.
#+end_quote

*-g*, *--tag-match=*/TAG/

#+begin_quote
  Trigger events for devices with a matching tag. When this option is
  specified multiple times, then each matching result is ANDed, that is,
  devices which have all specified tags are triggered.
#+end_quote

*-y*, *--sysname-match=*/NAME/

#+begin_quote
  Trigger events for devices for which the last component (i.e. the
  filename) of the /sys/ path matches the specified /PATH/. This option
  supports shell style pattern matching. When this option is specified
  more than once, then each matching result is ORed, that is, all
  devices which have any of the specified /NAME/ are triggered.
#+end_quote

*--name-match=*/NAME/

#+begin_quote
  Trigger events for devices with a matching device path. When this
  option is specified more than once, then each matching result is ORed,
  that is, all specified devices are triggered.
#+end_quote

*-b*, *--parent-match=*/SYSPATH/

#+begin_quote
  Trigger events for all children of a given device. When this option is
  specified more than once, then each matching result is ORed, that is,
  all children of each specified device are triggered.
#+end_quote

*-w*, *--settle*

#+begin_quote
  Apart from triggering events, also waits for those events to finish.
  Note that this is different from calling *udevadm settle*. *udevadm
  settle* waits for all events to finish. This option only waits for
  events triggered by the same command to finish.
#+end_quote

*--uuid*

#+begin_quote
  Trigger the synthetic device events, and associate a randomized UUID
  with each. These UUIDs are printed to standard output, one line for
  each event. These UUIDs are included in the uevent environment block
  (in the "SYNTH_UUID=" property) and may be used to track delivery of
  the generated events.
#+end_quote

*--wait-daemon[=*/SECONDS/*]*

#+begin_quote
  Before triggering uevents, wait for systemd-udevd daemon to be
  initialized. Optionally takes timeout value. Default timeout is 5
  seconds. This is equivalent to invoke invoking *udevadm control
  --ping* before *udevadm trigger*.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

In addition, optional positional arguments can be used to specify device
names or sys paths. They must start with /dev/ or /sys/ respectively.

** udevadm settle [/options/]
Watches the udev event queue, and exits if all current events are
handled.

*-t*, *--timeout=*/SECONDS/

#+begin_quote
  Maximum number of seconds to wait for the event queue to become empty.
  The default value is 120 seconds. A value of 0 will check if the queue
  is empty and always return immediately. A non-zero value will return
  an exit code of 0 if queue became empty before timeout was reached,
  non-zero otherwise.
#+end_quote

*-E*, *--exit-if-exists=*/FILE/

#+begin_quote
  Stop waiting if file exists.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

See *systemd-udev-settle.service*(8) for more information.

** udevadm control /option/
Modify the internal state of the running udev daemon.

*-e*, *--exit*

#+begin_quote
  Signal and wait for systemd-udevd to exit. No option except for
  *--timeout* can be specified after this option. Note that
  systemd-udevd.service contains *Restart=always* and so as a result,
  this option restarts systemd-udevd. If you want to stop
  systemd-udevd.service, please use the following:

  #+begin_quote
    #+begin_example
      systemctl stop systemd-udevd-control.socket systemd-udevd-kernel.socket systemd-udevd.service
    #+end_example
  #+end_quote
#+end_quote

*-l*, *--log-level=*/value/

#+begin_quote
  Set the internal log level of systemd-udevd. Valid values are the
  numerical syslog priorities or their textual representations: *emerg*,
  *alert*, *crit*, *err*, *warning*, *notice*, *info*, and *debug*.
#+end_quote

*-s*, *--stop-exec-queue*

#+begin_quote
  Signal systemd-udevd to stop executing new events. Incoming events
  will be queued.
#+end_quote

*-S*, *--start-exec-queue*

#+begin_quote
  Signal systemd-udevd to enable the execution of events.
#+end_quote

*-R*, *--reload*

#+begin_quote
  Signal systemd-udevd to reload the rules files and other databases
  like the kernel module index. Reloading rules and databases does not
  apply any changes to already existing devices; the new configuration
  will only be applied to new events.
#+end_quote

*-p*, *--property=*/KEY/*=*/value/

#+begin_quote
  Set a global property for all events.
#+end_quote

*-m*, *--children-max=*/value/

#+begin_quote
  Set the maximum number of events, systemd-udevd will handle at the
  same time.
#+end_quote

*--ping*

#+begin_quote
  Send a ping message to systemd-udevd and wait for the reply. This may
  be useful to check that systemd-udevd daemon is running.
#+end_quote

*-t*, *--timeout=*/seconds/

#+begin_quote
  The maximum number of seconds to wait for a reply from systemd-udevd.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

** udevadm monitor [/options/]
Listens to the kernel uevents and events sent out by a udev rule and
prints the devpath of the event to the console. It can be used to
analyze the event timing, by comparing the timestamps of the kernel
uevent and the udev event.

*-k*, *--kernel*

#+begin_quote
  Print the kernel uevents.
#+end_quote

*-u*, *--udev*

#+begin_quote
  Print the udev event after the rule processing.
#+end_quote

*-p*, *--property*

#+begin_quote
  Also print the properties of the event.
#+end_quote

*-s*, *--subsystem-match=*/string[/string]/

#+begin_quote
  Filter kernel uevents and udev events by subsystem[/devtype]. Only
  events with a matching subsystem value will pass. When this option is
  specified more than once, then each matching result is ORed, that is,
  all devices in the specified subsystems are monitored.
#+end_quote

*-t*, *--tag-match=*/string/

#+begin_quote
  Filter udev events by tag. Only udev events with a given tag attached
  will pass. When this option is specified more than once, then each
  matching result is ORed, that is, devices which have one of the
  specified tags are monitored.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

** udevadm test [/options/] [/devpath/]
Simulate a udev event run for the given device, and print debug output.

*-a*, *--action=*/ACTION/

#+begin_quote
  Type of event to be simulated. Possible actions are "add", "remove",
  "change", "move", "online", "offline", "bind", and "unbind". Also, the
  special value "help" can be used to list the possible actions. The
  default value is "add".
#+end_quote

*-N*, *--resolve-names=early|late|never*

#+begin_quote
  Specify when udevadm should resolve names of users and groups. When
  set to *early* (the default), names will be resolved when the rules
  are parsed. When set to *late*, names will be resolved for every
  event. When set to *never*, names will never be resolved and all
  devices will be owned by root.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

** udevadm test-builtin [/options/] [/command/] [/devpath/]
Run a built-in command /COMMAND/ for device /DEVPATH/, and print debug
output.

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

* SEE ALSO
*udev*(7), *systemd-udevd.service*(8)
