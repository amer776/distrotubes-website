#+TITLE: Manpages - systemd-backlight@.service.8
#+DESCRIPTION: Linux manpage for systemd-backlight@.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-backlight@.service, systemd-backlight - Load and save the
display backlight brightness at boot and shutdown

* SYNOPSIS
systemd-backlight@.service

/usr/lib/systemd/systemd-backlight save [backlight|leds]:DEVICE

/usr/lib/systemd/systemd-backlight load [backlight|leds]:DEVICE

* DESCRIPTION
systemd-backlight@.service is a service that restores the display
backlight brightness at early boot and saves it at shutdown. On disk,
the backlight brightness is stored in /var/lib/systemd/backlight/.
During loading, if the udev property *ID_BACKLIGHT_CLAMP* is not set to
false, the brightness is clamped to a value of at least 1 or 5% of
maximum brightness, whichever is greater. This restriction will be
removed when the kernel allows user space to reliably set a brightness
value which does not turn off the display.

* KERNEL COMMAND LINE
systemd-backlight understands the following kernel command line
parameter:

/systemd.restore_state=/

#+begin_quote
  Takes a boolean argument. Defaults to "1". If "0", does not restore
  the backlight settings on boot. However, settings will still be stored
  on shutdown.
#+end_quote

* SEE ALSO
*systemd*(1)
