#+TITLE: Manpages - migspeed.8
#+DESCRIPTION: Linux manpage for migspeed.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
migspeed - Test the speed of page migration

* SYNOPSIS
*migspeed* -p pages from-nodes to-nodes

* DESCRIPTION
*migspeed* attempts to move a sample of pages from the indicated node to
the target node and measures the time it takes to perform the move.

*-p pages*

The default sample is 1000 pages. Override that with another number.

* NOTES
Requires an NUMA policy aware kernel with support for page migration
(Linux 2.6.16 and later).

* COPYRIGHT
Copyright 2007 Christoph Lameter, Silicon Graphics, Inc. migratepages is
under the GNU General Public License, v.2

* SEE ALSO
/numactl(8)/
