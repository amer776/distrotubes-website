#+TITLE: Manpages - pam_limits.8
#+DESCRIPTION: Linux manpage for pam_limits.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_limits - PAM module to limit resources

* SYNOPSIS
*pam_limits.so* [conf=//path/to/limits.conf/] [debug] [set_all]
[utmp_early] [noaudit]

* DESCRIPTION
The pam_limits PAM module sets limits on the system resources that can
be obtained in a user-session. Users of /uid=0/ are affected by this
limits, too.

By default limits are taken from the /etc/security/limits.conf config
file. Then individual *.conf files from the /etc/security/limits.d/
directory are read. The files are parsed one after another in the order
of "C" locale. The effect of the individual files is the same as if all
the files were concatenated together in the order of parsing. If a
config file is explicitly specified with a module option then the files
in the above directory are not parsed.

The module must not be called by a multithreaded application.

If Linux PAM is compiled with audit support the module will report when
it denies access based on limit of maximum number of concurrent login
sessions.

* OPTIONS
*conf=*//path/to/limits.conf/

#+begin_quote
  Indicate an alternative limits.conf style configuration file to
  override the default.
#+end_quote

*debug*

#+begin_quote
  Print debug information.
#+end_quote

*set_all*

#+begin_quote
  Set the limits for which no value is specified in the configuration
  file to the one from the process with the PID 1. Please note that if
  the init process is systemd these limits will not be the kernel
  default limits and this option should not be used.
#+end_quote

*utmp_early*

#+begin_quote
  Some broken applications actually allocate a utmp entry for the user
  before the user is admitted to the system. If some of the services you
  are configuring PAM for do this, you can selectively use this module
  argument to compensate for this behavior and at the same time maintain
  system-wide consistency with a single limits.conf file.
#+end_quote

*noaudit*

#+begin_quote
  Do not report exceeded maximum logins count to the audit subsystem.
#+end_quote

* MODULE TYPES PROVIDED
Only the *session* module type is provided.

* RETURN VALUES
PAM_ABORT

#+begin_quote
  Cannot get current limits.
#+end_quote

PAM_IGNORE

#+begin_quote
  No limits found for this user.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  New limits could not be set.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  Cannot read config file.
#+end_quote

PAM_SESSION_ERR

#+begin_quote
  Error recovering account name.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Limits were changed.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  The user is not known to the system.
#+end_quote

* FILES
/etc/security/limits.conf

#+begin_quote
  Default configuration file
#+end_quote

* EXAMPLES
For the services you need resources limits (login for example) put a the
following line in /etc/pam.d/login as the last line for that service
(usually after the pam_unix session line):

#+begin_quote
  #+begin_example
    #%PAM-1.0
    #
    # Resource limits imposed on login sessions via pam_limits
    #
    session  required  pam_limits.so
        
  #+end_example
#+end_quote

Replace "login" for each service you are using this module.

* SEE ALSO
*limits.conf*(5), *pam.d*(5), *pam*(8).

* AUTHORS
pam_limits was initially written by Cristian Gafton <gafton@redhat.com>
