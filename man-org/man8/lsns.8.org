#+TITLE: Manpages - lsns.8
#+DESCRIPTION: Linux manpage for lsns.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lsns - list namespaces

* SYNOPSIS
*lsns* [options] /namespace/

* DESCRIPTION
*lsns* lists information about all the currently accessible namespaces
or about the given /namespace/. The /namespace/ identifier is an inode
number.

The default output is subject to change. So whenever possible, you
should avoid using default outputs in your scripts. Always explicitly
define expected columns by using the *--output* option together with a
columns list in environments where a stable output is required.

The *NSFS* column, printed when *net* is specified for the *--type*
option, is special; it uses multi-line cells. Use the option *--nowrap*
to switch to ","-separated single-line representation.

Note that *lsns* reads information directly from the //proc/ filesystem
and for non-root users it may return incomplete information. The current
//proc/ filesystem may be unshared and affected by a PID namespace (see
*unshare --mount-proc* for more details). *lsns* is not able to see
persistent namespaces without processes where the namespace instance is
held by a bind mount to /proc//pid//ns//type/.

* OPTIONS
*-J*, *--json*

#+begin_quote
  Use JSON output format.
#+end_quote

*-l*, *--list*

#+begin_quote
  Use list output format.
#+end_quote

*-n*, *--noheadings*

#+begin_quote
  Do not print a header line.
#+end_quote

*-o*, *--output* /list/

#+begin_quote
  Specify which output columns to print. Use *--help* to get a list of
  all supported columns.

  The default list of columns may be extended if /list/ is specified in
  the format *+*/list/ (e.g., *lsns -o +PATH*).
#+end_quote

*--output-all*

#+begin_quote
  Output all available columns.
#+end_quote

*-p*, *--task* /PID/

#+begin_quote
  Display only the namespaces held by the process with this /PID/.
#+end_quote

*-r*, *--raw*

#+begin_quote
  Use the raw output format.
#+end_quote

*-t*, *--type* /type/

#+begin_quote
  Display the specified /type/ of namespaces only. The supported types
  are *mnt*, *net*, *ipc*, *user*, *pid*, *uts*, *cgroup* and *time*.
  This option may be given more than once.
#+end_quote

*-u*, *--notruncate*

#+begin_quote
  Do not truncate text in columns.
#+end_quote

*-W*, *--nowrap*

#+begin_quote
  Do not use multi-line text in columns.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* AUTHORS
* SEE ALSO
*nsenter*(1), *unshare*(1), *clone*(2), *namespaces*(7), *ioctl_ns(2)*

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *lsns* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
