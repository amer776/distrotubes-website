#+TITLE: Manpages - pppoe-sniff.8
#+DESCRIPTION: Linux manpage for pppoe-sniff.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pppoe-sniff - examine network for non-standard PPPoE frames

* SYNOPSIS
*pppoe-sniff [/options/]*

* DESCRIPTION
*pppoe-sniff* listens for likely-looking PPPoE PADR and session frames
and deduces extra options required for *pppoe(8)* to work.

Some DSL providers seem to use non-standard frame types for PPPoE
frames, and/or require a certain value in the Service-Name field. It is
often easier to sniff those values from a machine which can successfully
connect rather than try to pry them out of the DSL provider.

To use *pppoe-sniff*, you need two computers, a DSL modem and an
Ethernet hub (/not/ an Ethernet switch.)

If the DSL modem normally connects directly to your computer's Ethernet
card, connect it to the "uplink" port on the Ethernet hub. Plug two
computers into normal ports on the hub. On one computer, run whatever
software the DSL provider gave you on whatever operating system the DSL
provider supports. On the other computer, run Linux and log in as root.

On the Linux machine, put the Ethernet interface into promiscuous mode
and start *pppoe-sniff*. If the ethernet interface is /eth0/, for
example, type these commands:

#+begin_example
  	ip link set eth0 promisc on
  	pppoe-sniff -I eth0
#+end_example

On the other machine, start your DSL connection as usual. After a short
time, *pppoe-sniff* should print recommendations for the value of
*PPPOE_EXTRA*. Set this value in */etc/ppp/pppoe.conf*.

After *pppoe-sniff* finishes (or you stop it if it seems hung), remember
to turn off promiscuous mode:

#+begin_example
  	ip link set eth0 promisc on
#+end_example

* OPTIONS
- *-I /interface/* :: The *-I* option specifies the Ethernet interface
  to use. Under Linux, it is typically /eth0/ or /eth1/. The interface
  should be "up" and in promiscuous mode before you start *pppoe-sniff*.

- *-V* :: The *-V* option causes *pppoe-sniff* to print its version
  number and exit.

* BUGS
*pppoe-sniff* only works on Linux.

* AUTHORS
*pppoe-sniff* was written by Dianne Skoll <dianne@skoll.ca>.

The *pppoe* home page is /https://dianne.skoll.ca/projects/rp-pppoe//.

* SEE ALSO
pppoe-start(8), pppoe-stop(8), pppoe-connect(8), pppd(8), pppoe.conf(5),
pppoe(8), pppoe-setup(8), pppoe-status(8), pppoe-server(8),
pppoe-relay(8)
