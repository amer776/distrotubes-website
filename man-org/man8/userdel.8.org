#+TITLE: Manpages - userdel.8
#+DESCRIPTION: Linux manpage for userdel.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
userdel - delete a user account and related files

* SYNOPSIS
*userdel* [options] /LOGIN/

* DESCRIPTION
The *userdel* command modifies the system account files, deleting all
entries that refer to the user name /LOGIN/. The named user must exist.

* OPTIONS
The options which apply to the *userdel* command are:

*-f*, *--force*

#+begin_quote
  This option forces the removal of the user account, even if the user
  is still logged in. It also forces *userdel* to remove the users home
  directory and mail spool, even if another user uses the same home
  directory or if the mail spool is not owned by the specified user. If
  *USERGROUPS_ENAB* is defined to /yes/ in /etc/login.defs and if a
  group exists with the same name as the deleted user, then this group
  will be removed, even if it is still the primary group of another
  user.

  /Note:/ This option is dangerous and may leave your system in an
  inconsistent state.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help message and exit.
#+end_quote

*-r*, *--remove*

#+begin_quote
  Files in the users home directory will be removed along with the home
  directory itself and the users mail spool. Files located in other file
  systems will have to be searched for and deleted manually.

  The mail spool is defined by the *MAIL_DIR* variable in the login.defs
  file.
#+end_quote

*-R*, *--root* /CHROOT_DIR/

#+begin_quote
  Apply changes in the /CHROOT_DIR/ directory and use the configuration
  files from the /CHROOT_DIR/ directory.
#+end_quote

*-P*, *--prefix* /PREFIX_DIR/

#+begin_quote
  Apply changes in the /PREFIX_DIR/ directory and use the configuration
  files from the /PREFIX_DIR/ directory. This option does not chroot and
  is intended for preparing a cross-compilation target. Some
  limitations: NIS and LDAP users/groups are not verified. PAM
  authentication is using the host files. No SELINUX support.
#+end_quote

*-Z*, *--selinux-user*

#+begin_quote
  Remove any SELinux user mapping for the users login.
#+end_quote

* CONFIGURATION
The following configuration variables in /etc/login.defs change the
behavior of this tool:

*MAIL_DIR* (string)

#+begin_quote
  The mail spool directory. This is needed to manipulate the mailbox
  when its corresponding user account is modified or deleted. If not
  specified, a compile-time default is used.
#+end_quote

*MAIL_FILE* (string)

#+begin_quote
  Defines the location of the users mail spool files relatively to their
  home directory.
#+end_quote

The *MAIL_DIR* and *MAIL_FILE* variables are used by *useradd*,
*usermod*, and *userdel* to create, move, or delete the users mail
spool.

If *MAIL_CHECK_ENAB* is set to /yes/, they are also used to define the
*MAIL* environment variable.

*MAX_MEMBERS_PER_GROUP* (number)

#+begin_quote
  Maximum members per group entry. When the maximum is reached, a new
  group entry (line) is started in /etc/group (with the same name, same
  password, and same GID).

  The default value is 0, meaning that there are no limits in the number
  of members in a group.

  This feature (split group) permits to limit the length of lines in the
  group file. This is useful to make sure that lines for NIS groups are
  not larger than 1024 characters.

  If you need to enforce such limit, you can use 25.

  Note: split groups may not be supported by all tools (even in the
  Shadow toolsuite). You should not use this variable unless you really
  need it.
#+end_quote

*USERDEL_CMD* (string)

#+begin_quote
  If defined, this command is run when removing a user. It should remove
  any at/cron/print jobs etc. owned by the user to be removed (passed as
  the first argument).

  The return code of the script is not taken into account.

  Here is an example script, which removes the users cron, at and print
  jobs:

  #+begin_quote
    #+begin_example
      #! /bin/sh

      # Check for the required argument.
      if [ $# != 1 ]; then
      	echo "Usage: $0 username"
      	exit 1
      fi

      # Remove cron jobs.
      crontab -r -u $1

      # Remove at jobs.
      # Note that it will remove any jobs owned by the same UID,
      # even if it was shared by a different username.
      AT_SPOOL_DIR=/var/spool/cron/atjobs
      find $AT_SPOOL_DIR -name "[^.]*" -type f -user $1 -delete \;

      # Remove print jobs.
      lprm $1

      # All done.
      exit 0
            
    #+end_example
  #+end_quote
#+end_quote

*USERGROUPS_ENAB* (boolean)

#+begin_quote
  Enable setting of the umask group bits to be the same as owner bits
  (examples: 022 -> 002, 077 -> 007) for non-root users, if the uid is
  the same as gid, and username is the same as the primary group name.

  If set to /yes/, *userdel* will remove the users group if it contains
  no more members, and *useradd* will create by default a group with the
  name of the user.
#+end_quote

* FILES
/etc/group

#+begin_quote
  Group account information.
#+end_quote

/etc/login.defs

#+begin_quote
  Shadow password suite configuration.
#+end_quote

/etc/passwd

#+begin_quote
  User account information.
#+end_quote

/etc/shadow

#+begin_quote
  Secure user account information.
#+end_quote

/etc/subgid

#+begin_quote
  Per user subordinate group IDs.
#+end_quote

/etc/subuid

#+begin_quote
  Per user subordinate user IDs.
#+end_quote

* EXIT VALUES
The *userdel* command exits with the following values:

/0/

#+begin_quote
  success
#+end_quote

/1/

#+begin_quote
  cant update password file
#+end_quote

/2/

#+begin_quote
  invalid command syntax
#+end_quote

/6/

#+begin_quote
  specified user doesnt exist
#+end_quote

/8/

#+begin_quote
  user currently logged in
#+end_quote

/10/

#+begin_quote
  cant update group file
#+end_quote

/12/

#+begin_quote
  cant remove home directory
#+end_quote

* CAVEATS
*userdel* will not allow you to remove an account if there are running
processes which belong to this account. In that case, you may have to
kill those processes or lock the users password or account and remove
the account later. The *-f* option can force the deletion of this
account.

You should manually check all file systems to ensure that no files
remain owned by this user.

You may not remove any NIS attributes on a NIS client. This must be
performed on the NIS server.

If *USERGROUPS_ENAB* is defined to /yes/ in /etc/login.defs, *userdel*
will delete the group with the same name as the user. To avoid
inconsistencies in the passwd and group databases, *userdel* will check
that this group is not used as a primary group for another user, and
will just warn without deleting the group otherwise. The *-f* option can
force the deletion of this group.

* SEE ALSO
*chfn*(1), *chsh*(1), *passwd*(1), *login.defs*(5), *gpasswd*(8),
*groupadd*(8), *groupdel*(8), *groupmod*(8), *subgid*(5), *subuid*(5),
*useradd*(8), *usermod*(8).
