#+TITLE: Manpages - nologin.8
#+DESCRIPTION: Linux manpage for nologin.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
nologin - politely refuse a login

* SYNOPSIS
*nologin* [*-V*] [*-h*]

* DESCRIPTION
*nologin* displays a message that an account is not available and exits
non-zero. It is intended as a replacement shell field to deny login
access to an account.

If the file //etc/nologin.txt/ exists, *nologin* displays its contents
to the user instead of the default message.

The exit status returned by *nologin* is always 1.

* OPTIONS
*-c*, *--command* /command/

*--init-file*

*-i* *--interactive*

*--init-file* /file/

*-i*, *--interactive*

*-l*, *--login*

*--noprofile*

*--norc*

*--posix*

*--rcfile* /file/

*-r*, *--restricted*

These shell command-line options are ignored to avoid *nologin* error.

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

* NOTES
*nologin* is a per-account way to disable login (usually used for system
accounts like http or ftp). *nologin* uses //etc/nologin.txt/ as an
optional source for a non-default message, the login access is always
refused independently of the file.

*pam_nologin*(8) PAM module usually prevents all non-root users from
logging into the system. *pam_nologin*(8) functionality is controlled by
//var/run/nologin/ or the //etc/nologin/ file.

* HISTORY
The *nologin* command appeared in 4.4BSD.

* AUTHORS
* SEE ALSO
*login*(1), *passwd*(5), *pam_nologin*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *nologin* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
