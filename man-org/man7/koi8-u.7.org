#+TITLE: Manpages - koi8-u.7
#+DESCRIPTION: Linux manpage for koi8-u.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
koi8-u - Ukrainian character set encoded in octal, decimal, and
hexadecimal

* DESCRIPTION
RFC 2310 defines an 8-bit character set, KOI8-U. KOI8-U encodes the
characters used in Ukrainian and Byelorussian.

** KOI8-U characters
The following table displays the characters in KOI8-U that are printable
and unlisted in the *ascii*(7) manual page.

TABLE

* NOTES
The differences from KOI8-R are in the hex positions A4, A6, A7, AD, B4,
B6, B7, and BD.

* SEE ALSO
*ascii*(7), *charsets*(7), *cp1251*(7), *iso_8859-5*(7), *koi8-r*(7),
*utf-8*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
