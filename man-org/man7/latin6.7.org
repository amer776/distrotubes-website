#+TITLE: Manpages - latin6.7
#+DESCRIPTION: Linux manpage for latin6.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about latin6.7 is found in manpage for: [[../man7/iso_8859-10.7][man7/iso_8859-10.7]]