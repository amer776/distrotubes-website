#+TITLE: Manpages - sndio.7
#+DESCRIPTION: Linux manpage for sndio.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Programs access audio and MIDI hardware using the

library. It allows both access through the

server and raw access to the hardware. The audio device or MIDI port, as
well as the access method, are designated by the sndio descriptor. It is
provided by the user with the program device selection method, or with
the

and

environment variables if there's no device selection method.

Usually, programs access the hardware via the

server, because raw access to the hardware is exclusive and requires
additional privileges. The

server supports multiple connections at a time, allowing multiple
programs to use the hardware concurrently. It performs the necessary
audio processing on the fly to overcome any incompatibility between
software and hardware. Connections to

may also be established through the network, including from virtual
machines.

The

server exposes

ports, allowing one program to send MIDI data to other programs, for
instance to allow a sequencer to send events to a synthesizer.

Additionally,

exposes a MIDI port used to control audio programs using standard MIDI
Machine Control (MMC), MIDI Time Code (MTC), and master volume messages.

From the user's perspective, every audio device or MIDI port exposed by

has a descriptor of the form:

type[@hostname][,servnum]/devnum[.option]

This information is used by programs to determine how to access the
audio device or MIDI port.

The type of the audio device or MIDI port. Possible values are:

Audio device exposed by

MIDI thru port created with

MIDI port exposed by

The hostname or address where the remote

server to connect to is running.

The number of the

server to connect to, corresponding to the integer specified using the

option of

Useful only if multiple

servers are running on the same system.

Device number. It corresponds to the number of the corresponding

or

option on the

command line.

Corresponds to the sub-device string registered using the

option of

Every raw audio device or MIDI port has a descriptor of the form:

The

can be either

or

The rsnd/0 device descriptor accesses the

device, rsnd/1 accesses

and so on. Similarly, rmidi/0 accesses

and so on.

When no audio device descriptor is provided to a program or when the
reserved word

is used as the audio device, the program will use the one specified in
the

and/or

environment variables. If they are not set, the program first tries to
connect to

If that fails, it then tries to use

Similarly, if no MIDI descriptor is provided to a program or when the
reserved word

is passed as the device descriptor, the program uses the one specified
in the

environment variable. If it is not set, the program first tries to
connect to

If that fails, it then tries to use

As long as

is running, this allows programs to exchange MIDI data on machines with
no MIDI hardware by default, e.g. a MIDI player could use a software
synthesizer with no manual configuration required.

For privacy reasons only one user may have connections to

at a given time. Users are identified by their

which is automatically generated by audio or MIDI programs upon the
first connection to the server. The cookie is stored in

and contains 128 bits of raw random data.

If a session needs to be shared between multiple users, they can connect
to the server using the same cookie.

Audio device descriptor to use when no descriptor is explicitly
specified to a program.

Audio device descriptor to use for play-only mode when no descriptor is
explicitly specified to a program. Overrides

Audio device descriptor to use for record-only mode when no descriptor
is explicitly specified to a program. Overrides

MIDI port descriptor to use when no descriptor is explicitly specified
to a program.

These environment variables are ignored by

if the program has the set-user-ID or set-group-ID bits set.

User's session authentication cookie.

Raw audio devices.

Raw MIDI ports.

Audio device referred to by the first

option of

Sub-device registered with

First MIDI thru port created with

Default audio or MIDI device.

Direct hardware access to

Direct hardware access to
