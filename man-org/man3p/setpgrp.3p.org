#+TITLE: Manpages - setpgrp.3p
#+DESCRIPTION: Linux manpage for setpgrp.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
setpgrp --- set the process group ID

* SYNOPSIS
#+begin_example
  #include <unistd.h>
  pid_t setpgrp(void);
#+end_example

* DESCRIPTION
If the calling process is not already a session leader, /setpgrp/() sets
the process group ID of the calling process to the process ID of the
calling process. If /setpgrp/() creates a new session, then the new
session has no controlling terminal.

The /setpgrp/() function has no effect when the calling process is a
session leader.

* RETURN VALUE
Upon completion, /setpgrp/() shall return the process group ID.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
It is unspecified whether this function behaves as /setpgid/(0,0) or
/setsid/() unless the process is already a session leader. Therefore,
applications are encouraged to use /setpgid/() or /setsid/() as
appropriate.

* RATIONALE
None.

* FUTURE DIRECTIONS
The /setpgrp/() function may be removed in a future version.

* SEE ALSO
//exec/ /, //fork/ ( )/, //getpid/ ( )/, //getsid/ ( )/, //kill/ ( )/,
//setpgid/ ( )/, //setsid/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<unistd.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
