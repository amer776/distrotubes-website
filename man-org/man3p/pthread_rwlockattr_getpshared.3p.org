#+TITLE: Manpages - pthread_rwlockattr_getpshared.3p
#+DESCRIPTION: Linux manpage for pthread_rwlockattr_getpshared.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
pthread_rwlockattr_getpshared, pthread_rwlockattr_setpshared --- get and
set the process-shared attribute of the read-write lock attributes
object

* SYNOPSIS
#+begin_example
  #include <pthread.h>
  int pthread_rwlockattr_getpshared(const pthread_rwlockattr_t
      *restrict attr, int *restrict pshared);
  int pthread_rwlockattr_setpshared(pthread_rwlockattr_t *attr,
      int pshared);
#+end_example

* DESCRIPTION
The /pthread_rwlockattr_getpshared/() function shall obtain the value of
the /process-shared/ attribute from the initialized attributes object
referenced by /attr/. The /pthread_rwlockattr_setpshared/() function
shall set the /process-shared/ attribute in an initialized attributes
object referenced by /attr/.

The /process-shared/ attribute shall be set to PTHREAD_PROCESS_SHARED to
permit a read-write lock to be operated upon by any thread that has
access to the memory where the read-write lock is allocated, even if the
read-write lock is allocated in memory that is shared by multiple
processes. See /Section 2.9.9/, /Synchronization Object Copies and
Alternative Mappings/ for further requirements. The default value of the
/process-shared/ attribute shall be PTHREAD_PROCESS_PRIVATE.

Additional attributes, their default values, and the names of the
associated functions to get and set those attribute values are
implementation-defined.

The behavior is undefined if the value specified by the /attr/ argument
to /pthread_rwlockattr_getpshared/() or
/pthread_rwlockattr_setpshared/() does not refer to an initialized
read-write lock attributes object.

* RETURN VALUE
Upon successful completion, the /pthread_rwlockattr_getpshared/()
function shall return zero and store the value of the /process-shared/
attribute of /attr/ into the object referenced by the /pshared/
parameter. Otherwise, an error number shall be returned to indicate the
error.

If successful, the /pthread_rwlockattr_setpshared/() function shall
return zero; otherwise, an error number shall be returned to indicate
the error.

* ERRORS
The /pthread_rwlockattr_setpshared/() function may fail if:

- *EINVAL* :: The new value specified for the attribute is outside the
  range of legal values for that attribute.

These functions shall not return an error code of *[EINTR]*.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//pthread_rwlock_destroy/ ( )/, //pthread_rwlockattr_destroy/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<pthread.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
