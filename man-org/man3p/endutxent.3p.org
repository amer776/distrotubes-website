#+TITLE: Manpages - endutxent.3p
#+DESCRIPTION: Linux manpage for endutxent.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
endutxent, getutxent, getutxid, getutxline, pututxline, setutxent ---
user accounting database functions

* SYNOPSIS
#+begin_example
  #include <utmpx.h>
  void endutxent(void);
  struct utmpx *getutxent(void);
  struct utmpx *getutxid(const struct utmpx *id);
  struct utmpx *getutxline(const struct utmpx *line);
  struct utmpx *pututxline(const struct utmpx *utmpx);
  void setutxent(void);
#+end_example

* DESCRIPTION
These functions shall provide access to the user accounting database.

The /getutxent/() function shall read the next entry from the user
accounting database. If the database is not already open, it shall open
it. If it reaches the end of the database, it shall fail.

The /getutxid/() function shall search forward from the current point in
the database. If the /ut_type/ value of the *utmpx* structure pointed to
by /id/ is BOOT_TIME, OLD_TIME, or NEW_TIME, then it shall stop when it
finds an entry with a matching /ut_type/ value. If the /ut_type/ value
is INIT_PROCESS, LOGIN_PROCESS, USER_PROCESS, or DEAD_PROCESS, then it
shall stop when it finds an entry whose type is one of these four and
whose /ut_id/ member matches the /ut_id/ member of the *utmpx* structure
pointed to by /id/. If the end of the database is reached without a
match, /getutxid/() shall fail.

The /getutxline/() function shall search forward from the current point
in the database until it finds an entry of the type LOGIN_PROCESS or
USER_PROCESS which also has a /ut_line/ value matching that in the
*utmpx* structure pointed to by /line/. If the end of the database is
reached without a match, /getutxline/() shall fail.

The /getutxid/() or /getutxline/() function may cache data. For this
reason, to use /getutxline/() to search for multiple occurrences, the
application shall zero out the static data after each success, or
/getutxline/() may return a pointer to the same *utmpx* structure.

There is one exception to the rule about clearing the structure before
further reads are done. The implicit read done by /pututxline/() (if it
finds that it is not already at the correct place in the user accounting
database) shall not modify the static structure returned by
/getutxent/(), /getutxid/(), or /getutxline/(), if the application has
modified this structure and passed the pointer back to /pututxline/().

For all entries that match a request, the /ut_type/ member indicates the
type of the entry. Other members of the entry shall contain meaningful
data based on the value of the /ut_type/ member as follows:

TABLE

An implementation that provides extended security controls may impose
implementation-defined restrictions on accessing the user accounting
database. In particular, the system may deny the existence of some or
all of the user accounting database entries associated with users other
than the caller.

If the process has appropriate privileges, the /pututxline/() function
shall write out the structure into the user accounting database. It
shall search for a record as if by /getutxid/() that satisfies the
request. If this search succeeds, then the entry shall be replaced.
Otherwise, a new entry shall be made at the end of the user accounting
database.

The /endutxent/() function shall close the user accounting database.

The /setutxent/() function shall reset the input to the beginning of the
database. This should be done before each search for a new entry if it
is desired that the entire database be examined.

These functions need not be thread-safe.

* RETURN VALUE
Upon successful completion, /getutxent/(), /getutxid/(), and
/getutxline/() shall return a pointer to a *utmpx* structure containing
a copy of the requested entry in the user accounting database.
Otherwise, a null pointer shall be returned.

The return value may point to a static area which is overwritten by a
subsequent call to /getutxid/() or /getutxline/().

Upon successful completion, /pututxline/() shall return a pointer to a
*utmpx* structure containing a copy of the entry added to the user
accounting database. Otherwise, a null pointer shall be returned.

The /endutxent/() and /setutxent/() functions shall not return a value.

* ERRORS
No errors are defined for the /endutxent/(), /getutxent/(),
/getutxid/(), /getutxline/(), and /setutxent/() functions.

The /pututxline/() function may fail if:

- *EPERM* :: The process does not have appropriate privileges.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
The sizes of the arrays in the structure can be found using the /sizeof/
operator.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /*<utmpx.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
