#+TITLE: Manpages - posix_spawnattr_getflags.3p
#+DESCRIPTION: Linux manpage for posix_spawnattr_getflags.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
posix_spawnattr_getflags, posix_spawnattr_setflags --- get and set the
spawn-flags attribute of a spawn attributes object (*ADVANCED REALTIME*)

* SYNOPSIS
#+begin_example
  #include <spawn.h>
  int posix_spawnattr_getflags(const posix_spawnattr_t *restrict attr,
      short *restrict flags);
  int posix_spawnattr_setflags(posix_spawnattr_t *attr, short flags);
#+end_example

* DESCRIPTION
The /posix_spawnattr_getflags/() function shall obtain the value of the
/spawn-flags/ attribute from the attributes object referenced by /attr/.

The /posix_spawnattr_setflags/() function shall set the /spawn-flags/
attribute in an initialized attributes object referenced by /attr/.

The /spawn-flags/ attribute is used to indicate which process attributes
are to be changed in the new process image when invoking /posix_spawn/()
or /posix_spawnp/(). It is the bitwise-inclusive OR of zero or more of
the following flags:

#+begin_example
  POSIX_SPAWN_RESETIDS
  POSIX_SPAWN_SETPGROUP
  POSIX_SPAWN_SETSIGDEF
  POSIX_SPAWN_SETSIGMASK
  POSIX_SPAWN_SETSCHEDPARAM
  POSIX_SPAWN_SETSCHEDULER
#+end_example

These flags are defined in /<spawn.h>/. The default value of this
attribute shall be as if no flags were set.

* RETURN VALUE
Upon successful completion, /posix_spawnattr_getflags/() shall return
zero and store the value of the /spawn-flags/ attribute of /attr/ into
the object referenced by the /flags/ parameter; otherwise, an error
number shall be returned to indicate the error.

Upon successful completion, /posix_spawnattr_setflags/() shall return
zero; otherwise, an error number shall be returned to indicate the
error.

* ERRORS
These functions may fail if:

- *EINVAL* :: The value specified by /attr/ is invalid.

The /posix_spawnattr_setflags/() function may fail if:

- *EINVAL* :: The value of the attribute being set is not valid.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
These functions are part of the Spawn option and need not be provided on
all implementations.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//posix_spawn/ ( )/, //posix_spawnattr_destroy/ ( )/,
//posix_spawnattr_getsigdefault/ ( )/,
//posix_spawnattr_getpgroup/ ( )/,
//posix_spawnattr_getschedparam/ ( )/,
//posix_spawnattr_getschedpolicy/ ( )/,
//posix_spawnattr_getsigmask/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<spawn.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
