#+TITLE: Manpages - tcdrain.3p
#+DESCRIPTION: Linux manpage for tcdrain.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
tcdrain --- wait for transmission of output

* SYNOPSIS
#+begin_example
  #include <termios.h>
  int tcdrain(int fildes);
#+end_example

* DESCRIPTION
The /tcdrain/() function shall block until all output written to the
object referred to by /fildes/ is transmitted. The /fildes/ argument is
an open file descriptor associated with a terminal.

Any attempts to use /tcdrain/() from a process which is a member of a
background process group on a /fildes/ associated with its controlling
terminal, shall cause the process group to be sent a SIGTTOU signal. If
the calling thread is blocking SIGTTOU signals or the process is
ignoring SIGTTOU signals, the process shall be allowed to perform the
operation, and no signal is sent.

* RETURN VALUE
Upon successful completion, 0 shall be returned. Otherwise, -1 shall be
returned and /errno/ set to indicate the error.

* ERRORS
The /tcdrain/() function shall fail if:

- *EBADF* :: The /fildes/ argument is not a valid file descriptor.

- *EINTR* :: A signal interrupted /tcdrain/().

- *EIO* :: The process group of the writing process is orphaned, the
  calling thread is not blocking SIGTTOU, and the process is not
  ignoring SIGTTOU.

- *ENOTTY* :: The file associated with /fildes/ is not a terminal.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//tcflush/ ( )/

The Base Definitions volume of POSIX.1‐2017, /Chapter 11/, /General
Terminal Interface/, /*<termios.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
