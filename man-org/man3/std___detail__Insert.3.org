#+TITLE: Manpages - std___detail__Insert.3
#+DESCRIPTION: Linux manpage for std___detail__Insert.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Insert< _Key, _Value, _Alloc, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, _Constant_iterators
>

* SYNOPSIS
\\

Inherited by *std::_Hashtable< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >*.

* Detailed Description
** "template<typename _Key, typename _Value, typename _Alloc, typename
_ExtractKey, typename _Equal, typename _Hash, typename _RangeHash,
typename _Unused, typename _RehashPolicy, typename _Traits, bool
_Constant_iterators = _Traits::__constant_iterators::value>
\\
struct std::__detail::_Insert< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits,
_Constant_iterators >"Primary class template _Insert.

Defines =insert= member functions that depend on _Hashtable policies,
via partial specializations.

Definition at line *952* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
