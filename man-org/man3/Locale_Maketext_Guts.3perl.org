#+TITLE: Manpages - Locale_Maketext_Guts.3perl
#+DESCRIPTION: Linux manpage for Locale_Maketext_Guts.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Locale::Maketext::Guts - Deprecated module to load Locale::Maketext utf8
code

* SYNOPSIS
# Do this instead please use Locale::Maketext

* DESCRIPTION
Previously Local::Maketext::GutsLoader performed some magic to load
Locale::Maketext when utf8 was unavailable. The subs this module
provided were merged back into Locale::Maketext
