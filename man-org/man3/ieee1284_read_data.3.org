#+TITLE: Manpages - ieee1284_read_data.3
#+DESCRIPTION: Linux manpage for ieee1284_read_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_read_data, ieee1284_write_data, ieee1284_data_dir,
ieee1284_wait_data - control the data lines

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*int ieee1284_read_data(struct parport **/port/*);*

*void ieee1284_write_data(struct parport **/port/*, unsigned char
*/dt/*);*

*int ieee1284_data_dir(struct parport **/port/*, int */reverse/*);*

*int ieee1284_wait_data(struct parport **/port/*, unsigned char
*/mask/*, unsigned char */val/*, struct timeval **/timeout/*);*

* DESCRIPTION
These functions manipulate the data lines of the parallel port
associated with /port/ (which must have been claimed using
*ieee1284_claim*(3)). The lines are represented by an 8-bit number (one
line per bit) and a direction. The data lines are driven as a group;
they may be all host-driven (forward direction) or not (reverse
direction). When the peripheral is driving them the host must not.

For *ieee1284_data_dir* the /reverse/ parameter should be zero to turn
the data line drivers on and non-zero to turn them off. Some port types
may be unable to switch off the data line drivers.

Setting the data lines may have side effects on some port types (for
example, some Amiga ports pulse nStrobe).

*ieee1284_wait_data* waits, up until the /timeout/, for the data bits
specified in /mask/ to have the corresponding values in /val/.

* RETURN VALUE
*ieee1284_read_data* returns the 8-bit number representing the data
lines unless it is not possible to return such a value with this port
type, in which case it returns an error code. Possible error codes:

*E1284_NOTAVAIL*

#+begin_quote
  Bi-directional data lines are not available on this system.
#+end_quote

*E1284_INVALIDPORT*

#+begin_quote
  The /port/ parameter is invalid (perhaps it has not been claimed, for
  instance).
#+end_quote

*E1284_SYS*

#+begin_quote
  There was an error at the operating system level, and /errno/ has been
  set accordingly.
#+end_quote

*E1284_TIMEDOUT*

#+begin_quote
  The /timeout/ has elapsed.
#+end_quote

Whereas *ieee1284_read_data* may return *E1284_NOTAVAIL* on its first
invocation on the port, if it does not do so then it cannot until
*ieee1284_close* is called for that port.

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
