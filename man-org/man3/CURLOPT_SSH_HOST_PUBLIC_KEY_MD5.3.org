#+TITLE: Manpages - CURLOPT_SSH_HOST_PUBLIC_KEY_MD5.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSH_HOST_PUBLIC_KEY_MD5.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSH_HOST_PUBLIC_KEY_MD5 - MD5 checksum of SSH server public key

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_HOST_PUBLIC_KEY_MD5,
                            char *md5);
#+end_example

* DESCRIPTION
Pass a char * pointing to a string containing 32 hexadecimal digits. The
string should be the 128 bit MD5 checksum of the remote host's public
key, and libcurl will reject the connection to the host unless the
md5sums match.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
SCP and SFTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
    curl_easy_setopt(curl, CURLOPT_SSH_HOST_PUBLIC_KEY_MD5,
                     "afe17cd62a0f3b61f1ab9cb22ba269a7");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.17.1

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_SSH_PUBLIC_KEYFILE*(3), *CURLOPT_SSH_AUTH_TYPES*(3),
