#+TITLE: Manpages - ZIP_SOURCE_GET_ARGS.3
#+DESCRIPTION: Linux manpage for ZIP_SOURCE_GET_ARGS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

macro casts

to a pointer to

On success,

returns

In case of error, it returns

and sets

fails if:

is less than the size of

was added in libzip 1.0.

and
