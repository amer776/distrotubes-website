#+TITLE: Manpages - frexpl.3
#+DESCRIPTION: Linux manpage for frexpl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about frexpl.3 is found in manpage for: [[../man3/frexp.3][man3/frexp.3]]