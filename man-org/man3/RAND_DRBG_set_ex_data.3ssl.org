#+TITLE: Manpages - RAND_DRBG_set_ex_data.3ssl
#+DESCRIPTION: Linux manpage for RAND_DRBG_set_ex_data.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
RAND_DRBG_set_ex_data, RAND_DRBG_get_ex_data,
RAND_DRBG_get_ex_new_index - store and retrieve extra data from the DRBG
instance

* SYNOPSIS
#include <openssl/rand_drbg.h> int RAND_DRBG_set_ex_data(RAND_DRBG
*drbg, int idx, void *data); void *RAND_DRBG_get_ex_data(const RAND_DRBG
*drbg, int idx); int RAND_DRBG_get_ex_new_index(long argl, void *argp,
CRYPTO_EX_new *new_func, CRYPTO_EX_dup *dup_func, CRYPTO_EX_free
*free_func);

* DESCRIPTION
*RAND_DRBG_set_ex_data()* enables an application to store arbitrary
application specific data *data* in a RAND_DRBG instance *drbg*. The
index *idx* should be a value previously returned from a call to
*RAND_DRBG_get_ex_new_index()*.

*RAND_DRBG_get_ex_data()* retrieves application specific data previously
stored in an RAND_DRBG instance *drbg*. The *idx* value should be the
same as that used when originally storing the data.

For more detailed information see *CRYPTO_get_ex_data* (3) and
*CRYPTO_set_ex_data* (3) which implement these functions and
*CRYPTO_get_ex_new_index* (3) for generating a unique index.

* RETURN VALUES
*RAND_DRBG_set_ex_data()* returns 1 for success or 0 for failure.

*RAND_DRBG_get_ex_data()* returns the previously stored value or NULL on
failure. NULL may also be a valid value.

* NOTES
RAND_DRBG_get_ex_new_index(...) is implemented as a macro and equivalent
to CRYPTO_get_ex_new_index(CRYPTO_EX_INDEX_DRBG,...).

* SEE ALSO
*CRYPTO_get_ex_data* (3), *CRYPTO_set_ex_data* (3),
*CRYPTO_get_ex_new_index* (3), *RAND_DRBG* (7)

* COPYRIGHT
Copyright 2017-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
