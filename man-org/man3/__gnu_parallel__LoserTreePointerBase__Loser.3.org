#+TITLE: Manpages - __gnu_parallel__LoserTreePointerBase__Loser.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__LoserTreePointerBase__Loser.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare >::_Loser -
Internal representation of _LoserTree __elements.

* SYNOPSIS
\\

=#include <losertree.h>=

** Public Attributes
const _Tp * *_M_keyp*\\

int *_M_source*\\

bool *_M_sup*\\

* Detailed Description
** "template<typename _Tp, typename _Compare>
\\
struct __gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::_Loser"Internal representation of _LoserTree __elements.

Definition at line *361* of file *losertree.h*.

* Member Data Documentation
** template<typename _Tp , typename _Compare > const _Tp*
*__gnu_parallel::_LoserTreePointerBase*< _Tp, _Compare
>::_Loser::_M_keyp
Definition at line *365* of file *losertree.h*.

** template<typename _Tp , typename _Compare > int
*__gnu_parallel::_LoserTreePointerBase*< _Tp, _Compare
>::_Loser::_M_source
Definition at line *364* of file *losertree.h*.

** template<typename _Tp , typename _Compare > bool
*__gnu_parallel::_LoserTreePointerBase*< _Tp, _Compare >::_Loser::_M_sup
Definition at line *363* of file *losertree.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
