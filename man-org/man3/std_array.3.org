#+TITLE: Manpages - std_array.3
#+DESCRIPTION: Linux manpage for std_array.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::array< _Tp, _Nm > - A standard container for storing a fixed size
sequence of elements.

* SYNOPSIS
\\

** Public Types
typedef __array_traits< _Tp, _Nm > *_AT_Type*\\

typedef const value_type * *const_iterator*\\

typedef const value_type * *const_pointer*\\

typedef const value_type & *const_reference*\\

typedef *std::reverse_iterator*< const_iterator >
*const_reverse_iterator*\\

typedef std::ptrdiff_t *difference_type*\\

typedef value_type * *iterator*\\

typedef value_type * *pointer*\\

typedef value_type & *reference*\\

typedef *std::reverse_iterator*< iterator > *reverse_iterator*\\

typedef std::size_t *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
constexpr reference *at* (size_type __n)\\

constexpr const_reference *at* (size_type __n) const\\

constexpr const_reference *back* () const noexcept\\

constexpr reference *back* () noexcept\\

constexpr const_iterator *begin* () const noexcept\\

constexpr iterator *begin* () noexcept\\

constexpr const_iterator *cbegin* () const noexcept\\

constexpr const_iterator *cend* () const noexcept\\

constexpr *const_reverse_iterator* *crbegin* () const noexcept\\

constexpr *const_reverse_iterator* *crend* () const noexcept\\

constexpr const_pointer *data* () const noexcept\\

constexpr pointer *data* () noexcept\\

constexpr bool *empty* () const noexcept\\

constexpr const_iterator *end* () const noexcept\\

constexpr iterator *end* () noexcept\\

constexpr void *fill* (const value_type &__u)\\

constexpr const_reference *front* () const noexcept\\

constexpr reference *front* () noexcept\\

constexpr size_type *max_size* () const noexcept\\

constexpr const_reference *operator[]* (size_type __n) const noexcept\\

constexpr reference *operator[]* (size_type __n) noexcept\\

constexpr *const_reverse_iterator* *rbegin* () const noexcept\\

constexpr *reverse_iterator* *rbegin* () noexcept\\

constexpr *const_reverse_iterator* *rend* () const noexcept\\

constexpr *reverse_iterator* *rend* () noexcept\\

constexpr size_type *size* () const noexcept\\

constexpr void *swap* (*array* &__other)
noexcept(_AT_Type::_Is_nothrow_swappable::value)\\

** Public Attributes
_AT_Type::_Type *_M_elems*\\

* Detailed Description
** "template<typename _Tp, std::size_t _Nm>
\\
struct std::array< _Tp, _Nm >"A standard container for storing a fixed
size sequence of elements.

Meets the requirements of a =container=, a =reversible container=, and a
=sequence=.

Sets support random access iterators.

*Template Parameters*

#+begin_quote
  /Tp/ Type of element. Required to be a complete type.\\
  /Nm/ Number of elements.
#+end_quote

Definition at line *95* of file *std/array*.

* Member Typedef Documentation
** template<typename _Tp , std::size_t _Nm> typedef __array_traits<_Tp,
_Nm> *std::array*< _Tp, _Nm >::_AT_Type
Definition at line *110* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef const value_type*
*std::array*< _Tp, _Nm >::const_iterator
Definition at line *103* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef const value_type*
*std::array*< _Tp, _Nm >::const_pointer
Definition at line *99* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef const value_type&
*std::array*< _Tp, _Nm >::const_reference
Definition at line *101* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef
*std::reverse_iterator*<const_iterator> *std::array*< _Tp, _Nm
>::*const_reverse_iterator*
Definition at line *107* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef std::ptrdiff_t
*std::array*< _Tp, _Nm >::difference_type
Definition at line *105* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef value_type*
*std::array*< _Tp, _Nm >::iterator
Definition at line *102* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef value_type*
*std::array*< _Tp, _Nm >::pointer
Definition at line *98* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef value_type&
*std::array*< _Tp, _Nm >::reference
Definition at line *100* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef
*std::reverse_iterator*<iterator> *std::array*< _Tp, _Nm
>::*reverse_iterator*
Definition at line *106* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef std::size_t
*std::array*< _Tp, _Nm >::size_type
Definition at line *104* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> typedef _Tp *std::array*<
_Tp, _Nm >::value_type
Definition at line *97* of file *std/array*.

* Member Function Documentation
** template<typename _Tp , std::size_t _Nm> constexpr reference
*std::array*< _Tp, _Nm >::at (size_type __n)= [inline]=, = [constexpr]=
Definition at line *202* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_reference
*std::array*< _Tp, _Nm >::at (size_type __n) const= [inline]=,
= [constexpr]=
Definition at line *212* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_reference
*std::array*< _Tp, _Nm >::back () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *247* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr reference
*std::array*< _Tp, _Nm >::back ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *240* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_iterator
*std::array*< _Tp, _Nm >::begin () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *131* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr iterator
*std::array*< _Tp, _Nm >::begin ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *127* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_iterator
*std::array*< _Tp, _Nm >::cbegin () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *159* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_iterator
*std::array*< _Tp, _Nm >::cend () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *163* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr
*const_reverse_iterator* *std::array*< _Tp, _Nm >::crbegin ()
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *167* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr
*const_reverse_iterator* *std::array*< _Tp, _Nm >::crend ()
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *171* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_pointer
*std::array*< _Tp, _Nm >::data () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *261* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr pointer
*std::array*< _Tp, _Nm >::data ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *257* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr bool *std::array*<
_Tp, _Nm >::empty () const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *182* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_iterator
*std::array*< _Tp, _Nm >::end () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *139* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr iterator
*std::array*< _Tp, _Nm >::end ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *135* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr void *std::array*<
_Tp, _Nm >::fill (const value_type & __u)= [inline]=, = [constexpr]=
Definition at line *117* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_reference
*std::array*< _Tp, _Nm >::front () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *231* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr reference
*std::array*< _Tp, _Nm >::front ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *224* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr size_type
*std::array*< _Tp, _Nm >::max_size () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *179* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr const_reference
*std::array*< _Tp, _Nm >::operator[] (size_type __n) const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *193* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr reference
*std::array*< _Tp, _Nm >::operator[] (size_type __n)= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *186* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr
*const_reverse_iterator* *std::array*< _Tp, _Nm >::rbegin ()
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *147* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr *reverse_iterator*
*std::array*< _Tp, _Nm >::rbegin ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *143* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr
*const_reverse_iterator* *std::array*< _Tp, _Nm >::rend ()
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *155* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr *reverse_iterator*
*std::array*< _Tp, _Nm >::rend ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *151* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr size_type
*std::array*< _Tp, _Nm >::size () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *176* of file *std/array*.

** template<typename _Tp , std::size_t _Nm> constexpr void *std::array*<
_Tp, _Nm >::swap (*array*< _Tp, _Nm > & __other)= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *121* of file *std/array*.

* Member Data Documentation
** template<typename _Tp , std::size_t _Nm> _AT_Type::_Type
*std::array*< _Tp, _Nm >::_M_elems
Definition at line *111* of file *std/array*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
