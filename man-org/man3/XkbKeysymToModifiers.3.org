#+TITLE: Manpages - XkbKeysymToModifiers.3
#+DESCRIPTION: Linux manpage for XkbKeysymToModifiers.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbKeysymToModifiers - Finds the set of modifiers bound to a particular
keysym on the core keyboard

* SYNOPSIS
*unsigned int XkbKeysymToModifiers* *( Display **/dpy/* ,* *KeySym
*/ks/* );*

* ARGUMENTS
- /- dpy/ :: connection to X server

- /- ks/ :: keysym of interest

* DESCRIPTION
/XkbKeysymToModifiers/ finds the set of modifiers currently bound to the
keysym /ks/ on the core keyboard. The value returned is the mask of
modifiers bound to the keysym /ks./ If no modifiers are bound to the
keysym, /XkbKeysymToModifiers/ returns zero; otherwise, it returns the
inclusive OR of zero or more of the following: ShiftMask, ControlMask,
LockMask, Mod1Mask, Mod2Mask, Mod3Mask, Mod4Mask, and Mod5Mask.
