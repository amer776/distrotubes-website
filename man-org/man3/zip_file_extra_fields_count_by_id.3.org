#+TITLE: Manpages - zip_file_extra_fields_count_by_id.3
#+DESCRIPTION: Linux manpage for zip_file_extra_fields_count_by_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function counts the extra fields for the file at position

in the zip archive.

The following

are supported:

Count extra fields from the archive's central directory.

Count extra fields from the local file headers.

Count the original unchanged extra fields, ignoring any changes made.

The

function counts the extra fields with ID (two-byte signature)

The other arguments are the same as for

Extra fields that are the same in the central directory and the local
file header are merged into one. Therefore, the counts with

and

do not need to add up to the same value as when given

at the same time.

Upon successful completion, the requested number of extra fields is
returned. Otherwise, -1 is returned and the error code in

is set to indicate the error.

and

fail if:

is not a valid file index in

and

were added in libzip 0.11.

and
