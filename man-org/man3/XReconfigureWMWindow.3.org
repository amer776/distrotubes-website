#+TITLE: Manpages - XReconfigureWMWindow.3
#+DESCRIPTION: Linux manpage for XReconfigureWMWindow.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XReconfigureWMWindow.3 is found in manpage for: [[../man3/XIconifyWindow.3][man3/XIconifyWindow.3]]