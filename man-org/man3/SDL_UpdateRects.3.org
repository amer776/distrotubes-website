#+TITLE: Manpages - SDL_UpdateRects.3
#+DESCRIPTION: Linux manpage for SDL_UpdateRects.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_UpdateRects - Makes sure the given list of rectangles is updated on
the given screen.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_UpdateRects*(*SDL_Surface *screen, int numrects, SDL_Rect
*rects*);

* DESCRIPTION
Makes sure the given list of rectangles is updated on the given screen.
The rectangles must all be confined within the screen boundaries (no
clipping is done).

This function should not be called while *screen* is /locked/.

#+begin_quote
  *Note: *

  It is adviced to call this function only once per frame, since each
  call has some processing overhead. This is no restriction since you
  can pass any number of rectangles each time.

  The rectangles are not automatically merged or checked for overlap. In
  general, the programmer can use his knowledge about his particular
  rectangles to merge them in an efficient way, to avoid overdraw.
#+end_quote

* SEE ALSO
*SDL_UpdateRect*, *SDL_Rect*, *SDL_Surface*, *SDL_LockSurface*
