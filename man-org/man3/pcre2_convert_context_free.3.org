#+TITLE: Manpages - pcre2_convert_context_free.3
#+DESCRIPTION: Linux manpage for pcre2_convert_context_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  void pcre2_convert_context_free(pcre2_convert_context *cvcontext);
#+end_example

* DESCRIPTION
This function is part of an experimental set of pattern conversion
functions. It frees the memory occupied by a convert context, using the
memory freeing function from the general context with which it was
created, or *free()* if that was not set. If the argument is NULL, the
function returns immediately without doing anything.

The pattern conversion functions are described in the *pcre2convert*
documentation.
