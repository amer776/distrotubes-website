#+TITLE: Manpages - SSL_CTX_set_ssl_version.3ssl
#+DESCRIPTION: Linux manpage for SSL_CTX_set_ssl_version.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CTX_set_ssl_version, SSL_set_ssl_method, SSL_get_ssl_method - choose
a new TLS/SSL method

* SYNOPSIS
#include <openssl/ssl.h> int SSL_CTX_set_ssl_version(SSL_CTX *ctx, const
SSL_METHOD *method); int SSL_set_ssl_method(SSL *s, const SSL_METHOD
*method); const SSL_METHOD *SSL_get_ssl_method(const SSL *ssl);

* DESCRIPTION
*SSL_CTX_set_ssl_version()* sets a new default TLS/SSL *method* for SSL
objects newly created from this *ctx*. SSL objects already created with
*SSL_new* (3) are not affected, except when *SSL_clear* (3) is being
called.

*SSL_set_ssl_method()* sets a new TLS/SSL *method* for a particular
*ssl* object. It may be reset, when *SSL_clear()* is called.

*SSL_get_ssl_method()* returns a function pointer to the TLS/SSL method
set in *ssl*.

* NOTES
The available *method* choices are described in *SSL_CTX_new* (3).

When *SSL_clear* (3) is called and no session is connected to an SSL
object, the method of the SSL object is reset to the method currently
set in the corresponding SSL_CTX object.

* RETURN VALUES
The following return values can occur for *SSL_CTX_set_ssl_version()*
and *SSL_set_ssl_method()*:

0. The new choice failed, check the error stack to find out the reason.

1. The operation succeeded.

* SEE ALSO
*SSL_CTX_new* (3), *SSL_new* (3), *SSL_clear* (3), *ssl* (7),
*SSL_set_connect_state* (3)

* COPYRIGHT
Copyright 2000-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
