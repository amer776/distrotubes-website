#+TITLE: Manpages - CURLOPT_CAINFO.3
#+DESCRIPTION: Linux manpage for CURLOPT_CAINFO.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
CURLOPT_CAINFO - path to Certificate Authority (CA) bundle

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CAINFO, char *path);

* DESCRIPTION
Pass a char * to a null-terminated string naming a file holding one or
more certificates to verify the peer with.

If /CURLOPT_SSL_VERIFYPEER(3)/ is zero and you avoid verifying the
server's certificate, /CURLOPT_CAINFO(3)/ need not even indicate an
accessible file.

This option is by default set to the system path where libcurl's cacert
bundle is assumed to be stored, as established at build time.

If curl is built against the NSS SSL library, the NSS PEM PKCS#11 module
(libnsspem.so) needs to be available for this option to work properly.
Starting with curl-7.55.0, if both /CURLOPT_CAINFO(3)/ and
/CURLOPT_CAPATH(3)/ are unset, NSS-linked libcurl tries to load
libnssckbi.so, which contains a more comprehensive set of trust
information than supported by nss-pem, because libnssckbi.so also
includes information about distrusted certificates.

(iOS and macOS) When curl uses Secure Transport this option is
supported. If the option is not set, then curl will use the certificates
in the system and user Keychain to verify the peer.

(Schannel) This option is supported for Schannel in Windows 7 or later
but we recommend not using it until Windows 8 since it works better
starting then. If the option is not set, then curl will use the
certificates in the Windows' store of root certificates (the default for
Schannel).

The application does not have to keep the string around after setting
this option.

* DEFAULT
Built-in system specific. When curl is built with Secure Transport or
Schannel, this option is not set by default.

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_CAINFO, "/etc/certs/cabundle.pem");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
For the SSL engines that do not support certificate files the
CURLOPT_CAINFO option is ignored. Schannel support added in libcurl
7.60.

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_CAINFO_BLOB*(3), *CURLOPT_CAPATH*(3),
*CURLOPT_SSL_VERIFYPEER*(3), *CURLOPT_SSL_VERIFYHOST*(3),

Information about CURLOPT_CAINFO.3 is found in manpage for: [[../supported by nss-pem, because libnssckbi.so also includes information][supported by nss-pem, because libnssckbi.so also includes information]]