#+TITLE: Manpages - XmbDrawString.3
#+DESCRIPTION: Linux manpage for XmbDrawString.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XmbDrawString, XwcDrawString, Xutf8DrawString - draw text using a single
font set

* SYNTAX
void XmbDrawString ( Display */display/ , Drawable /d/ , XFontSet
/font_set/ , GC /gc/ , int /x/ , int /y/ , _Xconst char */string/ , int
/num_bytes/ );

void XwcDrawString ( Display */display/ , Drawable /d/ , XFontSet
/font_set/ , GC /gc/ , int /x/ , int /y/ , _Xconst wchar_t */string/ ,
int /num_wchars/ );

void Xutf8DrawString ( Display */display/ , Drawable /d/ , XFontSet
/font_set/ , GC /gc/ , int /x/ , int /y/ , _Xconst char */string/ , int
/num_bytes/ );

* ARGUMENTS

4. Specifies the drawable.

- display :: Specifies the connection to the X server.

- font_set :: Specifies the font set.

- gc :: Specifies the GC.

- num_bytes :: Specifies the number of bytes in the string argument.

- num_wchars :: Specifies the number of characters in the string
  argument.

- string :: Specifies the character string.

24. \\

25. Specify the x and y coordinates.

* DESCRIPTION
The *XmbDrawString*, *XwcDrawString* and *Xutf8DrawString* functions
draw the specified text with the foreground pixel. When the *XFontSet*
has missing charsets, each unavailable character is drawn with the
default string returned by *XCreateFontSet*. The behavior for an invalid
codepoint is undefined.

The function *Xutf8DrawString* is an extension introduced by The XFree86
Project, Inc., in their 4.0.2 release. Its presence is indicated by the
macro *X_HAVE_UTF8_STRING*.

* SEE ALSO
XDrawImageString(3), XDrawString(3), XDrawText(3),
XmbDrawImageString(3), XmbDrawText(3)\\
/Xlib - C Language X Interface/
