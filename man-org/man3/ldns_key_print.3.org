#+TITLE: Manpages - ldns_key_print.3
#+DESCRIPTION: Linux manpage for ldns_key_print.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_key_print - print a ldns_key

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

void ldns_key_print(FILE *output, const ldns_key *k);

* DESCRIPTION
/ldns_key_print/() print a private key to the file output

.br *output*: the FILE descriptor where to print to .br *k*: the
ldns_key to print

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_key_new/, /ldns_key/. And *perldoc Net::DNS*, *RFC1034*,
*RFC1035*, *RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
