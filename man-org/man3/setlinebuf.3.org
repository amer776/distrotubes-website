#+TITLE: Manpages - setlinebuf.3
#+DESCRIPTION: Linux manpage for setlinebuf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setlinebuf.3 is found in manpage for: [[../man3/setbuf.3][man3/setbuf.3]]