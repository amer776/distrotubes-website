#+TITLE: Manpages - std_indirect_array.3
#+DESCRIPTION: Linux manpage for std_indirect_array.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::indirect_array< _Tp > - Reference to arbitrary subset of an array.

* SYNOPSIS
\\

=#include <indirect_array.h>=

** Public Types
typedef _Tp *value_type*\\

** Public Member Functions
*indirect_array* (const *indirect_array* &)\\
Copy constructor. Both slices refer to the same underlying array.

template<class _Dom > void *operator%=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator%=* (const *valarray*< _Tp > &) const\\
Modulo slice elements by corresponding elements of /v/.

template<class _Dom > void *operator&=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator&=* (const *valarray*< _Tp > &) const\\
Logical and slice elements with corresponding elements of /v/.

template<class _Dom > void *operator*=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator*=* (const *valarray*< _Tp > &) const\\
Multiply slice elements by corresponding elements of /v/.

template<class _Dom > void *operator+=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator+=* (const *valarray*< _Tp > &) const\\
Add corresponding elements of /v/ to slice elements.

template<class _Dom > void *operator-=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator-=* (const *valarray*< _Tp > &) const\\
Subtract corresponding elements of /v/ from slice elements.

template<class _Dom > void *operator/=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator/=* (const *valarray*< _Tp > &) const\\
Divide slice elements by corresponding elements of /v/.

template<class _Dom > void *operator<<=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator<<=* (const *valarray*< _Tp > &) const\\
Left shift slice elements by corresponding elements of /v/.

template<class _Dom > void *operator=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator=* (const _Tp &) const\\
Assign all slice elements to /t/.

*indirect_array* & *operator=* (const *indirect_array* &)\\
Assignment operator. Assigns elements to corresponding elements of /a/.

void *operator=* (const *valarray*< _Tp > &) const\\
Assign slice elements to corresponding elements of /v/.

template<class _Dom > void *operator>>=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator>>=* (const *valarray*< _Tp > &) const\\
Right shift slice elements by corresponding elements of /v/.

template<class _Dom > void *operator^=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator^=* (const *valarray*< _Tp > &) const\\
Logical xor slice elements with corresponding elements of /v/.

template<class _Dom > void *operator|=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator|=* (const *valarray*< _Tp > &) const\\
Logical or slice elements with corresponding elements of /v/.

** Friends
class *gslice_array< _Tp >*\\

class *valarray< _Tp >*\\

* Detailed Description
** "template<class _Tp>
\\
class std::indirect_array< _Tp >"Reference to arbitrary subset of an
array.

An indirect_array is a reference to the actual elements of an array
specified by an ordered array of indices. The way to get an
indirect_array is to call operator[](valarray<size_t>) on a valarray.
The returned indirect_array then permits carrying operations out on the
referenced subset of elements in the original valarray.

For example, if an indirect_array is obtained using the array (4,2,0) as
an argument, and then assigned to an array containing (1,2,3), then the
underlying array will have array[0]==3, array[2]==2, and array[4]==1.

*Parameters*

#+begin_quote
  /Tp/ Element type.
#+end_quote

Definition at line *62* of file *indirect_array.h*.

* Member Typedef Documentation
** template<class _Tp > typedef _Tp *std::indirect_array*< _Tp
>::value_type
Definition at line *65* of file *indirect_array.h*.

* Friends And Related Function Documentation
** template<class _Tp > friend class *gslice_array*< _Tp >= [friend]=
Definition at line *128* of file *indirect_array.h*.

** template<class _Tp > friend class *valarray*< _Tp >= [friend]=
Definition at line *128* of file *indirect_array.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
