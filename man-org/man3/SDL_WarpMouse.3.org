#+TITLE: Manpages - SDL_WarpMouse.3
#+DESCRIPTION: Linux manpage for SDL_WarpMouse.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WarpMouse - Set the position of the mouse cursor.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_WarpMouse*(*Uint16 x, Uint16 y*);

* DESCRIPTION
Set the position of the mouse cursor (generates a mouse motion event).

* SEE ALSO
*SDL_MouseMotionEvent*
