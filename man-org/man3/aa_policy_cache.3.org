#+TITLE: Manpages - aa_policy_cache.3
#+DESCRIPTION: Linux manpage for aa_policy_cache.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
aa_policy_cache - an opaque object representing an AppArmor policy cache

aa_policy_cache_new - create a new aa_policy_cache object from a path

aa_policy_cache_ref - increments the ref count of an aa_policy_cache
object

aa_policy_cache_unref - decrements the ref count and frees the
aa_policy_cache object when 0

aa_policy_cache_remove - removes all policy cache files under a path

aa_policy_cache_replace_all - performs a kernel policy replacement of
all cached policies

aa_policy_cache_dir_path - returns the path to the aa_policy_cache
directory

aa_policy_cache_dir_path_preview - returns a preview of the path to the
aa_policy_cache directory without an existing aa_policy_cache object

* SYNOPSIS
*#include <sys/apparmor.h>*

*typedef struct aa_policy_cache aa_policy_cache;*

*int aa_policy_cache_new(aa_policy_cache **policy_cache, aa_features
*kernel_features, int dirfd, const char *path, uint16_t max_caches);*

*int aa_policy_cache_add_ro_dir(aa_policy_cache *policy_cache, int
dirfd, const char *path);*

*aa_policy_cache *aa_policy_cache_ref(aa_policy_cache *policy_cache);*

*void aa_policy_cache_unref(aa_policy_cache *policy_cache);*

*int aa_policy_cache_remove(int dirfd, const char *path);*

*int aa_policy_cache_replace_all(aa_policy_cache *policy_cache,
aa_kernel_interface *kernel_interface);*

*char *aa_policy_cache_dir_path(aa_policy_cache *policy_cache, int
level);*

*char *aa_policy_cache_dir_path_preview(aa_features *kernel_features,
int dirfd, const char *path);*

Link with *-lapparmor* when compiling.

* DESCRIPTION
The /aa_policy_cache/ object contains information about a set of
AppArmor policy cache files. The policy cache files are the binary
representation of a human-readable AppArmor profile. The binary
representation is the form that is loaded into the kernel.

The *aa_policy_cache_new()* function creates an /aa_policy_cache/ object
based upon a directory file descriptor and path. See the *openat* (2)
man page for examples of /dirfd/ and /path/. The /path/ must point to a
directory and it will be used as the basis for the location of policy
cache files. See /aa_policy_cache_dir_path/ to find out which directory
will be used to store the binary policy cache files. If additional
overlay cache directories are used (see /aa_policy_cache_add_ro_dir/)
the directory specified in /aa_policy_cache_new/ is the first directory
searched and is the writable overlay. If /kernel_features/ is NULL, then
the features of the current kernel are used. When specifying a valid
/kernel_features/ object, it must be compatible with the features of the
kernel of interest. The value of /max_caches/ should be equal to the
number of caches that should be allowed before old caches are
automatically reaped. The definition of what is considered to be an old
cache is private to libapparmor. Specifying 0 means that no new caches
should be created and only existing, valid caches may be used.
Specifying UINT16_MAX means that a new cache may be created and that the
reaping of old caches is disabled. The allocated /aa_policy_cache/
object must be freed using *aa_policy_cache_unref()*.

The *aa_policy_cache_add_ro_dir()* function adds an existing cache
directory to the policy cache, as a readonly layer under the primary
directory the cache was created with. When the cache is searched for an
existing cache file the primary directory will be searched and then the
readonly directories in the order that they were added to the policy
cache. This allows the policy cache to be seeded with precompiled policy
that can be updated by overlaying the read only cache file with one
written to the primary cache dir.

*aa_policy_cache_ref()* increments the reference count on the
/policy_cache/ object.

*aa_policy_cache_unref()* decrements the reference count on the
/policy_cache/ object and releases all corresponding resources when the
reference count reaches zero.

The *aa_policy_cache_remove()* function deletes all of the policy cache
files based upon a directory file descriptor and path. The /path/ must
point to a directory. See the *openat* (2) man page for examples of
/dirfd/ and /path/.

The *aa_policy_cache_replace_all()* function can be used to perform a
policy replacement of all of the cache policies in the cache directory
represented by the /policy_cache/ object. If /kernel_interface/ is NULL,
then the current kernel interface is used. When specifying a valid
/kernel_interface/ object, it must be the interface of the currently
running kernel.

The *aa_policy_cache_dir_path()* function provides the path to the cache
directory for a /policy_cache/ object at /level/ in the policy cache
overlay of cache directories. A /level/ of 0 will always be present and
is the first directory to search in an overlay of cache directories, and
will also be the writable cache directory layer. Binary policy cache
files will be located in the directory returned by this function.

The *aa_policy_cache_dir_levels()* function provides access to the
number of directories that are being overlayed to create the policy
cache.

* RETURN VALUE
The *aa_policy_cache_new()* function returns 0 on success and
/*policy_cache/ will point to an /aa_policy_cache/ object that must be
freed by *aa_policy_cache_unref()*. -1 is returned on error, with errno
set appropriately, and /*policy_cache/ will be set to NULL.

*aa_policy_cache_ref()* returns the value of /policy_cache/.

*aa_policy_cache_remove()* and *aa_policy_cache_replace_all()* return 0
on success. -1 is returned on error, with errno set appropriately.

*aa_policy_cache_dir_path()* returns a path string which must be freed
by the caller. NULL is returned on error, with errno set appropriately.

*aa_policy_cache_dir_levels()* returns a number indicating the number of
directory levels there are associated with the /policy_cache/.

*aa_policy_cache_dir_path_preview()* is the same as
*aa_policy_cache_dir_path()* except that it doesn't require an existing
/aa_policy_cache/ object. This is useful if the calling program cannot
create an /aa_policy_cache/ object due to lack of privileges needed to
create the cache directory.

* ERRORS
The errno value will be set according to the underlying error in the
/aa_policy_cache/ family of functions that return -1 or NULL on error.

* NOTES
All aa_policy_cache functions described above, except for the
*aa_policy_cache_dir_path()* function was added in libapparmor version
2.13. All the other aa_policy_cache functions described above are
present in libapparmor version 2.10.

*aa_policy_cache_unref()* saves the value of errno when called and
restores errno before exiting in libapparmor version 2.12 and newer.

* BUGS
None known. If you find any, please report them at
<https://gitlab.com/apparmor/apparmor/-/issues>.

* SEE ALSO
*aa_features* (3), *aa_kernel_interface* (3), *openat* (2) and
<https://wiki.apparmor.net>.
