#+TITLE: Manpages - __gnu_cxx__Char_types.3
#+DESCRIPTION: Linux manpage for __gnu_cxx__Char_types.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::_Char_types< _CharT > - Mapping from character type to
associated types.

* SYNOPSIS
\\

=#include <char_traits.h>=

** Public Types
typedef unsigned long *int_type*\\

typedef *std::streamoff* *off_type*\\

typedef *std::streampos* *pos_type*\\

typedef std::mbstate_t *state_type*\\

* Detailed Description
** "template<typename _CharT>
\\
struct __gnu_cxx::_Char_types< _CharT >"Mapping from character type to
associated types.

*Note*

#+begin_quote
  This is an implementation class for the generic version of
  char_traits. It defines int_type, off_type, pos_type, and state_type.
  By default these are unsigned long, streamoff, streampos, and
  mbstate_t. Users who need a different set of types, but who don't need
  to change the definitions of any function defined in char_traits, can
  specialize __gnu_cxx::_Char_types while leaving __gnu_cxx::char_traits
  alone.
#+end_quote

Definition at line *65* of file *char_traits.h*.

* Member Typedef Documentation
** template<typename _CharT > typedef unsigned long
*__gnu_cxx::_Char_types*< _CharT >::int_type
Definition at line *67* of file *char_traits.h*.

** template<typename _CharT > typedef *std::streamoff*
*__gnu_cxx::_Char_types*< _CharT >::off_type
Definition at line *69* of file *char_traits.h*.

** template<typename _CharT > typedef *std::streampos*
*__gnu_cxx::_Char_types*< _CharT >::*pos_type*
Definition at line *68* of file *char_traits.h*.

** template<typename _CharT > typedef std::mbstate_t
*__gnu_cxx::_Char_types*< _CharT >::state_type
Definition at line *70* of file *char_traits.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
