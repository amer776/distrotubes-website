#+TITLE: Manpages - fcloseall.3
#+DESCRIPTION: Linux manpage for fcloseall.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fcloseall - close all open streams

* SYNOPSIS
#+begin_example
  #define _GNU_SOURCE /* See feature_test_macros(7) */
  #include <stdio.h>

  int fcloseall(void);
#+end_example

* DESCRIPTION
The *fcloseall*() function closes all of the calling process's open
streams. Buffered output for each stream is written before it is closed
(as for *fflush*(3)); buffered input is discarded.

The standard streams, /stdin/, /stdout/, and /stderr/ are also closed.

* RETURN VALUE
This function returns 0 if all files were successfully closed; on error,
*EOF* is returned.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface     | Attribute     | Value                  |
| *fcloseall*() | Thread safety | MT-Unsafe race:streams |

The *fcloseall*() function does not lock the streams, so it is not
thread-safe.

* CONFORMING TO
This function is a GNU extension.

* SEE ALSO
*close*(2), *fclose*(3), *fflush*(3), *fopen*(3), *setbuf*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
