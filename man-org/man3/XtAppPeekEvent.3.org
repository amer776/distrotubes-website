#+TITLE: Manpages - XtAppPeekEvent.3
#+DESCRIPTION: Linux manpage for XtAppPeekEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppPeekEvent.3 is found in manpage for: [[../man3/XtAppNextEvent.3][man3/XtAppNextEvent.3]]