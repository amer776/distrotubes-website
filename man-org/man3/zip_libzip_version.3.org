#+TITLE: Manpages - zip_libzip_version.3
#+DESCRIPTION: Linux manpage for zip_libzip_version.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

returns the version number of the library as string in the format

where

is the major version,

the minor,

the micro, and

a suffix that's only set for development versions.

was added in libzip 1.3.1.

and
