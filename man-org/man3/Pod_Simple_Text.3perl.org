#+TITLE: Manpages - Pod_Simple_Text.3perl
#+DESCRIPTION: Linux manpage for Pod_Simple_Text.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Simple::Text -- format Pod as plaintext

* SYNOPSIS
perl -MPod::Simple::Text -e \ "exit
Pod::Simple::Text->filter(shift)->any_errata_seen" \ thingy.pod

* DESCRIPTION
This class is a formatter that takes Pod and renders it as wrapped
plaintext.

Its wrapping is done by Text::Wrap, so you can change
=$Text::Wrap::columns= as you like.

This is a subclass of Pod::Simple and inherits all its methods.

* SEE ALSO
Pod::Simple, Pod::Simple::TextContent, Pod::Text

* SUPPORT
Questions or discussion about POD and Pod::Simple should be sent to the
pod-people@perl.org mail list. Send an empty email to
pod-people-subscribe@perl.org to subscribe.

This module is managed in an open GitHub repository,
<https://github.com/perl-pod/pod-simple/>. Feel free to fork and
contribute, or to clone <git://github.com/perl-pod/pod-simple.git> and
send patches!

Patches against Pod::Simple are welcome. Please send bug reports to
<bug-pod-simple@rt.cpan.org>.

* COPYRIGHT AND DISCLAIMERS
Copyright (c) 2002 Sean M. Burke.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

* AUTHOR
Pod::Simple was created by Sean M. Burke <sburke@cpan.org>. But don't
bother him, he's retired.

Pod::Simple is maintained by:

- Allison Randal =allison@perl.org=

- Hans Dieter Pearcey =hdp@cpan.org=

- David E. Wheeler =dwheeler@cpan.org=
