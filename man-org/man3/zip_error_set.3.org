#+TITLE: Manpages - zip_error_set.3
#+DESCRIPTION: Linux manpage for zip_error_set.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function sets the zip_error pointed to by

to the libzip error code

and the system error code

must be allocated and initialized with

before calling

was added in libzip 1.0.

and
