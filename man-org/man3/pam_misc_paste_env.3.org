#+TITLE: Manpages - pam_misc_paste_env.3
#+DESCRIPTION: Linux manpage for pam_misc_paste_env.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_misc_paste_env - transcribing an environment to that of PAM

* SYNOPSIS
#+begin_example
  #include <security/pam_misc.h>
#+end_example

*int pam_misc_paste_env(pam_handle_t **/pamh/*, const char * const
**/user/*);*

* DESCRIPTION
This function takes the supplied list of environment pointers and
/uploads/ its contents to the PAM environment. Success is indicated by
PAM_SUCCESS.

* SEE ALSO
*pam_putenv*(3), *pam*(8)

* STANDARDS
The *pam_misc_paste_env* function is part of the *libpam_misc* Library
and not defined in any standard.
