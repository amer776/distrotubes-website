#+TITLE: Manpages - SDL_SetColors.3
#+DESCRIPTION: Linux manpage for SDL_SetColors.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SetColors - Sets a portion of the colormap for the given 8-bit
surface.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_SetColors*(*SDL_Surface *surface, SDL_Color *colors, int
firstcolor, int ncolors*);

* DESCRIPTION
Sets a portion of the colormap for the given 8-bit surface.

When *surface* is the surface associated with the current display, the
display colormap will be updated with the requested colors. If
*SDL_HWPALETTE* was set in /SDL_SetVideoMode/ flags, *SDL_SetColors*
will always return *1*, and the palette is guaranteed to be set the way
you desire, even if the window colormap has to be warped or run under
emulation.

The color components of a *SDL_Color* structure are 8-bits in size,
giving you a total of 256^3 =16777216 colors.

Palettized (8-bit) screen surfaces with the *SDL_HWPALETTE* flag have
two palettes, a logical palette that is used for mapping blits to/from
the surface and a physical palette (that determines how the hardware
will map the colors to the display). *SDL_SetColors* modifies both
palettes (if present), and is equivalent to calling /SDL_SetPalette/
with the *flags* set to *(SDL_LOGPAL | SDL_PHYSPAL)*.

* RETURN VALUE
If *surface* is not a palettized surface, this function does nothing,
returning *0*. If all of the colors were set as passed to
*SDL_SetColors*, it will return *1*. If not all the color entries were
set exactly as given, it will return *0*, and you should look at the
surface palette to determine the actual color palette.

* EXAMPLE
#+begin_example
  /* Create a display surface with a grayscale palette */
  SDL_Surface *screen;
  SDL_Color colors[256];
  int i;
  .
  .
  .
  /* Fill colors with color information */
  for(i=0;i<256;i++){
    colors[i].r=i;
    colors[i].g=i;
    colors[i].b=i;
  }

  /* Create display */
  screen=SDL_SetVideoMode(640, 480, 8, SDL_HWPALETTE);
  if(!screen){
    printf("Couldn't set video mode: %s
  ", SDL_GetError());
    exit(-1);
  }

  /* Set palette */
  SDL_SetColors(screen, colors, 0, 256);
  .
  .
  .
  .
#+end_example

* SEE ALSO
*SDL_Color* *SDL_Surface*, *SDL_SetPalette*, *SDL_SetVideoMode*
