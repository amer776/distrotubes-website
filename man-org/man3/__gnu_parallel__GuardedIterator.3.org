#+TITLE: Manpages - __gnu_parallel__GuardedIterator.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__GuardedIterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_GuardedIterator< _RAIter, _Compare > - _Iterator
wrapper supporting an implicit supremum at the end of the sequence,
dominating all comparisons.

* SYNOPSIS
\\

=#include <multiway_merge.h>=

** Public Member Functions
*_GuardedIterator* (_RAIter __begin, _RAIter __end, _Compare &__comp)\\
Constructor. Sets iterator to beginning of sequence.

*operator _RAIter* () const\\
Convert to wrapped iterator.

*std::iterator_traits*< _RAIter >::value_type & *operator** () const\\
Dereference operator.

*_GuardedIterator*< _RAIter, _Compare > & *operator++* ()\\
Pre-increment operator.

** Friends
bool *operator<* (const *_GuardedIterator*< _RAIter, _Compare > &__bi1,
const *_GuardedIterator*< _RAIter, _Compare > &__bi2)\\
Compare two elements referenced by guarded iterators.

bool *operator<=* (const *_GuardedIterator*< _RAIter, _Compare > &__bi1,
const *_GuardedIterator*< _RAIter, _Compare > &__bi2)\\
Compare two elements referenced by guarded iterators.

* Detailed Description
** "template<typename _RAIter, typename _Compare>
\\
class __gnu_parallel::_GuardedIterator< _RAIter, _Compare >"_Iterator
wrapper supporting an implicit supremum at the end of the sequence,
dominating all comparisons.

The implicit supremum comes with a performance cost.

Deriving from _RAIter is not possible since _RAIter need not be a class.

Definition at line *73* of file *multiway_merge.h*.

* Constructor & Destructor Documentation
** template<typename _RAIter , typename _Compare >
*__gnu_parallel::_GuardedIterator*< _RAIter, _Compare
>::*_GuardedIterator* (_RAIter __begin, _RAIter __end, _Compare &
__comp)= [inline]=
Constructor. Sets iterator to beginning of sequence.

*Parameters*

#+begin_quote
  /__begin/ Begin iterator of sequence.\\
  /__end/ End iterator of sequence.\\
  /__comp/ Comparator provided for associated overloaded compare
  operators.
#+end_quote

Definition at line *91* of file *multiway_merge.h*.

* Member Function Documentation
** template<typename _RAIter , typename _Compare >
*__gnu_parallel::_GuardedIterator*< _RAIter, _Compare >::operator
_RAIter () const= [inline]=
Convert to wrapped iterator.

*Returns*

#+begin_quote
  Wrapped iterator.
#+end_quote

Definition at line *112* of file *multiway_merge.h*.

** template<typename _RAIter , typename _Compare >
*std::iterator_traits*< _RAIter >::value_type &
*__gnu_parallel::_GuardedIterator*< _RAIter, _Compare >::operator* ()
const= [inline]=
Dereference operator.

*Returns*

#+begin_quote
  Referenced element.
#+end_quote

Definition at line *107* of file *multiway_merge.h*.

** template<typename _RAIter , typename _Compare > *_GuardedIterator*<
_RAIter, _Compare > & *__gnu_parallel::_GuardedIterator*< _RAIter,
_Compare >::operator++ ()= [inline]=
Pre-increment operator.

*Returns*

#+begin_quote
  This.
#+end_quote

Definition at line *98* of file *multiway_merge.h*.

* Friends And Related Function Documentation
** template<typename _RAIter , typename _Compare > bool operator< (const
*_GuardedIterator*< _RAIter, _Compare > & __bi1, const
*_GuardedIterator*< _RAIter, _Compare > & __bi2)= [friend]=
Compare two elements referenced by guarded iterators.

*Parameters*

#+begin_quote
  /__bi1/ First iterator.\\
  /__bi2/ Second iterator.
#+end_quote

*Returns*

#+begin_quote
  =true= if less.
#+end_quote

Definition at line *119* of file *multiway_merge.h*.

** template<typename _RAIter , typename _Compare > bool operator<=
(const *_GuardedIterator*< _RAIter, _Compare > & __bi1, const
*_GuardedIterator*< _RAIter, _Compare > & __bi2)= [friend]=
Compare two elements referenced by guarded iterators.

*Parameters*

#+begin_quote
  /__bi1/ First iterator.\\
  /__bi2/ Second iterator.
#+end_quote

*Returns*

#+begin_quote
  =True= if less equal.
#+end_quote

Definition at line *134* of file *multiway_merge.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
