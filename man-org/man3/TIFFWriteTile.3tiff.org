#+TITLE: Manpages - TIFFWriteTile.3tiff
#+DESCRIPTION: Linux manpage for TIFFWriteTile.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFWriteTile - encode and write a tile of data to an open

file

* SYNOPSIS
*#include <tiffio.h>*

*tsize_t TIFFWriteTile(TIFF **/tif/*, tdata_t */buf/*, uint32_t */x/*,
uint32_t */y/*, uint32_t */z/*, tsample_t */sample/*)*

* DESCRIPTION
Write the data for the tile /containing/ the specified coordinates. The
data in /buf/ are is (potentially) compressed, and written to the
indicated file, normally being appended to the end of the file. The
buffer must be contain an entire tile of data. Applications should call
the routine /TIFFTileSize/ to find out the size (in bytes) of a tile
buffer. The /x/ and /y/ parameters are always used by /TIFFWriteTile/.
The /z/ parameter is used if the image is deeper than 1 slice (
/ImageDepth/>1). The /sample/ parameter is used only if data are
organized in separate planes ( /PlanarConfiguration/=2).

* RETURN VALUES
/TIFFWriteTile/ returns -1 if it detects an error; otherwise the number
of bytes in the tile is returned.

* DIAGNOSTICS
All error messages are directed to the *TIFFError*(3TIFF) routine.

* SEE ALSO
*TIFFCheckTile*(3TIFF), *TIFFComputeTile*(3TIFF), *TIFFOpen*(3TIFF),
*TIFFReadTile*(3TIFF), *TIFFWriteScanline*(3TIFF),
*TIFFWriteEncodedTile*(3TIFF), *TIFFWriteRawTile*(3TIFF),
*libtiff*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
