#+TITLE: Manpages - std_chrono_time_point.3
#+DESCRIPTION: Linux manpage for std_chrono_time_point.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::chrono::time_point< _Clock, _Dur > - time_point

* SYNOPSIS
\\

** Public Types
typedef _Clock *clock*\\

typedef _Dur *duration*\\

typedef duration::period *period*\\

typedef duration::rep *rep*\\

** Public Member Functions
constexpr *time_point* (const duration &__dur)\\

template<typename _Dur2 , typename = _Require<is_convertible<_Dur2,
_Dur>>> constexpr *time_point* (const *time_point*< clock, _Dur2 >
&__t)\\

constexpr *time_point* & *operator+=* (const duration &__dur)\\

constexpr *time_point* & *operator-=* (const duration &__dur)\\

constexpr duration *time_since_epoch* () const\\

** Static Public Member Functions
static constexpr *time_point* *max* () noexcept\\

static constexpr *time_point* *min* () noexcept\\

** Related Functions
(Note that these are not member functions.)

\\

template<typename _Clock , typename _Dur1 , typename _Rep2 , typename
_Period2 > constexpr *time_point*< _Clock, typename *common_type*<
_Dur1, duration< _Rep2, _Period2 > >::type > *operator+* (const
*time_point*< _Clock, _Dur1 > &__lhs, const duration< _Rep2, _Period2 >
&__rhs)\\

template<typename _Rep1 , typename _Period1 , typename _Clock , typename
_Dur2 > constexpr *time_point*< _Clock, typename *common_type*<
duration< _Rep1, _Period1 >, _Dur2 >::type > *operator+* (const
duration< _Rep1, _Period1 > &__lhs, const *time_point*< _Clock, _Dur2 >
&__rhs)\\
Adjust a time point forwards by the given duration.

template<typename _Clock , typename _Dur1 , typename _Rep2 , typename
_Period2 > constexpr *time_point*< _Clock, typename *common_type*<
_Dur1, duration< _Rep2, _Period2 > >::type > *operator-* (const
*time_point*< _Clock, _Dur1 > &__lhs, const duration< _Rep2, _Period2 >
&__rhs)\\
Adjust a time point backwards by the given duration.

\\

template<typename _Clock , typename _Dur1 , typename _Dur2 > constexpr
*common_type*< _Dur1, _Dur2 >::type *operator-* (const *time_point*<
_Clock, _Dur1 > &__lhs, const *time_point*< _Clock, _Dur2 > &__rhs)\\

* Detailed Description
** "template<typename _Clock, typename _Dur>
\\
struct std::chrono::time_point< _Clock, _Dur >"time_point

Definition at line *851* of file *std/chrono*.

* Member Typedef Documentation
** template<typename _Clock , typename _Dur > typedef _Clock
*std::chrono::time_point*< _Clock, _Dur >::clock
Definition at line *856* of file *std/chrono*.

** template<typename _Clock , typename _Dur > typedef _Dur
*std::chrono::time_point*< _Clock, _Dur >::duration
Definition at line *857* of file *std/chrono*.

** template<typename _Clock , typename _Dur > typedef duration::period
*std::chrono::time_point*< _Clock, _Dur >::period
Definition at line *859* of file *std/chrono*.

** template<typename _Clock , typename _Dur > typedef duration::rep
*std::chrono::time_point*< _Clock, _Dur >::rep
Definition at line *858* of file *std/chrono*.

* Constructor & Destructor Documentation
** template<typename _Clock , typename _Dur > constexpr
*std::chrono::time_point*< _Clock, _Dur >::*time_point* ()= [inline]=,
= [constexpr]=
Definition at line *861* of file *std/chrono*.

** template<typename _Clock , typename _Dur > constexpr
*std::chrono::time_point*< _Clock, _Dur >::*time_point* (const duration
& __dur)= [inline]=, = [explicit]=, = [constexpr]=
Definition at line *864* of file *std/chrono*.

** template<typename _Clock , typename _Dur > template<typename _Dur2 ,
typename = _Require<is_convertible<_Dur2, _Dur>>> constexpr
*std::chrono::time_point*< _Clock, _Dur >::*time_point* (const
*time_point*< clock, _Dur2 > & __t)= [inline]=, = [constexpr]=
Definition at line *871* of file *std/chrono*.

* Member Function Documentation
** template<typename _Clock , typename _Dur > static constexpr
*time_point* *std::chrono::time_point*< _Clock, _Dur >::max
()= [inline]=, = [static]=, = [constexpr]=, = [noexcept]=
Definition at line *925* of file *std/chrono*.

** template<typename _Clock , typename _Dur > static constexpr
*time_point* *std::chrono::time_point*< _Clock, _Dur >::min
()= [inline]=, = [static]=, = [constexpr]=, = [noexcept]=
Definition at line *921* of file *std/chrono*.

** template<typename _Clock , typename _Dur > constexpr *time_point* &
*std::chrono::time_point*< _Clock, _Dur >::operator+= (const duration &
__dur)= [inline]=, = [constexpr]=
Definition at line *906* of file *std/chrono*.

** template<typename _Clock , typename _Dur > constexpr *time_point* &
*std::chrono::time_point*< _Clock, _Dur >::operator-= (const duration &
__dur)= [inline]=, = [constexpr]=
Definition at line *913* of file *std/chrono*.

** template<typename _Clock , typename _Dur > constexpr duration
*std::chrono::time_point*< _Clock, _Dur >::time_since_epoch ()
const= [inline]=, = [constexpr]=
Definition at line *877* of file *std/chrono*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
