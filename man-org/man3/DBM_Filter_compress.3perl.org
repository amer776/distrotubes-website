#+TITLE: Manpages - DBM_Filter_compress.3perl
#+DESCRIPTION: Linux manpage for DBM_Filter_compress.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBM_Filter::compress - filter for DBM_Filter

* SYNOPSIS
use SDBM_File; # or DB_File, GDBM_File, NDBM_File, ODBM_File use
DBM_Filter ; $db = tie %hash, ... $db->Filter_Push(compress);

* DESCRIPTION
This DBM filter will compress all data before it is written to the
database and uncompressed it on reading.

A fatal error will be thrown if the Compress::Zlib module is not
available.

* SEE ALSO
DBM_Filter, perldbmfilter, Compress::Zlib

* AUTHOR
Paul Marquess pmqs@cpan.org
