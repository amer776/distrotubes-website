#+TITLE: Manpages - XkbRefreshKeyboardMapping.3
#+DESCRIPTION: Linux manpage for XkbRefreshKeyboardMapping.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbRefreshKeyboardMapping - Update the keyboard description that is
internal to the X library

* SYNOPSIS
*Status XkbRefreshKeyboardMapping* *( XkbMapNotifyEvent **/event/* );*

* ARGUMENTS
- /- event/ :: event initiating remapping

* DESCRIPTION
/XkbRefreshKeyboardMapping/ is the Xkb equivalent of the core
/XRefreshKeyboardMapping/ function. It requests that the X server send
the current key mapping information to this client. A client usually
invokes /XkbRefreshKeyboardMapping/ after receiving an XkbMapNotify
event. /XkbRefreshKeyboardMapping/ returns Success if it succeeds and
BadMatch if the event is not an Xkb event.

The XkbMapNotify event can be generated when some client calls
/XkbSetMap, XkbChangeMap, XkbGetKeyboardByName,/ or any of the standard
X library functions that change the keyboard mapping or modifier
mapping.

* RETURN VALUES
- Success :: The /XkbRefreshKeyboardMapping/ function returns Success
  when the request that the X server send the current key mapping
  information to this client is successful.

* DIAGNOSTICS
- *BadMatch* :: A compatible version of Xkb was not available in the
  server or an argument has correct type and range, but is otherwise
  invalid

* SEE ALSO
*XkbChangeMap*(3), *XkbGetKeyboardByName*(3), *XkbSetMap*(3),
*XRefreshKeyboardMapping*(3)
