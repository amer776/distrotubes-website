#+TITLE: Manpages - FcValuePrint.3
#+DESCRIPTION: Linux manpage for FcValuePrint.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcValuePrint - Print a value to stdout

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcValuePrint (FcValue /v/*);*

* DESCRIPTION
Prints a human-readable representation of /v/ to stdout. The format
should not be considered part of the library specification as it may
change in the future.
