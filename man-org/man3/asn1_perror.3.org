#+TITLE: Manpages - asn1_perror.3
#+DESCRIPTION: Linux manpage for asn1_perror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
asn1_perror - API function

* SYNOPSIS
*#include <libtasn1.h>*

*void asn1_perror(int */error/*);*

* ARGUMENTS
- int error :: is an error returned by a libtasn1 function.

* DESCRIPTION
Prints a string to stderr with a description of an error. This function
is like *perror()*. The only difference is that it accepts an error
returned by a libtasn1 function.

* SINCE
1.6

* COPYRIGHT
Copyright © 2006-2021 Free Software Foundation, Inc..\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libtasn1* is maintained as a Texinfo manual.
If the *info* and *libtasn1* programs are properly installed at your
site, the command

#+begin_quote
  *info libtasn1*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libtasn1/manual/*
#+end_quote
