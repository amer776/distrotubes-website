#+TITLE: Manpages - putwc.3
#+DESCRIPTION: Linux manpage for putwc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about putwc.3 is found in manpage for: [[../man3/fputwc.3][man3/fputwc.3]]