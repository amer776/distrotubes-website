#+TITLE: Manpages - ares_library_initialized.3
#+DESCRIPTION: Linux manpage for ares_library_initialized.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_library_initialized - get the initialization state

* SYNOPSIS
#+begin_example
  #include <ares.h>

  int ares_library_initialized(void)
#+end_example

* DESCRIPTION
Returns information if c-ares needs to get initialized.

* RETURN VALUE
/ARES_ENOTINITIALIZED/ if not initialized and /ARES_SUCCESS/ if no
initialization is needed.

* AVAILABILITY
This function was first introduced in c-ares version 1.11.0

* SEE ALSO
*ares_library_init(3),* *ares_library_cleanup(3)*
