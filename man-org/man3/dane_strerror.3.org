#+TITLE: Manpages - dane_strerror.3
#+DESCRIPTION: Linux manpage for dane_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dane_strerror - API function

* SYNOPSIS
*#include <gnutls/dane.h>*

*const char * dane_strerror(int */error/*);*

* ARGUMENTS
- int error :: is a DANE error code, a negative error code

* DESCRIPTION
This function is similar to strerror. The difference is that it accepts
an error number returned by a gnutls function; In case of an unknown
error a descriptive string is sent instead of *NULL*.

Error codes are always a negative error code.

* RETURNS
A string explaining the DANE error message.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
