#+TITLE: Manpages - Memoize_ExpireTest.3perl
#+DESCRIPTION: Linux manpage for Memoize_ExpireTest.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Memoize::ExpireTest - test for Memoize expiration semantics

* DESCRIPTION
This module is just for testing expiration semantics. It's not a very
good example of how to write an expiration module.

If you are looking for an example, I recommend that you look at the
simple example in the Memoize::Expire documentation, or at the code for
Memoize::Expire itself.

If you have questions, I will be happy to answer them if you send them
to mjd-perl-memoize+@plover.com.
