#+TITLE: Manpages - gnutls_priority_set_direct.3
#+DESCRIPTION: Linux manpage for gnutls_priority_set_direct.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_priority_set_direct - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_priority_set_direct(gnutls_session_t */session/*, const char
* */priorities/*, const char ** */err_pos/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- const char * priorities :: is a string describing priorities

- const char ** err_pos :: In case of an error this will have the
  position in the string the error occurred

* DESCRIPTION
Sets the priorities to use on the ciphers, key exchange methods, and
macs. This function avoids keeping a priority cache and is used to
directly set string priorities to a TLS session. For documentation check
the *gnutls_priority_init()*.

To use a reasonable default, consider using
*gnutls_set_default_priority()*, or
*gnutls_set_default_priority_append()* instead of this function.

* RETURNS
On syntax error *GNUTLS_E_INVALID_REQUEST* is returned,
*GNUTLS_E_SUCCESS* on success, or an error code.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
