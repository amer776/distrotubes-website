#+TITLE: Manpages - libssh2_session_get_blocking.3
#+DESCRIPTION: Linux manpage for libssh2_session_get_blocking.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_session_get_blocking - TODO

* SYNOPSIS
int libssh2_session_get_blocking(LIBSSH2_SESSION *session);

* DESCRIPTION
Returns 0 if the state of the session has previously be set to
non-blocking and it returns 1 if the state was set to blocking.

* RETURN VALUE
See description.

* SEE ALSO
*libssh2_session_set_blocking(3)*
