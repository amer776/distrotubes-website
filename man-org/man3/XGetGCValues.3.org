#+TITLE: Manpages - XGetGCValues.3
#+DESCRIPTION: Linux manpage for XGetGCValues.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetGCValues.3 is found in manpage for: [[../man3/XCreateGC.3][man3/XCreateGC.3]]