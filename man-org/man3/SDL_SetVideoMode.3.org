#+TITLE: Manpages - SDL_SetVideoMode.3
#+DESCRIPTION: Linux manpage for SDL_SetVideoMode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SetVideoMode - Set up a video mode with the specified width, height
and bits-per-pixel.

* SYNOPSIS
*#include "SDL.h"*

*SDL_Surface *SDL_SetVideoMode*(*int width, int height, int bpp, Uint32
flags*);

* DESCRIPTION
Set up a video mode with the specified width, height and bits-per-pixel.

If *bpp* is 0, it is treated as the current display bits per pixel.

The *flags* parameter is the same as the *flags* field of the
*SDL_Surface* structure. OR'd combinations of the following values are
valid.

- *SDL_SWSURFACE* :: Create the video surface in system memory

- *SDL_HWSURFACE* :: Create the video surface in video memory

- *SDL_ASYNCBLIT* :: Enables the use of asynchronous updates of the
  display surface. This will usually slow down blitting on single CPU
  machines, but may provide a speed increase on SMP systems.

- *SDL_ANYFORMAT* :: Normally, if a video surface of the requested
  bits-per-pixel (*bpp*) is not available, SDL will emulate one with a
  shadow surface. Passing *SDL_ANYFORMAT* prevents this and causes SDL
  to use the video surface, regardless of its pixel depth.

- *SDL_HWPALETTE* :: Give SDL exclusive palette access. Without this
  flag you may not always get the the colors you request with
  *SDL_SetColors* or *SDL_SetPalette*.

- *SDL_DOUBLEBUF* :: Enable hardware double buffering; only valid with
  SDL_HWSURFACE. Calling *SDL_Flip* will flip the buffers and update the
  screen. All drawing will take place on the surface that is not
  displayed at the moment. If double buffering could not be enabled then
  *SDL_Flip* will just perform a *SDL_UpdateRect* on the entire screen.

- *SDL_FULLSCREEN* :: SDL will attempt to use a fullscreen mode. If a
  hardware resolution change is not possible (for whatever reason), the
  next higher resolution will be used and the display window centered on
  a black background.

- *SDL_OPENGL* :: Create an OpenGL rendering context. You should have
  previously set OpenGL video attributes with *SDL_GL_SetAttribute*.

- *SDL_OPENGLBLIT* :: Create an OpenGL rendering context, like above,
  but allow normal blitting operations. The screen (2D) surface may have
  an alpha channel, and *SDL_UpdateRects* must be used for updating
  changes to the screen surface.

- *SDL_RESIZABLE* :: Create a resizable window. When the window is
  resized by the user a *SDL_VIDEORESIZE* event is generated and
  *SDL_SetVideoMode* can be called again with the new size.

- *SDL_NOFRAME* :: If possible, *SDL_NOFRAME* causes SDL to create a
  window with no title bar or frame decoration. Fullscreen modes
  automatically have this flag set.

#+begin_quote
  *Note: *

  Whatever *flags* *SDL_SetVideoMode* could satisfy are set in the
  *flags* member of the returned surface.
#+end_quote

#+begin_quote
  *Note: *

  The *bpp* parameter is the number of bits per pixel, so a *bpp* of 24
  uses the packed representation of 3 bytes/pixel. For the more common 4
  bytes/pixel mode, use a *bpp* of 32. Somewhat oddly, both 15 and 16
  will request a 2 bytes/pixel mode, but different pixel formats.
#+end_quote

* RETURN VALUE
The framebuffer surface, or *NULL* if it fails. The surface returned is
freed by SDL_Quit() and should nt be freed by the caller.

* SEE ALSO
*SDL_LockSurface*, *SDL_SetColors*, *SDL_Flip*, *SDL_Surface*
