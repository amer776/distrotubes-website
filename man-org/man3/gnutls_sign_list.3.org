#+TITLE: Manpages - gnutls_sign_list.3
#+DESCRIPTION: Linux manpage for gnutls_sign_list.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_sign_list - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*const gnutls_sign_algorithm_t * gnutls_sign_list( */void/*);*

* ARGUMENTS
-  void :: 

* DESCRIPTION
Get a list of supported public key signature algorithms. This function
is not thread safe.

* RETURNS
a (0)-terminated list of *gnutls_sign_algorithm_t* integers indicating
the available ciphers.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
