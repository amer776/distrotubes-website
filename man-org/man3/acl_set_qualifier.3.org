#+TITLE: Manpages - acl_set_qualifier.3
#+DESCRIPTION: Linux manpage for acl_set_qualifier.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function sets the qualifier of the ACL entry indicated by the argument

to the value referred to by the argument

If the value of the tag type in the ACL entry referred to by

is ACL_USER, then the value referred to by

shall be of type

If the value of the tag type in the ACL entry referred to by

is ACL_GROUP, then the value referred to by

shall be of type

If the value of the tag type in the ACL entry referred to by

is a tag type for which a qualifier is not supported,

returns an error.

Any ACL entry descriptors that refer to the entry referred to by

continue to refer to that entry. This function may cause memory to be
allocated. The caller should free any releasable memory, when the ACL is
no longer required, by calling

with a pointer to the ACL as argument.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid descriptor for an ACL entry.

The value of the tag type in the ACL entry referenced by the argument

is neither ACL_USER nor ACL_GROUP.

The value pointed to by the argument

is not valid.

The

function is unable to allocate the memory required for the ACL
qualifier.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
