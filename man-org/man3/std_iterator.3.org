#+TITLE: Manpages - std_iterator.3
#+DESCRIPTION: Linux manpage for std_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::iterator< _Category, _Tp, _Distance, _Pointer, _Reference > -
Common iterator class.

* SYNOPSIS
\\

=#include <stl_iterator_base_types.h>=

** Public Types
typedef _Distance *difference_type*\\
Distance between iterators is represented as this type.

typedef _Category *iterator_category*\\
One of the *tag types*.

typedef _Pointer *pointer*\\
This type represents a pointer-to-value_type.

typedef _Reference *reference*\\
This type represents a reference-to-value_type.

typedef _Tp *value_type*\\
The type 'pointed to' by the iterator.

* Detailed Description
** "template<typename _Category, typename _Tp, typename _Distance =
ptrdiff_t, typename _Pointer = _Tp*, typename _Reference = _Tp&>
\\
struct std::iterator< _Category, _Tp, _Distance, _Pointer, _Reference
>"Common iterator class.

This class does nothing but define nested typedefs. Iterator classes can
inherit from this class to save some work. The typedefs are then used in
specializations and overloading.

In particular, there are no default implementations of requirements such
as =operator++= and the like. (How could there be?)

Definition at line *127* of file *stl_iterator_base_types.h*.

* Member Typedef Documentation
** template<typename _Category , typename _Tp , typename _Distance =
ptrdiff_t, typename _Pointer = _Tp*, typename _Reference = _Tp&> typedef
_Distance *std::iterator*< _Category, _Tp, _Distance, _Pointer,
_Reference >::*difference_type*
Distance between iterators is represented as this type.

Definition at line *134* of file *stl_iterator_base_types.h*.

** template<typename _Category , typename _Tp , typename _Distance =
ptrdiff_t, typename _Pointer = _Tp*, typename _Reference = _Tp&> typedef
_Category *std::iterator*< _Category, _Tp, _Distance, _Pointer,
_Reference >::*iterator_category*
One of the *tag types*.

Definition at line *130* of file *stl_iterator_base_types.h*.

** template<typename _Category , typename _Tp , typename _Distance =
ptrdiff_t, typename _Pointer = _Tp*, typename _Reference = _Tp&> typedef
_Pointer *std::iterator*< _Category, _Tp, _Distance, _Pointer,
_Reference >::*pointer*
This type represents a pointer-to-value_type.

Definition at line *136* of file *stl_iterator_base_types.h*.

** template<typename _Category , typename _Tp , typename _Distance =
ptrdiff_t, typename _Pointer = _Tp*, typename _Reference = _Tp&> typedef
_Reference *std::iterator*< _Category, _Tp, _Distance, _Pointer,
_Reference >::*reference*
This type represents a reference-to-value_type.

Definition at line *138* of file *stl_iterator_base_types.h*.

** template<typename _Category , typename _Tp , typename _Distance =
ptrdiff_t, typename _Pointer = _Tp*, typename _Reference = _Tp&> typedef
_Tp *std::iterator*< _Category, _Tp, _Distance, _Pointer, _Reference
>::*value_type*
The type 'pointed to' by the iterator.

Definition at line *132* of file *stl_iterator_base_types.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
