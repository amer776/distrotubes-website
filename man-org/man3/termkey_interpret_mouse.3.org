#+TITLE: Manpages - termkey_interpret_mouse.3
#+DESCRIPTION: Linux manpage for termkey_interpret_mouse.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_interpret_mouse - interpret opaque mouse event data

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  TermKeyResult termkey_interpret_mouse(TermKey *tk, const TermKeyKey *key, 
   TermKeyMouseEvent *ev, int *button, int *line, int *col);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_interpret_mouse*() fills in variables in the passed pointers
according to the mouse event found in /key/. It should be called if
*termkey_getkey*(3) or similar have returned a key event with the type
of *TERMKEY_TYPE_MOUSE*.

Any pointer may instead be given as *NULL* to not return that value.

The /ev/ variable will take one of the following values:

- *TERMKEY_MOUSE_UNKNOWN* :: an unknown mouse event.

- *TERMKEY_MOUSE_PRESS* :: a mouse button was pressed; /button/ will
  contain its number.

- *TERMKEY_MOUSE_DRAG* :: the mouse was moved while holding a button;
  /button/ will contain its number.

- *TERMKEY_MOUSE_RELEASE* :: a mouse button was released, or the mouse
  was moved while no button was pressed. If known, /button/ will contain
  the number of the button released. Not all terminals can report this,
  so it may be 0 instead.

The /line/ and /col/ variables will be filled in with the mouse
position, indexed from 1. Note that due to the limited number of bytes
in the *TermKeyKey* structure, the line and column numbers are limited
to 2047 and 4095 respectively.

* RETURN VALUE
If passed a /key/ event of the type *TERMKEY_TYPE_MOUSE*, this function
will return *TERMKEY_RES_KEY* and will affect the variables whose
pointers were passed in, as described above.

For other event types it will return *TERMKEY_RES_NONE*, and its effects
on any variables whose pointers were passed in, are undefined.

* SEE ALSO
*termkey_waitkey*(3), *termkey_getkey*(3), *termkey*(7)
