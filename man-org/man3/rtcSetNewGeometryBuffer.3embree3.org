#+TITLE: Manpages - rtcSetNewGeometryBuffer.3embree3
#+DESCRIPTION: Linux manpage for rtcSetNewGeometryBuffer.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetNewGeometryBuffer - creates and assigns a new data buffer to
    the geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void* rtcSetNewGeometryBuffer(
    RTCGeometry geometry,
    enum RTCBufferType type,
    unsigned int slot,
    enum RTCFormat format,
    size_t byteStride,
    size_t itemCount
  );
#+end_example

** DESCRIPTION
The =rtcSetNewGeometryBuffer= function creates a new data buffer of
specified format (=format= argument), byte stride (=byteStride=
argument), and number of items (=itemCount= argument), and assigns it to
a geometry buffer slot (=type= and =slot= argument) of the specified
geometry (=geometry= argument). The buffer data is managed internally
and automatically freed when the geometry is destroyed.

The byte stride (=byteStride= argument) must be aligned to 4 bytes;
otherwise the =rtcSetNewGeometryBuffer= function will fail.

The allocated buffer will be automatically over-allocated slightly when
used as a vertex buffer, where a requirement is that each buffer element
should be readable using 16-byte SSE load instructions.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcSetGeometryBuffer], [rtcSetSharedGeometryBuffer]
