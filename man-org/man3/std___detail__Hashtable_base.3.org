#+TITLE: Manpages - std___detail__Hashtable_base.3
#+DESCRIPTION: Linux manpage for std___detail__Hashtable_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Hashtable_base< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherits *std::__detail::_Hash_code_base< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, _Traits::__hash_cached::value >*, and
*std::__detail::_Hashtable_ebo_helper< 0, _Equal >*.

Inherited by *std::_Hashtable< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >*.

** Public Types
using *__hash_cached* = typename __traits_type::__hash_cached\\

using *__hash_code* = typename __hash_code_base::__hash_code\\

using *__hash_code_base* = *_Hash_code_base*< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __hash_cached::value >\\

using *__traits_type* = _Traits\\

typedef std::ptrdiff_t *difference_type*\\

typedef _Hash *hasher*\\

typedef _Equal *key_equal*\\

typedef _Key *key_type*\\

typedef std::size_t *size_type*\\

typedef _Value *value_type*\\

** Public Member Functions
hasher *hash_function* () const\\

** Protected Member Functions
*_Hashtable_base* (const _Hash &__hash, const _Equal &__eq)\\

std::size_t *_M_bucket_index* (__hash_code __c, std::size_t __bkt_count)
const\\

std::size_t *_M_bucket_index* (const _Hash_node_value< _Value, false >
&__n, std::size_t __bkt_count) const noexcept(noexcept(declval< const
_Hash & >()(declval< const _Key & >())) &&noexcept(declval< const
_RangeHash & >()((__hash_code) 0,(std::size_t) 0)))\\

std::size_t *_M_bucket_index* (const _Hash_node_value< _Value, true >
&__n, std::size_t __bkt_count) const noexcept(noexcept(declval< const
_RangeHash & >()((__hash_code) 0,(std::size_t) 0)))\\

void *_M_copy_code* (*_Hash_node_code_cache*< false > &, const
*_Hash_node_code_cache*< false > &) const\\

void *_M_copy_code* (*_Hash_node_code_cache*< true > &__to, const
*_Hash_node_code_cache*< true > &__from) const\\

const _Equal & *_M_eq* () const\\

bool *_M_equals* (const _Key &__k, __hash_code __c, const
_Hash_node_value< _Value, __hash_cached::value > &__n) const\\

template<typename _Kt > bool *_M_equals_tr* (const _Kt &__k, __hash_code
__c, const _Hash_node_value< _Value, __hash_cached::value > &__n)
const\\

const _Hash & *_M_hash* () const\\

__hash_code *_M_hash_code* (const _Key &__k) const\\

__hash_code *_M_hash_code_tr* (const _Kt &__k) const\\

bool *_M_node_equals* (const _Hash_node_value< _Value,
__hash_cached::value > &__lhn, const _Hash_node_value< _Value,
__hash_cached::value > &__rhn) const\\

void *_M_store_code* (*_Hash_node_code_cache*< false > &, __hash_code)
const\\

void *_M_store_code* (*_Hash_node_code_cache*< true > &__n, __hash_code
__c) const\\

void *_M_swap* (*_Hash_code_base* &__x)\\

void *_M_swap* (*_Hashtable_base* &__x)\\

* Detailed Description
** "template<typename _Key, typename _Value, typename _ExtractKey,
typename _Equal, typename _Hash, typename _RangeHash, typename _Unused,
typename _Traits>
\\
struct std::__detail::_Hashtable_base< _Key, _Value, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _Traits >"Primary class template
_Hashtable_base.

Helper class adding management of _Equal functor to _Hash_code_base
type.

Base class templates are:

- __detail::_Hash_code_base

- __detail::_Hashtable_ebo_helper

Definition at line *1559* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > using *std::__detail::_Hashtable_base*<
_Key, _Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::__hash_cached = typename __traits_type::__hash_cached
Definition at line *1572* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > using *std::__detail::_Hashtable_base*<
_Key, _Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::__hash_code = typename __hash_code_base::__hash_code
Definition at line *1578* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > using *std::__detail::_Hashtable_base*<
_Key, _Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::*__hash_code_base* = *_Hash_code_base*<_Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __hash_cached::value>
Definition at line *1574* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > using *std::__detail::_Hashtable_base*<
_Key, _Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::__traits_type = _Traits
Definition at line *1571* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > typedef std::ptrdiff_t
*std::__detail::_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >::difference_type
Definition at line *1569* of file *hashtable_policy.h*.

** typedef _Hash *std::__detail::_Hash_code_base*< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache_hash_code
>::hasher= [inherited]=
Definition at line *1198* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > typedef _Equal
*std::__detail::_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >::key_equal
Definition at line *1567* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > typedef _Key
*std::__detail::_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >::key_type
Definition at line *1565* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > typedef std::size_t
*std::__detail::_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >::size_type
Definition at line *1568* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > typedef _Value
*std::__detail::_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >::value_type
Definition at line *1566* of file *hashtable_policy.h*.

* Constructor & Destructor Documentation
** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > *std::__detail::_Hashtable_base*< _Key,
_Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::*_Hashtable_base* (const _Hash & __hash, const _Equal &
__eq)= [inline]=, = [protected]=
Definition at line *1603* of file *hashtable_policy.h*.

* Member Function Documentation
** std::size_t *std::__detail::_Hash_code_base*< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache_hash_code
>::_M_bucket_index (__hash_code __c, std::size_t __bkt_count)
const= [inline]=, = [protected]=, = [inherited]=
Definition at line *1230* of file *hashtable_policy.h*.

** std::size_t *std::__detail::_Hash_code_base*< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache_hash_code
>::_M_bucket_index (const _Hash_node_value< _Value, false > & __n,
std::size_t __bkt_count) const= [inline]=, = [protected]=,
= [noexcept]=, = [inherited]=
Definition at line *1234* of file *hashtable_policy.h*.

** std::size_t *std::__detail::_Hash_code_base*< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache_hash_code
>::_M_bucket_index (const _Hash_node_value< _Value, true > & __n,
std::size_t __bkt_count) const= [inline]=, = [protected]=,
= [noexcept]=, = [inherited]=
Definition at line *1245* of file *hashtable_policy.h*.

** void *std::__detail::_Hash_code_base*< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __cache_hash_code >::_M_copy_code
(*_Hash_node_code_cache*< false > &, const *_Hash_node_code_cache*<
false > &) const= [inline]=, = [protected]=, = [inherited]=
Definition at line *1256* of file *hashtable_policy.h*.

** void *std::__detail::_Hash_code_base*< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __cache_hash_code >::_M_copy_code
(*_Hash_node_code_cache*< true > & __to, const *_Hash_node_code_cache*<
true > & __from) const= [inline]=, = [protected]=, = [inherited]=
Definition at line *1265* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > const _Equal &
*std::__detail::_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >::_M_eq () const= [inline]=,
= [protected]=
Definition at line *1647* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > bool *std::__detail::_Hashtable_base*<
_Key, _Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::_M_equals (const _Key & __k, __hash_code __c, const _Hash_node_value<
_Value, __hash_cached::value > & __n) const= [inline]=, = [protected]=
Definition at line *1608* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > template<typename _Kt > bool
*std::__detail::_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _Traits >::_M_equals_tr (const _Kt & __k,
__hash_code __c, const _Hash_node_value< _Value, __hash_cached::value >
& __n) const= [inline]=, = [protected]=
Definition at line *1619* of file *hashtable_policy.h*.

** const _Hash & *std::__detail::_Hash_code_base*< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache_hash_code >::_M_hash ()
const= [inline]=, = [protected]=, = [inherited]=
Definition at line *1274* of file *hashtable_policy.h*.

** __hash_code *std::__detail::_Hash_code_base*< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache_hash_code
>::_M_hash_code (const _Key & __k) const= [inline]=, = [protected]=,
= [inherited]=
Definition at line *1213* of file *hashtable_policy.h*.

** __hash_code *std::__detail::_Hash_code_base*< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache_hash_code
>::_M_hash_code_tr (const _Kt & __k) const= [inline]=, = [protected]=,
= [inherited]=
Definition at line *1222* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > bool *std::__detail::_Hashtable_base*<
_Key, _Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::_M_node_equals (const _Hash_node_value< _Value, __hash_cached::value
> & __lhn, const _Hash_node_value< _Value, __hash_cached::value > &
__rhn) const= [inline]=, = [protected]=
Definition at line *1631* of file *hashtable_policy.h*.

** void *std::__detail::_Hash_code_base*< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __cache_hash_code >::_M_store_code
(*_Hash_node_code_cache*< false > &, __hash_code) const= [inline]=,
= [protected]=, = [inherited]=
Definition at line *1252* of file *hashtable_policy.h*.

** void *std::__detail::_Hash_code_base*< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __cache_hash_code >::_M_store_code
(*_Hash_node_code_cache*< true > & __n, __hash_code __c)
const= [inline]=, = [protected]=, = [inherited]=
Definition at line *1261* of file *hashtable_policy.h*.

** void *std::__detail::_Hash_code_base*< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __cache_hash_code >::_M_swap
(*_Hash_code_base*< _Key, _Value, _ExtractKey, _Hash, _RangeHash,
_Unused, _Traits::__hash_cached::value > & __x)= [inline]=,
= [protected]=, = [inherited]=
Definition at line *1270* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Equal , typename _Hash , typename _RangeHash , typename
_Unused , typename _Traits > void *std::__detail::_Hashtable_base*<
_Key, _Value, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _Traits
>::_M_swap (*_Hashtable_base*< _Key, _Value, _ExtractKey, _Equal, _Hash,
_RangeHash, _Unused, _Traits > & __x)= [inline]=, = [protected]=
Definition at line *1640* of file *hashtable_policy.h*.

** hasher *std::__detail::_Hash_code_base*< _Key, _Value, _ExtractKey,
_Hash, _RangeHash, _Unused, __cache_hash_code >::hash_function ()
const= [inline]=, = [inherited]=
Definition at line *1201* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
