#+TITLE: Manpages - XRRFreeScreenConfigInfo.3
#+DESCRIPTION: Linux manpage for XRRFreeScreenConfigInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XRRFreeScreenConfigInfo.3 is found in manpage for: [[../man3/Xrandr.3][man3/Xrandr.3]]