#+TITLE: Manpages - setrpcent.3
#+DESCRIPTION: Linux manpage for setrpcent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setrpcent.3 is found in manpage for: [[../man3/getrpcent.3][man3/getrpcent.3]]