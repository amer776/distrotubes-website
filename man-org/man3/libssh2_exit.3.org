#+TITLE: Manpages - libssh2_exit.3
#+DESCRIPTION: Linux manpage for libssh2_exit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_exit - global library deinitialization

* SYNOPSIS
#include <libssh2.h>

void libssh2_exit(void);

* DESCRIPTION
Exit the libssh2 functions and free's all memory used internal.

* AVAILABILITY
Added in libssh2 1.2.5

* SEE ALSO
*libssh2_init(3)*
