#+TITLE: Manpages - __gnu_pbds_detail_select_value_type.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_select_value_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::select_value_type< Key, Mapped > - Choose value_type
to be a key/value pair or just a key.

* SYNOPSIS
\\

=#include <types_traits.hpp>=

** Public Types
typedef *std::pair*< const Key, Mapped > *type*\\

* Detailed Description
** "template<typename Key, typename Mapped>
\\
struct __gnu_pbds::detail::select_value_type< Key, Mapped >"Choose
value_type to be a key/value pair or just a key.

Definition at line *107* of file *types_traits.hpp*.

* Member Typedef Documentation
** template<typename Key , typename Mapped > typedef *std::pair*<const
Key, Mapped> *__gnu_pbds::detail::select_value_type*< Key, Mapped
>::*type*
Definition at line *109* of file *types_traits.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
