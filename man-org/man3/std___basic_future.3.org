#+TITLE: Manpages - std___basic_future.3
#+DESCRIPTION: Linux manpage for std___basic_future.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__basic_future< _Res > - Common implementation for future and
shared_future.

* SYNOPSIS
\\

Inherits *std::__future_base*.

Inherited by *std::future< _Res >*, and *std::shared_future< _Res >*.

** Public Types
template<typename _Res > using *_Ptr* = *unique_ptr*< _Res,
_Result_base::_Deleter >\\
A unique_ptr for result objects.

using *_State_base* = _State_baseV2\\

** Public Member Functions
*__basic_future* (const *__basic_future* &)=delete\\

*__basic_future* & *operator=* (const *__basic_future* &)=delete\\

bool *valid* () const noexcept\\

void *wait* () const\\

template<typename _Rep , typename _Period > *future_status* *wait_for*
(const *chrono::duration*< _Rep, _Period > &__rel) const\\

template<typename _Clock , typename _Duration > *future_status*
*wait_until* (const *chrono::time_point*< _Clock, _Duration > &__abs)
const\\

** Static Public Member Functions
template<typename _Res , typename _Allocator > static *_Ptr*<
*_Result_alloc*< _Res, _Allocator > > *_S_allocate_result* (const
_Allocator &__a)\\

template<typename _Res , typename _Tp > static *_Ptr*< *_Result*< _Res >
> *_S_allocate_result* (const *std::allocator*< _Tp > &__a)\\

template<typename _Res_ptr , typename _BoundFn > static _Task_setter<
_Res_ptr, _BoundFn > *_S_task_setter* (_Res_ptr &__ptr, _BoundFn
&__call)\\

** Protected Types
typedef *__future_base::_Result*< _Res > & *__result_type*\\

typedef *shared_ptr*< _State_base > *__state_type*\\

** Protected Member Functions
*__basic_future* (const *__state_type* &__state)\\

*__basic_future* (const *shared_future*< _Res > &) noexcept\\

*__basic_future* (*future*< _Res > &&) noexcept\\

*__basic_future* (*shared_future*< _Res > &&) noexcept\\

*__result_type* *_M_get_result* () const\\
Wait for the state to be ready and rethrow any stored exception.

void *_M_swap* (*__basic_future* &__that) noexcept\\

* Detailed Description
** "template<typename _Res>
\\
class std::__basic_future< _Res >"Common implementation for future and
shared_future.

Definition at line *673* of file *future*.

* Member Typedef Documentation
** template<typename _Res > typedef *__future_base::_Result*<_Res>&
*std::__basic_future*< _Res >::*__result_type*= [protected]=
Definition at line *677* of file *future*.

** template<typename _Res > typedef *shared_ptr*<_State_base>
*std::__basic_future*< _Res >::*__state_type*= [protected]=
Definition at line *676* of file *future*.

** template<typename _Res > using *std::__future_base::_Ptr* =
*unique_ptr*<_Res, _Result_base::_Deleter>= [inherited]=
A unique_ptr for result objects.

Definition at line *222* of file *future*.

** using std::__future_base::_State_base = _State_baseV2= [inherited]=
Definition at line *597* of file *future*.

* Constructor & Destructor Documentation
** template<typename _Res > *std::__basic_future*< _Res
>::*__basic_future* (const *__state_type* & __state)= [inline]=,
= [explicit]=, = [protected]=
Definition at line *732* of file *future*.

** template<typename _Res > constexpr *std::__basic_future*< _Res
>::*__basic_future* ()= [inline]=, = [constexpr]=, = [protected]=,
= [noexcept]=
Definition at line *750* of file *future*.

* Member Function Documentation
** template<typename _Res > *__result_type* *std::__basic_future*< _Res
>::_M_get_result () const= [inline]=, = [protected]=
Wait for the state to be ready and rethrow any stored exception.

Definition at line *716* of file *future*.

** template<typename _Res > void *std::__basic_future*< _Res >::_M_swap
(*__basic_future*< _Res > & __that)= [inline]=, = [protected]=,
= [noexcept]=
Definition at line *725* of file *future*.

** template<typename _Res , typename _Allocator > static *_Ptr*<
*_Result_alloc*< _Res, _Allocator > >
std::__future_base::_S_allocate_result (const _Allocator &
__a)= [inline]=, = [static]=, = [inherited]=
Definition at line *287* of file *future*.

** template<typename _Res , typename _Tp > static *_Ptr*< *_Result*<
_Res > > std::__future_base::_S_allocate_result (const *std::allocator*<
_Tp > & __a)= [inline]=, = [static]=, = [inherited]=
Definition at line *300* of file *future*.

** template<typename _Res_ptr , typename _BoundFn > static _Task_setter<
_Res_ptr, _BoundFn > std::__future_base::_S_task_setter (_Res_ptr &
__ptr, _BoundFn & __call)= [inline]=, = [static]=, = [inherited]=
Definition at line *621* of file *future*.

** template<typename _Res > bool *std::__basic_future*< _Res >::valid ()
const= [inline]=, = [noexcept]=
Definition at line *688* of file *future*.

** template<typename _Res > void *std::__basic_future*< _Res >::wait ()
const= [inline]=
Definition at line *691* of file *future*.

** template<typename _Res > template<typename _Rep , typename _Period >
*future_status* *std::__basic_future*< _Res >::wait_for (const
*chrono::duration*< _Rep, _Period > & __rel) const= [inline]=
Definition at line *699* of file *future*.

** template<typename _Res > template<typename _Clock , typename
_Duration > *future_status* *std::__basic_future*< _Res >::wait_until
(const *chrono::time_point*< _Clock, _Duration > & __abs)
const= [inline]=
Definition at line *707* of file *future*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
