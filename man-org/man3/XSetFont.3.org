#+TITLE: Manpages - XSetFont.3
#+DESCRIPTION: Linux manpage for XSetFont.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XSetFont - GC convenience routines

* SYNTAX
int XSetFont ( Display */display/ , GC /gc/ , Font /font/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

- font :: Specifies the font.

- gc :: Specifies the GC.

* DESCRIPTION
The *XSetFont* function sets the current font in the specified GC.

*XSetFont* can generate *BadAlloc*, *BadFont*, and *BadGC* errors.

* DIAGNOSTICS
- *BadAlloc* :: The server failed to allocate the requested resource or
  server memory.

- *BadFont* :: A value for a Font or GContext argument does not name a
  defined Font.

- *BadGC* :: A value for a GContext argument does not name a defined
  GContext.

* SEE ALSO
XCreateGC(3), XQueryBestSize(3), XSetArcMode(3), XSetClipOrigin(3),
XSetFillStyle(3), XSetLineAttributes(3), XSetState(3), XSetTile(3)\\
/Xlib - C Language X Interface/
