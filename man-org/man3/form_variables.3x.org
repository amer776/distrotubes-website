#+TITLE: Manpages - form_variables.3x
#+DESCRIPTION: Linux manpage for form_variables.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*TYPE_ALNUM*, *TYPE_ALPHA*, *TYPE_ENUM*, *TYPE_INTEGER*, *TYPE_IPV4*,
*TYPE_NUMERIC*, *TYPE_REGEXP* - form system global variables

* SYNOPSIS
#+begin_example
  #include <form.h>

  FIELDTYPE * TYPE_ALNUM;
  FIELDTYPE * TYPE_ALPHA;
  FIELDTYPE * TYPE_ENUM;
  FIELDTYPE * TYPE_INTEGER;
  FIELDTYPE * TYPE_IPV4;
  FIELDTYPE * TYPE_NUMERIC;
  FIELDTYPE * TYPE_REGEXP;
#+end_example

* DESCRIPTION
These are building blocks for the form library, defining fields that can
be created using the *form_fieldtype*(3X) functions. Each provides
functions for field- and character-validation, according to the given
datatype.

** TYPE_ALNUM
This holds alphanumeric data.

** TYPE_ALPHA
This holds alphabetic data.

** TYPE_ENUM
This holds an enumerated type.

** TYPE_INTEGER
This holds a decimal integer.

** TYPE_IPV4
This holds an IPv4 internet address, e.g., "127.0.0.1".

** TYPE_NUMERIC
This holds a decimal number, with optional sign and decimal point.

** TYPE_REGEXP
This holds a regular expression.

* PORTABILITY
The *TYPE_IPV4* variable is an extension not provided by older
implementations of the form library.

* SEE ALSO
*form*(3X).
