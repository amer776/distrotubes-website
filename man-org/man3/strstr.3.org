#+TITLE: Manpages - strstr.3
#+DESCRIPTION: Linux manpage for strstr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
strstr, strcasestr - locate a substring

* SYNOPSIS
#+begin_example
  #include <string.h>

  char *strstr(const char *haystack, const char *needle);

  #define _GNU_SOURCE /* See feature_test_macros(7) */
  #include <string.h>

  char *strcasestr(const char *haystack, const char *needle);
#+end_example

* DESCRIPTION
The *strstr*() function finds the first occurrence of the substring
/needle/ in the string /haystack/. The terminating null bytes ('\0') are
not compared.

The *strcasestr*() function is like *strstr*(), but ignores the case of
both arguments.

* RETURN VALUE
These functions return a pointer to the beginning of the located
substring, or NULL if the substring is not found.

If /needle/ is the empty string, the return value is always /haystack/
itself.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface      | Attribute     | Value          |
| *strstr*()     | Thread safety | MT-Safe        |
| *strcasestr*() | Thread safety | MT-Safe locale |

* CONFORMING TO
*strstr*(): POSIX.1-2001, POSIX.1-2008, C89, C99.

The *strcasestr*() function is a nonstandard extension.

* SEE ALSO
*index*(3), *memchr*(3), *memmem*(3), *rindex*(3), *strcasecmp*(3),
*strchr*(3), *string*(3), *strpbrk*(3), *strsep*(3), *strspn*(3),
*strtok*(3), *wcsstr*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
