#+TITLE: Manpages - Xpresent.3
#+DESCRIPTION: Linux manpage for Xpresent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
Xpresent - X Present Extension

* SYNTAX
#include <X11/extensions/Xpresent.h>

#+begin_example

  Bool XPresentQueryExtension  ( Display *dpy,
  	int *event_base_return, int *error_base_return );

  Status XPresentQueryVersion  ( Display *dpy,
  	int *major_version_return,
  	int *minor_version_return );

  void XPresentRegion  ( Display *dpy,
  	Window window,
  	Pixmap pixmap,
  	uint32_t serial,
  	XserverRegion valid,
  	XserverRegion update,
  	int x_off,
  	int y_off,
  	XID idle_fence,
  	XID target_crtc,
  	uint64_t target_msc,
  	uint64_t divisor,
  	uint64_t remainder);
#+end_example

* ARGUMENTS
- display :: Specifies the connection to the X server.

- window :: Specifies which window.

* DESCRIPTION
*Xpresent* is a library designed to interface the X Present Extension.

* RESTRICTIONS
*Xpresent* will remain upward compatible after the 1.0 release.

* AUTHORS
Keith Packard, Intel
