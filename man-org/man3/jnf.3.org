#+TITLE: Manpages - jnf.3
#+DESCRIPTION: Linux manpage for jnf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about jnf.3 is found in manpage for: [[../man3/j0.3][man3/j0.3]]