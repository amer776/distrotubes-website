#+TITLE: Manpages - timeradd.3bsd
#+DESCRIPTION: Linux manpage for timeradd.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

These macros are provided for manipulating the

and

structures described in

The

and

macros add the time information stored in

to

storing the result in

With

the results are simplified such that the value of

is always less than 1,000,000 (1 second). With

the

member of

is always less than 1,000,000,000.

The

and

macros subtract the time information stored in

from

and store the resulting structure in

The

and

macros initialize the structures to midnight (0 hour) January 1st, 1970
(the Epoch). In other words, they set the members of the structure to
zero.

The

and

macros return true if the input structure is set to any time value other
than the Epoch.

The

and

macros compare

to

using the comparison operator given in

The result of the comparison is returned.

The

family of macros first appeared in

These were later ported to

The

family of macros first appeared in
