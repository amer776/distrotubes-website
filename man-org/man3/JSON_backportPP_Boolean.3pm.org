#+TITLE: Manpages - JSON_backportPP_Boolean.3pm
#+DESCRIPTION: Linux manpage for JSON_backportPP_Boolean.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
JSON::PP::Boolean - dummy module providing JSON::PP::Boolean

* SYNOPSIS
# do not "use" yourself

* DESCRIPTION
This module exists only to provide overload resolution for Storable and
similar modules. See JSON::PP for more info about this class.

* AUTHOR
This idea is from JSON::XS::Boolean written by Marc Lehmann
<schmorp[at]schmorp.de>

* LICENSE
This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
