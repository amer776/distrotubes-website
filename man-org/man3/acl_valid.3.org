#+TITLE: Manpages - acl_valid.3
#+DESCRIPTION: Linux manpage for acl_valid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function checks the ACL referred to by the argument

for validity.

The three required entries ACL_USER_OBJ, ACL_GROUP_OBJ, and ACL_OTHER
must exist exactly once in the ACL. If the ACL contains any ACL_USER or
ACL_GROUP entries, then an ACL_MASK entry is also required. The ACL may
contain at most one ACL_MASK entry.

The user identifiers must be unique among all entries of type ACL_USER.
The group identifiers must be unique among all entries of type
ACL_GROUP.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The argument

does not point to a valid ACL.

One or more of the required ACL entries is not present in

The ACL contains entries that are not unique.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
