#+TITLE: Manpages - SDL_FreeWAV.3
#+DESCRIPTION: Linux manpage for SDL_FreeWAV.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_FreeWAV - Frees previously opened WAV data

* SYNOPSIS
*#include "SDL.h"*

*void SDL_FreeWAV*(*Uint8 *audio_buf*);

* DESCRIPTION
After a WAVE file has been opened with *SDL_LoadWAV* its data can
eventually be freed with *SDL_FreeWAV*. *audio_buf* is a pointer to the
buffer created by *SDL_LoadWAV*.

* SEE ALSO
*SDL_LoadWAV*
