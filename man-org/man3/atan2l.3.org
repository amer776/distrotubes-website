#+TITLE: Manpages - atan2l.3
#+DESCRIPTION: Linux manpage for atan2l.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about atan2l.3 is found in manpage for: [[../man3/atan2.3][man3/atan2.3]]