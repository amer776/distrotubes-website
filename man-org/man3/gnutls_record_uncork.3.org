#+TITLE: Manpages - gnutls_record_uncork.3
#+DESCRIPTION: Linux manpage for gnutls_record_uncork.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_record_uncork - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_record_uncork(gnutls_session_t */session/*, unsigned int
*/flags/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- unsigned int flags :: Could be zero or *GNUTLS_RECORD_WAIT*

* DESCRIPTION
This resets the effect of *gnutls_record_cork()*, and flushes any
pending data. If the *GNUTLS_RECORD_WAIT* flag is specified then this
function will block until the data is sent or a fatal error occurs
(i.e., the function will retry on *GNUTLS_E_AGAIN* and
*GNUTLS_E_INTERRUPTED*).

If the flag *GNUTLS_RECORD_WAIT* is not specified and the function is
interrupted then the *GNUTLS_E_AGAIN* or *GNUTLS_E_INTERRUPTED* errors
will be returned. To obtain the data left in the corked buffer use
*gnutls_record_check_corked()*.

* RETURNS
On success the number of transmitted data is returned, or otherwise a
negative error code.

* SINCE
3.1.9

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
