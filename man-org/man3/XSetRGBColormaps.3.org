#+TITLE: Manpages - XSetRGBColormaps.3
#+DESCRIPTION: Linux manpage for XSetRGBColormaps.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetRGBColormaps.3 is found in manpage for: [[../man3/XAllocStandardColormap.3][man3/XAllocStandardColormap.3]]