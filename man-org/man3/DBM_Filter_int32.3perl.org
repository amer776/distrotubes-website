#+TITLE: Manpages - DBM_Filter_int32.3perl
#+DESCRIPTION: Linux manpage for DBM_Filter_int32.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBM_Filter::int32 - filter for DBM_Filter

* SYNOPSIS
use SDBM_File; # or DB_File, GDBM_File, NDBM_File, or ODBM_File use
DBM_Filter ; $db = tie %hash, ... $db->Filter_Push(int32);

* DESCRIPTION
This DBM filter is used when interoperating with a C/C++ application
that uses a C int as either the key and/or value in the DBM file.

* SEE ALSO
DBM_Filter, perldbmfilter

* AUTHOR
Paul Marquess pmqs@cpan.org
