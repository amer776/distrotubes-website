#+TITLE: Manpages - SDL_SetGammaRamp.3
#+DESCRIPTION: Linux manpage for SDL_SetGammaRamp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SetGammaRamp - Sets the color gamma lookup tables for the display

* SYNOPSIS
*#include "SDL.h"*

*int SDL_SetGammaRamp*(*Uint16 *redtable, Uint16 *greentable, Uint16
*bluetable*);

* DESCRIPTION
Sets the gamma lookup tables for the display for each color component.
Each table is an array of 256 Uint16 values, representing a mapping
between the input and output for that channel. The input is the index
into the array, and the output is the 16-bit gamma value at that index,
scaled to the output color precision. You may pass NULL to any of the
channels to leave them unchanged.

This function adjusts the gamma based on lookup tables, you can also
have the gamma calculated based on a "gamma function" parameter with
/SDL_SetGamma/.

Not all display hardware is able to change gamma.

* RETURN VALUE
Returns -1 on error (or if gamma adjustment is not supported).

* SEE ALSO
/SDL_SetGamma/ /SDL_GetGammaRamp/
