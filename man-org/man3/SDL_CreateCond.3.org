#+TITLE: Manpages - SDL_CreateCond.3
#+DESCRIPTION: Linux manpage for SDL_CreateCond.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CreateCond - Create a condition variable

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*SDL_cond *SDL_CreateCond*(*void*);

* DESCRIPTION
Creates a condition variable.

* EXAMPLES
#+begin_example
  SDL_cond *cond;

  cond=SDL_CreateCond();
  .
  .
  /* Do stuff */

  .
  .
  SDL_DestroyCond(cond);
#+end_example

* SEE ALSO
*SDL_DestroyCond*, *SDL_CondWait*, *SDL_CondSignal*
