#+TITLE: Manpages - SDL_Palette.3
#+DESCRIPTION: Linux manpage for SDL_Palette.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_Palette - Color palette for 8-bit pixel formats

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    int ncolors;
    SDL_Color *colors;
  } SDL_Palette;
#+end_example

* STRUCTURE DATA
- *ncolors* :: Number of colors used in this palette

- *colors* :: Pointer to *SDL_Color* structures that make up the
  palette.

* DESCRIPTION
Each pixel in an 8-bit surface is an index into the *colors* field of
the *SDL_Palette* structure store in *SDL_PixelFormat*. A *SDL_Palette*
should never need to be created manually. It is automatically created
when SDL allocates a *SDL_PixelFormat* for a surface. The colors values
of a *SDL_Surface*s palette can be set with the *SDL_SetColors*.

* SEE ALSO
*SDL_Color*, *SDL_Surface*, *SDL_SetColors* *SDL_SetPalette*
