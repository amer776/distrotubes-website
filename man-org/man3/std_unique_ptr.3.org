#+TITLE: Manpages - std_unique_ptr.3
#+DESCRIPTION: Linux manpage for std_unique_ptr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::unique_ptr< _Tp, _Dp > - 20.7.1.2 unique_ptr for single objects.

* SYNOPSIS
\\

=#include <unique_ptr.h>=

** Public Types
using *deleter_type* = _Dp\\

using *element_type* = _Tp\\

using *pointer* = typename __uniq_ptr_impl< _Tp, _Dp >::pointer\\

** Public Member Functions
template<typename _Del = _Dp, typename = _DeleterConstraint<_Del>>
constexpr *unique_ptr* () noexcept\\
Default constructor, creates a unique_ptr that owns nothing.

template<typename _Up , typename > *unique_ptr* (*auto_ptr*< _Up >
&&__u) noexcept\\

*unique_ptr* (const *unique_ptr* &)=delete\\

template<typename _Del = _Dp, typename = _DeleterConstraint<_Del>>
constexpr *unique_ptr* (nullptr_t) noexcept\\
Creates a unique_ptr that owns nothing.

template<typename _Del = _Dp, typename = _DeleterConstraint<_Del>>
*unique_ptr* (pointer __p) noexcept\\

template<typename _Del = deleter_type, typename =
_Require<is_move_constructible<_Del>>> *unique_ptr* (pointer __p,
__enable_if_t<!*is_lvalue_reference*< _Del >::value, _Del && > __d)
noexcept\\

template<typename _Del = deleter_type, typename =
_Require<is_copy_constructible<_Del>>> *unique_ptr* (pointer __p, const
deleter_type &__d) noexcept\\

template<typename _Del = deleter_type, typename _DelUnref = typename
remove_reference<_Del>::type> *unique_ptr* (pointer, __enable_if_t<
*is_lvalue_reference*< _Del >::value, _DelUnref && >)=delete\\

*unique_ptr* (*unique_ptr* &&)=default\\
Move constructor.

template<typename _Up , typename _Ep , typename = _Require<
__safe_conversion_up<_Up, _Ep>, typename
conditional<is_reference<_Dp>::value, is_same<_Ep, _Dp>,
is_convertible<_Ep, _Dp>>::type>> *unique_ptr* (*unique_ptr*< _Up, _Ep >
&&__u) noexcept\\
Converting constructor from another type.

*~unique_ptr* () noexcept\\
Destructor, invokes the deleter if the stored pointer is not null.

pointer *get* () const noexcept\\
Return the stored pointer.

const deleter_type & *get_deleter* () const noexcept\\
Return a reference to the stored deleter.

deleter_type & *get_deleter* () noexcept\\
Return a reference to the stored deleter.

*operator bool* () const noexcept\\
Return =true= if the stored pointer is not null.

*add_lvalue_reference*< element_type >::type *operator** () const\\
Dereference the stored pointer.

pointer *operator->* () const noexcept\\
Return the stored pointer.

*unique_ptr* & *operator=* (const *unique_ptr* &)=delete\\

*unique_ptr* & *operator=* (nullptr_t) noexcept\\
Reset the unique_ptr to empty, invoking the deleter if necessary.

*unique_ptr* & *operator=* (*unique_ptr* &&)=default\\
Move assignment operator.

template<typename _Up , typename _Ep > *enable_if*< __and_<
__safe_conversion_up< _Up, _Ep >, *is_assignable*< deleter_type &, _Ep
&& > >::value, *unique_ptr* & >::type *operator=* (*unique_ptr*< _Up,
_Ep > &&__u) noexcept\\
Assignment from another type.

pointer *release* () noexcept\\
Release ownership of any stored pointer.

void *reset* (pointer __p=pointer()) noexcept\\
Replace the stored pointer.

void *swap* (*unique_ptr* &__u) noexcept\\
Exchange the pointer and deleter with another object.

** Related Functions
(Note that these are not member functions.)

\\

template<typename _Tp , typename _Dp > *enable_if*< __is_swappable< _Dp
>::value >::type *swap* (*unique_ptr*< _Tp, _Dp > &__x, *unique_ptr*<
_Tp, _Dp > &__y) noexcept\\

template<typename _Tp , typename _Dp , typename _Up , typename _Ep >
bool *operator==* (const *unique_ptr*< _Tp, _Dp > &__x, const
*unique_ptr*< _Up, _Ep > &__y)\\
Equality operator for unique_ptr objects, compares the owned pointers.

template<typename _Tp , typename _Dp > bool *operator==* (const
*unique_ptr*< _Tp, _Dp > &__x, nullptr_t) noexcept\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp > bool *operator==* (nullptr_t,
const *unique_ptr*< _Tp, _Dp > &__x) noexcept\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp , typename _Up , typename _Ep >
bool *operator!=* (const *unique_ptr*< _Tp, _Dp > &__x, const
*unique_ptr*< _Up, _Ep > &__y)\\
Inequality operator for unique_ptr objects, compares the owned pointers.

template<typename _Tp , typename _Dp > bool *operator!=* (const
*unique_ptr*< _Tp, _Dp > &__x, nullptr_t) noexcept\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp > bool *operator!=* (nullptr_t,
const *unique_ptr*< _Tp, _Dp > &__x) noexcept\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp , typename _Up , typename _Ep >
bool *operator<* (const *unique_ptr*< _Tp, _Dp > &__x, const
*unique_ptr*< _Up, _Ep > &__y)\\
Relational operator for unique_ptr objects, compares the owned pointers.

template<typename _Tp , typename _Dp > bool *operator<* (const
*unique_ptr*< _Tp, _Dp > &__x, nullptr_t)\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp > bool *operator<* (nullptr_t,
const *unique_ptr*< _Tp, _Dp > &__x)\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp , typename _Up , typename _Ep >
bool *operator<=* (const *unique_ptr*< _Tp, _Dp > &__x, const
*unique_ptr*< _Up, _Ep > &__y)\\
Relational operator for unique_ptr objects, compares the owned pointers.

template<typename _Tp , typename _Dp > bool *operator<=* (const
*unique_ptr*< _Tp, _Dp > &__x, nullptr_t)\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp > bool *operator<=* (nullptr_t,
const *unique_ptr*< _Tp, _Dp > &__x)\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp , typename _Up , typename _Ep >
bool *operator>* (const *unique_ptr*< _Tp, _Dp > &__x, const
*unique_ptr*< _Up, _Ep > &__y)\\
Relational operator for unique_ptr objects, compares the owned pointers.

template<typename _Tp , typename _Dp > bool *operator>* (const
*unique_ptr*< _Tp, _Dp > &__x, nullptr_t)\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp > bool *operator>* (nullptr_t,
const *unique_ptr*< _Tp, _Dp > &__x)\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp , typename _Up , typename _Ep >
bool *operator>=* (const *unique_ptr*< _Tp, _Dp > &__x, const
*unique_ptr*< _Up, _Ep > &__y)\\
Relational operator for unique_ptr objects, compares the owned pointers.

template<typename _Tp , typename _Dp > bool *operator>=* (const
*unique_ptr*< _Tp, _Dp > &__x, nullptr_t)\\
unique_ptr comparison with nullptr

template<typename _Tp , typename _Dp > bool *operator>=* (nullptr_t,
const *unique_ptr*< _Tp, _Dp > &__x)\\
unique_ptr comparison with nullptr

\\

template<typename _Tp , typename... _Args> _MakeUniq< _Tp
>::__single_object *make_unique* (_Args &&... __args)\\
std::make_unique for single objects

template<typename _Tp > _MakeUniq< _Tp >::__array *make_unique* (size_t
__num)\\
std::make_unique for arrays of unknown bound

template<typename _Tp , typename... _Args> _MakeUniq< _Tp
>::__invalid_type *make_unique* (_Args &&...)=delete\\
Disable std::make_unique for arrays of known bound.

* Detailed Description
** "template<typename _Tp, typename _Dp = default_delete<_Tp>>
\\
class std::unique_ptr< _Tp, _Dp >"20.7.1.2 unique_ptr for single
objects.

Definition at line *242* of file *unique_ptr.h*.

* Member Typedef Documentation
** template<typename _Tp , typename _Dp = default_delete<_Tp>> using
*std::unique_ptr*< _Tp, _Dp >::deleter_type = _Dp
Definition at line *253* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>> using
*std::unique_ptr*< _Tp, _Dp >::element_type = _Tp
Definition at line *252* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>> using
*std::unique_ptr*< _Tp, _Dp >::pointer = typename __uniq_ptr_impl<_Tp,
_Dp>::pointer
Definition at line *251* of file *unique_ptr.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Del = _Dp, typename = _DeleterConstraint<_Del>>
constexpr *std::unique_ptr*< _Tp, _Dp >::*unique_ptr* ()= [inline]=,
= [constexpr]=, = [noexcept]=
Default constructor, creates a unique_ptr that owns nothing.

Definition at line *269* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Del = _Dp, typename = _DeleterConstraint<_Del>>
*std::unique_ptr*< _Tp, _Dp >::*unique_ptr* (pointer __p)= [inline]=,
= [explicit]=, = [noexcept]=
Takes ownership of a pointer.

*Parameters*

#+begin_quote
  /__p/ A pointer to an object of =element_type=
#+end_quote

The deleter will be value-initialized.

Definition at line *281* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Del = deleter_type, typename =
_Require<is_copy_constructible<_Del>>> *std::unique_ptr*< _Tp, _Dp
>::*unique_ptr* (pointer __p, const deleter_type & __d)= [inline]=,
= [noexcept]=
Takes ownership of a pointer.

*Parameters*

#+begin_quote
  /__p/ A pointer to an object of =element_type=\\
  /__d/ A reference to a deleter.
#+end_quote

The deleter will be initialized with =__d=

Definition at line *294* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Del = deleter_type, typename =
_Require<is_move_constructible<_Del>>> *std::unique_ptr*< _Tp, _Dp
>::*unique_ptr* (pointer __p, __enable_if_t<!*is_lvalue_reference*< _Del
>::value, _Del && > __d)= [inline]=, = [noexcept]=
Takes ownership of a pointer.

*Parameters*

#+begin_quote
  /__p/ A pointer to an object of =element_type=\\
  /__d/ An rvalue reference to a (non-reference) deleter.
#+end_quote

The deleter will be initialized with =std::move(__d)=

Definition at line *306* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Del = _Dp, typename = _DeleterConstraint<_Del>>
constexpr *std::unique_ptr*< _Tp, _Dp >::*unique_ptr*
(nullptr_t)= [inline]=, = [constexpr]=, = [noexcept]=
Creates a unique_ptr that owns nothing.

Definition at line *320* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
*std::unique_ptr*< _Tp, _Dp >::*unique_ptr* (*unique_ptr*< _Tp, _Dp >
&&)= [default]=
Move constructor.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Up , typename _Ep , typename = _Require<
__safe_conversion_up<_Up, _Ep>, typename
conditional<is_reference<_Dp>::value, is_same<_Ep, _Dp>,
is_convertible<_Ep, _Dp>>::type>> *std::unique_ptr*< _Tp, _Dp
>::*unique_ptr* (*unique_ptr*< _Up, _Ep > && __u)= [inline]=,
= [noexcept]=
Converting constructor from another type. Requires that the pointer
owned by =__u= is convertible to the type of pointer owned by this
object, =__u= does not own an array, and =__u= has a compatible deleter
type.

Definition at line *340* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
*std::unique_ptr*< _Tp, _Dp >::~*unique_ptr* ()= [inline]=,
= [noexcept]=
Destructor, invokes the deleter if the stored pointer is not null.

Definition at line *355* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Up , typename > *std::unique_ptr*< _Tp, _Dp
>::*unique_ptr* (*auto_ptr*< _Up > && __u)= [inline]=, = [noexcept]=
Definition at line *328* of file *auto_ptr.h*.

* Member Function Documentation
** template<typename _Tp , typename _Dp = default_delete<_Tp>> pointer
*std::unique_ptr*< _Tp, _Dp >::get () const= [inline]=, = [noexcept]=
Return the stored pointer.

Definition at line *421* of file *unique_ptr.h*.

Referenced by *std::unique_ptr< _Tp, _Dp >::operator bool()*,
*std::unique_ptr< _Tp[], _Dp >::operator bool()*, *std::unique_ptr< _Tp,
_Dp >::operator!=()*, *std::unique_ptr< _Tp, _Dp >::operator==()*, and
*std::unique_ptr< _Tp, _Dp >::operator>()*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>> const
deleter_type & *std::unique_ptr*< _Tp, _Dp >::get_deleter ()
const= [inline]=, = [noexcept]=
Return a reference to the stored deleter.

Definition at line *431* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
deleter_type & *std::unique_ptr*< _Tp, _Dp >::get_deleter ()= [inline]=,
= [noexcept]=
Return a reference to the stored deleter.

Definition at line *426* of file *unique_ptr.h*.

Referenced by *std::unique_ptr< _Tp[], _Dp >::~unique_ptr()*,
*std::unique_ptr< _Tp, _Dp >::operator=()*, and *std::unique_ptr< _Tp[],
_Dp >::operator=()*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
*std::unique_ptr*< _Tp, _Dp >::operator bool () const= [inline]=,
= [explicit]=, = [noexcept]=
Return =true= if the stored pointer is not null.

Definition at line *435* of file *unique_ptr.h*.

References *std::unique_ptr< _Tp, _Dp >::get()*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
*add_lvalue_reference*< element_type >::type *std::unique_ptr*< _Tp, _Dp
>::operator* () const= [inline]=
Dereference the stored pointer.

Definition at line *405* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>> pointer
*std::unique_ptr*< _Tp, _Dp >::operator-> () const= [inline]=,
= [noexcept]=
Return the stored pointer.

Definition at line *413* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
*unique_ptr* & *std::unique_ptr*< _Tp, _Dp >::operator=
(nullptr_t)= [inline]=, = [noexcept]=
Reset the unique_ptr to empty, invoking the deleter if necessary.

Definition at line *395* of file *unique_ptr.h*.

References *std::unique_ptr< _Tp, _Dp >::reset()*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
*unique_ptr* & *std::unique_ptr*< _Tp, _Dp >::operator= (*unique_ptr*<
_Tp, _Dp > &&)= [default]=
Move assignment operator. Invokes the deleter if this object owns a
pointer.

** template<typename _Tp , typename _Dp = default_delete<_Tp>>
template<typename _Up , typename _Ep > *enable_if*< __and_<
__safe_conversion_up< _Up, _Ep >, *is_assignable*< deleter_type &, _Ep
&& > >::value, *unique_ptr* & >::type *std::unique_ptr*< _Tp, _Dp
>::operator= (*unique_ptr*< _Up, _Ep > && __u)= [inline]=, = [noexcept]=
Assignment from another type.

*Parameters*

#+begin_quote
  /__u/ The object to transfer ownership from, which owns a convertible
  pointer to a non-array object.
#+end_quote

Invokes the deleter if this object owns a pointer.

Definition at line *386* of file *unique_ptr.h*.

References *std::unique_ptr< _Tp, _Dp >::get_deleter()*, and
*std::unique_ptr< _Tp, _Dp >::reset()*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>> pointer
*std::unique_ptr*< _Tp, _Dp >::release ()= [inline]=, = [noexcept]=
Release ownership of any stored pointer.

Definition at line *442* of file *unique_ptr.h*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>> void
*std::unique_ptr*< _Tp, _Dp >::reset (pointer __p =
=pointer()=)= [inline]=, = [noexcept]=
Replace the stored pointer.

*Parameters*

#+begin_quote
  /__p/ The new pointer to store.
#+end_quote

The deleter will be invoked if a pointer is already owned.

Definition at line *452* of file *unique_ptr.h*.

Referenced by *std::unique_ptr< _Tp, _Dp >::operator=()*, and
*std::unique_ptr< _Tp[], _Dp >::operator=()*.

** template<typename _Tp , typename _Dp = default_delete<_Tp>> void
*std::unique_ptr*< _Tp, _Dp >::swap (*unique_ptr*< _Tp, _Dp > &
__u)= [inline]=, = [noexcept]=
Exchange the pointer and deleter with another object.

Definition at line *461* of file *unique_ptr.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
