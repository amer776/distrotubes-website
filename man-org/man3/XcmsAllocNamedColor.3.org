#+TITLE: Manpages - XcmsAllocNamedColor.3
#+DESCRIPTION: Linux manpage for XcmsAllocNamedColor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XcmsAllocNamedColor.3 is found in manpage for: [[../man3/XcmsAllocColor.3][man3/XcmsAllocColor.3]]