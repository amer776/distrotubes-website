#+TITLE: Manpages - __gnu_pbds_detail_trie_traits.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_trie_traits.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::trie_traits< Key, Data, _ATraits, Node_Update, Tag,
_Alloc > - Trie traits class, primary template.

* SYNOPSIS
\\

* Detailed Description
** "template<typename Key, typename Data, typename _ATraits, template<
typename Node_CItr, typename Node_Itr, typename _ATraits_, typename
_Alloc > class Node_Update, typename Tag, typename _Alloc>
\\
struct __gnu_pbds::detail::trie_traits< Key, Data, _ATraits,
Node_Update, Tag, _Alloc >"Trie traits class, primary template.

Definition at line *83* of file *branch_policy/traits.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
