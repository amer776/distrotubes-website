#+TITLE: Manpages - archive_read_format.3
#+DESCRIPTION: Linux manpage for archive_read_format.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

Enables support---including auto-detection code---for the specified
archive format. For example,

enables support for a variety of standard tar formats, old-style tar,
ustar, pax interchange format, and many common variants.

Enables support for all available formats except the

format (see below).

Enables a single format specified by the format code. This can be useful
when reading a single archive twice; use

after reading the first time and pass the resulting code to this
function to selectively enable only the necessary format support. Note:
In statically-linked executables, this will cause your program to
include support for every format. If executable size is a concern, you
may wish to avoid using this function.

Enables support for treating empty files as empty archives. Because
empty files are valid for several different formats, it is not possible
to accurately determine a format for an empty file based purely on
contents. So empty files are treated by libarchive as a distinct format.

The

format handler allows libarchive to be used to read arbitrary data. It
treats any data stream as an archive with a single entry. The pathname
of this entry is

all other entry fields are unset. This is not enabled by

in order to avoid erroneous handling of damaged archives.

These functions return

on success, or

Detailed error codes and textual descriptions are available from the

and

functions.

Many traditional archiver programs treat empty files as valid empty
archives. For example, many implementations of

allow you to append entries to an empty file. Of course, it is
impossible to determine the format of an empty file by inspecting the
contents, so this library treats empty files as having a special

format.

Using the

handler together with any other handler will often work but can produce
surprising results.
