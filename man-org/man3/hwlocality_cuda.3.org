#+TITLE: Manpages - hwlocality_cuda.3
#+DESCRIPTION: Linux manpage for hwlocality_cuda.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_cuda - Interoperability with the CUDA Driver API

* SYNOPSIS
\\

** Functions
static int *hwloc_cuda_get_device_pci_ids* (*hwloc_topology_t* topology,
CUdevice cudevice, int *domain, int *bus, int *dev)\\

static int *hwloc_cuda_get_device_cpuset* (*hwloc_topology_t* topology,
CUdevice cudevice, *hwloc_cpuset_t* set)\\

static *hwloc_obj_t* *hwloc_cuda_get_device_pcidev* (*hwloc_topology_t*
topology, CUdevice cudevice)\\

static *hwloc_obj_t* *hwloc_cuda_get_device_osdev* (*hwloc_topology_t*
topology, CUdevice cudevice)\\

static *hwloc_obj_t* *hwloc_cuda_get_device_osdev_by_index*
(*hwloc_topology_t* topology, unsigned idx)\\

* Detailed Description
This interface offers ways to retrieve topology information about CUDA
devices when using the CUDA Driver API.

* Function Documentation
** static int hwloc_cuda_get_device_cpuset (*hwloc_topology_t* topology,
CUdevice cudevice, *hwloc_cpuset_t* set)= [inline]=, = [static]=
Get the CPU set of processors that are physically close to device
=cudevice=. Store in =set= the CPU-set describing the locality of the
CUDA device =cudevice=.

Topology =topology= and device =cudevice= must match the local machine.
I/O devices detection and the CUDA component are not needed in the
topology.

The function only returns the locality of the device. If more
information about the device is needed, OS objects should be used
instead, see *hwloc_cuda_get_device_osdev()* and
*hwloc_cuda_get_device_osdev_by_index()*.

This function is currently only implemented in a meaningful way for
Linux; other systems will simply get a full cpuset.

** static *hwloc_obj_t* hwloc_cuda_get_device_osdev (*hwloc_topology_t*
topology, CUdevice cudevice)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to CUDA device =cudevice=.

*Returns*

#+begin_quote
  The hwloc OS device object that describes the given CUDA device
  =cudevice=.

  =NULL= if none could be found.
#+end_quote

Topology =topology= and device =cudevice= must match the local machine.
I/O devices detection and the CUDA component must be enabled in the
topology. If not, the locality of the object may still be found using
*hwloc_cuda_get_device_cpuset()*.

*Note*

#+begin_quote
  This function cannot work if PCI devices are filtered out.

  The corresponding hwloc PCI device may be found by looking at the
  result parent pointer (unless PCI devices are filtered out).
#+end_quote

** static *hwloc_obj_t* hwloc_cuda_get_device_osdev_by_index
(*hwloc_topology_t* topology, unsigned idx)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to the CUDA device whose
index is =idx=.

*Returns*

#+begin_quote
  The hwloc OS device object describing the CUDA device whose index is
  =idx=.

  =NULL= if none could be found.
#+end_quote

The topology =topology= does not necessarily have to match the current
machine. For instance the topology may be an XML import of a remote
host. I/O devices detection and the CUDA component must be enabled in
the topology.

*Note*

#+begin_quote
  The corresponding PCI device object can be obtained by looking at the
  OS device parent object (unless PCI devices are filtered out).

  This function is identical to
  *hwloc_cudart_get_device_osdev_by_index()*.
#+end_quote

** static int hwloc_cuda_get_device_pci_ids (*hwloc_topology_t*
topology, CUdevice cudevice, int * domain, int * bus, int *
dev)= [inline]=, = [static]=
Return the domain, bus and device IDs of the CUDA device =cudevice=.
Device =cudevice= must match the local machine.

** static *hwloc_obj_t* hwloc_cuda_get_device_pcidev (*hwloc_topology_t*
topology, CUdevice cudevice)= [inline]=, = [static]=
Get the hwloc PCI device object corresponding to the CUDA device
=cudevice=.

*Returns*

#+begin_quote
  The hwloc PCI device object describing the CUDA device =cudevice=.

  =NULL= if none could be found.
#+end_quote

Topology =topology= and device =cudevice= must match the local machine.
I/O devices detection must be enabled in topology =topology=. The CUDA
component is not needed in the topology.

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
