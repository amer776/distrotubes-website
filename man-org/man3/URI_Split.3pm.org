#+TITLE: Manpages - URI_Split.3pm
#+DESCRIPTION: Linux manpage for URI_Split.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
URI::Split - Parse and compose URI strings

* SYNOPSIS
use URI::Split qw(uri_split uri_join); ($scheme, $auth, $path, $query,
$frag) = uri_split($uri); $uri = uri_join($scheme, $auth, $path, $query,
$frag);

* DESCRIPTION
Provides functions to parse and compose URI strings. The following
functions are provided:

- ($scheme, $auth, $path, $query, $frag) = uri_split($uri) :: Breaks up
  a URI string into its component parts. An =undef= value is returned
  for those parts that are not present. The =$path= part is always
  present (but can be the empty string) and is thus never returned as
  =undef=. No sensible value is returned if this function is called in a
  scalar context.

- $uri = uri_join($scheme, $auth, $path, $query, $frag) :: Puts together
  a URI string from its parts. Missing parts are signaled by passing
  =undef= for the corresponding argument. Minimal escaping is applied to
  parts that contain reserved chars that would confuse a parser. For
  instance, any occurrence of '?' or '#' in =$path= is always escaped,
  as it would otherwise be parsed back as a query or fragment.

* SEE ALSO
URI, URI::Escape

* COPYRIGHT
Copyright 2003, Gisle Aas

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
