#+TITLE: Manpages - FcCharSetIntersectCount.3
#+DESCRIPTION: Linux manpage for FcCharSetIntersectCount.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetIntersectCount - Intersect and count charsets

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar32 FcCharSetIntersectCount (const FcCharSet */a/*, const FcCharSet
**/b/*);*

* DESCRIPTION
Returns the number of chars that are in both /a/ and /b/.
