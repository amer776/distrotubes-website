#+TITLE: Manpages - FcLangSetDestroy.3
#+DESCRIPTION: Linux manpage for FcLangSetDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetDestroy - destroy a langset object

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcLangSetDestroy (FcLangSet */ls/*);*

* DESCRIPTION
*FcLangSetDestroy* destroys a FcLangSet object, freeing all memory
associated with it.
