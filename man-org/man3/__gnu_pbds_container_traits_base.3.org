#+TITLE: Manpages - __gnu_pbds_container_traits_base.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_container_traits_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::container_traits_base< _Tag > - Primary template, container
traits base.

* SYNOPSIS
\\

* Detailed Description
** "template<typename _Tag>
\\
struct __gnu_pbds::container_traits_base< _Tag >"Primary template,
container traits base.

Definition at line *220* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
