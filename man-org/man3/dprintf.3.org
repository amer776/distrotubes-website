#+TITLE: Manpages - dprintf.3
#+DESCRIPTION: Linux manpage for dprintf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about dprintf.3 is found in manpage for: [[../man3/printf.3][man3/printf.3]]