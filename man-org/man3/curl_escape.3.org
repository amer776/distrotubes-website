#+TITLE: Manpages - curl_escape.3
#+DESCRIPTION: Linux manpage for curl_escape.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_escape - URL encodes the given string

* SYNOPSIS
*#include <curl/curl.h>*

*char *curl_escape( const char **/url/*, int */length/* );*

* DESCRIPTION
Obsolete function. Use /curl_easy_escape(3)/ instead!

This function will convert the given input string to an URL encoded
string and return that as a new allocated string. All input characters
that are not a-z, A-Z or 0-9 will be converted to their "URL escaped"
version (%NN where NN is a two-digit hexadecimal number).

If the *lengthf* argument is set to 0, curl_escape() will use strlen()
on the input *url* string to find out the size.

You must /curl_free(3)/ the returned string when you are done with it.

* EXAMPLE
#+begin_example
  char *output = curl_escape("data to convert", 15);
  if(output) {
    printf("Encoded: %s\n", output);
    curl_free(output);
  }
#+end_example

* AVAILABILITY
Since 7.15.4, /curl_easy_escape(3)/ should be used. This function will
be removed in a future release.

* RETURN VALUE
A pointer to a null-terminated string or NULL if it failed.

* SEE ALSO
*curl_unescape*(3), *curl_free*(3), *RFC*2396
