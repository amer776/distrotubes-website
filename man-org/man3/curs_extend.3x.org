#+TITLE: Manpages - curs_extend.3x
#+DESCRIPTION: Linux manpage for curs_extend.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*curses_version*, *use_extended_names* - miscellaneous curses extensions

* SYNOPSIS
*#include <curses.h>*

*const char * curses_version(void);*\\
*int use_extended_names(bool */enable/*);*

* DESCRIPTION
These functions are extensions to the curses library which do not fit
easily into other categories.

** curses_version
Use *curses_version* to get the version number, including patch level of
the library, prefixed by ncurses, e.g.,

#+begin_quote
  *ncurses 5.0.19991023*
#+end_quote

** use_extended_names
The *use_extended_names* function controls whether the calling
application is able to use user-defined or nonstandard names which may
be compiled into the terminfo description, i.e., via the terminfo or
termcap interfaces. Normally these names are available for use, since
the essential decision is made by using the *-x* option of *tic* to
compile extended terminal definitions. However you can disable this
feature to ensure compatibility with other implementations of curses.

* RETURN VALUE
*curses_version* returns a pointer to static memory; you should not free
this in your application.

*use_extended_names* returns the previous state, allowing you to save
this and restore it.

* PORTABILITY
These routines are specific to ncurses. They were not supported on
Version 7, BSD or System V implementations. It is recommended that any
code depending on them be conditioned using NCURSES_VERSION.

* SEE ALSO
*curs_getch*(3X), *curs_mouse*(3X), *curs_print*(3X), *curs_util*(3X),
*default_colors*(3X), *define_key*(3X), *keybound*(3X), *keyok*(3X),
*resizeterm*(3X), *wresize*(3X).

* AUTHOR
Thomas Dickey.
