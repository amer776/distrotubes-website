#+TITLE: Manpages - MPI_Type_vector.3
#+DESCRIPTION: Linux manpage for MPI_Type_vector.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Type_vector* - Creates a vector (strided) datatype.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Type_vector(int count, int blocklength, int stride,
  	MPI_Datatype oldtype, MPI_Datatype *newtype)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_TYPE_VECTOR(COUNT, BLOCKLENGTH, STRIDE, OLDTYPE, NEWTYPE,
  		IERROR)
  	INTEGER	COUNT, BLOCKLENGTH, STRIDE, OLDTYPE
  	INTEGER	NEWTYPE, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Type_vector(count, blocklength, stride, oldtype, newtype, ierror)
  	INTEGER, INTENT(IN) :: count, blocklength, stride
  	TYPE(MPI_Datatype), INTENT(IN) :: oldtype
  	TYPE(MPI_Datatype), INTENT(OUT) :: newtype
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  Datatype Datatype::Create_vector(int count, int blocklength,
  	int stride) const
#+end_example

* INPUT PARAMETERS
- count :: Number of blocks (nonnegative integer).

- blocklength :: Number of elements in each block (nonnegative integer).

- stride :: Number of elements between start of each block (integer).

- oldtype :: Old datatype (handle).

* OUTPUT PARAMETERS
- newtype :: New datatype (handle).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
The function MPI_Type_vector is a general constructor that allows
replication of a datatype into locations that consist of equally spaced
blocks. Each block is obtained by concatenating the same number of
copies of the old datatype. The spacing between blocks is a multiple of
the extent of the old datatype.

*Example 1:* Assume, again, that oldtype has type map {(double, 0),
(char, 8)}, with extent 16. A call to MPI_Type_vector(2, 3, 4, oldtype,
newtype) will create the datatype with type map

#+begin_example
      {(double, 0), (char, 8), (double, 16), (char, 24),
      (double, 32), (char, 40),
      (double, 64), (char, 72),
      (double, 80), (char, 88), (double, 96), (char, 104)}
#+end_example

That is, two blocks with three copies each of the old type, with a
stride of 4 elements (4 x 6 bytes) between the blocks.

*Example 2:* A call to MPI_Type_vector(3, 1, -2, oldtype, newtype) will
create the datatype

#+begin_example

      {(double, 0), (char, 8), (double, -32), (char, -24),
      (double, -64), (char, -56)}
#+end_example

In general, assume that oldtype has type map

#+begin_example

      {(type(0), disp(0)), ..., (type(n-1), disp(n-1))},
#+end_example

with extent ex. Let bl be the blocklength. The newly created datatype
has a type map with count x bl x n entries:

#+begin_example

      {(type(0), disp(0)), ..., (type(n-1), disp(n-1)),
      (type(0), disp(0) + ex), ..., (type(n-1), disp(n-1) + ex), ...,
      (type(0), disp(0) + (bl -1) * ex),...,
      (type(n-1), disp(n-1) + (bl -1)* ex),
      (type(0), disp(0) + stride * ex),..., (type(n-1),
      disp(n-1) + stride * ex), ...,
      (type(0), disp(0) + (stride + bl - 1) * ex), ...,
      (type(n-1), disp(n-1) + (stride + bl -1) * ex), ...,
      (type(0), disp(0) + stride * (count -1) * ex), ...,
      (type(n-1), disp(n-1) + stride * (count -1) * ex), ...,
      (type(0), disp(0) + (stride * (count -1) + bl -1) * ex), ...,
      (type(n-1), disp(n-1) + (stride * (count -1) + bl -1) * ex)}
#+end_example

A call to MPI_Type_contiguous(count, oldtype, newtype) is equivalent to
a call to MPI_Type_vector(count, 1, 1, oldtype, newtype), or to a call
to MPI_Type_vector(1, count, n, oldtype, newtype), n arbitrary.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
MPI_Type_create_hvector\\
MPI_Type_hvector\\
