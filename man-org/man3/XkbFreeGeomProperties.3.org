#+TITLE: Manpages - XkbFreeGeomProperties.3
#+DESCRIPTION: Linux manpage for XkbFreeGeomProperties.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFreeGeomProperties - Free geometry properties

* SYNOPSIS
*void XkbFreeGeomProperties* *( XkbGeometryPtr */geom/* ,* *int
*/first/* ,* *int */count/* ,* *Bool **/free_all/* );*

* ARGUMENTS
- /- geom/ :: geometry in which properties should be freed

- /- first/ :: first property to be freed

- /- count/ :: number of properties to be freed

- /- free_all/ :: True => all properties are freed

* DESCRIPTION
Xkb provides a number of functions to allocate and free subcomponents of
a keyboard geometry. Use these functions to create or modify keyboard
geometries. Note that these functions merely allocate space for the new
element(s), and it is up to you to fill in the values explicitly in your
code. These allocation functions increase /sz_*/ but never touch /num_*/
(unless there is an allocation failure, in which case they reset both
/sz_*/ and /num_*/ to zero). These functions return Success if they
succeed, BadAlloc if they are not able to allocate space, or BadValue if
a parameter is not as expected.

If /free_all/ is True, all properties are freed regardless of the value
of /first/ or /count./ Otherwise, /count/ properties are freed beginning
with the one specified by /first./

* RETURN VALUES
- Success :: The XkbFreeGeomProperties function returns Success when
  there are no allocation or argument errors.

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadValue* :: An argument is out of range
