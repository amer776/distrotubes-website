#+TITLE: Manpages - FcAtomicCreate.3
#+DESCRIPTION: Linux manpage for FcAtomicCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcAtomicCreate - create an FcAtomic object

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcAtomic * FcAtomicCreate (const FcChar8 */file/*);*

* DESCRIPTION
Creates a data structure containing data needed to control access to
/file/. Writing is done to a separate file. Once that file is complete,
the original configuration file is atomically replaced so that reading
process always see a consistent and complete file without the need to
lock for reading.
