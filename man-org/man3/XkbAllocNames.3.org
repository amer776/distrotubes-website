#+TITLE: Manpages - XkbAllocNames.3
#+DESCRIPTION: Linux manpage for XkbAllocNames.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocNames - Allocate symbolic names structures

* SYNOPSIS
*Status XkbAllocNames* *( XkbDescPtr */xkb/* ,* *unsigned int
*/which/* ,* *int */num_rg/* ,* *int */num_key_aliases/* );*

* ARGUMENTS
- /- xkb/ :: keyboard description for which names are to be allocated

- /- which/ :: mask of names to be allocated

- /- num_rg/ :: total number of radio group names needed

- /- num_key_aliases/ :: total number of key aliases needed

* DESCRIPTION
Most applications do not need to directly allocate symbolic names
structures. Do not allocate a names structure directly using /malloc/ or
/Xmalloc/ if your application changes the number of key aliases or radio
groups or constructs a symbolic names structure without loading the
necessary components from the X server. Instead use /XkbAllocNames./

/XkbAllocNames/ can return BadAlloc, BadMatch, and BadValue errors. The
/which/ parameter is the bitwise inclusive OR of the valid names mask
bits defined in Table 1.

TABLE

Do not free symbolic names structures directly using /free/ or /XFree./
Use /XkbFreeNames/ instead.

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadMatch* :: A compatible version of Xkb was not available in the
  server or an argument has correct type and range, but is otherwise
  invalid

- *BadValue* :: An argument is out of range

* SEE ALSO
*XkbFreeNames*(3)
