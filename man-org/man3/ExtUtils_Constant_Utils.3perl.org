#+TITLE: Manpages - ExtUtils_Constant_Utils.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_Constant_Utils.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::Constant::Utils - helper functions for ExtUtils::Constant

* SYNOPSIS
use ExtUtils::Constant::Utils qw (C_stringify); $C_code = C_stringify
$stuff;

* DESCRIPTION
ExtUtils::Constant::Utils packages up utility subroutines used by
ExtUtils::Constant, ExtUtils::Constant::Base and derived classes. All
its functions are explicitly exportable.

* USAGE
- C_stringify NAME :: A function which returns a 7 bit ASCII correctly \
  escaped version of the string passed suitable for C's "" or ''. It
  will die if passed Unicode characters.

- perl_stringify NAME :: A function which returns a 7 bit ASCII
  correctly \ escaped version of the string passed suitable for a perl
  "" string.

* AUTHOR
Nicholas Clark <nick@ccl4.org> based on the code in =h2xs= by Larry Wall
and others
