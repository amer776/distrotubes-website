#+TITLE: Manpages - DBM_Filter_utf8.3perl
#+DESCRIPTION: Linux manpage for DBM_Filter_utf8.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBM_Filter::utf8 - filter for DBM_Filter

* SYNOPSIS
use SDBM_File; # or DB_File, GDBM_File, NDBM_File, or ODBM_File use
DBM_Filter; $db = tie %hash, ... $db->Filter_Push(utf8);

* DESCRIPTION
This Filter will ensure that all data written to the DBM will be encoded
in UTF-8.

This module uses the Encode module.

* SEE ALSO
DBM_Filter, perldbmfilter, Encode

* AUTHOR
Paul Marquess pmqs@cpan.org
