#+TITLE: Manpages - opus_multistream_ctls.3
#+DESCRIPTION: Linux manpage for opus_multistream_ctls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
opus_multistream_ctls - These are convenience macros that are specific
to the *opus_multistream_encoder_ctl()* and
*opus_multistream_decoder_ctl()* interface. The CTLs from *Generic
CTLs*, *Encoder related CTLs*, and *Decoder related CTLs* may be applied
to a multistream encoder or decoder as well. In addition, you may
retrieve the encoder or decoder state for an specific stream via
*OPUS_MULTISTREAM_GET_ENCODER_STATE* or
*OPUS_MULTISTREAM_GET_DECODER_STATE* and apply CTLs to it individually.

* SYNOPSIS
\\

** Macros
#define *OPUS_MULTISTREAM_GET_ENCODER_STATE*(x, y)\\
Gets the encoder state for an individual stream of a multistream
encoder.

#define *OPUS_MULTISTREAM_GET_DECODER_STATE*(x, y)\\
Gets the decoder state for an individual stream of a multistream
decoder.

* Detailed Description
These are convenience macros that are specific to the
*opus_multistream_encoder_ctl()* and *opus_multistream_decoder_ctl()*
interface. The CTLs from *Generic CTLs*, *Encoder related CTLs*, and
*Decoder related CTLs* may be applied to a multistream encoder or
decoder as well. In addition, you may retrieve the encoder or decoder
state for an specific stream via *OPUS_MULTISTREAM_GET_ENCODER_STATE* or
*OPUS_MULTISTREAM_GET_DECODER_STATE* and apply CTLs to it individually.

* Macro Definition Documentation
** #define OPUS_MULTISTREAM_GET_DECODER_STATE(x, y)
Gets the decoder state for an individual stream of a multistream
decoder.

*Parameters*

#+begin_quote
  /x/ =opus_int32=: The index of the stream whose decoder you wish to
  retrieve. This must be non-negative and less than the =streams=
  parameter used to initialize the decoder.\\
  /y/ =OpusDecoder**=: Returns a pointer to the given decoder state.
#+end_quote

*Return values*

#+begin_quote
  /OPUS_BAD_ARG/ The index of the requested stream was out of range.
#+end_quote

** #define OPUS_MULTISTREAM_GET_ENCODER_STATE(x, y)
Gets the encoder state for an individual stream of a multistream
encoder.

*Parameters*

#+begin_quote
  /x/ =opus_int32=: The index of the stream whose encoder you wish to
  retrieve. This must be non-negative and less than the =streams=
  parameter used to initialize the encoder.\\
  /y/ =OpusEncoder**=: Returns a pointer to the given encoder state.
#+end_quote

*Return values*

#+begin_quote
  /OPUS_BAD_ARG/ The index of the requested stream was out of range.
#+end_quote

* Author
Generated automatically by Doxygen for Opus from the source code.
