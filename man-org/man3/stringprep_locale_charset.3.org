#+TITLE: Manpages - stringprep_locale_charset.3
#+DESCRIPTION: Linux manpage for stringprep_locale_charset.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stringprep_locale_charset - API function

* SYNOPSIS
*#include <stringprep.h>*

*const char * stringprep_locale_charset( */void/*);*

* ARGUMENTS
-  void :: 

* DESCRIPTION
Enumerated return codes of the TLD checking functions. The value 0 is
guaranteed to always correspond to success.

* 
Find out current locale charset. The function respect the CHARSET
environment variable, but typically uses nl_langinfo(CODESET) when it is
supported. It fall back on "ASCII" if CHARSET isn't set and nl_langinfo
isn't supported or return anything.

Note that this function return the application's locale's preferred
charset (or thread's locale's preferred charset, if your system support
thread-specific locales). It does not return what the system may be
using. Thus, if you receive data from external sources you cannot in
general use this function to guess what charset it is encoded in. Use
stringprep_convert from the external representation into the charset
returned by this function, to have data in the locale encoding.

Return value: Return the character set used by the current locale. It
will never return NULL, but use "ASCII" as a fallback.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
