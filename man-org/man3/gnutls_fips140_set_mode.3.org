#+TITLE: Manpages - gnutls_fips140_set_mode.3
#+DESCRIPTION: Linux manpage for gnutls_fips140_set_mode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_fips140_set_mode - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*void gnutls_fips140_set_mode(gnutls_fips_mode_t */mode/*, unsigned
*/flags/*);*

* ARGUMENTS
- gnutls_fips_mode_t mode :: the FIPS140-2 mode to switch to

- unsigned flags :: should be zero or *GNUTLS_FIPS140_SET_MODE_THREAD*

* DESCRIPTION
That function is not thread-safe when changing the mode with no flags
(globally), and should be called prior to creating any threads. Its
behavior with no flags after threads are created is undefined.

When the flag *GNUTLS_FIPS140_SET_MODE_THREAD* is specified then this
call will change the FIPS140-2 mode for this particular thread and not
for the whole process. That way an application can utilize this function
to set and reset mode for specific operations.

This function never fails but will be a no-op if used when the library
is not in FIPS140-2 mode. When asked to switch to unknown values for
/mode/ or to *GNUTLS_FIPS140_SELFTESTS* mode, the library switches to
*GNUTLS_FIPS140_STRICT* mode.

* SINCE
3.6.2

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
