#+TITLE: Manpages - io_uring_queue_init.3
#+DESCRIPTION: Linux manpage for io_uring_queue_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
io_uring_queue_init - setup io_uring submission and completion queues

* SYNOPSIS
#+begin_example
  #include <liburing.h>

  int io_uring_queue_init(unsigned entries, struct io_uring *ring,
   unsigned flags);
#+end_example

* DESCRIPTION
The io_uring_queue_init() function executes the io_uring_setup syscall
to initialize the submission and completion queues in the kernel with at
least /entries/ entries and then maps the resulting file descriptor to
memory shared between the application and the kernel.

On success io_uring_queue_init() returns 0 and /ring/ will point to the
shared memory containing the io_uring queues. On failure -errno is
returned.

/flags/ will be passed through to the io_uring_setup syscall (see
*io_uring_setup*(2)).

On success, the resources held by /ring/ should be released via a
corresponding call to *io_uring_queue_exit*(3).

* RETURN VALUE
*io_uring_queue_init*(3) returns 0 on success and -errno on failure.

* SEE ALSO
*io_uring_setup*(2), *mmap*(2), *io_uring_queue_exit*(3)
