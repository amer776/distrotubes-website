#+TITLE: Manpages - DBI_Gofer_Transport_stream.3pm
#+DESCRIPTION: Linux manpage for DBI_Gofer_Transport_stream.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBI::Gofer::Transport::stream - DBD::Gofer server-side transport for
stream

* SYNOPSIS
See DBD::Gofer::Transport::stream.

* AUTHOR
Tim Bunce, <http://www.tim.bunce.name>

* LICENCE AND COPYRIGHT
Copyright (c) 2007, Tim Bunce, Ireland. All rights reserved.

This module is free software; you can redistribute it and/or modify it
under the same terms as Perl itself. See perlartistic.
