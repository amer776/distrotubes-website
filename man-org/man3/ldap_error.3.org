#+TITLE: Manpages - ldap_error.3
#+DESCRIPTION: Linux manpage for ldap_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldap_perror, ld_errno, ldap_result2error, ldap_errlist,
ldap_err2string - LDAP protocol error handling routines

* LIBRARY
OpenLDAP LDAP (libldap, -lldap)

* SYNOPSIS
#+begin_example
  #include <ldap.h>
  char *ldap_err2string( int err );
#+end_example

* DESCRIPTION
The *ldap_err2string()* routine provides short description of the
various codes returned by routines in this library. The returned string
is a pointer to a static area that should not be modified.

These codes are either negative, indicating an API error code; positive,
indicating an LDAP resultCode other than 'success' (0), or - zero,
indicating both successful use of the API and the LDAP resultCode
'success' (0).

The code associated with an LDAP session is accessible using
*ldap_get_option*(3) and *ldap_set_option*(3) with the
*LDAP_OPT_RESULT_CODE* option (previously called
*LDAP_OPT_ERROR_NUMBER*).

* PROTOCOL RESULT CODES
This section provides a partial list of protocol codes recognized by the
library. As LDAP is extensible, additional values may be returned. A
complete listing of /registered/* LDAP result codes* can be obtained
from the /Internet Assigned Numbers Authority/ <http://www.iana.org>.

The request was successful.

An operations error occurred.

A protocol violation was detected.

An LDAP time limit was exceeded.

An LDAP size limit was exceeded.

A compare operation returned false.

A compare operation returned true.

The LDAP server does not support strong authentication.

Strong authentication is required for the operation.

Partial results only returned.

The attribute type specified does not exist in the entry.

The attribute type specified is invalid.

Filter type not supported for the specified attribute.

An attribute value specified violates some constraint (e.g., a
postalAddress has too many lines, or a line that is too long).

An attribute type or attribute value specified already exists in the
entry.

An invalid attribute value was specified.

The specified object does not exist in The Directory.

An alias in The Directory points to a nonexistent entry.

A syntactically invalid DN was specified.

The object specified is a leaf.

A problem was encountered when dereferencing an alias.

Inappropriate authentication was specified (e.g., LDAP_AUTH_SIMPLE was
specified and the entry does not have a userPassword attribute).

Invalid credentials were presented (e.g., the wrong password).

The user has insufficient access to perform the operation.

The DSA is busy.

The DSA is unavailable.

The DSA is unwilling to perform the operation.

A loop was detected.

A naming violation occurred.

An object class violation occurred (e.g., a "must" attribute was missing
from the entry).

The operation is not allowed on a nonleaf object.

The operation is not allowed on an RDN.

The entry already exists.

Object class modifications are not allowed.

An unknown error occurred.

* API ERROR CODES
This section provides a complete list of API error codes recognized by
the library. Note that LDAP_SUCCESS indicates success of an API call in
addition to representing the return of the LDAP 'success' resultCode.

The LDAP library can't contact the LDAP server.

Some local error occurred. This is usually a failed dynamic memory
allocation.

An error was encountered encoding parameters to send to the LDAP server.

An error was encountered decoding a result from the LDAP server.

A timelimit was exceeded while waiting for a result.

The authentication method specified to ldap_bind() is not known.

An invalid filter was supplied to ldap_search() (e.g., unbalanced
parentheses).

An ldap routine was called with a bad parameter.

An memory allocation (e.g., malloc(3) or other dynamic memory allocator)
call failed in an ldap library routine.

Indicates the user cancelled the operation.

Indicates a connection problem.

Indicates the routine was called in a manner not supported by the
library.

Indicates the control provided is unknown to the client library.

Indicates no results returned.

Indicates more results could be returned.

Indicates the library has detected a loop in its processing.

Indicates the referral limit has been exceeded.

* DEPRECATED
Deprecated interfaces generally remain in the library. The macro
LDAP_DEPRECATED can be defined to a non-zero value (e.g.,
-DLDAP_DEPRECATED=1) when compiling program designed to use deprecated
interfaces. It is recommended that developers writing new programs, or
updating old programs, avoid use of deprecated interfaces. Over time, it
is expected that documentation (and, eventually, support) for deprecated
interfaces to be eliminated.

* SEE ALSO
*ldap*(3),

* ACKNOWLEDGEMENTS
*OpenLDAP Software* is developed and maintained by The OpenLDAP Project
<http://www.openldap.org/>. *OpenLDAP Software* is derived from the
University of Michigan LDAP 3.3 Release.
