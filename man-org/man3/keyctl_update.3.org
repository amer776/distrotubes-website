#+TITLE: Manpages - keyctl_update.3
#+DESCRIPTION: Linux manpage for keyctl_update.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_update - update a key

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_update(key_serial_t key, const void *payload,
  size_t plen);
#+end_example

* DESCRIPTION
*keyctl_update*() updates the payload of a key if the key type permits
it.

The caller must have *write* permission on a key to be able to update
it.

/payload/ and /plen/ specify the data for the new payload. /payload/ may
be NULL and /plen/ may be zero if the key type permits that. The key
type may reject the data if it's in the wrong format or in some other
way invalid.

* RETURN VALUE
On success *keyctl_update*() returns *0*. On error, the value *-1* will
be returned and /errno/ will have been set to an appropriate error.

* ERRORS
- *ENOKEY* :: The key specified is invalid.

- *EKEYEXPIRED* :: The key specified has expired.

- *EKEYREVOKED* :: The key specified had been revoked.

- *EINVAL* :: The payload data was invalid.

- *ENOMEM* :: Insufficient memory to store the new payload.

- *EDQUOT* :: The key quota for this user would be exceeded by
  increasing the size of the key to accommodate the new payload.

- *EACCES* :: The key exists, but is not *writable* by the calling
  process.

- *EOPNOTSUPP* :: The key type does not support the update operation on
  its keys.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *add_key*(2), *keyctl*(2), *request_key*(2), *keyctl*(3),
*keyrings*(7), *keyutils*(7)
