#+TITLE: Manpages - ExtUtils_MY.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MY.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MY - ExtUtils::MakeMaker subclass for customization

* SYNOPSIS
# in your Makefile.PL sub MY::whatever { ... }

* DESCRIPTION
*FOR INTERNAL USE ONLY*

ExtUtils::MY is a subclass of ExtUtils::MM. Its provided in your
Makefile.PL for you to add and override MakeMaker functionality.

It also provides a convenient alias via the MY class.

ExtUtils::MY might turn out to be a temporary solution, but MY won't go
away.
