#+TITLE: Manpages - logf.3
#+DESCRIPTION: Linux manpage for logf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about logf.3 is found in manpage for: [[../man3/log.3][man3/log.3]]