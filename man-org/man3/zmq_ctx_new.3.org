#+TITLE: Manpages - zmq_ctx_new.3
#+DESCRIPTION: Linux manpage for zmq_ctx_new.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_ctx_new - create new 0MQ context

* SYNOPSIS
*void *zmq_ctx_new ();*

* DESCRIPTION
The /zmq_ctx_new()/ function creates a new 0MQ /context/.

This function replaces the deprecated function *zmq_init*(3).

*Thread safety*. A 0MQ /context/ is thread safe and may be shared among
as many application threads as necessary, without any additional locking
required on the part of the caller.

* RETURN VALUE
The /zmq_ctx_new()/ function shall return an opaque handle to the newly
created /context/ if successful. Otherwise it shall return NULL and set
/errno/ to one of the values defined below.

* ERRORS
*EMFILE*

#+begin_quote
  The limit on the total number of open files has been reached and it
  wasn't possible to create a new context.
#+end_quote

*EMFILE*

#+begin_quote
  The limit on the total number of open files in system has been reached
  and it wasn't possible to create a new context.
#+end_quote

* SEE ALSO
*zmq*(7) *zmq_ctx_set*(3) *zmq_ctx_get*(3) *zmq_ctx_term*(3)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
