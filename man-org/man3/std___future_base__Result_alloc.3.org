#+TITLE: Manpages - std___future_base__Result_alloc.3
#+DESCRIPTION: Linux manpage for std___future_base__Result_alloc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__future_base::_Result_alloc< _Res, _Alloc > - A result object that
uses an allocator.

* SYNOPSIS
\\

Inherits *std::__future_base::_Result< _Res >*, and _Alloc.

** Public Types
using *__allocator_type* = __alloc_rebind< _Alloc, *_Result_alloc* >\\

typedef _Res *result_type*\\

** Public Member Functions
*_Result_alloc* (const _Alloc &__a)\\

void *_M_set* (_Res &&__res)\\

void *_M_set* (const _Res &__res)\\

_Res & *_M_value* () noexcept\\

** Public Attributes
*exception_ptr* *_M_error*\\

* Detailed Description
** "template<typename _Res, typename _Alloc>
\\
struct std::__future_base::_Result_alloc< _Res, _Alloc >"A result object
that uses an allocator.

Definition at line *267* of file *future*.

* Member Typedef Documentation
** template<typename _Res , typename _Alloc > using
*std::__future_base::_Result_alloc*< _Res, _Alloc >::__allocator_type =
__alloc_rebind<_Alloc, *_Result_alloc*>
Definition at line *269* of file *future*.

** template<typename _Res > typedef _Res *std::__future_base::_Result*<
_Res >::result_type= [inherited]=
Definition at line *233* of file *future*.

* Constructor & Destructor Documentation
** template<typename _Res , typename _Alloc >
*std::__future_base::_Result_alloc*< _Res, _Alloc >::*_Result_alloc*
(const _Alloc & __a)= [inline]=, = [explicit]=
Definition at line *272* of file *future*.

* Member Function Documentation
** template<typename _Res > void *std::__future_base::_Result*< _Res
>::_M_set (_Res && __res)= [inline]=, = [inherited]=
Definition at line *255* of file *future*.

** template<typename _Res > void *std::__future_base::_Result*< _Res
>::_M_set (const _Res & __res)= [inline]=, = [inherited]=
Definition at line *248* of file *future*.

** template<typename _Res > _Res & *std::__future_base::_Result*< _Res
>::_M_value ()= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *245* of file *future*.

* Member Data Documentation
** *exception_ptr*
std::__future_base::_Result_base::_M_error= [inherited]=
Definition at line *202* of file *future*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
