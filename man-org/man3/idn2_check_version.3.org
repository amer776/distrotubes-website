#+TITLE: Manpages - idn2_check_version.3
#+DESCRIPTION: Linux manpage for idn2_check_version.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idn2_check_version - API function

* SYNOPSIS
*#include <idn2.h>*

*const char * idn2_check_version(const char * */req_version/*);*

* ARGUMENTS
- const char * req_version :: version string to compare with, or NULL.

* DESCRIPTION
Check IDN2 library version. This function can also be used to read out
the version of the library code used. See *IDN2_VERSION* for a suitable
/req_version/ string, it corresponds to the idn2.h header file version.
Normally these two version numbers match, but if you are using an
application built against an older libidn2 with a newer libidn2 shared
library they will be different.

Return value: Check that the version of the library is at minimum the
one given as a string in /req_version/ and return the actual version
string of the library; return NULL if the condition is not met. If NULL
is passed to this function no check is done and only the version string
is returned.

* SEE ALSO
The full documentation for *libidn2* is maintained as a Texinfo manual.
If the *info* and *libidn2* programs are properly installed at your
site, the command

#+begin_quote
  *info libidn2*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libidn/libidn2/manual/*
#+end_quote
