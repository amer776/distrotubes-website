#+TITLE: Manpages - dane_cert_usage_name.3
#+DESCRIPTION: Linux manpage for dane_cert_usage_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dane_cert_usage_name - API function

* SYNOPSIS
*#include <gnutls/dane.h>*

*const char * dane_cert_usage_name(dane_cert_usage_t */usage/*);*

* ARGUMENTS
- dane_cert_usage_t usage :: is a DANE certificate usage

* DESCRIPTION
Convert a *dane_cert_usage_t* value to a string.

* RETURNS
a string that contains the name of the specified type, or *NULL*.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
