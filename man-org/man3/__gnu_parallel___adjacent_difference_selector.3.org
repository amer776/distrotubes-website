#+TITLE: Manpages - __gnu_parallel___adjacent_difference_selector.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___adjacent_difference_selector.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__adjacent_difference_selector< _It > - Selector that
returns the difference between two adjacent __elements.

* SYNOPSIS
\\

=#include <for_each_selectors.h>=

Inherits *__gnu_parallel::__generic_for_each_selector< _It >*.

** Public Member Functions
template<typename _Op > bool *operator()* (_Op &__o, _It __i)\\

** Public Attributes
_It *_M_finish_iterator*\\
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

* Detailed Description
** "template<typename _It>
\\
struct __gnu_parallel::__adjacent_difference_selector< _It >"Selector
that returns the difference between two adjacent __elements.

Definition at line *269* of file *for_each_selectors.h*.

* Member Function Documentation
** template<typename _It > template<typename _Op > bool
*__gnu_parallel::__adjacent_difference_selector*< _It >::operator() (_Op
& __o, _It __i)= [inline]=
Definition at line *274* of file *for_each_selectors.h*.

* Member Data Documentation
** template<typename _It > _It
*__gnu_parallel::__generic_for_each_selector*< _It
>::_M_finish_iterator= [inherited]=
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

Definition at line *47* of file *for_each_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
