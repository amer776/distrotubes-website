#+TITLE: Manpages - CURLINFO_REDIRECT_TIME.3
#+DESCRIPTION: Linux manpage for CURLINFO_REDIRECT_TIME.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_REDIRECT_TIME - get the time for all redirection steps

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REDIRECT_TIME, double
*timep);

* DESCRIPTION
Pass a pointer to a double to receive the total time, in seconds, it
took for all redirection steps include name lookup, connect, pretransfer
and transfer before final transaction was started.
CURLINFO_REDIRECT_TIME contains the complete execution time for multiple
redirections.

See also the TIMES overview in the /curl_easy_getinfo(3)/ man page.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  curl = curl_easy_init();
  if(curl) {
    double redirect;
    curl_easy_setopt(curl, CURLOPT_URL, url);
    res = curl_easy_perform(curl);
    if(CURLE_OK == res) {
      res = curl_easy_getinfo(curl, CURLINFO_REDIRECT_TIME, &redirect);
      if(CURLE_OK == res) {
        printf("Time: %.1f", redirect);
      }
    }
    /* always cleanup */
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.9.7

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_REDIRECT_TIME_T*(3)
