#+TITLE: Manpages - gnutls_x509_crq_set_key_usage.3
#+DESCRIPTION: Linux manpage for gnutls_x509_crq_set_key_usage.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_x509_crq_set_key_usage - API function

* SYNOPSIS
*#include <gnutls/x509.h>*

*int gnutls_x509_crq_set_key_usage(gnutls_x509_crq_t */crq/*, unsigned
int */usage/*);*

* ARGUMENTS
- gnutls_x509_crq_t crq :: a certificate request of type
  *gnutls_x509_crq_t*

- unsigned int usage :: an ORed sequence of the GNUTLS_KEY_* elements.

* DESCRIPTION
This function will set the keyUsage certificate extension.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
2.8.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
