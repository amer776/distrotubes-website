#+TITLE: Manpages - xcb_xfixes_change_cursor_by_name.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_change_cursor_by_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xfixes_change_cursor_by_name -

* SYNOPSIS
*#include <xcb/xfixes.h>*

** Request function
xcb_void_cookie_t *xcb_xfixes_change_cursor_by_name*(xcb_connection_t
*/conn/, xcb_cursor_t /src/, uint16_t /nbytes/, const char */name/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- src :: TODO: NOT YET DOCUMENTED.

- nbytes :: TODO: NOT YET DOCUMENTED.

- name :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_xfixes_change_cursor_by_name_checked/. See *xcb-requests(3)*
for details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xfixes.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
