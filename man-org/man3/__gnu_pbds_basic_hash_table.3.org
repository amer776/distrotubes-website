#+TITLE: Manpages - __gnu_pbds_basic_hash_table.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_basic_hash_table.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::basic_hash_table< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >

* SYNOPSIS
\\

=#include <assoc_container.hpp>=

Inherits detail::container_base_dispatch::type.

** Protected Member Functions
*basic_hash_table* (const *basic_hash_table* &other)\\

template<typename T0 > *basic_hash_table* (T0 t0)\\

template<typename T0 , typename T1 > *basic_hash_table* (T0 t0, T1 t1)\\

template<typename T0 , typename T1 , typename T2 > *basic_hash_table*
(T0 t0, T1 t1, T2 t2)\\

template<typename T0 , typename T1 , typename T2 , typename T3 >
*basic_hash_table* (T0 t0, T1 t1, T2 t2, T3 t3)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 > *basic_hash_table* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 , typename T5 > *basic_hash_table* (T0 t0, T1 t1, T2 t2, T3
t3, T4 t4, T5 t5)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 , typename T5 , typename T6 > *basic_hash_table* (T0 t0, T1
t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 , typename T5 , typename T6 , typename T7 >
*basic_hash_table* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7
t7)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 , typename T5 , typename T6 , typename T7 , typename T8 >
*basic_hash_table* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7
t7, T8 t8)\\

* Detailed Description
** "template<typename Key, typename Mapped, typename Hash_Fn, typename
Eq_Fn, typename Resize_Policy, bool Store_Hash, typename Tag, typename
Policy_Tl, typename _Alloc>
\\
class __gnu_pbds::basic_hash_table< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >"A hashed container
abstraction.

*Template Parameters*

#+begin_quote
  /Key/ Key type.\\
  /Mapped/ Map type.\\
  /Hash_Fn/ Hashing functor.\\
  /Eq_Fn/ Equal functor.\\
  /Resize_Policy/ Resizes hash.\\
  /Store_Hash/ Indicates whether the hash value will be stored along
  with each key.\\
  /Tag/ Instantiating data structure type, see container_tag.\\
  /Policy_TL/ Policy typelist.\\
  /_Alloc/ Allocator type.
#+end_quote

Base is dispatched at compile time via Tag, from the following choices:
cc_hash_tag, gp_hash_tag, and descendants of basic_hash_tag.

Base choices are: detail::cc_ht_map, detail::gp_ht_map

Definition at line *104* of file *assoc_container.hpp*.

* Constructor & Destructor Documentation
** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > virtual
*__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >::~*basic_hash_table*
()= [inline]=, = [virtual]=
Definition at line *111* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > *__gnu_pbds::basic_hash_table*<
Key, Mapped, Hash_Fn, Eq_Fn, Resize_Policy, Store_Hash, Tag, Policy_Tl,
_Alloc >::*basic_hash_table* ()= [inline]=, = [protected]=
Definition at line *114* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > *__gnu_pbds::basic_hash_table*<
Key, Mapped, Hash_Fn, Eq_Fn, Resize_Policy, Store_Hash, Tag, Policy_Tl,
_Alloc >::*basic_hash_table* (const *basic_hash_table*< Key, Mapped,
Hash_Fn, Eq_Fn, Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc > &
other)= [inline]=, = [protected]=
Definition at line *116* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 >
*__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >::*basic_hash_table*
(T0 t0)= [inline]=, = [protected]=
Definition at line *120* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 > *__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >::*basic_hash_table*
(T0 t0, T1 t1)= [inline]=, = [protected]=
Definition at line *123* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 , typename T2 > *__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn,
Eq_Fn, Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc
>::*basic_hash_table* (T0 t0, T1 t1, T2 t2)= [inline]=, = [protected]=
Definition at line *126* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 , typename T2 , typename T3 > *__gnu_pbds::basic_hash_table*< Key,
Mapped, Hash_Fn, Eq_Fn, Resize_Policy, Store_Hash, Tag, Policy_Tl,
_Alloc >::*basic_hash_table* (T0 t0, T1 t1, T2 t2, T3 t3)= [inline]=,
= [protected]=
Definition at line *129* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 , typename T2 , typename T3 , typename T4 >
*__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >::*basic_hash_table*
(T0 t0, T1 t1, T2 t2, T3 t3, T4 t4)= [inline]=, = [protected]=
Definition at line *133* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 , typename T2 , typename T3 , typename T4 , typename T5 >
*__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >::*basic_hash_table*
(T0 t0, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5)= [inline]=, = [protected]=
Definition at line *138* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 , typename T2 , typename T3 , typename T4 , typename T5 , typename T6
> *__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn, Eq_Fn,
Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc >::*basic_hash_table*
(T0 t0, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6)= [inline]=,
= [protected]=
Definition at line *143* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 , typename T2 , typename T3 , typename T4 , typename T5 , typename T6
, typename T7 > *__gnu_pbds::basic_hash_table*< Key, Mapped, Hash_Fn,
Eq_Fn, Resize_Policy, Store_Hash, Tag, Policy_Tl, _Alloc
>::*basic_hash_table* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6,
T7 t7)= [inline]=, = [protected]=
Definition at line *148* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Hash_Fn , typename
Eq_Fn , typename Resize_Policy , bool Store_Hash, typename Tag ,
typename Policy_Tl , typename _Alloc > template<typename T0 , typename
T1 , typename T2 , typename T3 , typename T4 , typename T5 , typename T6
, typename T7 , typename T8 > *__gnu_pbds::basic_hash_table*< Key,
Mapped, Hash_Fn, Eq_Fn, Resize_Policy, Store_Hash, Tag, Policy_Tl,
_Alloc >::*basic_hash_table* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5,
T6 t6, T7 t7, T8 t8)= [inline]=, = [protected]=
Definition at line *153* of file *assoc_container.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
