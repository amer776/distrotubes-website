#+TITLE: Manpages - SDL_DisplayYUVOverlay.3
#+DESCRIPTION: Linux manpage for SDL_DisplayYUVOverlay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_DisplayYUVOverlay - Blit the overlay to the display

* SYNOPSIS
*#include "SDL.h"*

*int SDL_DisplayYUVOverlay*(*SDL_Overlay *overlay, SDL_Rect *dstrect*);

* DESCRIPTION
Blit the *overlay* to the surface specified when it was /created/. The
*SDL_Rect* structure, *dstrect*, specifies the position and size of the
destination. If the *dstrect* is a larger or smaller than the overlay
then the overlay will be scaled, this is optimized for 2x scaling.

* RETURN VALUES
Returns 0 on success

* SEE ALSO
*SDL_Overlay*, *SDL_CreateYUVOverlay*
