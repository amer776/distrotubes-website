#+TITLE: Manpages - __gnu_pbds_sample_range_hashing.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_sample_range_hashing.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::sample_range_hashing - A sample range-hashing functor.

* SYNOPSIS
\\

=#include <sample_range_hashing.hpp>=

** Public Types
typedef std::size_t *size_type*\\
Size type.

** Public Member Functions
*sample_range_hashing* ()\\
Default constructor.

*sample_range_hashing* (const *sample_range_hashing* &other)\\
Copy constructor.

void *swap* (*sample_range_hashing* &other)\\
Swaps content.

** Protected Member Functions
void *notify_resized* (*size_type*)\\
Notifies the policy object that the container's size has changed to
argument's size.

*size_type* *operator()* (*size_type*) const\\
Transforms the __hash value hash into a ranged-hash value.

* Detailed Description
A sample range-hashing functor.

Definition at line *47* of file *sample_range_hashing.hpp*.

* Member Typedef Documentation
** typedef std::size_t *__gnu_pbds::sample_range_hashing::size_type*
Size type.

Definition at line *51* of file *sample_range_hashing.hpp*.

* Constructor & Destructor Documentation
** __gnu_pbds::sample_range_hashing::sample_range_hashing ()
Default constructor.

** __gnu_pbds::sample_range_hashing::sample_range_hashing (const
*sample_range_hashing* & other)
Copy constructor.

* Member Function Documentation
** void __gnu_pbds::sample_range_hashing::notify_resized
(*size_type*)= [protected]=
Notifies the policy object that the container's size has changed to
argument's size.

** *size_type* __gnu_pbds::sample_range_hashing::operator()
(*size_type*) const= [inline]=, = [protected]=
Transforms the __hash value hash into a ranged-hash value.

** void __gnu_pbds::sample_range_hashing::swap (*sample_range_hashing* &
other)= [inline]=
Swaps content.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
