#+TITLE: Manpages - XConvertSelection.3
#+DESCRIPTION: Linux manpage for XConvertSelection.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XConvertSelection.3 is found in manpage for: [[../man3/XSetSelectionOwner.3][man3/XSetSelectionOwner.3]]