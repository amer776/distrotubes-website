#+TITLE: Manpages - std___detail__Hash_node.3
#+DESCRIPTION: Linux manpage for std___detail__Hash_node.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Hash_node< _Value, _Cache_hash_code >

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherits *std::__detail::_Hash_node_base*, and
std::__detail::_Hash_node_value< _Value, _Cache_hash_code >.

** Public Types
typedef _Value *value_type*\\

** Public Member Functions
*_Hash_node* * *_M_next* () const noexcept\\

const _Value & *_M_v* () const noexcept\\

_Value & *_M_v* () noexcept\\

const _Value * *_M_valptr* () const noexcept\\

_Value * *_M_valptr* () noexcept\\

** Public Attributes
*_Hash_node_base* * *_M_nxt*\\

__gnu_cxx::__aligned_buffer< _Value > *_M_storage*\\

* Detailed Description
** "template<typename _Value, bool _Cache_hash_code>
\\
struct std::__detail::_Hash_node< _Value, _Cache_hash_code >"Primary
template struct _Hash_node.

Definition at line *276* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _Value > typedef _Value
*std::__detail::_Hash_node_value_base*< _Value
>::value_type= [inherited]=
Definition at line *231* of file *hashtable_policy.h*.

* Member Function Documentation
** template<typename _Value , bool _Cache_hash_code> *_Hash_node* *
*std::__detail::_Hash_node*< _Value, _Cache_hash_code >::_M_next ()
const= [inline]=, = [noexcept]=
Definition at line *281* of file *hashtable_policy.h*.

** template<typename _Value > const _Value &
*std::__detail::_Hash_node_value_base*< _Value >::_M_v ()
const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *248* of file *hashtable_policy.h*.

** template<typename _Value > _Value &
*std::__detail::_Hash_node_value_base*< _Value >::_M_v ()= [inline]=,
= [noexcept]=, = [inherited]=
Definition at line *244* of file *hashtable_policy.h*.

** template<typename _Value > const _Value *
*std::__detail::_Hash_node_value_base*< _Value >::_M_valptr ()
const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *240* of file *hashtable_policy.h*.

** template<typename _Value > _Value *
*std::__detail::_Hash_node_value_base*< _Value >::_M_valptr
()= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *236* of file *hashtable_policy.h*.

* Member Data Documentation
** *_Hash_node_base**
std::__detail::_Hash_node_base::_M_nxt= [inherited]=
Definition at line *216* of file *hashtable_policy.h*.

** template<typename _Value > __gnu_cxx::__aligned_buffer<_Value>
*std::__detail::_Hash_node_value_base*< _Value
>::_M_storage= [inherited]=
Definition at line *233* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
