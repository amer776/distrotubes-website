#+TITLE: Manpages - afQueryPointer.3
#+DESCRIPTION: Linux manpage for afQueryPointer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about afQueryPointer.3 is found in manpage for: [[../afQuery.3][afQuery.3]]