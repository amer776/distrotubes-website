#+TITLE: Manpages - FcStrCopyFilename.3
#+DESCRIPTION: Linux manpage for FcStrCopyFilename.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrCopyFilename - create a complete path from a filename

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcStrCopyFilename (const FcChar8 */s/*);*

* DESCRIPTION
*FcStrCopyFilename* constructs an absolute pathname from /s/. It
converts any leading '~' characters in to the value of the HOME
environment variable, and any relative paths are converted to absolute
paths using the current working directory. Sequences of '/' characters
are converted to a single '/', and names containing the current
directory '.' or parent directory '..' are correctly reconstructed.
Returns NULL if '~' is the leading character and HOME is unset or
disabled (see *FcConfigEnableHome*).
