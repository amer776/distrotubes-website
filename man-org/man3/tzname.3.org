#+TITLE: Manpages - tzname.3
#+DESCRIPTION: Linux manpage for tzname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about tzname.3 is found in manpage for: [[../man3/tzset.3][man3/tzset.3]]