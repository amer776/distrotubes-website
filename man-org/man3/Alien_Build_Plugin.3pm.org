#+TITLE: Manpages - Alien_Build_Plugin.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin - Plugin base class for Alien::Build

* VERSION
version 2.44

* SYNOPSIS
Create your plugin:

package Alien::Build::Plugin::Type::MyPlugin; use Alien::Build::Plugin;
use Carp (); has prop1 => default value; has prop2 => sub { default
value }; has prop3 => sub { Carp::croak prop3 is a required property };
sub init { my($self, $meta) = @_; my $prop1 = $self->prop1; my $prop2 =
$self->prop2; my $prop3 = $self->prop3; $meta->register_hook(sub { build
=> [ %{make}, %{make} install ], }); }

From your alienfile

use alienfile; plugin Type::MyPlugin => ( prop2 => different value,
prop3 => need to provide since it is required, );

* DESCRIPTION
This document describes the Alien::Build plugin base class. For details
on how to write a plugin, see Alien::Build::Manual::PluginAuthor.

Listed are some common types of plugins:

- Alien::Build::Plugin::Build :: Tools for building.

- Alien::Build::Plugin::Core :: Tools already included.

- Alien::Build::Plugin::Download :: Methods for retrieving from the
  internet.

- Alien::Build::Plugin::Decode :: Normally use Download plugins which
  will pick the correct Decode plugins.

- Alien::Build::Plugin::Extract :: Extract from archives that have been
  downloaded.

- Alien::Build::Plugin::Fetch :: Normally use Download plugins which
  will pick the correct Fetch plugins.

- Alien::Build::Plugin::Prefer :: Normally use Download plugins which
  will pick the correct Prefer plugins.

- Alien::Build::Plugin::Probe :: Look for packages already installed on
  the system.

* CONSTRUCTOR
** new
my $plugin = Alien::Build::Plugin->new(%props);

** PROPERTIES
** instance_id
my $id = $plugin->instance_id;

Returns an instance id for the plugin. This is computed from the class
and arguments that are passed into the plugin constructor, so
technically two instances with the exact same arguments will have the
same instance id, but in practice you should never have two instances
with the exact same arguments.

* METHODS
** init
$plugin->init($ab_class->meta); # $ab is an Alien::Build class name

You provide the implementation for this. The intent is to register hooks
and set meta properties on the Alien::Build class.

** subplugin
*DEPRECATED*: Maybe removed, but not before 1 October 2018.

my $plugin2 = $plugin1->subplugin($plugin_name, %args);

Finds the given plugin and loads it (unless already loaded) and creats a
new instance and returns it. Most useful from a Negotiate plugin, like
this:

sub init { my($self, $meta) = @_; $self->subplugin( Foo::Bar, # loads
Alien::Build::Plugin::Foo::Bar, # or throw exception foo => 1, # these
key/value pairs passsed into new bar => 2, # for the plugin instance.
)->init($meta); }

** has
has $prop_name; has $prop_name => $default;

Specifies a property of the plugin. You may provide a default value as
either a string scalar, or a code reference. The code reference will be
called to compute the default value, and if you want the default to be a
list or hash reference, this is how you want to do it:

has foo => sub { [1,2,3] };

** meta
my $meta = $plugin->meta;

Returns the plugin meta object.

* SEE ALSO
Alien::Build, alienfile, Alien::Build::Manual::PluginAuthor

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
