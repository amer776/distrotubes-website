#+TITLE: Manpages - unibi_terminfo_dirs.3
#+DESCRIPTION: Linux manpage for unibi_terminfo_dirs.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_terminfo_dirs - fallback terminfo search path

* SYNOPSIS
#include <unibilium.h> extern const char *const unibi_terminfo_dirs;

* DESCRIPTION
A colon-separated list of directories that is used by =unibi_from_term=
if =TERMINFO_DIRS= is not set.

* SEE ALSO
*unibilium.h* (3), *unibi_from_term* (3)
