#+TITLE: Manpages - ptsname.3
#+DESCRIPTION: Linux manpage for ptsname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ptsname, ptsname_r - get the name of the slave pseudoterminal

* SYNOPSIS
#+begin_example
  #include <stdlib.h>

  char *ptsname(int fd);
  int ptsname_r(int fd, char *buf, size_t buflen);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*ptsname*():

#+begin_example
      Since glibc 2.24:
          _XOPEN_SOURCE >= 500
      Glibc 2.23 and earlier:
          _XOPEN_SOURCE
#+end_example

*ptsname_r*():

#+begin_example
      _GNU_SOURCE
#+end_example

* DESCRIPTION
The *ptsname*() function returns the name of the slave pseudoterminal
device corresponding to the master referred to by the file descriptor
/fd/.

The *ptsname_r*() function is the reentrant equivalent of *ptsname*().
It returns the name of the slave pseudoterminal device as a
null-terminated string in the buffer pointed to by /buf/. The /buflen/
argument specifies the number of bytes available in /buf/.

* RETURN VALUE
On success, *ptsname*() returns a pointer to a string in static storage
which will be overwritten by subsequent calls. This pointer must not be
freed. On failure, NULL is returned.

On success, *ptsname_r*() returns 0. On failure, an error number is
returned to indicate the error.

* ERRORS
- *EINVAL* :: (*ptsname_r*() only) /buf/ is NULL. (This error is
  returned only for glibc 2.25 and earlier.)

- *ENOTTY* :: /fd/ does not refer to a pseudoterminal master device.

- *ERANGE* :: (*ptsname_r*() only) /buf/ is too small.

* VERSIONS
*ptsname*() is provided in glibc since version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface     | Attribute     | Value                  |
| *ptsname*()   | Thread safety | MT-Unsafe race:ptsname |
| *ptsname_r*() | Thread safety | MT-Safe                |

* CONFORMING TO
*ptsname*(): POSIX.1-2001, POSIX.1-2008.

*ptsname*() is part of the UNIX 98 pseudoterminal support (see
*pts*(4)).

*ptsname_r*() is a Linux extension, that is proposed for inclusion in
the next major revision of POSIX.1 (Issue 8). A version of this function
is documented on Tru64 and HP-UX, but on those implementations, -1 is
returned on error, with /errno/ set to indicate the error. Avoid using
this function in portable programs.

* SEE ALSO
*grantpt*(3), *posix_openpt*(3), *ttyname*(3), *unlockpt*(3), *pts*(4),
*pty*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
