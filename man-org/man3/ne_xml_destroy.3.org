#+TITLE: Manpages - ne_xml_destroy.3
#+DESCRIPTION: Linux manpage for ne_xml_destroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ne_xml_destroy.3 is found in manpage for: [[../ne_xml_create.3][ne_xml_create.3]]