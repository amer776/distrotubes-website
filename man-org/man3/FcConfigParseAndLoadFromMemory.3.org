#+TITLE: Manpages - FcConfigParseAndLoadFromMemory.3
#+DESCRIPTION: Linux manpage for FcConfigParseAndLoadFromMemory.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigParseAndLoadFromMemory - load a configuration from memory

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigParseAndLoadFromMemory (FcConfig */config/*, const
FcChar8 **/buffer/*, FcBool */complain/*);*

* DESCRIPTION
Walks the configuration in 'memory' and constructs the internal
representation in 'config'. Any includes files referenced from within
'memory' will be loaded and dparsed. If 'complain' is FcFalse, no
warning will be displayed if 'file' does not exist. Error and warning
messages will be output to stderr. Returns FcFalse if fsome error
occurred while loading the file, either a parse error, semantic error or
allocation failure. Otherwise returns FcTrue.

* SINCE
version 2.12.5
