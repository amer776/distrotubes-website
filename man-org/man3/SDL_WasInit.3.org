#+TITLE: Manpages - SDL_WasInit.3
#+DESCRIPTION: Linux manpage for SDL_WasInit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WasInit - Check which subsystems are initialized

* SYNOPSIS
*#include "SDL.h"*

*Uint32 SDL_WasInit*(*Uint32 flags*);

* DESCRIPTION
*SDL_WasInit* allows you to see which SDL subsytems have been
/initialized/. *flags* is a bitwise OR'd combination of the subsystems
you wish to check (see *SDL_Init* for a list of subsystem flags).

* RETURN VALUE
*SDL_WasInit* returns a bitwised OR'd combination of the initialized
subsystems.

* EXAMPLES
#+begin_example
  /* Here are several ways you can use SDL_WasInit() */

  /* Get init data on all the subsystems */
  Uint32 subsystem_init;

  subsystem_init=SDL_WasInit(SDL_INIT_EVERYTHING);

  if(subsystem_init&SDL_INIT_VIDEO)
    printf("Video is initialized.
  ");
  else
    printf("Video is not initialized.
  ");



  /* Just check for one specfic subsystem */

  if(SDL_WasInit(SDL_INIT_VIDEO)!=0)
    printf("Video is initialized.
  ");
  else
    printf("Video is not initialized.
  ");




  /* Check for two subsystems */

  Uint32 subsystem_mask=SDL_INIT_VIDEO|SDL_INIT_AUDIO;

  if(SDL_WasInit(subsystem_mask)==subsystem_mask)
    printf("Video and Audio initialized.
  ");
  else
    printf("Video and Audio not initialized.
  ");
#+end_example

* SEE ALSO
*SDL_Init*, *SDL_Subsystem*
