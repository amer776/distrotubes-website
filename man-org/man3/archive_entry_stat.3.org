#+TITLE: Manpages - archive_entry_stat.3
#+DESCRIPTION: Linux manpage for archive_entry_stat.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

The function

converts the various fields stored in the archive entry to the format
used by

The return value remains valid until either

or

is called. It is not affected by calls to the set accessor functions. It
currently sets the following values in

In addition,

and high-precision information for time-related fields will be included
on platforms that support it.

The function

copies fields from the platform's

Fields not provided by

are unchanged.

The functions

and

get respectively set the filetype. The file type is one of the following
constants:

Regular file

Symbolic link

Socket

Character device

Block device

Directory

Named pipe (fifo)

Not all file types are supported by all platforms. The constants used by

may have different numeric values from the corresponding constants
above.

The functions

and

get/set a combination of file type and permissions and provide the
equivalent of

Use of

and

for getting and

and

for setting is recommended.

The function

returns the file size, if it has been set, and 0 otherwise.

can be used to query that status.

and

set and unset the size, respectively.

The number of references (hardlinks) can be obtained by calling

and set with

The functions

and

are used by

to find hardlinks. The pair of device and inode is supposed to identify
hardlinked files.

The device major and minor number can be obtained independently using

and

The device can be set either via

or by the combination of major and minor number using

and

The inode number can be obtained using

This is a legacy interface that uses the platform

which may be very small. To set the inode number,

is the preferred interface.

Block and character devices are characterised either using a device
number or a pair of major and minor number. The combined device number
can be obtained with

and set with

The major and minor numbers are accessed by

and

The process of splitting the combined device number into major and minor
number and the reverse process of combing them differs between
platforms. Some archive formats use the combined form, while other
formats use the split form.
