#+TITLE: Manpages - std__Enable_destructor.3
#+DESCRIPTION: Linux manpage for std__Enable_destructor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Enable_destructor< _Switch, _Tag > - A mixin helper to
conditionally enable or disable the default destructor.

* SYNOPSIS
\\

=#include <enable_special_members.h>=

* Detailed Description
** "template<bool _Switch, typename _Tag = void>
\\
struct std::_Enable_destructor< _Switch, _Tag >"A mixin helper to
conditionally enable or disable the default destructor.

*See also*

#+begin_quote
  _Enable_special_members
#+end_quote

Definition at line *76* of file *enable_special_members.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
