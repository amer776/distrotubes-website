#+TITLE: Manpages - XCloseDevice.3
#+DESCRIPTION: Linux manpage for XCloseDevice.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XCloseDevice.3 is found in manpage for: [[../man3/XOpenDevice.3][man3/XOpenDevice.3]]