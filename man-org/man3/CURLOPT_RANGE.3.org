#+TITLE: Manpages - CURLOPT_RANGE.3
#+DESCRIPTION: Linux manpage for CURLOPT_RANGE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_RANGE - byte range to request

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RANGE, char *range);

* DESCRIPTION
Pass a char * as parameter, which should contain the specified range you
want to retrieve. It should be in the format "X-Y", where either X or Y
may be left out and X and Y are byte indexes.

HTTP transfers also support several intervals, separated with commas as
in /"X-Y,N-M"/. Using this kind of multiple intervals will cause the
HTTP server to send the response document in pieces (using standard MIME
separation techniques). Unfortunately, the HTTP standard (RFC 7233
section 3.1) allows servers to ignore range requests so even when you
set /CURLOPT_RANGE(3)/ for a request, you may end up getting the full
response sent back.

For RTSP, the formatting of a range should follow RFC2326 Section 12.29.
For RTSP, byte ranges are *not* permitted. Instead, ranges should be
given in npt, utc, or smpte formats.

For HTTP PUT uploads this option should not be used, since it may
conflict with other options. If you need to upload arbitrary parts of a
file (like for Amazon's web services) support is limited. We suggest set
resume position using /CURLOPT_RESUME_FROM(3)/, set end (resume+size)
position using /CURLOPT_INFILESIZE(3)/ and seek to the resume position
before initiating the transfer for each part. For more information refer
to https://curl.se/mail/lib-2019-05/0012.html

Pass a NULL to this option to disable the use of ranges.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
HTTP, FTP, FILE, RTSP and SFTP.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* get the first 200 bytes */
    curl_easy_setopt(curl, CURLOPT_RANGE, "0-199");

    /* Perform the request */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
FILE since 7.18.0, RTSP since 7.20.0

* RETURN VALUE
Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was
insufficient heap space.

* SEE ALSO
*CURLOPT_RESUME_FROM*(3),
