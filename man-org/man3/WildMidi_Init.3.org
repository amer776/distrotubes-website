#+TITLE: Manpages - WildMidi_Init.3
#+DESCRIPTION: Linux manpage for WildMidi_Init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
WildMidi_Init - Initialize the library

* LIBRARY
*libWildMidi*

* SYNOPSIS
*#include <wildmidi_lib.h>*

*WildMidi_Init (const char */config_file/, uint16_t /rate/, uint16_t
/options/)*

* DESCRIPTION
Initializes libWildMidi in preparation for playback. This function only
needs to be called once by the program using libWildMidi.

- config-file :: The file that contains the instrument configuration for
  the library.

- rate :: The sound rate you want the the audio data output at. Rates
  accepted by libWildMidi are 11025 - 65000.

- options :: The initial options to set for the library. see below.

  - WM_MO_LOG_VOLUME :: By default the library uses linear volume levels
    typically used in computer MIDI players. These can differ somewhat
    to volume levels found on some midi hardware which may use a volume
    curve based on decibels. This option sets the volume levels to what
    you'd expect on such devices.

  - WM_MO_ENHANCED_RESAMPLING :: By default libWildMidi uses linear
    interpolation for the resampling of the sound samples. Setting this
    option enables the library to use a resampling method that attempts
    to fill in the gaps giving richer sound.

  - WM_MO_REVERB :: libWildMidi has an 8 reflection reverb engine. Use
    this option to give more depth to the output.

  - WM_MO_WHOLETEMPO :: Ignores the fractional or decimal part of a
    tempo setting. If you are having timing issues try
    /WM_MO_ROUNDTEMPO/ before trying this option. This option added due
    to some software not supporting fractional tempos allowable in the
    MIDI specification.

  - WM_MO_ROUNDTEMPO :: Rounds the fractional or decimal part of a tempo
    setting. Try this option is you are having timing issues, if this
    fails then try /WM_MO_WHOLETEMPO/. This option added due to some
    software not supporting fractional tempos allowable in the MIDI
    specification.

* SEE ALSO
*WildMidi_GetVersion*(3)*,* *WildMidi_MasterVolume*(3)*,*
*WildMidi_Open*(3)*,* *WildMidi_OpenBuffer*(3)*,*
*WildMidi_SetOption*(3)*,* *WildMidi_GetOutput*(3)*,*
*WildMidi_GetMidiOutput*(3)*,* *WildMidi_GetInfo*(3)*,*
*WildMidi_FastSeek*(3)*,* *WildMidi_Close*(3)*,*
*WildMidi_Shutdown*(3)*,* *wildmidi.cfg*(5)

* AUTHOR
Chris Ison <chrisisonwildcode@gmail.com> Bret Curtis <psi29a@gmail.com>

* COPYRIGHT
Copyright (C) WildMidi Developers 2001-2016

This file is part of WildMIDI.

WildMIDI is free software: you can redistribute and/or modify the player
under the terms of the GNU General Public License and you can
redistribute and/or modify the library under the terms of the GNU Lesser
General Public License as published by the Free Software Foundation,
either version 3 of the licenses, or(at your option) any later version.

WildMIDI is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License and
the GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License and
the GNU Lesser General Public License along with WildMIDI. If not, see
<http://www.gnu.org/licenses/>.

This manpage is licensed under the Creative Commons Attribution-Share
Alike 3.0 Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California, 94105, USA.
