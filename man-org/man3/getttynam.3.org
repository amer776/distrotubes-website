#+TITLE: Manpages - getttynam.3
#+DESCRIPTION: Linux manpage for getttynam.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getttynam.3 is found in manpage for: [[../man3/getttyent.3][man3/getttyent.3]]