#+TITLE: Manpages - XAllocNamedColor.3
#+DESCRIPTION: Linux manpage for XAllocNamedColor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XAllocNamedColor.3 is found in manpage for: [[../man3/XAllocColor.3][man3/XAllocColor.3]]