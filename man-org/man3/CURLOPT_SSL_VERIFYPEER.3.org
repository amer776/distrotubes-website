#+TITLE: Manpages - CURLOPT_SSL_VERIFYPEER.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSL_VERIFYPEER.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSL_VERIFYPEER - verify the peer's SSL certificate

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_VERIFYPEER, long
verify);

* DESCRIPTION
Pass a long as parameter to enable or disable.

This option determines whether curl verifies the authenticity of the
peer's certificate. A value of 1 means curl verifies; 0 (zero) means it
does not.

When negotiating a TLS or SSL connection, the server sends a certificate
indicating its identity. Curl verifies whether the certificate is
authentic, i.e. that you can trust that the server is who the
certificate says it is. This trust is based on a chain of digital
signatures, rooted in certification authority (CA) certificates you
supply. curl uses a default bundle of CA certificates (the path for that
is determined at build time) and you can specify alternate certificates
with the /CURLOPT_CAINFO(3)/ option or the /CURLOPT_CAPATH(3)/ option.

When /CURLOPT_SSL_VERIFYPEER(3)/ is enabled, and the verification fails
to prove that the certificate is authentic, the connection fails. When
the option is zero, the peer certificate verification succeeds
regardless.

Authenticating the certificate is not enough to be sure about the
server. You typically also want to ensure that the server is the server
you mean to be talking to. Use /CURLOPT_SSL_VERIFYHOST(3)/ for that. The
check that the host name in the certificate is valid for the host name
you are connecting to is done independently of the
/CURLOPT_SSL_VERIFYPEER(3)/ option.

WARNING: disabling verification of the certificate allows bad guys to
man-in-the-middle the communication without you knowing it. Disabling
verification makes the communication insecure. Just having encryption on
a transfer is not enough as you cannot be sure that you are
communicating with the correct end-point.

NOTE: even when this option is disabled, depending on the used TLS
backend, curl may still load the certificate file specified in
/CURLOPT_CAINFO(3)/. curl default settings in some distributions might
use quite a large file as a default setting for /CURLOPT_CAINFO(3)/, so
loading the file can be quite expensive, especially when dealing with
many connections. Thus, in some situations, you might want to disable
verification fully to save resources by setting /CURLOPT_CAINFO(3)/ to
NULL - but please also consider the warning above!

* DEFAULT
By default, curl assumes a value of 1.

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* Set the default value: strict certificate check please */
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
If built TLS enabled.

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_SSL_VERIFYHOST*(3), *CURLOPT_PROXY_SSL_VERIFYPEER*(3),
*CURLOPT_PROXY_SSL_VERIFYHOST*(3), *CURLOPT_CAINFO*(3),
