#+TITLE: Manpages - rpc_svc_calls.3t
#+DESCRIPTION: Linux manpage for rpc_svc_calls.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines are part of the RPC library which allows C language
programs to make procedure calls on other machines across the network.

These routines are associated with the server side of the RPC mechanism.
Some of them are called by the server side dispatch function, while
others (such as

are called when the server is initiated.

See

for the definition of the

data structure.

This function allocates a duplicate request cache for the service
endpoint

large enough to hold

entries. Once enabled, there is no way to disable caching. This routine
returns 0 if space necessary for a cache of the given size was
successfully allocated, and 1 otherwise.

This function, when called by any of the RPC server procedure or
otherwise, causes

to return.

As currently implemented,

zeroes the

global variable. If RPC server activity is to be resumed, services must
be reregistered with the RPC library either through one of the

functions, or using

The

function has global scope and ends all RPC server activity.

A global variable reflecting the RPC server's read file descriptor bit
mask; it is suitable as an argument to the

system call. This is only of interest if service implementors do not
call

but rather do their own asynchronous event processing. This variable is
read-only (do not pass its address to

yet it may change after calls to

or any creation routines.

A function macro that frees any data allocated by the RPC/XDR system
when it decoded the arguments to a service procedure using

This routine returns

if the results were successfully freed, and

otherwise.

A function macro that decodes the arguments of an RPC request associated
with the RPC service transport handle

The

argument is the address where the arguments will be placed;

is the XDR routine used to decode the arguments. This routine returns

if decoding succeeds, and

otherwise.

This routine is called to handle a request on the given file descriptor.

This routine is only of interest if a service implementor does not call

but instead implements custom asynchronous event processing. It is
called when

has determined that an RPC request has arrived on some RPC file
descriptors;

is the return value from

and

is the array of

structures on which the

was done. It is assumed to be an array large enough to contain the
maximal number of descriptors allowed.

This routine is only of interest if a service implementor does not call

but instead implements custom asynchronous event processing. It is
called when

has determined that an RPC request has arrived on some RPC file
descriptors;

is the resultant read file descriptor bit mask. The routine returns when
all file descriptors associated with the value of

have been serviced.

The approved way of getting the network address of the caller of a
procedure associated with the RPC service transport handle

this macro is specific to

and thus not portable. This macro returns a pointer to a

structure, defined in

identifying the calling client. This only works if the client is calling
the server over an

socket.

is an array of

structures derived from

It is suitable as an argument to the

system call. The derivation of

from

is made in the current implementation in

Service implementors who do not call

and who wish to use this array must perform this derivation themselves.

This routine never returns. It waits for RPC requests to arrive, and
calls the appropriate service procedure using

when one arrives. This procedure is usually waiting for the

system call to return.

Called by an RPC service's dispatch routine to send the results of a
remote procedure call. The

argument is the request's associated transport handle;

is the XDR routine which is used to encode the results; and

is the address of the results. This routine returns

if it succeeds,

otherwise.

These functions are part of libtirpc.
