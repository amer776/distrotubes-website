#+TITLE: Manpages - MPIX_Query_cuda_support.3
#+DESCRIPTION: Linux manpage for MPIX_Query_cuda_support.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPIX_Query_cuda_support* - Returns 1 if there is CUDA aware support and
0 if there is not.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  #include <mpi-ext.h>

  int MPIX_Query_cuda_support(void)
#+end_example

* Fortran Syntax
There is no Fortran binding for this function.

* C++ Syntax
There is no C++ binding for this function.

* DESCRIPTION
* Examples
* See Also
#+begin_example
#+end_example
