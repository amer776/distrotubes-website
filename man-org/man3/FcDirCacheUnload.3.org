#+TITLE: Manpages - FcDirCacheUnload.3
#+DESCRIPTION: Linux manpage for FcDirCacheUnload.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheUnload - unload a cache file

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcDirCacheUnload (FcCache */cache/*);*

* DESCRIPTION
This function dereferences /cache/. When no other references to it
remain, all memory associated with the cache will be freed.
