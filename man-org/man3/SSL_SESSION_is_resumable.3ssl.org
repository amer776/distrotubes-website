#+TITLE: Manpages - SSL_SESSION_is_resumable.3ssl
#+DESCRIPTION: Linux manpage for SSL_SESSION_is_resumable.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_SESSION_is_resumable - determine whether an SSL_SESSION object can
be used for resumption

* SYNOPSIS
#include <openssl/ssl.h> int SSL_SESSION_is_resumable(const SSL_SESSION
*s);

* DESCRIPTION
*SSL_SESSION_is_resumable()* determines whether an SSL_SESSION object
can be used to resume a session or not. Returns 1 if it can or 0 if not.
Note that attempting to resume with a non-resumable session will result
in a full handshake.

* RETURN VALUES
*SSL_SESSION_is_resumable()* returns 1 if the session is resumable or 0
otherwise.

* SEE ALSO
*ssl* (7), *SSL_get_session* (3), *SSL_CTX_sess_set_new_cb* (3)

* HISTORY
The *SSL_SESSION_is_resumable()* function was added in OpenSSL 1.1.1.

* COPYRIGHT
Copyright 2017-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
