#+TITLE: Manpages - bitstring.3bsd
#+DESCRIPTION: Linux manpage for bitstring.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

These macros operate on strings of bits.

The macro

returns a pointer of type

to sufficient space to store

bits, or

if no space is available.

The macro

allocates sufficient space to store

bits on the stack.

The macro

returns the number of elements of type

necessary to store

bits. This is useful for copying bit strings.

The macros

and

clear or set the zero-based numbered bit

in the bit string

The

and

macros set or clear the zero-based numbered bits from

through

in the bit string

The

macro evaluates to non-zero if the zero-based numbered bit

of bit string

is set, and zero otherwise.

The

macro stores in the location referenced by

the zero-based number of the first bit set in the array of

bits referenced by

If no bits are set, the location referenced by

is set to -1.

The macro

stores in the location referenced by

the zero-based number of the first bit not set in the array of

bits referenced by

If all bits are set, the location referenced by

is set to -1.

The arguments to these macros are evaluated only once and may safely
have side effects.

#include <limits.h> #include <bsd/bitstring.h>

... #define LPR_BUSY_BIT 0 #define LPR_FORMAT_BIT 1 #define
LPR_DOWNLOAD_BIT 2 ... #define LPR_AVAILABLE_BIT 9 #define LPR_MAX_BITS
10

make_lpr_available() { bitstr_t bit_decl(bitlist, LPR_MAX_BITS); ...
bit_nclear(bitlist, 0, LPR_MAX_BITS - 1); ... if (!bit_test(bitlist,
LPR_BUSY_BIT)) { bit_clear(bitlist, LPR_FORMAT_BIT); bit_clear(bitlist,
LPR_DOWNLOAD_BIT); bit_set(bitlist, LPR_AVAILABLE_BIT); } }

The

functions first appeared in
