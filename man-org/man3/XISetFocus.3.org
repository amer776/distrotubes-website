#+TITLE: Manpages - XISetFocus.3
#+DESCRIPTION: Linux manpage for XISetFocus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XISetFocus, XIGetFocus - set or get the devices focus.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput2.h>
#+end_example

#+begin_example
  Status XISetFocus( Display *display,
                     int deviceid,
                     Window focus,
                     Time time);
#+end_example

#+begin_example
  Status XIGetFocus( Display *display,
                     Window *focus_return);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  deviceid
         Specifies the device whose focus is to be queried or
         changed.
#+end_example

#+begin_example
  focus
         The new focus window.
#+end_example

#+begin_example
  focus_return
         Returns the current focus window.
#+end_example

#+begin_example
  time
         A valid timestamp or CurrentTime.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    XISetFocus changes the focus of the specified device and its
    last-focus-change time. It has no effect if the specified time
    is earlier than the current last-focus-change time or is later
    than the current X server time. Otherwise, the
    last-focus-change time is set to the specified time.
    CurrentTime is replaced by the current X server time).
    XISetFocus causes the X server to generate core, XI and XI2
    focus events.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If the focus window is None all keyboard events by this device
    are discarded until a new focus window is set. Otherwise, if
    focus is a window, it becomes the devices focus window. If a
    generated device event would normally be reported to this
    window or one of its inferiors, the event is reported as usual.
    Otherwise, the event is reported relative to the focus window.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The specified focus window must be viewable at the time
    XISetFocus is called, or a BadMatch error results. If the focus
    window later becomes not viewable, the focus reverts to the
    parent (or the closest viewable ancestor. When the focus
    reverts, the X server generates core, XI and XI2 focus events
    but the last-focus-change time is not affected.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    Attempting to set the focus on a master pointer device or an
    attached slave device will result in a BadDevice error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XISetFocus can generate BadDevice, BadMatch, BadValue, and
    BadWindow errors.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadValue
           A value is outside of the permitted range.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The device does not
           exist or is not a appropriate for the type of change.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           The window is not viewable.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadWindow
           A value for a Window argument does not name a defined
           Window.
  #+end_example
#+end_quote
