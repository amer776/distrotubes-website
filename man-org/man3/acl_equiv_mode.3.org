#+TITLE: Manpages - acl_equiv_mode.3
#+DESCRIPTION: Linux manpage for acl_equiv_mode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function checks if the ACL pointed to by the argument

contains only the required ACL entries of tag types ACL_USER_OBJ,
ACL_GROUP_OBJ, and ACL_OTHER, and contains no permissions other that
ACL_READ, ACL_WRITE or ACL_EXECUTE. If the ACL has this form, it can can
be fully represented with the traditional file permission bits, and is
considered equivalent with the traditional file permission bits.

If

is an equivalent ACL and the pointer

is not

the value pointed to by

is set to the value that defines the same owner, group and other
permissions as contained in the ACL.

On success, this function returns the value

if

is an equivalent ACL, and the value

if

is not an equivalent ACL. On error, the value

is returned, and

is set appropriately.

If any of the following conditions occur, the

function returns the value

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

This is a non-portable, Linux specific extension to the ACL manipulation
functions defined in IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned).

Written by
