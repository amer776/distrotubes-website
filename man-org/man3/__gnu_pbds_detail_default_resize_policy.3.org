#+TITLE: Manpages - __gnu_pbds_detail_default_resize_policy.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_default_resize_policy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::default_resize_policy< Comb_Hash_Fn > - Primary
template, default_resize_policy.

* SYNOPSIS
\\

=#include <standard_policies.hpp>=

** Public Types
typedef *hash_standard_resize_policy*< size_policy_type, *trigger*,
false, size_type > *type*\\
Dispatched type.

* Detailed Description
** "template<typename Comb_Hash_Fn>
\\
struct __gnu_pbds::detail::default_resize_policy< Comb_Hash_Fn >"Primary
template, default_resize_policy.

Definition at line *88* of file *standard_policies.hpp*.

* Member Typedef Documentation
** template<typename Comb_Hash_Fn > typedef
*hash_standard_resize_policy*<size_policy_type, *trigger*, false,
size_type> *__gnu_pbds::detail::default_resize_policy*< Comb_Hash_Fn
>::*type*
Dispatched type.

Definition at line *105* of file *standard_policies.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
