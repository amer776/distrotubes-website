#+TITLE: Manpages - Net_HTTPS.3pm
#+DESCRIPTION: Linux manpage for Net_HTTPS.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Net::HTTPS - Low-level HTTP over SSL/TLS connection (client)

* VERSION
version 6.21

* DESCRIPTION
The =Net::HTTPS= is a low-level HTTP over SSL/TLS client. The interface
is the same as the interface for =Net::HTTP=, but the constructor takes
additional parameters as accepted by IO::Socket::SSL. The =Net::HTTPS=
object is an =IO::Socket::SSL= too, which makes it inherit additional
methods from that base class.

For historical reasons this module also supports using =Net::SSL= (from
the Crypt-SSLeay distribution) as its SSL driver and base class. This
base is automatically selected if available and =IO::Socket::SSL= isn't.
You might also force which implementation to use by setting
=$Net::HTTPS::SSL_SOCKET_CLASS= before loading this module. If not set
this variable is initialized from the =PERL_NET_HTTPS_SSL_SOCKET_CLASS=
environment variable.

* ENVIRONMENT
You might set the =PERL_NET_HTTPS_SSL_SOCKET_CLASS= environment variable
to the name of the base SSL implementation (and Net::HTTPS base class)
to use. The default is =IO::Socket::SSL=. Currently the only other
supported value is =Net::SSL=.

* SEE ALSO
Net::HTTP, IO::Socket::SSL

* AUTHOR
Gisle Aas <gisle@activestate.com>

* COPYRIGHT AND LICENSE
This software is copyright (c) 2001-2017 by Gisle Aas.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
