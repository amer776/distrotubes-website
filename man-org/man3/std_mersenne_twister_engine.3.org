#+TITLE: Manpages - std_mersenne_twister_engine.3
#+DESCRIPTION: Linux manpage for std_mersenne_twister_engine.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::mersenne_twister_engine< _UIntType, __w, __n, __m, __r, __a, __u,
__d, __s, __b, __t, __c, __l, __f >

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef _UIntType *result_type*\\

** Public Member Functions
template<typename _Sseq , typename = _If_seed_seq<_Sseq>>
*mersenne_twister_engine* (_Sseq &__q)\\
Constructs a mersenne_twister_engine random number generator engine
seeded from the seed sequence =__q=.

*mersenne_twister_engine* (*result_type* __sd)\\

void *discard* (unsigned long long __z)\\
Discard a sequence of random numbers.

*result_type* *operator()* ()\\

template<typename _Sseq > _If_seed_seq< _Sseq > *seed* (_Sseq &__q)\\

template<typename _Sseq > auto *seed* (_Sseq &__q) -> _If_seed_seq<
_Sseq >\\

void *seed* (*result_type* __sd=default_seed)\\

** Static Public Member Functions
static constexpr *result_type* *max* ()\\
Gets the largest possible value in the output range.

static constexpr *result_type* *min* ()\\
Gets the smallest possible value in the output range.

** Static Public Attributes
static constexpr *result_type* *default_seed*\\

static constexpr *result_type* *initialization_multiplier*\\

static constexpr size_t *mask_bits*\\

static constexpr size_t *shift_size*\\

static constexpr size_t *state_size*\\

static constexpr *result_type* *tempering_b*\\

static constexpr *result_type* *tempering_c*\\

static constexpr *result_type* *tempering_d*\\

static constexpr size_t *tempering_l*\\

static constexpr size_t *tempering_s*\\

static constexpr size_t *tempering_t*\\

static constexpr size_t *tempering_u*\\

static constexpr size_t *word_size*\\

static constexpr *result_type* *xor_mask*\\

** Friends
template<typename _UIntType1 , size_t __w1, size_t __n1, size_t __m1,
size_t __r1, _UIntType1 __a1, size_t __u1, _UIntType1 __d1, size_t __s1,
_UIntType1 __b1, size_t __t1, _UIntType1 __c1, size_t __l1, _UIntType1
__f1, typename _CharT , typename _Traits > *std::basic_ostream*< _CharT,
_Traits > & *operator<<* (*std::basic_ostream*< _CharT, _Traits > &__os,
const *std::mersenne_twister_engine*< _UIntType1, __w1, __n1, __m1,
__r1, __a1, __u1, __d1, __s1, __b1, __t1, __c1, __l1, __f1 > &__x)\\
Inserts the current state of a % mersenne_twister_engine random number
generator engine =__x= into the output stream =__os=.

bool *operator==* (const *mersenne_twister_engine* &__lhs, const
*mersenne_twister_engine* &__rhs)\\
Compares two % mersenne_twister_engine random number generator objects
of the same type for equality.

template<typename _UIntType1 , size_t __w1, size_t __n1, size_t __m1,
size_t __r1, _UIntType1 __a1, size_t __u1, _UIntType1 __d1, size_t __s1,
_UIntType1 __b1, size_t __t1, _UIntType1 __c1, size_t __l1, _UIntType1
__f1, typename _CharT , typename _Traits > *std::basic_istream*< _CharT,
_Traits > & *operator>>* (*std::basic_istream*< _CharT, _Traits > &__is,
*std::mersenne_twister_engine*< _UIntType1, __w1, __n1, __m1, __r1,
__a1, __u1, __d1, __s1, __b1, __t1, __c1, __l1, __f1 > &__x)\\
Extracts the current state of a % mersenne_twister_engine random number
generator engine =__x= from the input stream =__is=.

* Detailed Description
** "template<typename _UIntType, size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
\\
class std::mersenne_twister_engine< _UIntType, __w, __n, __m, __r, __a,
__u, __d, __s, __b, __t, __c, __l, __f >"A generalized feedback shift
register discrete random number generator.

This algorithm avoids multiplication and division and is designed to be
friendly to a pipelined architecture. If the parameters are chosen
correctly, this generator will produce numbers with a very long period
and fairly good apparent entropy, although still not cryptographically
strong.

The best way to use this generator is with the predefined mt19937 class.

This algorithm was originally invented by Makoto Matsumoto and Takuji
Nishimura.

*Template Parameters*

#+begin_quote
  /__w/ Word size, the number of bits in each element of the state
  vector.\\
  /__n/ The degree of recursion.\\
  /__m/ The period parameter.\\
  /__r/ The separation point bit index.\\
  /__a/ The last row of the twist matrix.\\
  /__u/ The first right-shift tempering matrix parameter.\\
  /__d/ The first right-shift tempering matrix mask.\\
  /__s/ The first left-shift tempering matrix parameter.\\
  /__b/ The first left-shift tempering matrix mask.\\
  /__t/ The second left-shift tempering matrix parameter.\\
  /__c/ The second left-shift tempering matrix mask.\\
  /__l/ The second right-shift tempering matrix parameter.\\
  /__f/ Initialization multiplier.
#+end_quote

Definition at line *472* of file *random.h*.

* Member Typedef Documentation
** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
typedef _UIntType *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f >::*result_type*
The type of the generated random value.

Definition at line *507* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
*std::mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a, __u,
__d, __s, __b, __t, __c, __l, __f >::*mersenne_twister_engine*
()= [inline]=
Definition at line *527* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
*std::mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a, __u,
__d, __s, __b, __t, __c, __l, __f >::*mersenne_twister_engine*
(*result_type* __sd)= [inline]=, = [explicit]=
Definition at line *530* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
template<typename _Sseq , typename = _If_seed_seq<_Sseq>>
*std::mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a, __u,
__d, __s, __b, __t, __c, __l, __f >::*mersenne_twister_engine* (_Sseq &
__q)= [inline]=, = [explicit]=
Constructs a mersenne_twister_engine random number generator engine
seeded from the seed sequence =__q=.

*Parameters*

#+begin_quote
  /__q/ the seed sequence.
#+end_quote

Definition at line *541* of file *random.h*.

* Member Function Documentation
** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
void *std::mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a,
__u, __d, __s, __b, __t, __c, __l, __f >::discard (unsigned long long
__z)
Discard a sequence of random numbers.

Definition at line *431* of file *bits/random.tcc*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
static constexpr *result_type* *std::mersenne_twister_engine*<
_UIntType, __w, __n, __m, __r, __a, __u, __d, __s, __b, __t, __c, __l,
__f >::max ()= [inline]=, = [static]=, = [constexpr]=
Gets the largest possible value in the output range.

Definition at line *562* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
static constexpr *result_type* *std::mersenne_twister_engine*<
_UIntType, __w, __n, __m, __r, __a, __u, __d, __s, __b, __t, __c, __l,
__f >::min ()= [inline]=, = [static]=, = [constexpr]=
Gets the smallest possible value in the output range.

Definition at line *555* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
*mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a, __u, __d,
__s, __b, __t, __c, __l, __f >::*result_type*
*std::mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a, __u,
__d, __s, __b, __t, __c, __l, __f >::operator()
Definition at line *451* of file *bits/random.tcc*.

** "template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
template<typename _Sseq > auto *std::mersenne_twister_engine*<
_UIntType, __w, __n, __m, __r, __a, __u, __d, __s, __b, __t, __c, __l,
__f >::seed (_Sseq & __q) -> _If_seed_seq<_Sseq>
"

Definition at line *350* of file *bits/random.tcc*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
void *std::mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a,
__u, __d, __s, __b, __t, __c, __l, __f >::seed (*result_type* __sd =
=default_seed=)
Definition at line *324* of file *bits/random.tcc*.

* Friends And Related Function Documentation
** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
template<typename _UIntType1 , size_t __w1, size_t __n1, size_t __m1,
size_t __r1, _UIntType1 __a1, size_t __u1, _UIntType1 __d1, size_t __s1,
_UIntType1 __b1, size_t __t1, _UIntType1 __c1, size_t __l1, _UIntType1
__f1, typename _CharT , typename _Traits > *std::basic_ostream*< _CharT,
_Traits > & operator<< (*std::basic_ostream*< _CharT, _Traits > & __os,
const *std::mersenne_twister_engine*< _UIntType1, __w1, __n1, __m1,
__r1, __a1, __u1, __d1, __s1, __b1, __t1, __c1, __l1, __f1 > &
__x)= [friend]=
Inserts the current state of a % mersenne_twister_engine random number
generator engine =__x= into the output stream =__os=.

*Parameters*

#+begin_quote
  /__os/ An output stream.\\
  /__x/ A % mersenne_twister_engine random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The output stream with the state of =__x= inserted or in an error
  state.
#+end_quote

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
bool operator== (const *mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f > & __lhs, const
*mersenne_twister_engine*< _UIntType, __w, __n, __m, __r, __a, __u, __d,
__s, __b, __t, __c, __l, __f > & __rhs)= [friend]=
Compares two % mersenne_twister_engine random number generator objects
of the same type for equality.

*Parameters*

#+begin_quote
  /__lhs/ A % mersenne_twister_engine random number generator object.\\
  /__rhs/ Another % mersenne_twister_engine random number generator
  object.
#+end_quote

*Returns*

#+begin_quote
  true if the infinite sequences of generated values would be equal,
  false otherwise.
#+end_quote

Definition at line *587* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
template<typename _UIntType1 , size_t __w1, size_t __n1, size_t __m1,
size_t __r1, _UIntType1 __a1, size_t __u1, _UIntType1 __d1, size_t __s1,
_UIntType1 __b1, size_t __t1, _UIntType1 __c1, size_t __l1, _UIntType1
__f1, typename _CharT , typename _Traits > *std::basic_istream*< _CharT,
_Traits > & operator>> (*std::basic_istream*< _CharT, _Traits > & __is,
*std::mersenne_twister_engine*< _UIntType1, __w1, __n1, __m1, __r1,
__a1, __u1, __d1, __s1, __b1, __t1, __c1, __l1, __f1 > & __x)= [friend]=
Extracts the current state of a % mersenne_twister_engine random number
generator engine =__x= from the input stream =__is=.

*Parameters*

#+begin_quote
  /__is/ An input stream.\\
  /__x/ A % mersenne_twister_engine random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The input stream with the state of =__x= extracted or in an error
  state.
#+end_quote

* Member Data Documentation
** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr _UIntType *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::default_seed= [static]=, = [constexpr]=
Definition at line *523* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr _UIntType *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::initialization_multiplier= [static]=, = [constexpr]=
Definition at line *522* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::mask_bits= [static]=, = [constexpr]=
Definition at line *513* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::shift_size= [static]=, = [constexpr]=
Definition at line *512* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::state_size= [static]=, = [constexpr]=
Definition at line *511* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr _UIntType *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::tempering_b= [static]=, = [constexpr]=
Definition at line *518* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr _UIntType *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::tempering_c= [static]=, = [constexpr]=
Definition at line *520* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr _UIntType *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::tempering_d= [static]=, = [constexpr]=
Definition at line *516* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::tempering_l= [static]=, = [constexpr]=
Definition at line *521* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::tempering_s= [static]=, = [constexpr]=
Definition at line *517* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::tempering_t= [static]=, = [constexpr]=
Definition at line *519* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::tempering_u= [static]=, = [constexpr]=
Definition at line *515* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr size_t *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::word_size= [static]=, = [constexpr]=
Definition at line *510* of file *random.h*.

** template<typename _UIntType , size_t __w, size_t __n, size_t __m,
size_t __r, _UIntType __a, size_t __u, _UIntType __d, size_t __s,
_UIntType __b, size_t __t, _UIntType __c, size_t __l, _UIntType __f>
constexpr _UIntType *std::mersenne_twister_engine*< _UIntType, __w, __n,
__m, __r, __a, __u, __d, __s, __b, __t, __c, __l, __f
>::xor_mask= [static]=, = [constexpr]=
Definition at line *514* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
