#+TITLE: Manpages - uuid_unparse.3
#+DESCRIPTION: Linux manpage for uuid_unparse.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
uuid_unparse - convert a UUID from binary representation to a string

* SYNOPSIS
*#include <uuid.h>*

*void uuid_unparse(uuid_t */uu/*, char **/out/*);*\\
*void uuid_unparse_upper(uuid_t */uu/*, char **/out/*);*\\
*void uuid_unparse_lower(uuid_t */uu/*, char **/out/*);*

* DESCRIPTION
The *uuid_unparse*/() function converts the supplied UUID uu from the
binary representation into a 36-byte string (plus trailing '\0') of the
form 1b4e28ba-2fa1-11d2-883f-0016d3cca427 and stores this value in the
character string pointed to by out. The case of the hex digits returned
by /*uuid_unparse*/() may be upper or lower case, and is dependent on
the system-dependent local default./

If the case of the hex digits is important then the functions
*uuid_unparse_upper*/() and /*uuid_unparse_lower*/() may be used./

* CONFORMING TO
This library unparses UUIDs compatible with OSF DCE 1.1.

* AUTHORS
Theodore Y. Ts'o

* SEE ALSO
*uuid*/(3),/ *uuid_clear*/(3),/ *uuid_compare*/(3),/ *uuid_copy*/(3),/
*uuid_generate*/(3),/ *uuid_time*/(3),/ *uuid_is_null*/(3),/
*uuid_parse*/(3)/

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *libuuid*/ library is part of the util-linux package since version
2.15.1. It can be downloaded from / /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
