#+TITLE: Manpages - XtToolkitThreadInitialize.3
#+DESCRIPTION: Linux manpage for XtToolkitThreadInitialize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtToolkitThreadInitialize - initialize the toolkit for multiple threads

* SYNTAX
#include <X11/Intrinsic.h>

Boolean XtToolkitThreadInitialize(void);

* DESCRIPTION
If *XtToolkitThreadInitialize* was previously called, it returns. The
application programmer must ensure that two or more threads do not
simultaneously attempt to call *XtToolkitThreadInitialize*; the effect
of this is undefined. *XtToolkitThreadInitialize* returns *True* if the
host operating system has threads and the Intrinsics are thread safe.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
