#+TITLE: Manpages - libssh2_channel_get_exit_status.3
#+DESCRIPTION: Linux manpage for libssh2_channel_get_exit_status.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_get_exit_status - get the remote exit code

* SYNOPSIS
#include <libssh2.h>

int libssh2_channel_get_exit_status(LIBSSH2_CHANNEL* channel)

* DESCRIPTION
/channel/ - Closed channel stream to retrieve exit status from.

Returns the exit code raised by the process running on the remote host
at the other end of the named channel. Note that the exit status may not
be available if the remote end has not yet set its status to closed.

* RETURN VALUE
Returns 0 on failure, otherwise the /Exit Status/ reported by remote
host
