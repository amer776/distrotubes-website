#+TITLE: Manpages - reallocarray.3bsd
#+DESCRIPTION: Linux manpage for reallocarray.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

Designed for safe allocation of arrays, the

function is similar to

except it operates on

members of size

and checks for integer overflow in the calculation

*

Used for the allocation of memory holding sensitive data, the

function guarantees that memory becoming unallocated is explicitly

meaning cached free objects are cleared with

The

function is similar to

except it ensures newly allocated memory is cleared similar to

If

is

is ignored and the call is equivalent to

If

is not

must be a value such that

*

is the size of the earlier allocation that returned

otherwise the behavior is undefined. The

function is similar to the

function except it ensures memory is explicitly discarded. If

is

no action occurs. If

is not

the

argument must be equal to or smaller than the size of the earlier
allocation that returned

guarantees the memory range starting at

with length

is discarded while deallocating the whole object originally allocated.

The

and

functions return a pointer to the allocated space if successful;
otherwise, a null pointer is returned and

is set to

If multiplying

and

results in integer overflow,

and

return

and set

to

If

is not

and multiplying

and

results in integer overflow

returns

and sets

to

Consider

or the extensions

and

when there is multiplication in the

argument of

or

For example, avoid this common idiom as it may lead to integer overflow:

if ((p = malloc(num * size)) == NULL) err(1, NULL);

A drop-in replacement is

if ((p = reallocarray(NULL, num, size)) == NULL) err(1, NULL);

Alternatively,

may be used at the cost of initialization overhead.

When using

be careful to avoid the following idiom:

size += 50; if ((p = realloc(p, size)) == NULL) return (NULL);

Do not adjust the variable describing how much memory has been allocated
until the allocation has been successful. This can cause aberrant
program behavior if the incorrect size value is used. In most cases, the
above sample will also result in a leak of memory. As stated earlier, a
return value of

indicates that the old object still remains allocated. Better code looks
like this:

newsize = size + 50; if ((newp = realloc(p, newsize)) == NULL) {
free(p); p = NULL; size = 0; return (NULL); } p = newp; size = newsize;

As with

it is important to ensure the new size value will not overflow; i.e.
avoid allocations like the following:

if ((newp = realloc(p, num * size)) == NULL) { ...

Instead, use

if ((newp = reallocarray(p, num, size)) == NULL) { ...

Calling

with a

is equivalent to calling

Instead of this idiom:

if (p == NULL) newp = malloc(newsize); else newp = realloc(p, newsize);

Use the following:

newp = realloc(p, newsize);

The

function should be used for resizing objects containing sensitive data
like keys. To avoid leaking information, it guarantees memory is cleared
before placing it on the internal free list. Deallocation of such an
object should be done by calling

The

function appeared in

and glibc 2.26. The

function appeared in

The

function appeared in
