#+TITLE: Manpages - SDL_AddTimer.3
#+DESCRIPTION: Linux manpage for SDL_AddTimer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_AddTimer - Add a timer which will call a callback after the
specified number of milliseconds has elapsed.

* SYNOPSIS
*#include "SDL.h"*

*SDL_TimerID SDL_AddTimer*(*Uint32 interval, SDL_NewTimerCallback
callback, void *param*);

* CALLBACK
#+begin_example
  /* type definition for the "new" timer callback function */
  typedef Uint32 (*SDL_NewTimerCallback)(Uint32 interval, void *param);
#+end_example

* DESCRIPTION
Adds a callback function to be run after the specified number of
milliseconds has elapsed. The callback function is passed the current
timer interval and the user supplied parameter from the *SDL_AddTimer*
call and returns the next timer interval. If the returned value from the
callback is the same as the one passed in, the periodic alarm continues,
otherwise a new alarm is scheduled.

To cancel a currently running timer call /SDL_RemoveTimer/ with the
timer ID returned from *SDL_AddTimer*.

The timer callback function may run in a different thread than your main
program, and so shouldn't call any functions from within itself. You may
always call /SDL_PushEvent/, however.

The granularity of the timer is platform-dependent, but you should count
on it being at least 10 ms as this is the most common number. This means
that if you request a 16 ms timer, your callback will run approximately
20 ms later on an unloaded system. If you wanted to set a flag signaling
a frame update at 30 frames per second (every 33 ms), you might set a
timer for 30 ms (see example below). If you use this function, you need
to pass *SDL_INIT_TIMER* to /SDL_Init/.

* RETURN VALUE
Returns an ID value for the added timer or *NULL* if there was an error.

* EXAMPLES
#+begin_example
  my_timer_id = SDL_AddTimer((33/10)*10, my_callbackfunc, my_callback_param);
#+end_example

* SEE ALSO
*SDL_RemoveTimer*, *SDL_PushEvent*
