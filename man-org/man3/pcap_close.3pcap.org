#+TITLE: Manpages - pcap_close.3pcap
#+DESCRIPTION: Linux manpage for pcap_close.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_close - close a capture device or savefile

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  void pcap_close(pcap_t *p);
#+end_example

* DESCRIPTION
*pcap_close*() closes the files associated with /p/ and deallocates
resources.

* SEE ALSO
*pcap*(3PCAP)
