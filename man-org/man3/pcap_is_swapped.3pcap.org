#+TITLE: Manpages - pcap_is_swapped.3pcap
#+DESCRIPTION: Linux manpage for pcap_is_swapped.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_is_swapped - find out whether a savefile has the native byte order

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_is_swapped(pcap_t *p);
#+end_example

* DESCRIPTION
*pcap_is_swapped*() returns true (*1*) if /p/ refers to a ``savefile''
that uses a different byte order than the current system. For a live
capture, it always returns false (*0*).

It must not be called on a pcap descriptor created by
*pcap_create*(3PCAP) that has not yet been activated by
*pcap_activate*(3PCAP).

* RETURN VALUE
*pcap_is_swapped*() returns true (*1*) or false (*0*) on success and
*PCAP_ERROR_NOT_ACTIVATED* if called on a capture handle that has been
created but not activated.

* SEE ALSO
*pcap*(3PCAP)
