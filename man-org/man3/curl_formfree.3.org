#+TITLE: Manpages - curl_formfree.3
#+DESCRIPTION: Linux manpage for curl_formfree.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_formfree - free a previously build multipart/formdata HTTP POST
chain

* SYNOPSIS
*#include <curl/curl.h>*

*void curl_formfree(struct curl_httppost **/form);/

* DESCRIPTION
This function is deprecated. Do not use! See /curl_mime_init(3)/
instead!

curl_formfree() is used to clean up data previously built/appended with
/curl_formadd(3)/. This must be called when the data has been used,
which typically means after /curl_easy_perform(3)/ has been called.

The pointer to free is the same pointer you passed to the
/CURLOPT_HTTPPOST(3)/ option, which is the /firstitem/ pointer from the
/curl_formadd(3)/ invoke(s).

*form* is the pointer as returned from a previous call to
/curl_formadd(3)/ and may be NULL.

Passing in a NULL pointer in /form/ will make this function return
immediately with no action.

* EXAMPLE
#+begin_example
    /* Fill in a file upload field */
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "file",
                 CURLFORM_FILE, "nice-image.jpg",
                 CURLFORM_END);

    curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

    curl_easy_perform(curl);

    /* then cleanup the formpost chain */
    curl_formfree(formpost);
#+end_example

* AVAILABILITY
Deprecated in 7.56.0.

* RETURN VALUE
None

* SEE ALSO
*curl_formadd*(3), *curl_mime_init*(3), *curl_mime_free*(3)
