#+TITLE: Manpages - Encode_EBCDIC.3perl
#+DESCRIPTION: Linux manpage for Encode_EBCDIC.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Encode::EBCDIC - EBCDIC Encodings

* SYNOPSIS
use Encode qw/encode decode/; $posix_bc = encode("posix-bc", $utf8); #
loads Encode::EBCDIC implicitly $utf8 = decode("", $posix_bc); # ditto

* ABSTRACT
This module implements various EBCDIC-Based encodings. Encodings
supported are as follows.

Canonical Alias Description
--------------------------------------------------------------------
cp37 cp500 cp875 cp1026 cp1047 posix-bc

* DESCRIPTION
To find how to use this module in detail, see Encode.

* SEE ALSO
Encode, perlebcdic
