#+TITLE: Manpages - ccosf.3
#+DESCRIPTION: Linux manpage for ccosf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ccosf.3 is found in manpage for: [[../man3/ccos.3][man3/ccos.3]]