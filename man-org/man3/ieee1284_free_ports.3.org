#+TITLE: Manpages - ieee1284_free_ports.3
#+DESCRIPTION: Linux manpage for ieee1284_free_ports.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_free_ports - safely deallocate a port list

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*void ieee1284_free_ports(struct parport_list **/list/*);*

* DESCRIPTION
When the port list will no longer be used, the program should call
*ieee1284_free_ports* giving it a pointer to the parport_list structure
that holds the list of ports returned by *ieee1284_find_ports*(3). The
ports are reference counted with the *ieee1284_open* and
*ieee1284_close* functions, and so the port list may be freed even if it
contains pointers to ports that are still open.

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
