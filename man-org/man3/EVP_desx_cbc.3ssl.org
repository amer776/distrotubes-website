#+TITLE: Manpages - EVP_desx_cbc.3ssl
#+DESCRIPTION: Linux manpage for EVP_desx_cbc.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_desx_cbc - EVP DES-X cipher

* SYNOPSIS
#include <openssl/evp.h> const EVP_CIPHER *EVP_desx_cbc(void)

* DESCRIPTION
The DES-X encryption algorithm for EVP.

All modes below use a key length of 128 bits and acts on blocks of
128-bits.

- EVP_desx_cbc() :: The DES-X algorithm in CBC mode.

* RETURN VALUES
These functions return an *EVP_CIPHER* structure that contains the
implementation of the symmetric cipher. See *EVP_CIPHER_meth_new* (3)
for details of the *EVP_CIPHER* structure.

* SEE ALSO
*evp* (7), *EVP_EncryptInit* (3), *EVP_CIPHER_meth_new* (3)

* COPYRIGHT
Copyright 2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
