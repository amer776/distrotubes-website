#+TITLE: Manpages - sd_listen_fds_with_names.3
#+DESCRIPTION: Linux manpage for sd_listen_fds_with_names.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_listen_fds_with_names.3 is found in manpage for: [[../sd_listen_fds.3][sd_listen_fds.3]]