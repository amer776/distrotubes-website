#+TITLE: Manpages - ldap_first_reference.3
#+DESCRIPTION: Linux manpage for ldap_first_reference.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldap_first_reference, ldap_next_reference, ldap_count_references -
Stepping through continuation references in a result chain

* LIBRARY
OpenLDAP LDAP (libldap, -lldap)

* SYNOPSIS
#+begin_example
  #include <ldap.h>
  int ldap_count_references( LDAP *ld, LDAPMessage *result )
  LDAPMessage *ldap_first_reference( LDAP *ld, LDAPMessage *result )
  LDAPMessage *ldap_next_reference( LDAP *ld, LDAPMessage *reference )
#+end_example

* DESCRIPTION
These routines are used to step through the continuation references in a
result chain received from *ldap_result*(3) or the synchronous LDAP
search operation routines.

The *ldap_first_reference()* routine is used to retrieve the first
reference message in a result chain. It takes the /result/ as returned
by a call to *ldap_result*(3)*,* *ldap_search_s*(3) or
*ldap_search_st*(3) and returns a pointer to the first reference message
in the result chain.

This pointer should be supplied on a subsequent call to
*ldap_next_reference()* to get the next reference message, the result of
which should be supplied to the next call to *ldap_next_reference()*,
etc. *ldap_next_reference()* will return NULL when there are no more
reference messages. The reference messages returned from these calls are
used by *ldap_parse_reference*(3) to extract referrals and controls.

A count of the number of reference messages in the search result can be
obtained by calling *ldap_count_references()*. It can also be used to
count the number of reference messages remaining in a result chain.

* ERRORS
If an error occurs in *ldap_first_reference()* or
*ldap_next_reference()*, NULL is returned. If an error occurs in
*ldap_count_references()*, -1 is returned.

* SEE ALSO
*ldap*(3), *ldap_result*(3), *ldap_search*(3), *ldap_parse_reference*(3)

* ACKNOWLEDGEMENTS
*OpenLDAP Software* is developed and maintained by The OpenLDAP Project
<http://www.openldap.org/>. *OpenLDAP Software* is derived from the
University of Michigan LDAP 3.3 Release.
