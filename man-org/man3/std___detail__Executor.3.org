#+TITLE: Manpages - std___detail__Executor.3
#+DESCRIPTION: Linux manpage for std___detail__Executor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Executor< _BiIter, _Alloc, _TraitsT, __dfs_mode > -
Takes a regex and an input string and does the matching.

* SYNOPSIS
\\

=#include <regex_executor.h>=

** Public Types
typedef *iterator_traits*< _BiIter >::value_type *_CharT*\\

typedef _TraitsT::char_class_type *_ClassT*\\

typedef *regex_constants::match_flag_type* *_FlagT*\\

typedef _NFA< _TraitsT > *_NFAT*\\

typedef *basic_regex*< _CharT, _TraitsT > *_RegexT*\\

typedef *std::vector*< *sub_match*< _BiIter >, _Alloc > *_ResultsVec*\\

** Public Member Functions
*_Executor* (_BiIter __begin, _BiIter __end, *_ResultsVec* &__results,
const *_RegexT* &__re, *_FlagT* __flags)\\

bool *_M_match* ()\\

bool *_M_search* ()\\

bool *_M_search_from_first* ()\\

** Public Attributes
_BiIter *_M_begin*\\

*_ResultsVec* *_M_cur_results*\\

_BiIter *_M_current*\\

const _BiIter *_M_end*\\

*_FlagT* *_M_flags*\\

bool *_M_has_sol*\\

const _NFAT & *_M_nfa*\\

const *_RegexT* & *_M_re*\\

*vector*< *pair*< _BiIter, int > > *_M_rep_count*\\

*_ResultsVec* & *_M_results*\\

_State_info< *__search_mode*, *_ResultsVec* > *_M_states*\\

* Detailed Description
** "template<typename _BiIter, typename _Alloc, typename _TraitsT, bool
__dfs_mode>
\\
class std::__detail::_Executor< _BiIter, _Alloc, _TraitsT, __dfs_mode
>"Takes a regex and an input string and does the matching.

The _Executor class has two modes: DFS mode and BFS mode, controlled by
the template parameter __dfs_mode.

Definition at line *52* of file *regex_executor.h*.

* Member Typedef Documentation
** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> typedef *iterator_traits*<_BiIter>::value_type
*std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT, __dfs_mode
>::_CharT
Definition at line *61* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> typedef _TraitsT::char_class_type
*std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT, __dfs_mode
>::_ClassT
Definition at line *65* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> typedef *regex_constants::match_flag_type*
*std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT, __dfs_mode
>::*_FlagT*
Definition at line *64* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> typedef _NFA<_TraitsT> *std::__detail::_Executor*<
_BiIter, _Alloc, _TraitsT, __dfs_mode >::_NFAT
Definition at line *66* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> typedef *basic_regex*<_CharT, _TraitsT>
*std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT, __dfs_mode
>::*_RegexT*
Definition at line *62* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> typedef *std::vector*<*sub_match*<_BiIter>, _Alloc>
*std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT, __dfs_mode
>::*_ResultsVec*
Definition at line *63* of file *regex_executor.h*.

* Constructor & Destructor Documentation
** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> *std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT,
__dfs_mode >::*_Executor* (_BiIter __begin, _BiIter __end, *_ResultsVec*
& __results, const *_RegexT* & __re, *_FlagT* __flags)= [inline]=
Definition at line *69* of file *regex_executor.h*.

* Member Function Documentation
** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> bool *std::__detail::_Executor*< _BiIter, _Alloc,
_TraitsT, __dfs_mode >::_M_match ()= [inline]=
Definition at line *90* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> bool *std::__detail::_Executor*< _BiIter, _Alloc,
_TraitsT, __dfs_mode >::_M_search
Definition at line *39* of file *regex_executor.tcc*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> bool *std::__detail::_Executor*< _BiIter, _Alloc,
_TraitsT, __dfs_mode >::_M_search_from_first ()= [inline]=
Definition at line *98* of file *regex_executor.h*.

* Member Data Documentation
** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> _BiIter *std::__detail::_Executor*< _BiIter, _Alloc,
_TraitsT, __dfs_mode >::_M_begin
Definition at line *241* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> *_ResultsVec* *std::__detail::_Executor*< _BiIter,
_Alloc, _TraitsT, __dfs_mode >::_M_cur_results
Definition at line *239* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> _BiIter *std::__detail::_Executor*< _BiIter, _Alloc,
_TraitsT, __dfs_mode >::_M_current
Definition at line *240* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> const _BiIter *std::__detail::_Executor*< _BiIter,
_Alloc, _TraitsT, __dfs_mode >::_M_end
Definition at line *242* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> *_FlagT* *std::__detail::_Executor*< _BiIter, _Alloc,
_TraitsT, __dfs_mode >::_M_flags
Definition at line *248* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> bool *std::__detail::_Executor*< _BiIter, _Alloc,
_TraitsT, __dfs_mode >::_M_has_sol
Definition at line *250* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> const _NFAT& *std::__detail::_Executor*< _BiIter,
_Alloc, _TraitsT, __dfs_mode >::_M_nfa
Definition at line *244* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> const *_RegexT*& *std::__detail::_Executor*< _BiIter,
_Alloc, _TraitsT, __dfs_mode >::_M_re
Definition at line *243* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> *vector*<*pair*<_BiIter, int> >
*std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT, __dfs_mode
>::_M_rep_count
Definition at line *246* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> *_ResultsVec*& *std::__detail::_Executor*< _BiIter,
_Alloc, _TraitsT, __dfs_mode >::_M_results
Definition at line *245* of file *regex_executor.h*.

** template<typename _BiIter , typename _Alloc , typename _TraitsT ,
bool __dfs_mode> _State_info<*__search_mode*, *_ResultsVec*>
*std::__detail::_Executor*< _BiIter, _Alloc, _TraitsT, __dfs_mode
>::_M_states
Definition at line *247* of file *regex_executor.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
