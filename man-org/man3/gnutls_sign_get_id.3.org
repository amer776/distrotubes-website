#+TITLE: Manpages - gnutls_sign_get_id.3
#+DESCRIPTION: Linux manpage for gnutls_sign_get_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_sign_get_id - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*gnutls_sign_algorithm_t gnutls_sign_get_id(const char * */name/*);*

* ARGUMENTS
- const char * name :: is a sign algorithm name

* DESCRIPTION
The names are compared in a case insensitive way.

* RETURNS
return a *gnutls_sign_algorithm_t* value corresponding to the specified
algorithm, or *GNUTLS_SIGN_UNKNOWN* on error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
