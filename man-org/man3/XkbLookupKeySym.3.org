#+TITLE: Manpages - XkbLookupKeySym.3
#+DESCRIPTION: Linux manpage for XkbLookupKeySym.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbLookupKeySym - Find the symbol associated with a key for a particular
state

* SYNOPSIS
*Bool XkbLookupKeySym* *( Display **/dpy/* ,* *KeyCode */key/* ,*
*unsigned int */state/* ,* *unsigned int **/mods_rtrn/* ,* *KeySym
**/sym_rtrn/* );*

* ARGUMENTS
- /- dpy/ :: connection to X server

- /- key/ :: key for which symbols are to be found

- /- state/ :: state for which symbol should be found

- /- mods_rtrn/ :: backfilled with consumed modifiers

- /- sym_rtrn/ :: backfilled with symbol associated with key + state

* DESCRIPTION
/XkbLookupKeySym/ is the equivalent of the core /XLookupKeySym/
function. For the core keyboard, given a keycode /key/ and an Xkb state
/state, XkbLookupKeySym/ returns the symbol associated with the key in
/sym_rtrn/ and the list of modifiers that should still be applied in
/mods_rtrn./ The /state/ parameter is the state from a KeyPress or
KeyRelease event. /XkbLookupKeySym/ returns True if it succeeds.

* SEE ALSO
*XLookupKeySym*(3)
