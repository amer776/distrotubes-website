#+TITLE: Manpages - ares_set_sortlist.3
#+DESCRIPTION: Linux manpage for ares_set_sortlist.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_set_sortlist - Initialize an ares_channel sortlist configuration

* SYNOPSIS
#+begin_example
  #include <ares.h>

  int ares_set_sortlist(ares_channel channel, const char *sortstr)
#+end_example

* DESCRIPTION
The *ares_set_sortlist(3)* function initializes an address sortlist
configuration for the channel data identified by /channel/, so that
addresses returned by *ares_gethostbyname(3)* are sorted according to
the sortlist. The provided /sortstr/ string that holds a space separated
list of IP-address-netmask pairs. The netmask is optional but follows
the address after a slash if present. For example,
"130.155.160.0/255.255.240.0 130.155.0.0".

This function replaces any potentially previously configured address
sortlist with the ones given in the configuration string.

* RETURN VALUES
*ares_set_sortlist(3)* may return any of the following values:

- *ARES_SUCCESS* :: The sortlist configuration was successfully
  initialized.

- *ARES_ENOMEM* :: The process's available memory was exhausted.

- *ARES_ENODATA* :: The channel data identified by /channel/ was
  invalid.

- *ARES_ENOTINITIALIZED* :: c-ares library initialization not yet
  performed.

* SEE ALSO
*ares_init_options*(3), *ares_dup(3)*

* AVAILABILITY
ares_set_sortlist(3) was added in c-ares 1.11.0
