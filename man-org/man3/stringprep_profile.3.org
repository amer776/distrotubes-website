#+TITLE: Manpages - stringprep_profile.3
#+DESCRIPTION: Linux manpage for stringprep_profile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stringprep_profile - API function

* SYNOPSIS
*#include <stringprep.h>*

*int stringprep_profile(const char * */in/*, char ** */out/*, const char
* */profile/*, Stringprep_profile_flags */flags/*);*

* ARGUMENTS
- const char * in :: input array with UTF-8 string to prepare.

- char ** out :: output variable with pointer to newly allocate string.

- const char * profile :: name of stringprep profile to use.

- Stringprep_profile_flags flags :: a *Stringprep_profile_flags* value,
  or 0.

* DESCRIPTION
Prepare the input zero terminated UTF-8 string according to the
stringprep profile, and return the result in a newly allocated variable.

Note that you must convert strings entered in the systems locale into
UTF-8 before using this function, see *stringprep_locale_to_utf8()*.

The output /out/ variable must be deallocated by the caller.

The /flags/ are one of *Stringprep_profile_flags* values, or 0.

The /profile/ specifies the name of the stringprep profile to use. It
must be one of the internally supported stringprep profiles.

Return value: Returns *STRINGPREP_OK* iff successful, or an error code.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
