#+TITLE: Manpages - clog2.3
#+DESCRIPTION: Linux manpage for clog2.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
clog2, clog2f, clog2l - base-2 logarithm of a complex number

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double complex clog2(double complex z);
  float complex clog2f(float complex z);
  long double complex clog2l(long double complex z);
#+end_example

* DESCRIPTION
The call /clog2(z)/ is equivalent to /clog(z)/log(2)/.

The other functions perform the same task for /float/ and /long double/.

Note that /z/ close to zero will cause an overflow.

* CONFORMING TO
These function names are reserved for future use in C99.

Not yet in glibc, as at version 2.19.

* SEE ALSO
*cabs*(3), *cexp*(3), *clog*(3), *clog10*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
