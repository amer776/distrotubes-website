#+TITLE: Manpages - wcscat.3
#+DESCRIPTION: Linux manpage for wcscat.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wcscat - concatenate two wide-character strings

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  wchar_t *wcscat(wchar_t *restrict dest",constwchar_t*restrict"src);
#+end_example

* DESCRIPTION
The *wcscat*() function is the wide-character equivalent of the
*strcat*(3) function. It copies the wide-character string pointed to by
/src/, including the terminating null wide character (L'\0'), to the end
of the wide-character string pointed to by /dest/.

The strings may not overlap.

The programmer must ensure that there is room for at least
/wcslen(dest)/+/wcslen(src)/+1 wide characters at /dest/.

* RETURN VALUE
*wcscat*() returns /dest/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface  | Attribute     | Value   |
| *wcscat*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* SEE ALSO
*strcat*(3), *wcpcpy*(3), *wcscpy*(3), *wcsncat*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
