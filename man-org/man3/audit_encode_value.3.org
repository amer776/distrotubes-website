#+TITLE: Manpages - audit_encode_value.3
#+DESCRIPTION: Linux manpage for audit_encode_value.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_encode_value - encode input string to ASCII code string

* SYNOPSIS
#+begin_example
  #include <libaudit.h>

  char *audit_encode_value(char *final, const char *buf, unsigned int size");
#+end_example

* DESCRIPTION
*audit_encode_value*() encodes a string given by /buf/ to a ASCII code
string. /final/ is the hexadecimal string encoded to ASCII code. /size/
is the length of the string given by /buf/.

e.g.: "foo bar" is encoded as "666F6F20626172". "\1\2\3\4" is encoded as
"01020304".

* RETURN VALUE
Returns a encoded string same as /final/ or, NULL on error.

* SEE ALSO
*audit_encode_nv_string*(3), *audit_value_needs_encoding*(3).

* AUTHOR
Steve Grubb
