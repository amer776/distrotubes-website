#+TITLE: Manpages - md5.3bsd
#+DESCRIPTION: Linux manpage for md5.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The MD5 functions calculate a 128-bit cryptographic checksum (digest)
for any number of input bytes. A cryptographic checksum is a one-way
hash-function, that is, you cannot find (except by exhaustive search)
the input corresponding to a particular output. This net result is a

of the input-data, which doesn't disclose the actual input.

MD4 has been broken; it should only be used where necessary for backward
compatibility. MD5 has not yet (1999-02-11) been broken, but recent
attacks have cast some doubt on its security properties. The attacks on
both MD4 and MD5 are both in the nature of finding

- that is, multiple inputs which hash to the same value; it is still
unlikely for an attacker to be able to determine the exact original
input given a hash value.

The

and

functions are the core functions. Allocate an MD5_CTX, initialize it
with

run over the data with

and finally extract the result using

The

function can be used to apply padding to the message digest as in

but the current context can still be used with

The

function is used by

to hash 512-bit blocks and forms the core of the algorithm. Most
programs should use the interface provided by

and

instead of calling

directly.

is a wrapper for

which converts the return value to an MD5_DIGEST_STRING_LENGTH-character
(including the terminating '\0')

string which represents the 128 bits in hexadecimal.

calculates the digest of a file, and uses

to return the result. If the file cannot be opened, a null pointer is
returned.

behaves like

but calculates the digest only for that portion of the file starting at

and continuing for

bytes or until end of file is reached, whichever comes first. A zero

can be specified to read until end of file. A negative

or

will be ignored.

calculates the digest of a chunk of data in memory, and uses

to return the result.

When using

or

the

argument can be a null pointer, in which case the returned string is
allocated with

and subsequently must be explicitly deallocated using

after use. If the

argument is non-null it must point to at least MD5_DIGEST_STRING_LENGTH
characters of buffer space.

These functions appeared in

The original MD5 routines were developed by

Data Security, Inc., and published in the above references. This code is
derived from a public domain implementation written by Colin Plumb.

The

and

helper functions are derived from code written by Poul-Henning Kamp.

Collisions have been found for the full versions of both MD4 and MD5 as
well as strong attacks against the SHA-0 and SHA-1 family. The use of

or

is recommended instead.
