#+TITLE: Manpages - capng_get_rootid.3
#+DESCRIPTION: Linux manpage for capng_get_rootid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
capng_get_rootid - get namespace root id

* SYNOPSIS
*#include <cap-ng.h>*

int capng_get_rootid(void);

* DESCRIPTION
capng_get_rootid gets the rootid for capabilities operations. This is
only applicable for file system operations.

* RETURN VALUE
If the file is in the init namespace or the kernel does not support V3
file system capabilities, it returns CAPNG_UNSET_ROOTID. Otherwise it
return an integer for the namespace root id.

* SEE ALSO
*capng_get_caps_fd*(3), *capabilities*(7)

* AUTHOR
Steve Grubb
