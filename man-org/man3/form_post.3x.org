#+TITLE: Manpages - form_post.3x
#+DESCRIPTION: Linux manpage for form_post.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*post_form*, *unpost_form* - write or erase forms from associated
subwindows

* SYNOPSIS
*#include <form.h>*

*int post_form(FORM **/form/*);*\\
*int unpost_form(FORM **/form/*);*\\

* DESCRIPTION
The function *post_form* displays a form to its associated subwindow. To
trigger physical display of the subwindow, use *refresh*(3X) or some
equivalent *curses* routine (the implicit *doupdate* triggered by an
*curses* input request will do).

The function *unpost_form* erases form from its associated subwindow.

* RETURN VALUE
These routines return one of the following:

- *E_OK* :: The routine succeeded.

- *E_BAD_ARGUMENT* :: Routine detected an incorrect or out-of-range
  argument.

- *E_BAD_STATE* :: Routine was called from an initialization or
  termination function.

- *E_NOT_POSTED* :: The form has not been posted.

- *E_NOT_CONNECTED* :: No items are connected to the form.

- *E_NO_ROOM* :: Form is too large for its window.

- *E_POSTED* :: The form has already been posted.

- *E_SYSTEM_ERROR* :: System error occurred (see *errno*(3)).

* SEE ALSO
*curses*(3X), *form*(3X).

* NOTES
The header file *<form.h>* automatically includes the header file
*<curses.h>*.

* PORTABILITY
These routines emulate the System V forms library. They were not
supported on Version 7 or BSD versions.

* AUTHORS
Juergen Pfeifer. Manual pages and adaptation for new curses by Eric S.
Raymond.
