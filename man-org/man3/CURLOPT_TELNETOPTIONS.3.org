#+TITLE: Manpages - CURLOPT_TELNETOPTIONS.3
#+DESCRIPTION: Linux manpage for CURLOPT_TELNETOPTIONS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TELNETOPTIONS - set of telnet options

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TELNETOPTIONS, struct
curl_slist *cmds);

* DESCRIPTION
Provide a pointer to a curl_slist with variables to pass to the telnet
negotiations. The variables should be in the format <option=value>.
libcurl supports the options 'TTYPE', 'XDISPLOC' and 'NEW_ENV'. See the
TELNET standard for details.

* DEFAULT
NULL

* PROTOCOLS
TELNET

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    struct curl_slist *options;
    options = curl_slist_append(NULL, "TTTYPE=vt100");
    options = curl_slist_append(options, "USER=foobar");
    curl_easy_setopt(curl, CURLOPT_URL, "telnet://example.com/");
    curl_easy_setopt(curl, CURLOPT_TELNETOPTIONS, options);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    curl_slist_free_all(options);
  }
#+end_example

* AVAILABILITY
Along with TELNET

* RETURN VALUE
Returns CURLE_OK if TELNET is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_HTTPHEADER*(3), *CURLOPT_QUOTE*(3),
