#+TITLE: Manpages - __gnu_pbds_null_node_update.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_null_node_update.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::null_node_update< _Tp1, _Tp2, _Tp3, _Tp4 > - A null node
updator, indicating that no node updates are required.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::null_type*.

* Detailed Description
** "template<typename _Tp1, typename _Tp2, typename _Tp3, typename _Tp4>
\\
struct __gnu_pbds::null_node_update< _Tp1, _Tp2, _Tp3, _Tp4 >"A null
node updator, indicating that no node updates are required.

Definition at line *214* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
