#+TITLE: Manpages - CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE.3
#+DESCRIPTION: Linux manpage for CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE - size threshold for pipelining
penalty

* SYNOPSIS
#include <curl/curl.h>

CURLMcode curl_multi_setopt(CURLM *handle,
CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE, long size);

* DESCRIPTION
No function since pipelining was removed in 7.62.0.

Pass a long with a *size* in bytes. If a pipelined connection is
currently processing a request with a Content-Length larger than this
/CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE(3)/, that pipeline will then not
be considered for additional requests, even if it is shorter than
/CURLMOPT_MAX_PIPELINE_LENGTH(3)/.

* DEFAULT
The default value is 0, which means that the size penalization is
inactive.

* PROTOCOLS
HTTP(S)

* EXAMPLE
#+begin_example
  CURLM *m = curl_multi_init();
  long maxlength = 10000;
  curl_multi_setopt(m, CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE, maxlength);
#+end_example

* AVAILABILITY
Added in 7.30.0

* RETURN VALUE
Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLMOPT_PIPELINING*(3), *CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE*(3),
