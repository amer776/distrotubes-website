#+TITLE: Manpages - FcConfigGetFontDirs.3
#+DESCRIPTION: Linux manpage for FcConfigGetFontDirs.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigGetFontDirs - Get font directories

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcStrList * FcConfigGetFontDirs (FcConfig */config/*);*

* DESCRIPTION
Returns the list of font directories in /config/. This includes the
configured font directories along with any directories below those in
the filesystem. If /config/ is NULL, the current configuration is used.
