#+TITLE: Manpages - FcIsUpper.3
#+DESCRIPTION: Linux manpage for FcIsUpper.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcIsUpper - check for upper case ASCII character

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcIsUpper (FcChar8/c/*);*

* DESCRIPTION
This macro checks whether /c/ is a upper case ASCII letter.
