#+TITLE: Manpages - rtcSetGeometryUserPrimitiveCount.3embree3
#+DESCRIPTION: Linux manpage for rtcSetGeometryUserPrimitiveCount.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetGeometryUserPrimitiveCount - sets the number of primitives
    of a user-defined geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetGeometryUserPrimitiveCount(
    RTCGeometry geometry,
    unsigned int userPrimitiveCount
  );
#+end_example

** DESCRIPTION
The =rtcSetGeometryUserPrimitiveCount= function sets the number of
user-defined primitives (=userPrimitiveCount= parameter) of the
specified user-defined geometry (=geometry= parameter).

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[RTC_GEOMETRY_TYPE_USER]
