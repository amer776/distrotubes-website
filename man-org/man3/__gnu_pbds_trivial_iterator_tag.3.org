#+TITLE: Manpages - __gnu_pbds_trivial_iterator_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_trivial_iterator_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::trivial_iterator_tag - A trivial iterator tag. Signifies
that the iterators has none of std::iterators's movement abilities.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

* Detailed Description
A trivial iterator tag. Signifies that the iterators has none of
std::iterators's movement abilities.

Definition at line *75* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
