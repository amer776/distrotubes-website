#+TITLE: Manpages - XIGetSelectedEvents.3
#+DESCRIPTION: Linux manpage for XIGetSelectedEvents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XIGetSelectedEvents.3 is found in manpage for: [[../man3/XISelectEvents.3][man3/XISelectEvents.3]]