#+TITLE: Manpages - pam_get_authtok.3
#+DESCRIPTION: Linux manpage for pam_get_authtok.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_get_authtok, pam_get_authtok_verify, pam_get_authtok_noverify - get
authentication token

* SYNOPSIS
#+begin_example
  #include <security/pam_ext.h>
#+end_example

*int pam_get_authtok(pam_handle_t **/pamh/*, int */item/*, const char
***/authtok/*, const char **/prompt/*);*

*int pam_get_authtok_noverify(pam_handle_t **/pamh/*, const char
***/authtok/*, const char **/prompt/*);*

*int pam_get_authtok_verify(pam_handle_t **/pamh/*, const char
***/authtok/*, const char **/prompt/*);*

* DESCRIPTION
The *pam_get_authtok* function returns the cached authentication token,
or prompts the user if no token is currently cached. It is intended for
internal use by Linux-PAM and PAM service modules. Upon successful
return, /authtok/ contains a pointer to the value of the authentication
token. Note, this is a pointer to the /actual/ data and should *not* be
/free()/ed or over-written!

The /prompt/ argument specifies a prompt to use if no token is cached.
If a NULL pointer is given, *pam_get_authtok* uses pre-defined prompts.

The following values are supported for /item/:

PAM_AUTHTOK

#+begin_quote
  Returns the current authentication token. Called from
  *pam_sm_chauthtok*(3) *pam_get_authtok* will ask the user to confirm
  the new token by retyping it. If a prompt was specified, "Retype" will
  be used as prefix.
#+end_quote

PAM_OLDAUTHTOK

#+begin_quote
  Returns the previous authentication token when changing authentication
  tokens.
#+end_quote

The *pam_get_authtok_noverify* function can only be used for changing
the password (from *pam_sm_chauthtok*(3)). It returns the cached
authentication token, or prompts the user if no token is currently
cached. The difference to *pam_get_authtok* is, that this function does
not ask a second time for the password to verify it. Upon successful
return, /authtok/ contains a pointer to the value of the authentication
token. Note, this is a pointer to the /actual/ data and should *not* be
/free()/ed or over-written!

The *pam_get_authtok_verify* function can only be used to verify a
password for mistypes gotten by *pam_get_authtok_noverify*(3). This
function asks a second time for the password and verify it with the
password provided by /authtok/ argument. In case of an error, the value
of /authtok/ is undefined. Else this argument will point to the /actual/
data and should *not* be /free()/ed or over-written!

* OPTIONS
*pam_get_authtok* honours the following module options:

*try_first_pass*

#+begin_quote
  Before prompting the user for their password, the module first tries
  the previous stacked modules password in case that satisfies this
  module as well.
#+end_quote

*use_first_pass*

#+begin_quote
  The argument *use_first_pass* forces the module to use a previous
  stacked modules password and will never prompt the user - if no
  password is available or the password is not appropriate, the user
  will be denied access.
#+end_quote

*use_authtok*

#+begin_quote
  When password changing enforce the module to set the new token to the
  one provided by a previously stacked *password* module. If no token is
  available token changing will fail.
#+end_quote

*authtok_type=*/XXX/

#+begin_quote
  The default action is for the module to use the following prompts when
  requesting passwords: "New UNIX password: " and "Retype UNIX password:
  ". The example word /UNIX/ can be replaced with this option, by
  default it is empty.
#+end_quote

* RETURN VALUES
PAM_AUTH_ERR

#+begin_quote
  Authentication token could not be retrieved.
#+end_quote

PAM_AUTHTOK_ERR

#+begin_quote
  New authentication could not be retrieved.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Authentication token was successfully retrieved.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  No space for an authentication token was provided.
#+end_quote

PAM_TRY_AGAIN

#+begin_quote
  New authentication tokens mismatch.
#+end_quote

* SEE ALSO
*pam*(8)

* STANDARDS
The *pam_get_authtok* function is a Linux-PAM extensions.
