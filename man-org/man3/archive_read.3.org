#+TITLE: Manpages - archive_read.3
#+DESCRIPTION: Linux manpage for archive_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

These functions provide a complete API for reading streaming archives.
The general process is to first create the

object, set options, initialize the reader, iterate over the archive
headers and associated data, then close the archive and release all
resources.

See

To read an archive, you must first obtain an initialized

object from

See

and

You can then modify this object for the desired operations with the
various

and

functions. In particular, you will need to invoke appropriate

functions to enable the corresponding compression and format support.
Note that these latter functions perform two distinct operations: they
cause the corresponding support code to be linked into your program, and
they enable the corresponding auto-detect code. Unless you have specific
constraints, you will generally want to invoke

and

to enable auto-detect for all formats and compression types currently
supported by the library.

See

See

Once you have prepared the

object, you call

to actually open the archive and prepare it for reading. There are
several variants of this function; the most basic expects you to provide
pointers to several functions that can provide blocks of bytes from the
archive. There are convenience forms that allow you to specify a
filename, file descriptor,

object, or a block of memory from which to read the archive data. Note
that the core library makes no assumptions about the size of the blocks
read; callback functions are free to read whatever block size is most
appropriate for the medium.

See

and

Each archive entry consists of a header followed by a certain amount of
data. You can obtain the next header with

which returns a pointer to an

structure with information about the current archive element. If the
entry is a regular file, then the header will be followed by the file
data. You can use

(which works much like the

system call) to read this data from the archive, or

which provides a slightly more efficient interface. You may prefer to
use the higher-level

which reads and discards the data for this entry,

which copies the data to the provided file descriptor, or

which recreates the specified entry on disk and copies data from the
archive. In particular, note that

uses the

structure that you provide it, which may differ from the entry just read
from the archive. In particular, many applications will want to override
the pathname, file permissions, or ownership.

See

Once you have finished reading data from the archive, you should call

to close the archive, then call

to release all resources, including all memory allocated by the library.

The following illustrates basic usage of the library. In this example,
the callback functions are simply wrappers around the standard

and

system calls.

void list_archive(const char *name) { struct mydata *mydata; struct
archive *a; struct archive_entry *entry;

mydata = malloc(sizeof(struct mydata)); a = archive_read_new();
mydata->name = name; archive_read_support_filter_all(a);
archive_read_support_format_all(a); archive_read_open(a, mydata, myopen,
myread, myclose); while (archive_read_next_header(a, &entry) ==
ARCHIVE_OK) { printf("%s\n",archive_entry_pathname(entry));
archive_read_data_skip(a); } archive_read_free(a); free(mydata); }

la_ssize_t myread(struct archive *a, void *client_data, const void
**buff) { struct mydata *mydata = client_data;

*buff = mydata->buff; return (read(mydata->fd, mydata->buff, 10240)); }

int myopen(struct archive *a, void *client_data) { struct mydata *mydata
= client_data;

mydata->fd = open(mydata->name, O_RDONLY); return (mydata->fd >= 0 ?
ARCHIVE_OK : ARCHIVE_FATAL); }

int myclose(struct archive *a, void *client_data) { struct mydata
*mydata = client_data;

if (mydata->fd > 0) close(mydata->fd); return (ARCHIVE_OK); }

The

library first appeared in

The

library was written by

Many traditional archiver programs treat empty files as valid empty
archives. For example, many implementations of

allow you to append entries to an empty file. Of course, it is
impossible to determine the format of an empty file by inspecting the
contents, so this library treats empty files as having a special

format.
