#+TITLE: Manpages - CURLOPT_READDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_READDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_READDATA - pointer passed to the read callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_READDATA, void
*pointer);

* DESCRIPTION
Data /pointer/ to pass to the file read function. If you use the
/CURLOPT_READFUNCTION(3)/ option, this is the pointer you will get as
input in the 4th argument to the callback.

If you do not specify a read callback but instead rely on the default
internal read function, this data must be a valid readable FILE * (cast
to 'void *').

If you are using libcurl as a win32 DLL, you *MUST* use a
/CURLOPT_READFUNCTION(3)/ if you set this option or you will experience
crashes.

* DEFAULT
By default, this is a FILE * to stdin.

* PROTOCOLS
This is used for all protocols when sending data.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  struct MyData this;
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* pass pointer that gets passed in to the
       CURLOPT_READFUNCTION callback */
    curl_easy_setopt(curl, CURLOPT_READDATA, &this);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
This option was once known by the older name CURLOPT_INFILE, the name
/CURLOPT_READDATA(3)/ was introduced in 7.9.7.

* RETURN VALUE
This will return CURLE_OK.

* SEE ALSO
*CURLOPT_READFUNCTION*(3), *CURLOPT_WRITEDATA*(3),
