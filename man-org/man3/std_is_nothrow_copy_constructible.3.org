#+TITLE: Manpages - std_is_nothrow_copy_constructible.3
#+DESCRIPTION: Linux manpage for std_is_nothrow_copy_constructible.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_nothrow_copy_constructible< _Tp > -
is_nothrow_copy_constructible

* SYNOPSIS
\\

Inherits __is_nothrow_copy_constructible_impl::type.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_nothrow_copy_constructible< _Tp
>"is_nothrow_copy_constructible

Definition at line *1006* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
