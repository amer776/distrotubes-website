#+TITLE: Manpages - XkbAllocControls.3
#+DESCRIPTION: Linux manpage for XkbAllocControls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocControls - Allocates an XkbControlsRec structure in the
XkbDescRec

* SYNOPSIS
*Status XkbAllocControls* *( XkbDescPtr */xkb/* ,* *unsigned int
*/which/* );*

* ARGUMENTS
- /- xkb/ :: Xkb description in which to allocate ctrls rec

- /- which/ :: mask of components of ctrls to allocate

* DESCRIPTION
The need to allocate an XkbControlsRec structure seldom arises; Xkb
creates one when an application calls /XkbGetControls/ or a related
function. For those situations where there is not an XkbControlsRec
structure allocated in the XkbDescRec, allocate one by calling
/XkbAllocControls./

/XkbAllocControls/ allocates the /ctrls/ field of the /xkb/ parameter,
initializes all fields to zero, and returns Success. If the /ctrls/
field is not NULL, /XkbAllocControls/ simply returns Success. If /xkb/
is NULL, /XkbAllocControls/ reports a BadMatch error. If the /ctrls/
field could not be allocated, it reports a BadAlloc error.

The /which/ mask specifies the individual fields of the /ctrls/
structure to be allocated and can contain any of the valid masks defined
in Table 1. (SHOULD THIS COMMENT BE LEFT IN????) Because none of the
currently existing controls have any structures associated with them,
which is currently of little practical value in this call.

Table 1 shows the actual values for the individual mask bits used to
select controls for modification and to enable and disable the control.
Note that the same mask bit is used to specify general modifications to
the parameters used to configure the control (which), and to enable and
disable the control (enabled_ctrls). The anomalies in the table (no "ok"
in column) are for controls that have no configurable attributes; and
for controls that are not boolean controls and therefore cannot be
enabled or disabled.

TABLE

* RETURN VALUES
- SUCCESS :: allocates the ctrls field of the xkb parameter, initializes
  all fields to zero

the ctrls field is not NULL

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadMatch* :: A compatible version of Xkb was not available in the
  server or an argument has correct type and range, but is otherwise
  invalid

* SEE ALSO
*XkbGetControls*(3)
