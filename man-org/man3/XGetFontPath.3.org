#+TITLE: Manpages - XGetFontPath.3
#+DESCRIPTION: Linux manpage for XGetFontPath.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetFontPath.3 is found in manpage for: [[../man3/XSetFontPath.3][man3/XSetFontPath.3]]