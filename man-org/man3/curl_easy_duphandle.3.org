#+TITLE: Manpages - curl_easy_duphandle.3
#+DESCRIPTION: Linux manpage for curl_easy_duphandle.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_easy_duphandle - Clone a libcurl session handle

* SYNOPSIS
*#include <curl/curl.h>*

*CURL *curl_easy_duphandle(CURL **/handle/*);*

* DESCRIPTION
This function will return a new curl handle, a duplicate, using all the
options previously set in the input curl /handle/. Both handles can
subsequently be used independently and they must both be freed with
/curl_easy_cleanup(3)/.

All strings that the input handle has been told to point to (as opposed
to copy) with previous calls to /curl_easy_setopt(3)/ using char *
inputs, will be pointed to by the new handle as well. You must therefore
make sure to keep the data around until both handles have been cleaned
up.

The new handle will *not* inherit any state information, no connections,
no SSL sessions and no cookies. It also will not inherit any share
object states or options (it will be made as if /CURLOPT_SHARE(3)/ was
set to NULL).

In multi-threaded programs, this function must be called in a
synchronous way, the input handle may not be in use when cloned.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  CURL *nother;
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    nother = curl_easy_duphandle(curl);
    res = curl_easy_perform(nother);
    curl_easy_cleanup(nother);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.9

* RETURN VALUE
If this function returns NULL, something went wrong and no valid handle
was returned.

* SEE ALSO
*curl_easy_init*(3),*curl_easy_cleanup*(3),*curl_easy_reset*(3),
*curl_global_init*(3)
