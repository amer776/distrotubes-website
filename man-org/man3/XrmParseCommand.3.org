#+TITLE: Manpages - XrmParseCommand.3
#+DESCRIPTION: Linux manpage for XrmParseCommand.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XrmParseCommand.3 is found in manpage for: [[../man3/XrmInitialize.3][man3/XrmInitialize.3]]