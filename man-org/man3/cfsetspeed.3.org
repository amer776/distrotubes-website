#+TITLE: Manpages - cfsetspeed.3
#+DESCRIPTION: Linux manpage for cfsetspeed.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about cfsetspeed.3 is found in manpage for: [[../man3/termios.3][man3/termios.3]]