#+TITLE: Manpages - islessgreater.3
#+DESCRIPTION: Linux manpage for islessgreater.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about islessgreater.3 is found in manpage for: [[../man3/isgreater.3][man3/isgreater.3]]