#+TITLE: Manpages - FcWeightToOpenTypeDouble.3
#+DESCRIPTION: Linux manpage for FcWeightToOpenTypeDouble.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcWeightToOpenTypeDouble - Convert from fontconfig weight values to
OpenType ones

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

double FcWeightToOpenTypeDouble (double/ot_weight/*);*

* DESCRIPTION
*FcWeightToOpenTypeDouble* is the inverse of *FcWeightFromOpenType*. If
the input is less than FC_WEIGHT_THIN or greater than
FC_WEIGHT_EXTRABLACK, returns -1. Otherwise returns a number in the
range 1 to 1000.

* SINCE
version 2.12.92
