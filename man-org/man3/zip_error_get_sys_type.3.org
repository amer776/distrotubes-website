#+TITLE: Manpages - zip_error_get_sys_type.3
#+DESCRIPTION: Linux manpage for zip_error_get_sys_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

is deprecated; use

and

instead.

Replace

int i = zip_error_get_sys_type(ze);

with

zip_error_t error; zip_error_init_with_code(&error, ze); int i =
zip_error_system_type(&error);

was added in libzip 0.6. It was deprecated in libzip 1.0, use

instead.

and
