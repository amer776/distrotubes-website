#+TITLE: Manpages - strfmon_l.3
#+DESCRIPTION: Linux manpage for strfmon_l.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strfmon_l.3 is found in manpage for: [[../man3/strfmon.3][man3/strfmon.3]]