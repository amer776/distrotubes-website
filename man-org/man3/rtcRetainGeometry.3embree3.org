#+TITLE: Manpages - rtcRetainGeometry.3embree3
#+DESCRIPTION: Linux manpage for rtcRetainGeometry.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcRetainGeometry - increments the geometry reference count
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcRetainGeometry(RTCGeometry geometry);
#+end_example

** DESCRIPTION
Geometry objects are reference counted. The =rtcRetainGeometry= function
increments the reference count of the passed geometry object (=geometry=
argument). This function together with =rtcReleaseGeometry= allows to
use the internal reference counting in a C++ wrapper class to handle the
ownership of the object.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewGeometry], [rtcReleaseGeometry]
