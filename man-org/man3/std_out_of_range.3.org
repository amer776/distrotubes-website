#+TITLE: Manpages - std_out_of_range.3
#+DESCRIPTION: Linux manpage for std_out_of_range.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::out_of_range

* SYNOPSIS
\\

Inherits *std::logic_error*.

** Public Member Functions
*out_of_range* (const char *) _GLIBCXX_TXN_SAFE\\

*out_of_range* (const *out_of_range* &)=default\\

*out_of_range* (const *string* &__arg) _GLIBCXX_TXN_SAFE\\

*out_of_range* (*out_of_range* &&)=default\\

*out_of_range* & *operator=* (const *out_of_range* &)=default\\

*out_of_range* & *operator=* (*out_of_range* &&)=default\\

virtual const char * *what* () const noexcept\\

* Detailed Description
This represents an argument whose value is not within the expected range
(e.g., boundary checks in basic_string).\\

Definition at line *200* of file *stdexcept*.

* Member Function Documentation
** virtual const char * std::logic_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::future_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
