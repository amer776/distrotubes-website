#+TITLE: Manpages - __gnu_parallel__LoserTreeBase__Loser.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__LoserTreeBase__Loser.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_LoserTreeBase< _Tp, _Compare >::_Loser - Internal
representation of a _LoserTree element.

* SYNOPSIS
\\

=#include <losertree.h>=

** Public Attributes
_Tp *_M_key*\\
_M_key of the element in the _LoserTree.

int *_M_source*\\
__index of the __source __sequence.

bool *_M_sup*\\
flag, true iff this is a 'maximum' __sentinel.

* Detailed Description
** "template<typename _Tp, typename _Compare>
\\
struct __gnu_parallel::_LoserTreeBase< _Tp, _Compare >::_Loser"Internal
representation of a _LoserTree element.

Definition at line *59* of file *losertree.h*.

* Member Data Documentation
** template<typename _Tp , typename _Compare > _Tp
*__gnu_parallel::_LoserTreeBase*< _Tp, _Compare >::_Loser::_M_key
_M_key of the element in the _LoserTree.

Definition at line *66* of file *losertree.h*.

Referenced by *__gnu_parallel::_LoserTreeBase< _Tp, _Compare
>::__insert_start()*.

** template<typename _Tp , typename _Compare > int
*__gnu_parallel::_LoserTreeBase*< _Tp, _Compare >::_Loser::_M_source
__index of the __source __sequence.

Definition at line *64* of file *losertree.h*.

Referenced by *__gnu_parallel::_LoserTreeBase< _Tp, _Compare
>::__get_min_source()*, and *__gnu_parallel::_LoserTreeBase< _Tp,
_Compare >::__insert_start()*.

** template<typename _Tp , typename _Compare > bool
*__gnu_parallel::_LoserTreeBase*< _Tp, _Compare >::_Loser::_M_sup
flag, true iff this is a 'maximum' __sentinel.

Definition at line *62* of file *losertree.h*.

Referenced by *__gnu_parallel::_LoserTreeBase< _Tp, _Compare
>::__insert_start()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
