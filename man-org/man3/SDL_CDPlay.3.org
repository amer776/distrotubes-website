#+TITLE: Manpages - SDL_CDPlay.3
#+DESCRIPTION: Linux manpage for SDL_CDPlay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDPlay - Play a CD

* SYNOPSIS
*#include "SDL.h"*

*int SDL_CDPlay*(*SDL_CD *cdrom, int start, int length*);

* DESCRIPTION
Plays the given *cdrom*, starting a frame *start* for *length* frames.

* RETURN VALUES
Returns *0* on success, or *-1* on an error.

* SEE ALSO
*SDL_CDPlayTracks*, *SDL_CDStop*
