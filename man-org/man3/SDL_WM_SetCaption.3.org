#+TITLE: Manpages - SDL_WM_SetCaption.3
#+DESCRIPTION: Linux manpage for SDL_WM_SetCaption.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WM_SetCaption - Sets the window tile and icon name.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_WM_SetCaption*(*const char *title, const char *icon*);

* DESCRIPTION
Sets the title-bar and icon name of the display window.

* SEE ALSO
*SDL_WM_GetCaption*, *SDL_WM_SetIcon*
