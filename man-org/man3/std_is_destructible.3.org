#+TITLE: Manpages - std_is_destructible.3
#+DESCRIPTION: Linux manpage for std_is_destructible.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_destructible< _Tp > - is_destructible

* SYNOPSIS
\\

Inherits __is_destructible_safe::type.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_destructible< _Tp >"is_destructible

Definition at line *846* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
