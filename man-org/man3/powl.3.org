#+TITLE: Manpages - powl.3
#+DESCRIPTION: Linux manpage for powl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about powl.3 is found in manpage for: [[../man3/pow.3][man3/pow.3]]