#+TITLE: Manpages - cap_iab_compare.3
#+DESCRIPTION: Linux manpage for cap_iab_compare.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about cap_iab_compare.3 is found in manpage for: [[../man3/cap_iab.3][man3/cap_iab.3]]