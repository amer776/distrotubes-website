#+TITLE: Manpages - ceil.3
#+DESCRIPTION: Linux manpage for ceil.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ceil, ceilf, ceill - ceiling function: smallest integral value not less
than argument

* SYNOPSIS
#+begin_example
  #include <math.h>

  double ceil(double x);
  float ceilf(float x);
  long double ceill(long double x);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*ceilf*(), *ceill*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
These functions return the smallest integral value that is not less than
/x/.

For example, /ceil(0.5)/ is 1.0, and /ceil(-0.5)/ is 0.0.

* RETURN VALUE
These functions return the ceiling of /x/.

If /x/ is integral, +0, -0, NaN, or infinite, /x/ itself is returned.

* ERRORS
No errors occur. POSIX.1-2001 documents a range error for overflows, but
see NOTES.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *ceil*(), *ceilf*(), *ceill*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

The variant returning /double/ also conforms to SVr4, 4.3BSD, C89.

* NOTES
SUSv2 and POSIX.1-2001 contain text about overflow (which might set
/errno/ to *ERANGE*, or raise an *FE_OVERFLOW* exception). In practice,
the result cannot overflow on any current machine, so this
error-handling stuff is just nonsense. (More precisely, overflow can
happen only when the maximum value of the exponent is smaller than the
number of mantissa bits. For the IEEE-754 standard 32-bit and 64-bit
floating-point numbers the maximum value of the exponent is 128
(respectively, 1024), and the number of mantissa bits is 24
(respectively, 53).)

The integral value returned by these functions may be too large to store
in an integer type (/int/, /long/, etc.). To avoid an overflow, which
will produce undefined results, an application should perform a range
check on the returned value before assigning it to an integer type.

* SEE ALSO
*floor*(3), *lrint*(3), *nearbyint*(3), *rint*(3), *round*(3),
*trunc*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
