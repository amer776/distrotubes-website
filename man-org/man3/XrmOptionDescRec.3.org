#+TITLE: Manpages - XrmOptionDescRec.3
#+DESCRIPTION: Linux manpage for XrmOptionDescRec.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XrmOptionDescRec.3 is found in manpage for: [[../man3/XrmInitialize.3][man3/XrmInitialize.3]]