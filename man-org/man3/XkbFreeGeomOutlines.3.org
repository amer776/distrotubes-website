#+TITLE: Manpages - XkbFreeGeomOutlines.3
#+DESCRIPTION: Linux manpage for XkbFreeGeomOutlines.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFreeGeomOutlines - Free geometry outlines

* SYNOPSIS
*void XkbFreeGeomOutlines* *( XkbShapePtr */shape/* ,* *int */first/* ,*
*int */count/* ,* *Bool */free_all/* );*

* ARGUMENTS
- /- shape/ :: shape in which outlines should be freed

- /- first/ :: first outline to be freed

- /- count/ :: number of outlines to be freed

- /- free_all/ :: True => all outlines are freed

* DESCRIPTION
If /free_all/ is True, all outlines are freed regardless of the value of
/first/ or /count./ Otherwise, /count/ outlines are freed beginning with
the one specified by /first./
