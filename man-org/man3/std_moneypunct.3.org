#+TITLE: Manpages - std_moneypunct.3
#+DESCRIPTION: Linux manpage for std_moneypunct.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::moneypunct< _CharT, _Intl > - Primary class template moneypunct.

* SYNOPSIS
\\

=#include <locale_facets_nonio.h>=

Inherits *std::locale::facet*, and *std::money_base*.

Inherited by *std::moneypunct_byname< _CharT, _Intl >*.

** Public Types
enum { *_S_minus*, *_S_zero*, *_S_end* }\\

typedef __moneypunct_cache< _CharT, _Intl > *__cache_type*\\

enum *part* { *none*, *space*, *symbol*, *sign*, *value* }\\

\\

typedef _CharT *char_type*\\
Public typedefs.

typedef *basic_string*< _CharT > *string_type*\\
Public typedefs.

** Public Member Functions
*moneypunct* (__c_locale __cloc, const char *__s, size_t __refs=0)\\
Internal constructor. Not for general use.

*moneypunct* (__cache_type *__cache, size_t __refs=0)\\
Constructor performs initialization.

*moneypunct* (size_t __refs=0)\\
Constructor performs initialization.

*string_type* *curr_symbol* () const\\
Return currency symbol string.

*char_type* *decimal_point* () const\\
Return decimal point character.

int *frac_digits* () const\\
Return number of digits in fraction.

*string* *grouping* () const\\
Return grouping specification.

*string_type* *negative_sign* () const\\
Return negative sign string.

*string_type* *positive_sign* () const\\
Return positive sign string.

*char_type* *thousands_sep* () const\\
Return thousands separator character.

\\

pattern *pos_format* () const\\
Return pattern for money values.

pattern *neg_format* () const\\
Return pattern for money values.

** Static Public Member Functions
static pattern *_S_construct_pattern* (char __precedes, char __space,
char __posn) throw ()\\

** Static Public Attributes
static const char * *_S_atoms*\\

static const pattern *_S_default_pattern*\\

static *locale::id* *id*\\
Numpunct facet id.

static const bool *intl*\\
This value is provided by the standard, but no reason for its existence.

** Protected Member Functions
virtual *~moneypunct* ()\\
Destructor.

void *_M_initialize_moneypunct* (__c_locale __cloc=0, const char
*__name=0)\\

void *_M_initialize_moneypunct* (__c_locale, const char *)\\

void *_M_initialize_moneypunct* (__c_locale, const char *)\\

void *_M_initialize_moneypunct* (__c_locale, const char *)\\

void *_M_initialize_moneypunct* (__c_locale, const char *)\\

virtual *string_type* *do_curr_symbol* () const\\
Return currency symbol string.

virtual *char_type* *do_decimal_point* () const\\
Return decimal point character.

virtual int *do_frac_digits* () const\\
Return number of digits in fraction.

virtual *string* *do_grouping* () const\\
Return grouping specification.

virtual pattern *do_neg_format* () const\\
Return pattern for money values.

virtual *string_type* *do_negative_sign* () const\\
Return negative sign string.

virtual pattern *do_pos_format* () const\\
Return pattern for money values.

virtual *string_type* *do_positive_sign* () const\\
Return positive sign string.

virtual *char_type* *do_thousands_sep* () const\\
Return thousands separator character.

** Static Protected Member Functions
static __c_locale *_S_clone_c_locale* (__c_locale &__cloc) throw ()\\

static void *_S_create_c_locale* (__c_locale &__cloc, const char *__s,
__c_locale __old=0)\\

static void *_S_destroy_c_locale* (__c_locale &__cloc)\\

static __c_locale *_S_get_c_locale* ()\\

static const char * *_S_get_c_name* () throw ()\\

static __c_locale *_S_lc_ctype_c_locale* (__c_locale __cloc, const char
*__s)\\

* Detailed Description
** "template<typename _CharT, bool _Intl>
\\
class std::moneypunct< _CharT, _Intl >"Primary class template
moneypunct.

This facet encapsulates the punctuation, grouping and other formatting
features of money amount string representations.

Definition at line *1024* of file *locale_facets_nonio.h*.

* Member Typedef Documentation
** template<typename _CharT , bool _Intl> typedef
__moneypunct_cache<_CharT, _Intl> *std::moneypunct*< _CharT, _Intl
>::__cache_type
Definition at line *1033* of file *locale_facets_nonio.h*.

** template<typename _CharT , bool _Intl> typedef _CharT
*std::moneypunct*< _CharT, _Intl >::*char_type*
Public typedefs.

Definition at line *1030* of file *locale_facets_nonio.h*.

** template<typename _CharT , bool _Intl> typedef *basic_string*<_CharT>
*std::moneypunct*< _CharT, _Intl >::*string_type*
Public typedefs.

Definition at line *1031* of file *locale_facets_nonio.h*.

* Member Enumeration Documentation
** anonymous enum= [inherited]=
Definition at line *936* of file *locale_facets_nonio.h*.

** enum std::money_base::part= [inherited]=
Definition at line *931* of file *locale_facets_nonio.h*.

* Constructor & Destructor Documentation
** template<typename _CharT , bool _Intl> *std::moneypunct*< _CharT,
_Intl >::*moneypunct* (size_t __refs = =0=)= [inline]=, = [explicit]=
Constructor performs initialization. This is the constructor provided by
the standard.

*Parameters*

#+begin_quote
  /__refs/ Passed to the base facet class.
#+end_quote

Definition at line *1053* of file *locale_facets_nonio.h*.

** template<typename _CharT , bool _Intl> *std::moneypunct*< _CharT,
_Intl >::*moneypunct* (__cache_type * __cache, size_t __refs =
=0=)= [inline]=, = [explicit]=
Constructor performs initialization. This is an internal constructor.

*Parameters*

#+begin_quote
  /__cache/ Cache for optimization.\\
  /__refs/ Passed to the base facet class.
#+end_quote

Definition at line *1066* of file *locale_facets_nonio.h*.

** template<typename _CharT , bool _Intl> *std::moneypunct*< _CharT,
_Intl >::*moneypunct* (__c_locale __cloc, const char * __s, size_t
__refs = =0=)= [inline]=, = [explicit]=
Internal constructor. Not for general use. This is a constructor for use
by the library itself to set up new locales.

*Parameters*

#+begin_quote
  /__cloc/ The C locale.\\
  /__s/ The name of a locale.\\
  /__refs/ Passed to the base facet class.
#+end_quote

Definition at line *1081* of file *locale_facets_nonio.h*.

** template<typename _CharT , bool _Intl> virtual *std::moneypunct*<
_CharT, _Intl >::~*moneypunct* ()= [protected]=, = [virtual]=
Destructor.

* Member Function Documentation
** template<typename _CharT , bool _Intl> *string_type*
*std::moneypunct*< _CharT, _Intl >::curr_symbol () const= [inline]=
Return currency symbol string. This function returns a string_type to
use as a currency symbol. It does so by returning returning
moneypunct<char_type>::do_curr_symbol().

*Returns*

#+begin_quote
  /string_type/ representing a currency symbol.
#+end_quote

Definition at line *1151* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_curr_symbol()*.

** template<typename _CharT , bool _Intl> *char_type* *std::moneypunct*<
_CharT, _Intl >::decimal_point () const= [inline]=
Return decimal point character. This function returns a char_type to use
as a decimal point. It does so by returning returning
moneypunct<char_type>::do_decimal_point().

*Returns*

#+begin_quote
  /char_type/ representing a decimal point.
#+end_quote

Definition at line *1095* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_decimal_point()*.

** template<typename _CharT , bool _Intl> virtual *string_type*
*std::moneypunct*< _CharT, _Intl >::do_curr_symbol () const= [inline]=,
= [protected]=, = [virtual]=
Return currency symbol string. This function returns a string_type to
use as a currency symbol. This function is a hook for derived classes to
change the value returned.

*See also*

#+begin_quote
  curr_symbol() for details.
#+end_quote

*Returns*

#+begin_quote
  /string_type/ representing a currency symbol.
#+end_quote

Definition at line *1297* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::curr_symbol()*.

** template<typename _CharT , bool _Intl> virtual *char_type*
*std::moneypunct*< _CharT, _Intl >::do_decimal_point ()
const= [inline]=, = [protected]=, = [virtual]=
Return decimal point character. Returns a char_type to use as a decimal
point. This function is a hook for derived classes to change the value
returned.

*Returns*

#+begin_quote
  /char_type/ representing a decimal point.
#+end_quote

Definition at line *1259* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::decimal_point()*.

** template<typename _CharT , bool _Intl> virtual int *std::moneypunct*<
_CharT, _Intl >::do_frac_digits () const= [inline]=, = [protected]=,
= [virtual]=
Return number of digits in fraction. This function returns the exact
number of digits that make up the fractional part of a money amount.
This function is a hook for derived classes to change the value
returned.

*See also*

#+begin_quote
  frac_digits() for details.
#+end_quote

*Returns*

#+begin_quote
  Number of digits in amount fraction.
#+end_quote

Definition at line *1337* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::frac_digits()*.

** template<typename _CharT , bool _Intl> virtual *string*
*std::moneypunct*< _CharT, _Intl >::do_grouping () const= [inline]=,
= [protected]=, = [virtual]=
Return grouping specification. Returns a string representing groupings
for the integer part of a number. This function is a hook for derived
classes to change the value returned.

*See also*

#+begin_quote
  grouping() for details.
#+end_quote

*Returns*

#+begin_quote
  String representing grouping specification.
#+end_quote

Definition at line *1284* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::grouping()*.

** template<typename _CharT , bool _Intl> virtual pattern
*std::moneypunct*< _CharT, _Intl >::do_neg_format () const= [inline]=,
= [protected]=, = [virtual]=
Return pattern for money values. This function returns a pattern
describing the formatting of a negative valued money amount. This
function is a hook for derived classes to change the value returned.

*See also*

#+begin_quote
  neg_format() for details.
#+end_quote

*Returns*

#+begin_quote
  Pattern for money values.
#+end_quote

Definition at line *1365* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::neg_format()*.

** template<typename _CharT , bool _Intl> virtual *string_type*
*std::moneypunct*< _CharT, _Intl >::do_negative_sign ()
const= [inline]=, = [protected]=, = [virtual]=
Return negative sign string. This function returns a string_type to use
as a sign for negative amounts. This function is a hook for derived
classes to change the value returned.

*See also*

#+begin_quote
  negative_sign() for details.
#+end_quote

*Returns*

#+begin_quote
  /string_type/ representing a negative sign.
#+end_quote

Definition at line *1323* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::negative_sign()*.

** template<typename _CharT , bool _Intl> virtual pattern
*std::moneypunct*< _CharT, _Intl >::do_pos_format () const= [inline]=,
= [protected]=, = [virtual]=
Return pattern for money values. This function returns a pattern
describing the formatting of a positive valued money amount. This
function is a hook for derived classes to change the value returned.

*See also*

#+begin_quote
  pos_format() for details.
#+end_quote

*Returns*

#+begin_quote
  Pattern for money values.
#+end_quote

Definition at line *1351* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::pos_format()*.

** template<typename _CharT , bool _Intl> virtual *string_type*
*std::moneypunct*< _CharT, _Intl >::do_positive_sign ()
const= [inline]=, = [protected]=, = [virtual]=
Return positive sign string. This function returns a string_type to use
as a sign for positive amounts. This function is a hook for derived
classes to change the value returned.

*See also*

#+begin_quote
  positive_sign() for details.
#+end_quote

*Returns*

#+begin_quote
  /string_type/ representing a positive sign.
#+end_quote

Definition at line *1310* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::positive_sign()*.

** template<typename _CharT , bool _Intl> virtual *char_type*
*std::moneypunct*< _CharT, _Intl >::do_thousands_sep ()
const= [inline]=, = [protected]=, = [virtual]=
Return thousands separator character. Returns a char_type to use as a
thousands separator. This function is a hook for derived classes to
change the value returned.

*Returns*

#+begin_quote
  /char_type/ representing a thousands separator.
#+end_quote

Definition at line *1271* of file *locale_facets_nonio.h*.

Referenced by *std::moneypunct< _CharT, _Intl >::thousands_sep()*.

** template<typename _CharT , bool _Intl> int *std::moneypunct*< _CharT,
_Intl >::frac_digits () const= [inline]=
Return number of digits in fraction. This function returns the exact
number of digits that make up the fractional part of a money amount. It
does so by returning returning moneypunct<char_type>::do_frac_digits().

The fractional part of a money amount is optional. But if it is present,
there must be frac_digits() digits.

*Returns*

#+begin_quote
  Number of digits in amount fraction.
#+end_quote

Definition at line *1201* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_frac_digits()*.

** template<typename _CharT , bool _Intl> *string* *std::moneypunct*<
_CharT, _Intl >::grouping () const= [inline]=
Return grouping specification. This function returns a string
representing groupings for the integer part of an amount. Groupings
indicate where thousands separators should be inserted.

Each char in the return string is interpret as an integer rather than a
character. These numbers represent the number of digits in a group. The
first char in the string represents the number of digits in the least
significant group. If a char is negative, it indicates an unlimited
number of digits for the group. If more chars from the string are
required to group a number, the last char is used repeatedly.

For example, if the grouping() returns =\003\002= and is applied to the
number 123456789, this corresponds to 12,34,56,789. Note that if the
string was =32=, this would put more than 50 digits into the least
significant group if the character set is ASCII.

The string is returned by calling moneypunct<char_type>::do_grouping().

*Returns*

#+begin_quote
  string representing grouping specification.
#+end_quote

Definition at line *1138* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_grouping()*.

** template<typename _CharT , bool _Intl> pattern *std::moneypunct*<
_CharT, _Intl >::neg_format () const= [inline]=
Return pattern for money values. This function returns a pattern
describing the formatting of a positive or negative valued money amount.
It does so by returning returning moneypunct<char_type>::do_pos_format()
or moneypunct<char_type>::do_neg_format().

The pattern has 4 fields describing the ordering of symbol, sign, value,
and none or space. There must be one of each in the pattern. The none
and space enums may not appear in the first field and space may not
appear in the final field.

The parts of a money string must appear in the order indicated by the
fields of the pattern. The symbol field indicates that the value of
curr_symbol() may be present. The sign field indicates that the value of
positive_sign() or negative_sign() must be present. The value field
indicates that the absolute value of the money amount is present. none
indicates 0 or more whitespace characters, except at the end, where it
permits no whitespace. space indicates that 1 or more whitespace
characters must be present.

For example, for the US locale and pos_format() pattern
{symbol,sign,value,none}, curr_symbol() == '$' positive_sign() == '+',
and value 10.01, and options set to force the symbol, the corresponding
string is =$+10.01=.

*Returns*

#+begin_quote
  Pattern for money values.
#+end_quote

Definition at line *1241* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_neg_format()*.

** template<typename _CharT , bool _Intl> *string_type*
*std::moneypunct*< _CharT, _Intl >::negative_sign () const= [inline]=
Return negative sign string. This function returns a string_type to use
as a sign for negative amounts. It does so by returning returning
moneypunct<char_type>::do_negative_sign().

If the return value contains more than one character, the first
character appears in the position indicated by neg_format() and the
remainder appear at the end of the formatted string.

*Returns*

#+begin_quote
  /string_type/ representing a negative sign.
#+end_quote

Definition at line *1185* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_negative_sign()*.

** template<typename _CharT , bool _Intl> pattern *std::moneypunct*<
_CharT, _Intl >::pos_format () const= [inline]=
Return pattern for money values. This function returns a pattern
describing the formatting of a positive or negative valued money amount.
It does so by returning returning moneypunct<char_type>::do_pos_format()
or moneypunct<char_type>::do_neg_format().

The pattern has 4 fields describing the ordering of symbol, sign, value,
and none or space. There must be one of each in the pattern. The none
and space enums may not appear in the first field and space may not
appear in the final field.

The parts of a money string must appear in the order indicated by the
fields of the pattern. The symbol field indicates that the value of
curr_symbol() may be present. The sign field indicates that the value of
positive_sign() or negative_sign() must be present. The value field
indicates that the absolute value of the money amount is present. none
indicates 0 or more whitespace characters, except at the end, where it
permits no whitespace. space indicates that 1 or more whitespace
characters must be present.

For example, for the US locale and pos_format() pattern
{symbol,sign,value,none}, curr_symbol() == '$' positive_sign() == '+',
and value 10.01, and options set to force the symbol, the corresponding
string is =$+10.01=.

*Returns*

#+begin_quote
  Pattern for money values.
#+end_quote

Definition at line *1237* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_pos_format()*.

** template<typename _CharT , bool _Intl> *string_type*
*std::moneypunct*< _CharT, _Intl >::positive_sign () const= [inline]=
Return positive sign string. This function returns a string_type to use
as a sign for positive amounts. It does so by returning returning
moneypunct<char_type>::do_positive_sign().

If the return value contains more than one character, the first
character appears in the position indicated by pos_format() and the
remainder appear at the end of the formatted string.

*Returns*

#+begin_quote
  /string_type/ representing a positive sign.
#+end_quote

Definition at line *1168* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_positive_sign()*.

** template<typename _CharT , bool _Intl> *char_type* *std::moneypunct*<
_CharT, _Intl >::thousands_sep () const= [inline]=
Return thousands separator character. This function returns a char_type
to use as a thousands separator. It does so by returning returning
moneypunct<char_type>::do_thousands_sep().

*Returns*

#+begin_quote
  char_type representing a thousands separator.
#+end_quote

Definition at line *1108* of file *locale_facets_nonio.h*.

References *std::moneypunct< _CharT, _Intl >::do_thousands_sep()*.

* Member Data Documentation
** const char* std::money_base::_S_atoms= [static]=, = [inherited]=
Definition at line *945* of file *locale_facets_nonio.h*.

** const pattern std::money_base::_S_default_pattern= [static]=,
= [inherited]=
Definition at line *934* of file *locale_facets_nonio.h*.

** template<typename _CharT , bool _Intl> *locale::id*
*std::moneypunct*< _CharT, _Intl >::id= [static]=
Numpunct facet id.

Definition at line *1043* of file *locale_facets_nonio.h*.

** template<typename _CharT , bool _Intl> const bool *std::moneypunct*<
_CharT, _Intl >::intl= [static]=
This value is provided by the standard, but no reason for its existence.

Definition at line *1041* of file *locale_facets_nonio.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
