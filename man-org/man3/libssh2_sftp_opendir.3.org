#+TITLE: Manpages - libssh2_sftp_opendir.3
#+DESCRIPTION: Linux manpage for libssh2_sftp_opendir.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_sftp_opendir - convenience macro for /libssh2_sftp_open_ex(3)/
calls

* SYNOPSIS
#include <libssh2.h>

LIBSSH2_SFTP_HANDLE * libssh2_sftp_opendir(LIBSSH2_SFTP *sftp, const
char *path);

* DESCRIPTION
This is a macro defined in a public libssh2 header file that is using
the underlying function /libssh2_sftp_open_ex(3)/.

* RETURN VALUE
See /libssh2_sftp_open_ex(3)/

* ERRORS
See /libssh2_sftp_open_ex(3)/

* SEE ALSO
*libssh2_sftp_open_ex(3)*
