#+TITLE: Manpages - SDL_DestroyCond.3
#+DESCRIPTION: Linux manpage for SDL_DestroyCond.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_DestroyCond - Destroy a condition variable

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*void SDL_DestroyCond*(*SDL_cond *cond*);

* DESCRIPTION
Destroys a condition variable.

* SEE ALSO
*SDL_CreateCond*
