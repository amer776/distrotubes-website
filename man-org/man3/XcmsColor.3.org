#+TITLE: Manpages - XcmsColor.3
#+DESCRIPTION: Linux manpage for XcmsColor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XcmsColor, XcmsRGB, XcmsRGBi, XcmsCIEXYZ, XcmsCIEuvY, XcmsCIExyY,
XcmsCIELab, XcmsCIELuv, XcmsTekHVC, XcmsPad - Xcms color structure

* STRUCTURES
The structure for *XcmsColor* contains:

#+begin_example
  typedef unsigned long XcmsColorFormat;			/* Color Specification Format */

  typedef struct {
  	union {
  		XcmsRGB RGB;
  		XcmsRGBi RGBi;
  		XcmsCIEXYZ CIEXYZ;
  		XcmsCIEuvY CIEuvY;
  		XcmsCIExyY CIExyY;
  		XcmsCIELab CIELab;
  		XcmsCIELuv CIELuv;
  		XcmsTekHVC TekHVC;
  		XcmsPad Pad;
  	} spec;
  	unsigned long pixel;
  	XcmsColorFormat format;
  } XcmsColor;			/* Xcms Color Structure */
#+end_example

#+begin_example
  typedef double XcmsFloat;

  typedef struct {
  	unsigned short red;	/* 0x0000 to 0xffff */
  	unsigned short green;	/* 0x0000 to 0xffff */
  	unsigned short blue;	/* 0x0000 to 0xffff */
  } XcmsRGB;		/* RGB Device */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat red;	/* 0.0 to 1.0 */
  	XcmsFloat green;	/* 0.0 to 1.0 */
  	XcmsFloat blue;	/* 0.0 to 1.0 */
  } XcmsRGBi;		/* RGB Intensity */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat X;
  	XcmsFloat Y;	/* 0.0 to 1.0 */
  	XcmsFloat Z;
  } XcmsCIEXYZ;		/* CIE XYZ */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat u_prime;	/* 0.0 to ~0.6 */
  	XcmsFloat v_prime;	/* 0.0 to ~0.6 */
  	XcmsFloat Y; 	/* 0.0 to 1.0 */
  } XcmsCIEuvY;		/* CIE u'v'Y */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat x; 	/* 0.0 to ~.75 */
  	XcmsFloat y; 	/* 0.0 to ~.85 */
  	XcmsFloat Y; 	/* 0.0 to 1.0 */
  } XcmsCIExyY;		/* CIE xyY */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat L_star; 	/* 0.0 to 100.0 */
  	XcmsFloat a_star;
  	XcmsFloat b_star;
  } XcmsCIELab;		/* CIE L*a*b* */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat L_star; 	/* 0.0 to 100.0 */
  	XcmsFloat u_star;
  	XcmsFloat v_star;
  } XcmsCIELuv;		/* CIE L*u*v* */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat H; 	/* 0.0 to 360.0 */
  	XcmsFloat V; 	/* 0.0 to 100.0 */
  	XcmsFloat C; 	/* 0.0 to 100.0 */
  } XcmsTekHVC;		/* TekHVC */
#+end_example

#+begin_example
  typedef struct {
  	XcmsFloat pad0;
  	XcmsFloat pad1;
  	XcmsFloat pad2;
  	XcmsFloat pad3;
  } XcmsPad;		/* four doubles */
#+end_example

* DESCRIPTION
The *XcmsColor* structure contains a union of substructures, each
supporting color specification encoding for a particular color space.

* SEE ALSO
XcmsAllocColor(3), XcmsStoreColor(3), XcmsConvertColors(3),

/Xlib - C Language X Interface/
