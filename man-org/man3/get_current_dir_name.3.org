#+TITLE: Manpages - get_current_dir_name.3
#+DESCRIPTION: Linux manpage for get_current_dir_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about get_current_dir_name.3 is found in manpage for: [[../man3/getcwd.3][man3/getcwd.3]]