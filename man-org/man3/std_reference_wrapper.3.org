#+TITLE: Manpages - std_reference_wrapper.3
#+DESCRIPTION: Linux manpage for std_reference_wrapper.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::reference_wrapper< _Tp > - Primary class template for
reference_wrapper.

* SYNOPSIS
\\

=#include <refwrap.h>=

Inherits _Reference_wrapper_base_memfun< remove_cv< _Tp >::type >.

** Public Types
typedef _Tp *type*\\

** Public Member Functions
template<typename _Up , typename = __not_same<_Up>, typename =
decltype(reference_wrapper::_S_fun(std::declval<_Up>()))> constexpr
*reference_wrapper* (_Up &&__uref)
noexcept(noexcept(reference_wrapper::_S_fun(std::declval< _Up >())))\\

*reference_wrapper* (const *reference_wrapper* &)=default\\

constexpr _Tp & *get* () const noexcept\\

constexpr *operator _Tp &* () const noexcept\\

template<typename... _Args> constexpr *result_of*< _Tp &(_Args
&&...)>::type *operator()* (_Args &&... __args) const\\

*reference_wrapper* & *operator=* (const *reference_wrapper*
&)=default\\

** Related Functions
(Note that these are not member functions.)

\\

template<typename _Tp > constexpr *reference_wrapper*< _Tp > *ref* (_Tp
&__t) noexcept\\

template<typename _Tp > constexpr *reference_wrapper*< const _Tp >
*cref* (const _Tp &__t) noexcept\\
Denotes a const reference should be taken to a variable.

template<typename _Tp > constexpr *reference_wrapper*< _Tp > *ref*
(*reference_wrapper*< _Tp > __t) noexcept\\
std::ref overload to prevent wrapping a reference_wrapper

template<typename _Tp > constexpr *reference_wrapper*< const _Tp >
*cref* (*reference_wrapper*< _Tp > __t) noexcept\\
std::cref overload to prevent wrapping a reference_wrapper

* Detailed Description
** "template<typename _Tp>
\\
class std::reference_wrapper< _Tp >"Primary class template for
reference_wrapper.

Definition at line *294* of file *refwrap.h*.

* Member Typedef Documentation
** template<typename _Tp > typedef _Tp *std::reference_wrapper*< _Tp
>::type
Definition at line *313* of file *refwrap.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > template<typename _Up , typename =
__not_same<_Up>, typename =
decltype(reference_wrapper::_S_fun(std::declval<_Up>()))> constexpr
*std::reference_wrapper*< _Tp >::*reference_wrapper* (_Up &&
__uref)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *321* of file *refwrap.h*.

* Member Function Documentation
** template<typename _Tp > constexpr _Tp & *std::reference_wrapper*< _Tp
>::get () const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *337* of file *refwrap.h*.

** template<typename _Tp > constexpr *std::reference_wrapper*< _Tp
>::operator _Tp & () const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *332* of file *refwrap.h*.

** template<typename _Tp > template<typename... _Args> constexpr
*result_of*< _Tp &(_Args &&...)>::type *std::reference_wrapper*< _Tp
>::operator() (_Args &&... __args) const= [inline]=, = [constexpr]=
Definition at line *343* of file *refwrap.h*.

* Friends And Related Function Documentation
** template<typename _Tp > constexpr *reference_wrapper*< const _Tp >
cref (const _Tp & __t)= [related]=
Denotes a const reference should be taken to a variable.

Definition at line *371* of file *refwrap.h*.

** template<typename _Tp > constexpr *reference_wrapper*< const _Tp >
cref (*reference_wrapper*< _Tp > __t)= [related]=
std::cref overload to prevent wrapping a reference_wrapper

Definition at line *391* of file *refwrap.h*.

** template<typename _Tp > constexpr *reference_wrapper*< _Tp > ref (_Tp
& __t)= [related]=
Denotes a reference should be taken to a variable.

Definition at line *364* of file *refwrap.h*.

** template<typename _Tp > constexpr *reference_wrapper*< _Tp > ref
(*reference_wrapper*< _Tp > __t)= [related]=
std::ref overload to prevent wrapping a reference_wrapper

Definition at line *384* of file *refwrap.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
