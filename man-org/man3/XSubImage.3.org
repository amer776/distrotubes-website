#+TITLE: Manpages - XSubImage.3
#+DESCRIPTION: Linux manpage for XSubImage.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSubImage.3 is found in manpage for: [[../man3/XInitImage.3][man3/XInitImage.3]]