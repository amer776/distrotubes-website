#+TITLE: Manpages - SDL_MouseButtonEvent.3
#+DESCRIPTION: Linux manpage for SDL_MouseButtonEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_MouseButtonEvent - Mouse button event structure

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    Uint8 type;
    Uint8 button;
    Uint8 state;
    Uint16 x, y;
  } SDL_MouseButtonEvent;
#+end_example

* STRUCTURE DATA
- *type* :: *SDL_MOUSEBUTTONDOWN* or *SDL_MOUSEBUTTONUP*

- *button* :: The mouse button index (SDL_BUTTON_LEFT,
  SDL_BUTTON_MIDDLE, SDL_BUTTON_RIGHT)

- *state* :: *SDL_PRESSED* or *SDL_RELEASED*

- *x*, *y* :: The X/Y coordinates of the mouse at press/release time

* DESCRIPTION
*SDL_MouseButtonEvent* is a member of the *SDL_Event* union and is used
when an event of type *SDL_MOUSEBUTTONDOWN* or *SDL_MOUSEBUTTONUP* is
reported.

When a mouse button press or release is detected then number of the
button pressed (from 1 to 255, with 1 usually being the left button and
2 the right) is placed into *button*, the position of the mouse when
this event occured is stored in the *x* and the *y* fields. Like
*SDL_KeyboardEvent*, information on whether the event was a press or a
release event is stored in both the *type* and *state* fields, but this
should be obvious.

* SEE ALSO
*SDL_Event*, *SDL_MouseMotionEvent*
