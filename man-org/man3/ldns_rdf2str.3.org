#+TITLE: Manpages - ldns_rdf2str.3
#+DESCRIPTION: Linux manpage for ldns_rdf2str.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr2str, ldns_pkt2str, ldns_rdf2str, ldns_rr_list2str,
ldns_key2str - functions for conversions to string

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

char* ldns_rr2str(const ldns_rr *rr);

char* ldns_pkt2str(const ldns_pkt *pkt);

char* ldns_rdf2str(const ldns_rdf *rdf);

char* ldns_rr_list2str(const ldns_rr_list *rr_list);

char* ldns_key2str(const ldns_key *k);

* DESCRIPTION
/ldns_rr2str/() Converts the data in the resource record to presentation
format and returns that as a char *. Remember to free it.

.br *rr*: The rdata field to convert .br Returns null terminated char *
data, or NULL on error

/ldns_pkt2str/() Converts the data in the DNS packet to presentation
format and returns that as a char *. Remember to free it.

.br *pkt*: The rdata field to convert .br Returns null terminated char *
data, or NULL on error

/ldns_rdf2str/() Converts the data in the rdata field to presentation
format and returns that as a char *. Remember to free it.

.br *rdf*: The rdata field to convert .br Returns null terminated char *
data, or NULL on error

/ldns_rr_list2str/() Converts a list of resource records to presentation
format and returns that as a char *. Remember to free it.

.br *rr_list*: the rr_list to convert to text .br Returns null
terminated char * data, or NULL on error

/ldns_key2str/() Converts a private key to the test presentation fmt and
returns that as a char *. Remember to free it.

.br *k*: the key to convert to text .br Returns null terminated char *
data, or NULL on error

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr_print/, /ldns_rdf_print/, /ldns_pkt_print/,
/ldns_rr_list_print/, /ldns_resolver_print/, /ldns_zone_print/. And
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
