#+TITLE: Manpages - ExtUtils_MM_AIX.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_AIX.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_AIX - AIX specific subclass of ExtUtils::MM_Unix

* SYNOPSIS
Dont use this module directly. Use ExtUtils::MM and let it choose.

* DESCRIPTION
This is a subclass of ExtUtils::MM_Unix which contains functionality for
AIX.

Unless otherwise stated it works just like ExtUtils::MM_Unix.

** Overridden methods
/dlsyms/

Define DL_FUNCS and DL_VARS and write the *.exp files.

/xs_dlsyms_ext/

On AIX, is =.exp=.

* AUTHOR
Michael G Schwern <schwern@pobox.com> with code from ExtUtils::MM_Unix

* SEE ALSO
ExtUtils::MakeMaker
