#+TITLE: Manpages - Test2_Formatter_TAP.3perl
#+DESCRIPTION: Linux manpage for Test2_Formatter_TAP.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::Formatter::TAP - Standard TAP formatter

* DESCRIPTION
This is what takes events and turns them into TAP.

* SYNOPSIS
use Test2::Formatter::TAP; my $tap = Test2::Formatter::TAP->new(); #
Switch to utf8 $tap->encoding(utf8); $tap->write($event, $number); #
Output an event

* METHODS
- $bool = $tap->no_numbers :: 

- $tap->set_no_numbers($bool) :: 

Use to turn numbers on and off.

- $arrayref = $tap->handles :: 

- $tap->set_handles(\@handles); :: 

Can be used to get/set the filehandles. Indexes are identified by the
=OUT_STD= and =OUT_ERR= constants.

- $encoding = $tap->encoding :: 

- $tap->encoding($encoding) :: 

Get or set the encoding. By default no encoding is set, the original
settings of STDOUT and STDERR are used. This directly modifies the
stored filehandles, it does not create new ones.

- $tap->write($e, $num) :: Write an event to the console.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

- Kent Fredric <kentnl@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
