#+TITLE: Manpages - termkey_get_buffer_remaining.3
#+DESCRIPTION: Linux manpage for termkey_get_buffer_remaining.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_get_buffer_remaining - returns the free buffer space

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  size_t termkey_get_buffer_remaining(TermKey *tk");
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_get_buffer_remaining*() returns the number of bytes of buffer
space currently free in the *termkey*(7) instance. These bytes are free
to use by *termkey_push_bytes*(3), or may be filled by
*termkey_advisereadable*(3).

* RETURN VALUE
*termkey_get_buffer_remaining*() returns a size in bytes.

* SEE ALSO
*termkey_push_bytes*(3), *termkey_advisereadable*(3),
*termkey_set_buffer_size*(3), *termkey_get_buffer_size*(3),
*termkey_getkey*(3), *termkey*(7)
