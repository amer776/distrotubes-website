#+TITLE: Manpages - pr29_strerror.3
#+DESCRIPTION: Linux manpage for pr29_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pr29_strerror - API function

* SYNOPSIS
*#include <pr29.h>*

*const char * pr29_strerror(Pr29_rc */rc/*);*

* ARGUMENTS
- Pr29_rc rc :: an *Pr29_rc* return code.

* DESCRIPTION
Convert a return code integer to a text string. This string can be used
to output a diagnostic message to the user.

* PR29_SUCCESS
Successful operation. This value is guaranteed to always be zero, the
remaining ones are only guaranteed to hold non-zero values, for logical
comparison purposes.

* PR29_PROBLEM
A problem sequence was encountered.

* PR29_STRINGPREP_ERROR
The character set conversion failed (only for *pr29_8z()*).

Return value: Returns a pointer to a statically allocated string
containing a description of the error with the return code /rc/ .

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
