#+TITLE: Manpages - std_regex_traits.3
#+DESCRIPTION: Linux manpage for std_regex_traits.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::regex_traits< _Ch_type > - Describes aspects of a regular
expression.

* SYNOPSIS
\\

=#include <regex.h>=

** Public Types
typedef _RegexMask *char_class_type*\\

typedef _Ch_type *char_type*\\

typedef *std::locale* *locale_type*\\

typedef *std::basic_string*< char_type > *string_type*\\

** Public Member Functions
*regex_traits* ()\\
Constructs a default traits object.

*locale_type* *getloc* () const\\
Gets a copy of the current locale in use by the regex_traits object.

*locale_type* *imbue* (*locale_type* __loc)\\
Imbues the regex_traits object with a copy of a new locale.

bool *isctype* (_Ch_type __c, char_class_type __f) const\\
Determines if =c= is a member of an identified class.

template<typename _Fwd_iter > char_class_type *lookup_classname*
(_Fwd_iter __first, _Fwd_iter __last, bool __icase=false) const\\
Maps one or more characters to a named character classification.

template<typename _Fwd_iter > *string_type* *lookup_collatename*
(_Fwd_iter __first, _Fwd_iter __last) const\\
Gets a collation element by name.

template<typename _Fwd_iter > *string_type* *transform* (_Fwd_iter
__first, _Fwd_iter __last) const\\
Gets a sort key for a character sequence.

template<typename _Fwd_iter > *string_type* *transform_primary*
(_Fwd_iter __first, _Fwd_iter __last) const\\
Gets a sort key for a character sequence, independent of case.

char_type *translate* (char_type __c) const\\
Performs the identity translation.

char_type *translate_nocase* (char_type __c) const\\
Translates a character into a case-insensitive equivalent.

int *value* (_Ch_type __ch, int __radix) const\\
Converts a digit to an int.

** Static Public Member Functions
static std::size_t *length* (const char_type *__p)\\
Gives the length of a C-style string starting at =__p=.

** Protected Attributes
*locale_type* *_M_locale*\\

* Detailed Description
** "template<typename _Ch_type>
\\
class std::regex_traits< _Ch_type >"Describes aspects of a regular
expression.

A regular expression traits class that satisfies the requirements of
section [28.7].

The class regex is parameterized around a set of related types and
functions used to complete the definition of its semantics. This class
satisfies the requirements of such a traits class.

Definition at line *80* of file *regex.h*.

* Member Typedef Documentation
** template<typename _Ch_type > typedef _RegexMask *std::regex_traits*<
_Ch_type >::char_class_type
Definition at line *154* of file *regex.h*.

** template<typename _Ch_type > typedef _Ch_type *std::regex_traits*<
_Ch_type >::char_type
Definition at line *83* of file *regex.h*.

** template<typename _Ch_type > typedef *std::locale*
*std::regex_traits*< _Ch_type >::*locale_type*
Definition at line *85* of file *regex.h*.

** template<typename _Ch_type > typedef *std::basic_string*<char_type>
*std::regex_traits*< _Ch_type >::*string_type*
Definition at line *84* of file *regex.h*.

* Constructor & Destructor Documentation
** template<typename _Ch_type > *std::regex_traits*< _Ch_type
>::*regex_traits* ()= [inline]=
Constructs a default traits object.

Definition at line *160* of file *regex.h*.

* Member Function Documentation
** template<typename _Ch_type > *locale_type* *std::regex_traits*<
_Ch_type >::getloc () const= [inline]=
Gets a copy of the current locale in use by the regex_traits object.

Definition at line *373* of file *regex.h*.

** template<typename _Ch_type > *locale_type* *std::regex_traits*<
_Ch_type >::imbue (*locale_type* __loc)= [inline]=
Imbues the regex_traits object with a copy of a new locale.

*Parameters*

#+begin_quote
  /__loc/ A locale.
#+end_quote

*Returns*

#+begin_quote
  a copy of the previous locale in use by the regex_traits object.
#+end_quote

*Note*

#+begin_quote
  Calling imbue with a different locale than the one currently in use
  invalidates all cached data held by *this.
#+end_quote

Definition at line *362* of file *regex.h*.

References *std::swap()*.

** template<typename _Ch_type > bool *std::regex_traits*< _Ch_type
>::isctype (_Ch_type __c, char_class_type __f) const
Determines if =c= is a member of an identified class.

*Parameters*

#+begin_quote
  /__c/ a character.\\
  /__f/ a class type (as returned from lookup_classname).
#+end_quote

*Returns*

#+begin_quote
  true if the character =__c= is a member of the classification
  represented by =__f=, false otherwise.
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_cast/ if the current locale does not have a ctype facet.
#+end_quote

** template<typename _Ch_type > static std::size_t *std::regex_traits*<
_Ch_type >::length (const char_type * __p)= [inline]=, = [static]=
Gives the length of a C-style string starting at =__p=.

*Parameters*

#+begin_quote
  /__p/ a pointer to the start of a character sequence.
#+end_quote

*Returns*

#+begin_quote
  the number of characters between =*__p= and the first
  default-initialized value of type =char_type=. In other words, uses
  the C-string algorithm for determining the length of a sequence of
  characters.
#+end_quote

Definition at line *173* of file *regex.h*.

** template<typename _Ch_type > template<typename _Fwd_iter >
char_class_type *std::regex_traits*< _Ch_type >::lookup_classname
(_Fwd_iter __first, _Fwd_iter __last, bool __icase = =false=) const
Maps one or more characters to a named character classification.

*Parameters*

#+begin_quote
  /__first/ beginning of the character sequence.\\
  /__last/ one-past-the-end of the character sequence.\\
  /__icase/ ignores the case of the classification name.
#+end_quote

*Returns*

#+begin_quote
  an unspecified value that represents the character classification
  named by the character sequence designated by the iterator range
  [__first, __last). If =icase= is true, the returned mask identifies
  the classification regardless of the case of the characters to be
  matched (for example, [[:lower:]] is the same as [[:alpha:]]),
  otherwise a case-dependent classification is returned. The value
  returned shall be independent of the case of the characters in the
  character sequence. If the name is not recognized then returns a value
  that compares equal to 0.
#+end_quote

At least the following names (or their wide-character equivalent) are
supported.

- d

- w

- s

- alnum

- alpha

- blank

- cntrl

- digit

- graph

- lower

- print

- punct

- space

- upper

- xdigit

** template<typename _Ch_type > template<typename _Fwd_iter >
*string_type* *std::regex_traits*< _Ch_type >::lookup_collatename
(_Fwd_iter __first, _Fwd_iter __last) const
Gets a collation element by name.

*Parameters*

#+begin_quote
  /__first/ beginning of the collation element name.\\
  /__last/ one-past-the-end of the collation element name.
#+end_quote

*Returns*

#+begin_quote
  a sequence of one or more characters that represents the collating
  element consisting of the character sequence designated by the
  iterator range [__first, __last). Returns an empty string if the
  character sequence is not a valid collating element.
#+end_quote

** template<typename _Ch_type > template<typename _Fwd_iter >
*string_type* *std::regex_traits*< _Ch_type >::transform (_Fwd_iter
__first, _Fwd_iter __last) const= [inline]=
Gets a sort key for a character sequence.

*Parameters*

#+begin_quote
  /__first/ beginning of the character sequence.\\
  /__last/ one-past-the-end of the character sequence.
#+end_quote

Returns a sort key for the character sequence designated by the iterator
range [F1, F2) such that if the character sequence [G1, G2) sorts before
the character sequence [H1, H2) then v.transform(G1, G2) <
v.transform(H1, H2).

What this really does is provide a more efficient way to compare a
string to multiple other strings in locales with fancy collation rules
and equivalence classes.

*Returns*

#+begin_quote
  a locale-specific sort key equivalent to the input range.
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_cast/ if the current locale does not have a collate facet.
#+end_quote

Definition at line *226* of file *regex.h*.

References *std::basic_string< _CharT, _Traits, _Alloc >::data()*, and
*std::basic_string< _CharT, _Traits, _Alloc >::size()*.

Referenced by *std::regex_traits< _Ch_type >::transform_primary()*.

** template<typename _Ch_type > template<typename _Fwd_iter >
*string_type* *std::regex_traits*< _Ch_type >::transform_primary
(_Fwd_iter __first, _Fwd_iter __last) const= [inline]=
Gets a sort key for a character sequence, independent of case.

*Parameters*

#+begin_quote
  /__first/ beginning of the character sequence.\\
  /__last/ one-past-the-end of the character sequence.
#+end_quote

Effects: if typeid(use_facet<collate<_Ch_type> >) ==
typeid(collate_byname<_Ch_type>) and the form of the sort key returned
by collate_byname<_Ch_type>::transform(__first, __last) is known and can
be converted into a primary sort key then returns that key, otherwise
returns an empty string.

*Todo*

#+begin_quote
  Implement this function correctly.
#+end_quote

Definition at line *250* of file *regex.h*.

References *std::vector< _Tp, _Alloc >::data()*, *std::vector< _Tp,
_Alloc >::size()*, and *std::regex_traits< _Ch_type >::transform()*.

** template<typename _Ch_type > char_type *std::regex_traits*< _Ch_type
>::translate (char_type __c) const= [inline]=
Performs the identity translation.

*Parameters*

#+begin_quote
  /__c/ A character to the locale-specific character set.
#+end_quote

*Returns*

#+begin_quote
  __c.
#+end_quote

Definition at line *184* of file *regex.h*.

** template<typename _Ch_type > char_type *std::regex_traits*< _Ch_type
>::translate_nocase (char_type __c) const= [inline]=
Translates a character into a case-insensitive equivalent.

*Parameters*

#+begin_quote
  /__c/ A character to the locale-specific character set.
#+end_quote

*Returns*

#+begin_quote
  the locale-specific lower-case equivalent of __c.
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_cast/ if the imbued locale does not support the ctype facet.
#+end_quote

Definition at line *197* of file *regex.h*.

** template<typename _Ch_type > int *std::regex_traits*< _Ch_type
>::value (_Ch_type __ch, int __radix) const
Converts a digit to an int.

*Parameters*

#+begin_quote
  /__ch/ a character representing a digit.\\
  /__radix/ the radix if the numeric conversion (limited to 8, 10, or
  16).
#+end_quote

*Returns*

#+begin_quote
  the value represented by the digit __ch in base radix if the character
  __ch is a valid digit in base radix; otherwise returns -1.
#+end_quote

* Member Data Documentation
** template<typename _Ch_type > *locale_type* *std::regex_traits*<
_Ch_type >::_M_locale= [protected]=
Definition at line *377* of file *regex.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
