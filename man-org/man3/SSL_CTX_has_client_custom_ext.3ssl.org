#+TITLE: Manpages - SSL_CTX_has_client_custom_ext.3ssl
#+DESCRIPTION: Linux manpage for SSL_CTX_has_client_custom_ext.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CTX_has_client_custom_ext - check whether a handler exists for a
particular client extension type

* SYNOPSIS
#include <openssl/ssl.h> int SSL_CTX_has_client_custom_ext(const SSL_CTX
*ctx, unsigned int ext_type);

* DESCRIPTION
*SSL_CTX_has_client_custom_ext()* checks whether a handler has been set
for a client extension of type *ext_type* using
*SSL_CTX_add_client_custom_ext()*.

* RETURN VALUES
Returns 1 if a handler has been set, 0 otherwise.

* SEE ALSO
*ssl* (7), *SSL_CTX_add_client_custom_ext* (3)

* COPYRIGHT
Copyright 2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
