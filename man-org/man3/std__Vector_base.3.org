#+TITLE: Manpages - std__Vector_base.3
#+DESCRIPTION: Linux manpage for std__Vector_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Vector_base< _Tp, _Alloc > - See bits/stl_deque.h's _Deque_base
for an explanation.

* SYNOPSIS
\\

=#include <stl_vector.h>=

** Public Types
typedef *__gnu_cxx::__alloc_traits*< _Alloc >::template rebind< _Tp
>::other *_Tp_alloc_type*\\

typedef _Alloc *allocator_type*\\

typedef *__gnu_cxx::__alloc_traits*< _Tp_alloc_type >::pointer
*pointer*\\

** Public Member Functions
*_Vector_base* (_Tp_alloc_type &&__a) noexcept\\

*_Vector_base* (*_Vector_base* &&)=default\\

*_Vector_base* (*_Vector_base* &&__x, const allocator_type &__a)\\

*_Vector_base* (const allocator_type &__a) noexcept\\

*_Vector_base* (const allocator_type &__a, *_Vector_base* &&__x)\\

*_Vector_base* (size_t __n)\\

*_Vector_base* (size_t __n, const allocator_type &__a)\\

pointer *_M_allocate* (size_t __n)\\

void *_M_deallocate* (pointer __p, size_t __n)\\

const _Tp_alloc_type & *_M_get_Tp_allocator* () const noexcept\\

_Tp_alloc_type & *_M_get_Tp_allocator* () noexcept\\

allocator_type *get_allocator* () const noexcept\\

** Public Attributes
_Vector_impl *_M_impl*\\

** Protected Member Functions
void *_M_create_storage* (size_t __n)\\

* Detailed Description
** "template<typename _Tp, typename _Alloc>
\\
struct std::_Vector_base< _Tp, _Alloc >"See bits/stl_deque.h's
_Deque_base for an explanation.

Definition at line *84* of file *stl_vector.h*.

* Member Typedef Documentation
** template<typename _Tp , typename _Alloc > typedef
*__gnu_cxx::__alloc_traits*<_Alloc>::template rebind<_Tp>::other
*std::_Vector_base*< _Tp, _Alloc >::_Tp_alloc_type
Definition at line *87* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > typedef _Alloc
*std::_Vector_base*< _Tp, _Alloc >::allocator_type
Definition at line *273* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > typedef
*__gnu_cxx::__alloc_traits*<_Tp_alloc_type>::pointer
*std::_Vector_base*< _Tp, _Alloc >::pointer
Definition at line *89* of file *stl_vector.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Alloc > *std::_Vector_base*< _Tp,
_Alloc >::*_Vector_base* (const allocator_type & __a)= [inline]=,
= [noexcept]=
Definition at line *293* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > *std::_Vector_base*< _Tp,
_Alloc >::*_Vector_base* (size_t __n)= [inline]=
Definition at line *298* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > *std::_Vector_base*< _Tp,
_Alloc >::*_Vector_base* (size_t __n, const allocator_type &
__a)= [inline]=
Definition at line *303* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > *std::_Vector_base*< _Tp,
_Alloc >::*_Vector_base* (_Tp_alloc_type && __a)= [inline]=,
= [noexcept]=
Definition at line *312* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > *std::_Vector_base*< _Tp,
_Alloc >::*_Vector_base* (*_Vector_base*< _Tp, _Alloc > && __x, const
allocator_type & __a)= [inline]=
Definition at line *315* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > *std::_Vector_base*< _Tp,
_Alloc >::*_Vector_base* (const allocator_type & __a, *_Vector_base*<
_Tp, _Alloc > && __x)= [inline]=
Definition at line *328* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > *std::_Vector_base*< _Tp,
_Alloc >::~*_Vector_base* ()= [inline]=, = [noexcept]=
Definition at line *333* of file *stl_vector.h*.

* Member Function Documentation
** template<typename _Tp , typename _Alloc > pointer
*std::_Vector_base*< _Tp, _Alloc >::_M_allocate (size_t __n)= [inline]=
Definition at line *343* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > void *std::_Vector_base*<
_Tp, _Alloc >::_M_create_storage (size_t __n)= [inline]=, = [protected]=
Definition at line *359* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > void *std::_Vector_base*<
_Tp, _Alloc >::_M_deallocate (pointer __p, size_t __n)= [inline]=
Definition at line *350* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > const _Tp_alloc_type &
*std::_Vector_base*< _Tp, _Alloc >::_M_get_Tp_allocator ()
const= [inline]=, = [noexcept]=
Definition at line *280* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > _Tp_alloc_type &
*std::_Vector_base*< _Tp, _Alloc >::_M_get_Tp_allocator ()= [inline]=,
= [noexcept]=
Definition at line *276* of file *stl_vector.h*.

** template<typename _Tp , typename _Alloc > allocator_type
*std::_Vector_base*< _Tp, _Alloc >::get_allocator () const= [inline]=,
= [noexcept]=
Definition at line *284* of file *stl_vector.h*.

* Member Data Documentation
** template<typename _Tp , typename _Alloc > _Vector_impl
*std::_Vector_base*< _Tp, _Alloc >::_M_impl
Definition at line *340* of file *stl_vector.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
