#+TITLE: Manpages - XFreeFontNames.3
#+DESCRIPTION: Linux manpage for XFreeFontNames.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFreeFontNames.3 is found in manpage for: [[../man3/XListFonts.3][man3/XListFonts.3]]