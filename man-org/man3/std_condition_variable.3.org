#+TITLE: Manpages - std_condition_variable.3
#+DESCRIPTION: Linux manpage for std_condition_variable.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::condition_variable - condition_variable

* SYNOPSIS
\\

** Public Types
typedef __gthread_cond_t * *native_handle_type*\\

** Public Member Functions
*condition_variable* (const *condition_variable* &)=delete\\

native_handle_type *native_handle* ()\\

void *notify_all* () noexcept\\

void *notify_one* () noexcept\\

*condition_variable* & *operator=* (const *condition_variable*
&)=delete\\

void *wait* (*unique_lock*< *mutex* > &__lock) noexcept\\

template<typename _Predicate > void *wait* (*unique_lock*< *mutex* >
&__lock, _Predicate __p)\\

template<typename _Rep , typename _Period > *cv_status* *wait_for*
(*unique_lock*< *mutex* > &__lock, const *chrono::duration*< _Rep,
_Period > &__rtime)\\

template<typename _Rep , typename _Period , typename _Predicate > bool
*wait_for* (*unique_lock*< *mutex* > &__lock, const *chrono::duration*<
_Rep, _Period > &__rtime, _Predicate __p)\\

template<typename _Clock , typename _Duration > *cv_status* *wait_until*
(*unique_lock*< *mutex* > &__lock, const *chrono::time_point*< _Clock,
_Duration > &__atime)\\

template<typename _Clock , typename _Duration , typename _Predicate >
bool *wait_until* (*unique_lock*< *mutex* > &__lock, const
*chrono::time_point*< _Clock, _Duration > &__atime, _Predicate __p)\\

template<typename _Duration > *cv_status* *wait_until* (*unique_lock*<
*mutex* > &__lock, const *chrono::time_point*< *system_clock*, _Duration
> &__atime)\\

* Detailed Description
condition_variable

Definition at line *68* of file *condition_variable*.

* Member Typedef Documentation
** typedef __gthread_cond_t* std::condition_variable::native_handle_type
Definition at line *81* of file *condition_variable*.

* Member Function Documentation
** native_handle_type std::condition_variable::native_handle
()= [inline]=
Definition at line *182* of file *condition_variable*.

** template<typename _Predicate > void std::condition_variable::wait
(*unique_lock*< *mutex* > & __lock, _Predicate __p)= [inline]=
Definition at line *100* of file *condition_variable*.

** template<typename _Rep , typename _Period > *cv_status*
std::condition_variable::wait_for (*unique_lock*< *mutex* > & __lock,
const *chrono::duration*< _Rep, _Period > & __rtime)= [inline]=
Definition at line *159* of file *condition_variable*.

** template<typename _Rep , typename _Period , typename _Predicate >
bool std::condition_variable::wait_for (*unique_lock*< *mutex* > &
__lock, const *chrono::duration*< _Rep, _Period > & __rtime, _Predicate
__p)= [inline]=
Definition at line *170* of file *condition_variable*.

** template<typename _Clock , typename _Duration > *cv_status*
std::condition_variable::wait_until (*unique_lock*< *mutex* > & __lock,
const *chrono::time_point*< _Clock, _Duration > & __atime)= [inline]=
Definition at line *122* of file *condition_variable*.

** template<typename _Clock , typename _Duration , typename _Predicate >
bool std::condition_variable::wait_until (*unique_lock*< *mutex* > &
__lock, const *chrono::time_point*< _Clock, _Duration > & __atime,
_Predicate __p)= [inline]=
Definition at line *147* of file *condition_variable*.

** template<typename _Duration > *cv_status*
std::condition_variable::wait_until (*unique_lock*< *mutex* > & __lock,
const *chrono::time_point*< *system_clock*, _Duration > &
__atime)= [inline]=
Definition at line *116* of file *condition_variable*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
