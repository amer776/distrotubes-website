#+TITLE: Manpages - zip_source_stat.3
#+DESCRIPTION: Linux manpage for zip_source_stat.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function obtains information about the zip source

The

argument is a pointer to a

(shown below), into which information about the zip source is placed.

struct zip_source_stat { zip_uint64_t valid; /* which fields have valid
values */ const char *name; /* name of the file */ zip_uint64_t index;
/* index within archive */ zip_uint64_t size; /* size of file
(uncompressed) */ zip_uint64_t comp_size; /* size of file (compressed)
*/ time_t mtime; /* modification time */ zip_uint32_t crc; /* crc of
file data */ zip_uint16_t comp_method; /* compression method used */
zip_uint16_t encryption_method; /* encryption method used */
zip_uint32_t flags; /* reserved for future use */ };

The structure pointed to by

must be initialized with

before calling

The

field of the structure specifies which other fields are valid. Check if
the flag defined by the following defines are in

before accessing the fields:

Some fields may only be filled out after all data has been read from the
source, for example the

or

fields.

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

was added in libzip 1.0.

and
