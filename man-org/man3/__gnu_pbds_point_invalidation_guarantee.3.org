#+TITLE: Manpages - __gnu_pbds_point_invalidation_guarantee.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_point_invalidation_guarantee.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::point_invalidation_guarantee

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::basic_invalidation_guarantee*.

Inherited by *__gnu_pbds::range_invalidation_guarantee*.

* Detailed Description
Signifies an invalidation guarantee that includes all those of its base,
and additionally, that any point-type iterator, pointer, or reference to
a container object's mapped value type is valid as long as its
corresponding entry has not be erased, regardless of modifications to
the container object.

Definition at line *103* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
