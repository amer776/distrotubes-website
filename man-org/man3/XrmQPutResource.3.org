#+TITLE: Manpages - XrmQPutResource.3
#+DESCRIPTION: Linux manpage for XrmQPutResource.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XrmQPutResource.3 is found in manpage for: [[../man3/XrmPutResource.3][man3/XrmPutResource.3]]