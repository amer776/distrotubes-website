#+TITLE: Manpages - XCompositeQueryExtension.3
#+DESCRIPTION: Linux manpage for XCompositeQueryExtension.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XCompositeQueryExtension.3 is found in manpage for: [[../man3/Xcomposite.3][man3/Xcomposite.3]]