#+TITLE: Manpages - xcb_create_colormap.3
#+DESCRIPTION: Linux manpage for xcb_create_colormap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_create_colormap -

* SYNOPSIS
*#include <xcb/xproto.h>*

** Request function
xcb_void_cookie_t *xcb_create_colormap*(xcb_connection_t */conn/,
uint8_t /alloc/, xcb_colormap_t /mid/, xcb_window_t /window/,
xcb_visualid_t /visual/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- alloc :: One of the following values:

  - XCB_COLORMAP_ALLOC_NONE :: TODO: NOT YET DOCUMENTED.

  - XCB_COLORMAP_ALLOC_ALL :: TODO: NOT YET DOCUMENTED.

  TODO: NOT YET DOCUMENTED.

- mid :: TODO: NOT YET DOCUMENTED.

- window :: TODO: NOT YET DOCUMENTED.

- visual :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_create_colormap_checked/. See *xcb-requests(3)* for details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
