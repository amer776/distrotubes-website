#+TITLE: Manpages - ldns_update_pkt_new.3
#+DESCRIPTION: Linux manpage for ldns_update_pkt_new.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_update_pkt_new - create an update packet

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_pkt* ldns_update_pkt_new(ldns_rdf *zone_rdf, ldns_rr_class clas,
const ldns_rr_list *pr_rrlist, const ldns_rr_list *up_rrlist, const
ldns_rr_list *ad_rrlist);

* DESCRIPTION
/ldns_update_pkt_new/() create an update packet from zone name, class
and the rr lists .br *zone_rdf*: name of the zone The returned packet
will take ownership of zone_rdf, so the caller should not free it .br
*clas*: zone class .br *pr_rrlist*: list of Prerequisite Section RRs .br
*up_rrlist*: list of Updates Section RRs .br *ad_rrlist*: list of
Additional Data Section RRs (currently unused) .br Returns the new
packet

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_update_pkt_tsig_add/, /ldns_update_pkt_tsig_add/,
/ldns_update_zocount/, /ldns_update_prcount/, /ldns_update_upcount/,
/ldns_update_adcount/, /ldns_update_set_zocount/,
/ldns_update_set_prcount/, /ldns_update_set_upcount/,
/ldns_update_set_adcount/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
