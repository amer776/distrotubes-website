#+TITLE: Manpages - std___detail__Node_iterator_base.3
#+DESCRIPTION: Linux manpage for std___detail__Node_iterator_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Node_iterator_base< _Value, _Cache_hash_code > - Base
class for node iterators.

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

** Public Types
using *__node_type* = *_Hash_node*< _Value, _Cache_hash_code >\\

** Public Member Functions
*_Node_iterator_base* (*__node_type* *__p) noexcept\\

void *_M_incr* () noexcept\\

** Public Attributes
*__node_type* * *_M_cur*\\

** Friends
bool *operator!=* (const *_Node_iterator_base* &__x, const
*_Node_iterator_base* &__y) noexcept\\

bool *operator==* (const *_Node_iterator_base* &__x, const
*_Node_iterator_base* &__y) noexcept\\

* Detailed Description
** "template<typename _Value, bool _Cache_hash_code>
\\
struct std::__detail::_Node_iterator_base< _Value, _Cache_hash_code
>"Base class for node iterators.

Definition at line *287* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _Value , bool _Cache_hash_code> using
*std::__detail::_Node_iterator_base*< _Value, _Cache_hash_code
>::*__node_type* = *_Hash_node*<_Value, _Cache_hash_code>
Definition at line *289* of file *hashtable_policy.h*.

* Constructor & Destructor Documentation
** template<typename _Value , bool _Cache_hash_code>
*std::__detail::_Node_iterator_base*< _Value, _Cache_hash_code
>::*_Node_iterator_base* ()= [inline]=
Definition at line *293* of file *hashtable_policy.h*.

** template<typename _Value , bool _Cache_hash_code>
*std::__detail::_Node_iterator_base*< _Value, _Cache_hash_code
>::*_Node_iterator_base* (*__node_type* * __p)= [inline]=, = [noexcept]=
Definition at line *294* of file *hashtable_policy.h*.

* Member Function Documentation
** template<typename _Value , bool _Cache_hash_code> void
*std::__detail::_Node_iterator_base*< _Value, _Cache_hash_code
>::_M_incr ()= [inline]=, = [noexcept]=
Definition at line *298* of file *hashtable_policy.h*.

* Friends And Related Function Documentation
** template<typename _Value , bool _Cache_hash_code> bool operator!=
(const *_Node_iterator_base*< _Value, _Cache_hash_code > & __x, const
*_Node_iterator_base*< _Value, _Cache_hash_code > & __y)= [friend]=
Definition at line *308* of file *hashtable_policy.h*.

** template<typename _Value , bool _Cache_hash_code> bool operator==
(const *_Node_iterator_base*< _Value, _Cache_hash_code > & __x, const
*_Node_iterator_base*< _Value, _Cache_hash_code > & __y)= [friend]=
Definition at line *302* of file *hashtable_policy.h*.

* Member Data Documentation
** template<typename _Value , bool _Cache_hash_code> *__node_type**
*std::__detail::_Node_iterator_base*< _Value, _Cache_hash_code >::_M_cur
Definition at line *291* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
