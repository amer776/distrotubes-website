#+TITLE: Manpages - SDL_CreateRGBSurfaceFrom.3
#+DESCRIPTION: Linux manpage for SDL_CreateRGBSurfaceFrom.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CreateRGBSurfaceFrom - Create an SDL_Surface from pixel data

* SYNOPSIS
*#include "SDL.h"*

*SDL_Surface *SDL_CreateRGBSurfaceFrom*(*void *pixels, int width, int
height, int depth, int pitch, Uint32 Rmask, Uint32 Gmask, Uint32 Bmask,
Uint32 Amask*);

* DESCRIPTION
Creates an SDL_Surface from the provided pixel data.

The data stored in *pixels* is assumed to be of the *depth* specified in
the parameter list. The pixel data is not copied into the *SDL_Surface*
structure so it should not be freed until the surface has been freed
with a called to /SDL_FreeSurface/. *pitch* is the length of each
scanline in bytes.

See *SDL_CreateRGBSurface* for a more detailed description of the other
parameters.

* RETURN VALUE
Returns the created surface, or *NULL* upon error.

* SEE ALSO
*SDL_CreateRGBSurface*, *SDL_FreeSurface*
