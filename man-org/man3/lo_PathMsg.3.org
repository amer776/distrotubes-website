#+TITLE: Manpages - lo_PathMsg.3
#+DESCRIPTION: Linux manpage for lo_PathMsg.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lo::PathMsg - Class representing an OSC path (std::string) and
*lo::Message* pair.

* SYNOPSIS
\\

* Detailed Description
Class representing an OSC path (std::string) and *lo::Message* pair.

Definition at line 943 of file lo_cpp.h.

* Author
Generated automatically by Doxygen for liblo from the source code.
