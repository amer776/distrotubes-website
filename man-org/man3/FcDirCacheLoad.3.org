#+TITLE: Manpages - FcDirCacheLoad.3
#+DESCRIPTION: Linux manpage for FcDirCacheLoad.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheLoad - load a directory cache

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcCache * FcDirCacheLoad (const FcChar8 */dir/*, FcConfig **/config/*,
FcChar8 ***/cache_file/*);*

* DESCRIPTION
Loads the cache related to /dir/. If no cache file exists, returns NULL.
The name of the cache file is returned in /cache_file/, unless that is
NULL. See also FcDirCacheRead.
