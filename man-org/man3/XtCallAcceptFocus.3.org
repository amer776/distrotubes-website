#+TITLE: Manpages - XtCallAcceptFocus.3
#+DESCRIPTION: Linux manpage for XtCallAcceptFocus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtCallAcceptFocus - calla widget's accept_focus procedure

* SYNTAX
#include <X11/Intrinsic.h>

Boolean XtCallAcceptFocus(Widget /w/, Time */time/);

* ARGUMENTS
- time :: Specifies the X time of the event that is causing the accept
  focus.

23. Specifies the widget.

* DESCRIPTION
The *XtCallAcceptFocus* function calls the specified widget's
accept_focus procedure, passing it the specified widget and time, and
returns what the accept_focus procedure returns. If accept_focus is
NULL, *XtCallAcceptFocus* returns *False*.

* SEE ALSO
XtSetKeyboardFocus(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
