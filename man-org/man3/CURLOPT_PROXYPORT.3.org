#+TITLE: Manpages - CURLOPT_PROXYPORT.3
#+DESCRIPTION: Linux manpage for CURLOPT_PROXYPORT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PROXYPORT - port number the proxy listens on

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYPORT, long port);

* DESCRIPTION
Pass a long with this option to set the proxy port to connect to unless
it is specified in the proxy string /CURLOPT_PROXY(3)/ or uses 443 for
https proxies and 1080 for all others as default.

While this accepts a 'long', the port number is 16 bit so it cannot be
larger than 65535.

* DEFAULT
0, not specified which makes it use the default port

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
    curl_easy_setopt(curl, CURLOPT_PROXY, "localhost");
    curl_easy_setopt(curl, CURLOPT_PROXYPORT, 8080L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_PROXY*(3), *CURLOPT_PROXYTYPE*(3),
