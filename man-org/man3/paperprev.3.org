#+TITLE: Manpages - paperprev.3
#+DESCRIPTION: Linux manpage for paperprev.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about paperprev.3 is found in manpage for: [[../man3/paperinfo.3][man3/paperinfo.3]]