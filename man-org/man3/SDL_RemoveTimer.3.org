#+TITLE: Manpages - SDL_RemoveTimer.3
#+DESCRIPTION: Linux manpage for SDL_RemoveTimer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_RemoveTimer - Remove a timer which was added with /SDL_AddTimer/.

* SYNOPSIS
*#include "SDL.h"*

*SDL_bool SDL_RemoveTimer*(*SDL_TimerID id*);

* DESCRIPTION
Removes a timer callback previously added with /SDL_AddTimer/.

* RETURN VALUE
Returns a boolean value indicating success.

* EXAMPLES
#+begin_example
  SDL_RemoveTimer(my_timer_id);
#+end_example

* SEE ALSO
*SDL_AddTimer*
