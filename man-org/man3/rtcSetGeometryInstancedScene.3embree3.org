#+TITLE: Manpages - rtcSetGeometryInstancedScene.3embree3
#+DESCRIPTION: Linux manpage for rtcSetGeometryInstancedScene.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetGeometryInstancedScene - sets the instanced scene of
    an instance geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetGeometryInstancedScene(
    RTCGeometry geometry,
    RTCScene scene
  );
#+end_example

** DESCRIPTION
The =rtcSetGeometryInstancedScene= function sets the instanced scene
(=scene= argument) of the specified instance geometry (=geometry=
argument).

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[RTC_GEOMETRY_TYPE_INSTANCE], [rtcSetGeometryTransform]
