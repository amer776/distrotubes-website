#+TITLE: Manpages - XkbAddGeomOverlay.3
#+DESCRIPTION: Linux manpage for XkbAddGeomOverlay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAddGeomOverlay - Add one overlay to a section

* SYNOPSIS
*XkbOverlayPtr XkbAddGeomOverlay* *( XkbSectionPtr */section/* ,* *Atom
*/name/* ,* *int */sz_rows/* );*

* ARGUMENTS
- /- section/ :: section to which an overlay will be added

- /- name/ :: name of the overlay

- /- sz_rows/ :: number of rows to reserve in the overlay

* DESCRIPTION
Xkb provides functions to add a single new element to the top-level
keyboard geometry. In each case the /num_ */ fields of the corresponding
structure is incremented by 1. These functions do not change /sz_*/
unless there is no more room in the array. Some of these functions fill
in the values of the element's structure from the arguments. For other
functions, you must explicitly write code to fill the structure's
elements.

The top-level geometry description includes a list of /geometry
properties./ A geometry property associates an arbitrary string with an
equally arbitrary name. Programs that display images of keyboards can
use geometry properties as hints, but they are not interpreted by Xkb.
No other geometry structures refer to geometry properties.

/XkbAddGeomOverlay/ adds an overlay with the specified name to the
specified /section./ The new overlay is created with space allocated for
/sz_rows/ rows. If an overlay with name /name/ already exists in the
section, a pointer to the existing overlay is returned.
/XkbAddGeomOverlay/ returns NULL if any of the parameters is empty or if
it was not able to allocate space for the overlay. To allocate space for
an arbitrary number of overlays to a section, use the
/XkbAllocGeomOverlay/ function.

* STRUCTURES
#+begin_example

      typedef struct _XkbOverlayRec {
          Atom                  name;            /* overlay name */
          XkbSectionPtr         section_under;   /* the section under this overlay */
          unsigned short        num_rows;        /* number of rows in the rows array */
          unsigned short        sz_rows;         /* size of the rows array */
          XkbOverlayRowPtr      rows;            /* array of rows in the overlay */
          XkbBoundsPtr          bounds;          /* bounding box for the overlay */
      } XkbOverlayRec,*XkbOverlayPtr;
      
#+end_example

* SEE ALSO
*XkbAllocGeomOverlay*(3)
