#+TITLE: Manpages - paperinfo.3
#+DESCRIPTION: Linux manpage for paperinfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
paperinfo, paperwithsize, paperfirst, paperlast, papernext, paperprev -
return informations about a paper

* SYNOPSYS
#+begin_example
  #include <paper.h>

  const struct paper* paperinfo(const char* papername)
  const struct paper* paperwithsize(double psw, double psh)

  char* papername(const struct paper*)
  double paperpswidth(const struct paper*)
  double paperpsheigth(const struct paper*)

  const struct paper* paperfirst(void)
  const struct paper* papernext(const struct paper* pinfo)
  const struct paper* paperprev(const struct paper* pinfo)
  const struct paper* paperlast(void)
#+end_example

* DESCRIPTION
*paperinfo()* returns a pointer to a *struct paper* containing
informations about the paper with name *papername* *paperwithsize()*
looks for a paper whose width and height is *psw* and *psh* in
PostScript points, and return a pointer to a *struct paper*
corresponding to the paper found.

*papername()* returns the name of a paper described by an opaque struct
paper object *paperpswidth()* returns the width, in PostScript points,
of a paper described by an opaque struct paper object *paperpsheight()*
returns the height, in PostScript points, of a paper described by an
opaque struct paper object

*paperfirst()* and *paperlast()* return the first and last entries for
papers. Iteration from one entry to the next or the previous one can be
done with *papernext()* and *paperprev()* respectively.

* SEE ALSO
*paperinit*(3), *paperdone*(3) *defaultpapername*(3)\\
*papersize*(5)
