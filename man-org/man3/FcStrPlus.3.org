#+TITLE: Manpages - FcStrPlus.3
#+DESCRIPTION: Linux manpage for FcStrPlus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrPlus - concatenate two strings

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcStrPlus (const FcChar8 */s1/*, const FcChar8 **/s2/*);*

* DESCRIPTION
This function allocates new storage and places the concatenation of /s1/
and /s2/ there, returning the new string.
