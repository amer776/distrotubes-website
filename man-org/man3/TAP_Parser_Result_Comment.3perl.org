#+TITLE: Manpages - TAP_Parser_Result_Comment.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_Result_Comment.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::Result::Comment - Comment result token.

* VERSION
Version 3.43

* DESCRIPTION
This is a subclass of TAP::Parser::Result. A token of this class will be
returned if a comment line is encountered.

1..1 ok 1 - woo hooo! # this is a comment

* OVERRIDDEN METHODS
Mainly listed here to shut up the pitiful screams of the pod coverage
tests. They keep me awake at night.

- =as_string= Note that this method merely returns the comment preceded
  by a '# '.

** Instance Methods
/=comment=/

if ( $result->is_comment ) { my $comment = $result->comment; print "I
have something to say: $comment"; }
