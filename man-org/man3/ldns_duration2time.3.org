#+TITLE: Manpages - ldns_duration2time.3
#+DESCRIPTION: Linux manpage for ldns_duration2time.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_duration_type, ldns_duration_create,
ldns_duration_create_from_string, ldns_duration_cleanup,
ldns_duration_compare, ldns_duration2string, ldns_duration2time -
duration type and related functions

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_duration_type* ldns_duration_create(void);

ldns_duration_type* ldns_duration_create_from_string(const char* str);

void ldns_duration_cleanup(ldns_duration_type* duration);

int ldns_duration_compare(const ldns_duration_type* d1, const
ldns_duration_type* d2);

char* ldns_duration2string(const ldns_duration_type* duration);

time_t ldns_duration2time(const ldns_duration_type* duration);

* DESCRIPTION
/ldns_duration_type/\\
Duration.\\

\\
struct ldns_duration_struct\\
{\\
time_t years;\\
time_t months;\\
time_t weeks;\\
time_t days;\\
time_t hours;\\
time_t minutes;\\
time_t seconds;\\
};\\

\\
typedef struct ldns_duration_struct ldns_duration_type;

/ldns_duration_create/() Create a new 'instant' duration. .br Returns
ldns_duration_type* created duration

/ldns_duration_create_from_string/() Create a duration from string. .br
*str*: string-format duration .br Returns ldns_duration_type* created
duration

/ldns_duration_cleanup/() Clean up duration. .br *duration*: duration to
be cleaned up

/ldns_duration_compare/() Compare durations. .br *d1*: one duration .br
*d2*: another duration .br Returns int 0 if equal, -1 if d1 < d2, 1 if
d2 < d1

/ldns_duration2string/() Convert a duration to a string. .br *duration*:
duration to be converted .br Returns char* string-format duration

/ldns_duration2time/() Convert a duration to a time. .br *duration*:
duration to be converted .br Returns time_t time-format duration

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
