#+TITLE: Manpages - SDL_Delay.3
#+DESCRIPTION: Linux manpage for SDL_Delay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_Delay - Wait a specified number of milliseconds before returning.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_Delay*(*Uint32 ms*);

* DESCRIPTION
Wait a specified number of milliseconds before returning. *SDL_Delay*
will wait at /least/ the specified time, but possible longer due to OS
scheduling.

#+begin_quote
  *Note: *

  Count on a delay granularity of /at least/* 10 ms. Some platforms have
  shorter clock ticks but this is the most common.*
#+end_quote

* SEE ALSO
*SDL_AddTimer*
