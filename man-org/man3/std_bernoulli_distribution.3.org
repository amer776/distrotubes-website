#+TITLE: Manpages - std_bernoulli_distribution.3
#+DESCRIPTION: Linux manpage for std_bernoulli_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bernoulli_distribution - A Bernoulli random number distribution.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef bool *result_type*\\

** Public Member Functions
*bernoulli_distribution* ()\\
Constructs a Bernoulli distribution with likelihood 0.5.

*bernoulli_distribution* (const *param_type* &__p)\\

*bernoulli_distribution* (double __p)\\
Constructs a Bernoulli distribution with likelihood =p=.

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

*result_type* *max* () const\\
Returns the least upper bound value of the distribution.

*result_type* *min* () const\\
Returns the greatest lower bound value of the distribution.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

double *p* () const\\
Returns the =p= parameter of the distribution.

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\
Resets the distribution state.

** Friends
bool *operator==* (const *bernoulli_distribution* &__d1, const
*bernoulli_distribution* &__d2)\\
Return true if two Bernoulli distributions have the same parameters.

* Detailed Description
A Bernoulli random number distribution.

Generates a sequence of true and false values with likelihood $p$ that
true will come up and $(1 - p)$ that false will appear.

Definition at line *3523* of file *random.h*.

* Member Typedef Documentation
** typedef bool *std::bernoulli_distribution::result_type*
The type of the range of the distribution.

Definition at line *3527* of file *random.h*.

* Constructor & Destructor Documentation
** std::bernoulli_distribution::bernoulli_distribution ()= [inline]=
Constructs a Bernoulli distribution with likelihood 0.5.

Definition at line *3563* of file *random.h*.

** std::bernoulli_distribution::bernoulli_distribution (double
__p)= [inline]=, = [explicit]=
Constructs a Bernoulli distribution with likelihood =p=.

*Parameters*

#+begin_quote
  /__p/ [IN] The likelihood of a true result being returned. Must be in
  the interval $[0, 1]$.
#+end_quote

Definition at line *3572* of file *random.h*.

** std::bernoulli_distribution::bernoulli_distribution (const
*param_type* & __p)= [inline]=, = [explicit]=
Definition at line *3577* of file *random.h*.

* Member Function Documentation
** template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void
std::bernoulli_distribution::__generate (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator & __urng)= [inline]=
Definition at line *3649* of file *random.h*.

** template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void
std::bernoulli_distribution::__generate (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator & __urng, const
*param_type* & __p)= [inline]=
Definition at line *3656* of file *random.h*.

** template<typename _UniformRandomNumberGenerator > void
std::bernoulli_distribution::__generate (*result_type* * __f,
*result_type* * __t, _UniformRandomNumberGenerator & __urng, const
*param_type* & __p)= [inline]=
Definition at line *3662* of file *random.h*.

** *result_type* std::bernoulli_distribution::max () const= [inline]=
Returns the least upper bound value of the distribution.

Definition at line *3622* of file *random.h*.

References *std::numeric_limits< _Tp >::max()*.

** *result_type* std::bernoulli_distribution::min () const= [inline]=
Returns the greatest lower bound value of the distribution.

Definition at line *3615* of file *random.h*.

References *std::numeric_limits< _Tp >::min()*.

** template<typename _UniformRandomNumberGenerator > *result_type*
std::bernoulli_distribution::operator() (_UniformRandomNumberGenerator &
__urng)= [inline]=
Generating functions.

Definition at line *3630* of file *random.h*.

References *operator()()*.

Referenced by *operator()()*.

** template<typename _UniformRandomNumberGenerator > *result_type*
std::bernoulli_distribution::operator() (_UniformRandomNumberGenerator &
__urng, const *param_type* & __p)= [inline]=
Definition at line *3635* of file *random.h*.

** double std::bernoulli_distribution::p () const= [inline]=
Returns the =p= parameter of the distribution.

Definition at line *3593* of file *random.h*.

** *param_type* std::bernoulli_distribution::param () const= [inline]=
Returns the parameter set of the distribution.

Definition at line *3600* of file *random.h*.

Referenced by *std::operator>>()*.

** void std::bernoulli_distribution::param (const *param_type* &
__param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *3608* of file *random.h*.

** void std::bernoulli_distribution::reset ()= [inline]=
Resets the distribution state. Does nothing for a Bernoulli
distribution.

Definition at line *3587* of file *random.h*.

* Friends And Related Function Documentation
** bool operator== (const *bernoulli_distribution* & __d1, const
*bernoulli_distribution* & __d2)= [friend]=
Return true if two Bernoulli distributions have the same parameters.

Definition at line *3672* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
