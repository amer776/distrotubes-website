#+TITLE: Manpages - capgetp.3
#+DESCRIPTION: Linux manpage for capgetp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about capgetp.3 is found in manpage for: [[../man3/cap_get_proc.3][man3/cap_get_proc.3]]