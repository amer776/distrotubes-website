#+TITLE: Manpages - systemd-user.conf.5
#+DESCRIPTION: Linux manpage for systemd-user.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-user.conf.5 is found in manpage for: [[../systemd-system.conf.5][systemd-system.conf.5]]