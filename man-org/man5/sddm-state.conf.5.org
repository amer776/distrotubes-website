#+TITLE: Manpages - sddm-state.conf.5
#+DESCRIPTION: Linux manpage for sddm-state.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
state.conf - sddm display manager configuration

* SYNOPSIS
~sddm/state.conf

* DESCRIPTION
This file carries state configuration for the sddm display manager
*sddm*(1).

* OPTIONS
All options are configured in the [Last] section:

#+begin_quote
  - */Session=/* :: Name of the session file of the last session
    selected. This session will be preselected when the login screen
    shows up. Default value is empty.

  - */User=/* :: Name of last logged-in user. This username will be
    preselected/shown when the login screen shows up. Default value is
    empty.
#+end_quote

* SEE ALSO
*sddm*(1)

The full documentation for sddm is available at
/https://github.com/sddm/sddm/
