#+TITLE: Manpages - sane-kvs1025.5
#+DESCRIPTION: Linux manpage for sane-kvs1025.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sane-kvs1025 - SANE backend for Panasonic KV-S102xC USB ADF scanners.

* DESCRIPTION
The *sane-kvs1025* library implements a SANE (Scanner Access Now Easy)
backend which provides access to the Panasonic KV-S1020C/1025C and
KV-S1045C scanners.

* KNOWN ISSUES
This document was written by the SANE project, which has no information
regarding the capabilities or reliability of the backend. All
information contained here is suspect.

* CREDITS
The backend was written by Tao Zhang / Panasonic Russia Ltd.

The backend was ported to sane-backends 1.0.21 and updated to use
sanei_usb instead of libusb by m. allan noah.

The backend was tested on KV-S1025C and 1045C by Tiago Zaniquelli.

* SEE ALSO
*sane*(7)*,* *sane-usb*(5)

* AUTHOR
m. allan noah: </kitno455 a t gmail d o t com/>
