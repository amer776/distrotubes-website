#+TITLE: Manpages - mu-bookmarks.5
#+DESCRIPTION: Linux manpage for mu-bookmarks.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
bookmarks - file with bookmarks (shortcuts) for mu search expressions

* DESCRIPTION
Bookmarks are named shortcuts for search queries. They allow using a
convenient name for often-used queries. The bookmarks are also visible
as shortcuts in the mu experimental user interfaces, /mug/ and /mug2/.

The bookmarks file is read from /<muhome>/bookmarks/. On Unix this would
typically be w be /~/.config/mu/bookmarks/, but this can be influenced
using the *--muhome* parameter for *mu-find*(1) and *mug*(1).

The bookmarks file is a typical key=value *.ini*-file, which is best
shown by means of an example:

#+begin_example
      [mu]
      inbox=maildir:/inbox                  # inbox
      oldhat=maildir:/archive subject:hat   # archived with subject containing 'hat'
#+end_example

The *[mu]* group header is required.

For practical uses of bookmarks, see *mu-find*(1).

* AUTHOR
Dirk-Jan C. Binnema <djcb@djcbsoftware.nl>

* SEE ALSO
*mu*(1),*mu-find*(1)
