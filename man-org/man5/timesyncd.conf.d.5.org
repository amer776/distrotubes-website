#+TITLE: Manpages - timesyncd.conf.d.5
#+DESCRIPTION: Linux manpage for timesyncd.conf.d.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about timesyncd.conf.d.5 is found in manpage for: [[../timesyncd.conf.5][timesyncd.conf.5]]