#+TITLE: Man1 - git-upload-pack.1
#+DESCRIPTION: Linux manpage for git-upload-pack.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-upload-pack - Send objects packed back to git-fetch-pack

* SYNOPSIS
#+begin_example
  git-upload-pack [--[no-]strict] [--timeout=<n>] [--stateless-rpc]
                    [--advertise-refs] <directory>
#+end_example

* DESCRIPTION
Invoked by /git fetch-pack/, learns what objects the other side is
missing, and sends them after packing.

This command is usually not invoked directly by the end user. The UI for
the protocol is on the /git fetch-pack/ side, and the program pair is
meant to be used to pull updates from a remote repository. For push
operations, see /git send-pack/.

* OPTIONS
--[no-]strict

#+begin_quote
  Do not try <directory>/.git/ if <directory> is no Git directory.
#+end_quote

--timeout=<n>

#+begin_quote
  Interrupt transfer after <n> seconds of inactivity.
#+end_quote

--stateless-rpc

#+begin_quote
  Perform only a single read-write cycle with stdin and stdout. This
  fits with the HTTP POST request processing model where a program may
  read the request, write a response, and must exit.
#+end_quote

--http-backend-info-refs

#+begin_quote
  Used by *git-http-backend*(1) to serve up
  *$GIT_URL/info/refs?service=git-upload-pack* requests. See "Smart
  Clients" in *the HTTP transfer protocols*[1] documentation and "HTTP
  Transport" in *the Git Wire Protocol, Version 2*[2] documentation.
  Also understood by *git-receive-pack*(1).
#+end_quote

<directory>

#+begin_quote
  The repository to sync from.
#+end_quote

* ENVIRONMENT
*GIT_PROTOCOL*

#+begin_quote
  Internal variable used for handshaking the wire protocol. Server
  admins may need to configure some transports to allow this variable to
  be passed. See the discussion in *git*(1).
#+end_quote

* SEE ALSO
*gitnamespaces*(7)

* GIT
Part of the *git*(1) suite

* NOTES
-  1. :: the HTTP transfer protocols

  file:///usr/share/doc/git-doc/technical/http-protocol.html

-  2. :: the Git Wire Protocol, Version 2

  file:///usr/share/doc/git-doc/technical/protocol-v2.html
