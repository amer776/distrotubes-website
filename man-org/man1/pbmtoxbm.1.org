#+TITLE: Man1 - pbmtoxbm.1
#+DESCRIPTION: Linux manpage for pbmtoxbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtoxbm - convert a PBM image to an X11 bitmap

* SYNOPSIS
*pbmtoxbm*

[{*-x10*|*-x11*}]

[/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtoxbm* reads a PBM image as input and produces an X10 or X11 bitmap
as output.

* OPTIONS
- *-x10* :: This option causes *pbmtoxbm* to generate the X10 version of
  XBM.

You may not specify this with *-x11*.

This option was new with Netpbm 10.37 (December 2006). Before that, use
*pbmtox10bm* instead.

- *-x11* :: This option causes *pbmtoxbm* to generate the X11 version of
  XBM.

You may not specify this with *-x10*.

The X11 version is the default, so this option has no effect.

This option was new with Netpbm 10.37 (December 2006).

* SEE ALSO
*pbmtox10bm*(1) , *xbmtopbm*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1988 by Jef Poskanzer.
