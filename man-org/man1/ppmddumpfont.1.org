#+TITLE: Man1 - ppmddumpfont.1
#+DESCRIPTION: Linux manpage for ppmddumpfont.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmddumpfont - dump a Ppmdfont file

* SYNOPSIS
*ppmddumpfont*

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmddumpfont* reads a Ppmdfont file on Standard Input and writes to
Standard Error a human readable description of the font.

(There are no arguments or options)

See *Libnetpbm*PPM*Drawing*Function Manual (1) for details on Ppmdfont
files.

* SEE ALSO
*ppmdraw*(1) , *ppmdmkfont*(1) , *ppmdcfont*(1) ,
*Libnetpbm*PPM*Drawing*Function*Manual*(1)
