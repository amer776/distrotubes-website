#+TITLE: Man1 - gts2dxf.1
#+DESCRIPTION: Linux manpage for gts2dxf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gts2dxf - converts a GTS file to DXF format.

* SYNOPSIS
*gts2dxf* < /input.gts /> /output.dxf/

* DESCRIPTION
This manual page documents briefly the *gts2dxf* command.

* AUTHOR
gts2dxf was written by Stephane Popinet <popinet@users.sourceforge.net>.

This manual page was written by Ruben Molina <rmolina@udea.edu.co>, for
the Debian project (but may be used by others).
