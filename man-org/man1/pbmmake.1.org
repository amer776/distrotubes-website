#+TITLE: Man1 - pbmmake.1
#+DESCRIPTION: Linux manpage for pbmmake.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmmake - create a blank bitmap of a specified size

* SYNOPSIS
*pbmmake* [*-white*|*-black*|*-gray*] /width/ /height/

You can abbreviate any option to its shortest unique prefix.

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmmake* produces a PBM image of the specified width and height, either
all black, all white, or a dithered gray. The default is white.

* OPTIONS
In addition to the usual *-white* and *-black*, this program implements
*-gray*. This gives a simple 50% gray pattern with 1's and 0's
alternating.

* SEE ALSO
*pgmmake*(1) , *ppmmake*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
