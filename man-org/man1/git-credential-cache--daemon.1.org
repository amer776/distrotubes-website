#+TITLE: Man1 - git-credential-cache--daemon.1
#+DESCRIPTION: Linux manpage for git-credential-cache--daemon.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-credential-cache--daemon - Temporarily store user credentials in
memory

* SYNOPSIS
#+begin_example
  git credential-cache—daemon [--debug] <socket>
#+end_example

* DESCRIPTION

#+begin_quote
  \\

  *Note*

  \\

  You probably don't want to invoke this command yourself; it is started
  automatically when you use *git-credential-cache*(1).
#+end_quote

This command listens on the Unix domain socket specified by *<socket>*
for *git-credential-cache* clients. Clients may store and retrieve
credentials. Each credential is held for a timeout specified by the
client; once no credentials are held, the daemon exits.

If the *--debug* option is specified, the daemon does not close its
stderr stream, and may output extra diagnostics to it even after it has
begun listening for clients.

* GIT
Part of the *git*(1) suite
