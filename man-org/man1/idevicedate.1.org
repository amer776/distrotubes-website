#+TITLE: Man1 - idevicedate.1
#+DESCRIPTION: Linux manpage for idevicedate.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idevicedate - Display the current date or set it on a device.

* SYNOPSIS
*idevicedate* [OPTIONS]

* DESCRIPTION
Simple utility to manage the clock on a device.

* OPTIONS
- *-u, --udid UDID* :: target specific device by UDID.

- *-n, --network* :: connect to network device.

- *-d, --debug* :: enable communication debugging.

- *-s, --set TIMESTAMP* :: set UTC time described by TIMESTAMP

- *-c, --sync* :: set time of device to current system time

- *-h, --help* :: prints usage information

- *-v, --version* :: prints version information.

* AUTHOR
Martin Szulecki

* ON THE WEB
https://libimobiledevice.org

https://github.com/libimobiledevice/libimobiledevice
