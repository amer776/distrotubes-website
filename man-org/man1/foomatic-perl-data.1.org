#+TITLE: Man1 - foomatic-perl-data.1
#+DESCRIPTION: Linux manpage for foomatic-perl-data.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
foomatic-perl-data - generate Perl data structures from XML

* SYNOPSIS
*foomatic-perl-data* [ /-O/ ] [ /-C/ ] [ /-P/ ] [ /-D/ ] [ /-o
option=setting/ ] [ /-o .../ ] [ /-v/ ] [ /filename/ ]

** Options
*-O* Parse overview XML data

*-C* Parse printer/driver combo XML data (default)

*-P* Parse printer entry XML data

*-D* Parse driver entry XML data

*-o*/ option=setting/ Default option settings for the generated Perl
data (combo only)

*-v* Verbose (debug) mode

*filename* Read input from a file and not from standard input

* EXIT STATUS
*foomatic-perl-data* returns ...

* AUTHOR
Manfred Wassmann </manolo@NCC-1701.B.Shuttle.de/> for the foomatic
project using output from the associated binary.

* BUGS
This manpage needs editing.
