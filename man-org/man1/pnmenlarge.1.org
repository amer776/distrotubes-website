#+TITLE: Man1 - pnmenlarge.1
#+DESCRIPTION: Linux manpage for pnmenlarge.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pnmenlarge* - replaced by pamenlarge

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmenlarge* was replaced in Netpbm 10.25 (October 2004) by
*pamenlarge*(1)

*pamenlarge* is backward compatible with *pnmenlarge*, but works on PAM
images too.
