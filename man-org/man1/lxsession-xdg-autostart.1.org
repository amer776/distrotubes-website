#+TITLE: Man1 - lxsession-xdg-autostart.1
#+DESCRIPTION: Linux manpage for lxsession-xdg-autostart.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxsession-xdg-autostart - lxsession's autostart runner

* DESCRIPTION
This program is for internal usage by lxsession. Never run it manually.
