#+TITLE: Man1 - sxhkd.1
#+DESCRIPTION: Linux manpage for sxhkd.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sxhkd - Simple X hotkey daemon

* SYNOPSIS
*sxhkd* [/OPTIONS/] [/EXTRA_CONFIG/ ...]

* DESCRIPTION
sxhkd is a simple X hotkey daemon with a powerful and compact
configuration syntax.

* OPTIONS
*-h*

#+begin_quote
  Print the synopsis to standard output and exit.
#+end_quote

*-v*

#+begin_quote
  Print the version information to standard output and exit.
#+end_quote

*-m* /COUNT/

#+begin_quote
  Handle the first /COUNT/ mapping notify events.
#+end_quote

*-t* /TIMEOUT/

#+begin_quote
  Timeout in seconds for the recording of chord chains.
#+end_quote

*-c* /CONFIG_FILE/

#+begin_quote
  Read the main configuration from the given file.
#+end_quote

*-r* /REDIR_FILE/

#+begin_quote
  Redirect the commands output to the given file.
#+end_quote

*-s* /STATUS_FIFO/

#+begin_quote
  Output status information to the given FIFO.
#+end_quote

*-a* /ABORT_KEYSYM/

#+begin_quote
  Name of the keysym used for aborting chord chains.
#+end_quote

* BEHAVIOR
*sxhkd* is a daemon that listens to keyboard events and execute
commands.

It reads its configuration file from *$XDG_CONFIG_HOME/sxhkd/sxhkdrc* by
default, or from the given file if the *-c* option is used.

Additional configuration files can be passed as arguments.

If *sxhkd* receives a /SIGUSR1/ (resp. /SIGUSR2/) signal, it will reload
its configuration file (resp. toggle the grabbing state of all its
bindings).

The commands are executed via *SHELL* /-c/ *COMMAND* (hence you can use
environment variables).

*SHELL* will be the content of the first defined environment variable in
the following list: *SXHKD_SHELL*, *SHELL*.

If you have a non-QWERTY keyboard or a non-standard layout
configuration, you should provide a /COUNT/ of /1/ to the *-m* option or
/-1/ (interpreted as infinity) if you constantly switch from one layout
to the other (*sxhkd* ignores all mapping notify events by default
because the majority of those events are pointless).

* CONFIGURATION
Each line of the configuration file is interpreted as so:

#+begin_quote
  ·

  If it is empty or starts with #, it is ignored.
#+end_quote

#+begin_quote
  ·

  If it starts with a space, it is read as a command.
#+end_quote

#+begin_quote
  ·

  Otherwise, it is read as a hotkey.
#+end_quote

General syntax:

#+begin_quote
  #+begin_example
    HOTKEY
        [;]COMMAND

    HOTKEY      := CHORD_1 ; CHORD_2 ; ... ; CHORD_n
    CHORD_i     := [MODIFIERS_i +] [~][@]KEYSYM_i
    MODIFIERS_i := MODIFIER_i1 + MODIFIER_i2 + ... + MODIFIER_ik
  #+end_example
#+end_quote

The valid modifier names are: /super/, /hyper/, /meta/, /alt/,
/control/, /ctrl/, /shift/, /mode_switch/, /lock/, /mod1/, /mod2/,
/mod3/, /mod4/, /mod5/ and /any/.

The keysym names are given by the output of *xev -event keyboard*.

Hotkeys and commands can be spread across multiple lines by ending each
partial line with a backslash character.

When multiple chords are separated by semicolons, the hotkey is a chord
chain: the command will only be executed after receiving each chord of
the chain in consecutive order.

The colon character can be used instead of the semicolon to indicate
that the chord chain shall not be aborted when the chain tail is
reached.

If a command starts with a semicolon, it will be executed synchronously,
otherwise asynchronously.

The /Escape/ key can be used to abort a chord chain.

If *@* is added at the beginning of the keysym, the command will be run
on key release events, otherwise on key press events.

If *~* is added at the beginning of the keysym, the captured event will
be replayed for the other clients.

Pointer hotkeys can be defined by using one of the following special
keysym names: /button1/, /button2/, /button3/, ..., /button24/.

The hotkey and the command may contain sequences of the form
/{STRING_1,...,STRING_N}/.

In addition, the sequences can contain ranges of the form /A/-/Z/ where
/A/ and /Z/ are alphanumeric characters.

The underscore character represents an empty sequence element.

* AUTHOR
Bastien Dejean <nihilhill at gmail.com>

* MAILING LIST
sxhkd at librelist.com
