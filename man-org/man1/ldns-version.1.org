#+TITLE: Man1 - ldns-version.1
#+DESCRIPTION: Linux manpage for ldns-version.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns-version - print out the version of the ldns-library and tools on
this system

* SYNOPSIS
*ldns-version*

* DESCRIPTION
*ldns-version* is used to print out version information of the ldns
library and tools

* OPTIONS
*ldns-version* has no options.

* AUTHOR
Written by the ldns team as an example for ldns usage.

* REPORTING BUGS
Report bugs to <ldns-team@nlnetlabs.nl>.

* COPYRIGHT
Copyright (C) 2005 NLnet Labs. This is free software. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.
