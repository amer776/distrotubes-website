#+TITLE: Man1 - git-stage.1
#+DESCRIPTION: Linux manpage for git-stage.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-stage - Add file contents to the staging area

* SYNOPSIS
#+begin_example
  git stage args...
#+end_example

* DESCRIPTION
This is a synonym for *git-add*(1). Please refer to the documentation of
that command.

* GIT
Part of the *git*(1) suite
