#+TITLE: Man1 - xmlsec1.1
#+DESCRIPTION: Linux manpage for xmlsec1.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xmlsec1 - sign, verify, encrypt and decrypt XML documents

* SYNOPSIS
*xmlsec* /<command> /[/<options>/] [/<files>/]

* DESCRIPTION
xmlsec is a command line tool for signing, verifying, encrypting and
decrypting XML documents. The allowed <command> values are:

- *--help* :: display this help information and exit

- *--help-all* :: display help information for all commands/options and
  exit

- *--help-*<cmd> :: display help information for command <cmd> and exit

- *--version* :: print version information and exit

- *--keys* :: keys XML file manipulation

- *--sign* :: sign data and output XML document

- *--verify* :: verify signed document

- *--sign-tmpl* :: create and sign dynamicaly generated signature
  template

- *--encrypt* :: encrypt data and output XML document

- *--decrypt* :: decrypt data from XML document

* OPTIONS
*--ignore-manifests*

#+begin_quote
  do not process <dsig:Manifest> elements
#+end_quote

*--store-references*

#+begin_quote
  store and print the result of <dsig:Reference/> element processing
  just before calculating digest
#+end_quote

*--store-signatures*

#+begin_quote
  store and print the result of <dsig:Signature> processing just before
  calculating signature
#+end_quote

*--enabled-reference-uris* <list>

#+begin_quote
  comma separated list of of the following values: "empty", "same-doc",
  "local","remote" to restrict possible URI attribute values for the
  <dsig:Reference> element
#+end_quote

*--enable-visa3d-hack*

#+begin_quote
  enables Visa3D protocol specific hack for URI attributes processing
  when we are trying not to use XPath/XPointer engine; this is a hack
  and I don't know what else might be broken in your application when
  you use it (also check "--id-attr" option because you might need it)
#+end_quote

*--binary-data* <file>

#+begin_quote
  binary <file> to encrypt
#+end_quote

*--xml-data* <file>

#+begin_quote
  XML <file> to encrypt
#+end_quote

*--enabled-cipher-reference-uris* <list>

#+begin_quote
  comma separated list of of the following values: "empty", "same-doc",
  "local","remote" to restrict possible URI attribute values for the
  <enc:CipherReference> element
#+end_quote

*--session-key* <keyKlass>-<keySize>

#+begin_quote
  generate new session <keyKlass> key of <keySize> bits size (for
  example, "--session des-192" generates a new 192 bits DES key for DES3
  encryption)
#+end_quote

*--output* <filename>

#+begin_quote
  write result document to file <filename>
#+end_quote

*--print-debug*

#+begin_quote
  print debug information to stdout
#+end_quote

*--print-xml-debug*

#+begin_quote
  print debug information to stdout in xml format
#+end_quote

*--dtd-file* <file>

#+begin_quote
  load the specified file as the DTD
#+end_quote

*--node-id* <id>

#+begin_quote
  set the operation start point to the node with given <id>
#+end_quote

*--node-name* [<namespace-uri>:]<name>

#+begin_quote
  set the operation start point to the first node with given <name> and
  <namespace> URI
#+end_quote

*--node-xpath* <expr>

#+begin_quote
  set the operation start point to the first node selected by the
  specified XPath expression
#+end_quote

*--id-attr[*:<attr-name>] [<node-namespace-uri>:]<node-name>

#+begin_quote
  adds attributes <attr-name> (default value "id") from all nodes
  with<node-name> and namespace <node-namespace-uri> to the list of
  known ID attributes; this is a hack and if you can use DTD or schema
  to declare ID attributes instead (see "--dtd-file" option), I don't
  know what else might be broken in your application when you use this
  hack
#+end_quote

*--enabled-key-data* <list>

#+begin_quote
  comma separated list of enabled key data (list of registered key data
  klasses is available with "--list-key-data" command); by default, all
  registered key data are enabled
#+end_quote

*--enabled-retrieval-uris* <list>

#+begin_quote
  comma separated list of of the following values: "empty", "same-doc",
  "local","remote" to restrict possible URI attribute values for the
  <dsig:RetrievalMethod> element.
#+end_quote

*--gen-key[*:<name>] <keyKlass>-<keySize>

#+begin_quote
  generate new <keyKlass> key of <keySize> bits size, set the key name
  to <name> and add the result to keys manager (for example,
  "--gen:mykey rsa-1024" generates a new 1024 bits RSA key and sets it's
  name to "mykey")
#+end_quote

*--keys-file* <file>

#+begin_quote
  load keys from XML file
#+end_quote

*--privkey-pem[*:<name>] <file>[,<cafile>[,<cafile>[...]]]

#+begin_quote
  load private key from PEM file and certificates that verify this key
#+end_quote

*--privkey-der[*:<name>] <file>[,<cafile>[,<cafile>[...]]]

#+begin_quote
  load private key from DER file and certificates that verify this key
#+end_quote

*--pkcs8-pem[*:<name>] <file>[,<cafile>[,<cafile>[...]]]

#+begin_quote
  load private key from PKCS8 PEM file and PEM certificates that verify
  this key
#+end_quote

*--pkcs8-der[*:<name>] <file>[,<cafile>[,<cafile>[...]]]

#+begin_quote
  load private key from PKCS8 DER file and DER certificates that verify
  this key
#+end_quote

*--pubkey-pem[*:<name>] <file>

#+begin_quote
  load public key from PEM file
#+end_quote

*--pubkey-der[*:<name>] <file>

#+begin_quote
  load public key from DER file
#+end_quote

*--aeskey[*:<name>] <file>

#+begin_quote
  load AES key from binary file <file>
#+end_quote

*--deskey[*:<name>] <file>

#+begin_quote
  load DES key from binary file <file>
#+end_quote

*--hmackey[*:<name>] <file>

#+begin_quote
  load HMAC key from binary file <file>
#+end_quote

*--pwd* <password>

#+begin_quote
  the password to use for reading keys and certs
#+end_quote

*--pkcs12[*:<name>] <file>

#+begin_quote
  load load private key from pkcs12 file <file>
#+end_quote

*--pkcs12-persist*

#+begin_quote
  persist loaded private key
#+end_quote

*--pubkey-cert-pem[*:<name>] <file>

#+begin_quote
  load public key from PEM cert file
#+end_quote

*--pubkey-cert-der[*:<name>] <file>

#+begin_quote
  load public key from DER cert file
#+end_quote

*--trusted-pem* <file>

#+begin_quote
  load trusted (root) certificate from PEM file <file>
#+end_quote

*--untrusted-pem* <file>

#+begin_quote
  load untrusted certificate from PEM file <file>
#+end_quote

*--trusted-der* <file>

#+begin_quote
  load trusted (root) certificate from DER file <file>
#+end_quote

*--untrusted-der* <file>

#+begin_quote
  load untrusted certificate from DER file <file>
#+end_quote

*--verification-time* <time>

#+begin_quote
  the local time in "YYYY-MM-DD HH:MM:SS" format used certificates
  verification
#+end_quote

*--depth* <number>

#+begin_quote
  maximum certificates chain depth
#+end_quote

*--X509-skip-strict-checks*

#+begin_quote
  skip strict checking of X509 data
#+end_quote

*--insecure*

#+begin_quote
  do not verify certificates
#+end_quote

*--privkey-openssl-engine[*:<name>]
<openssl-engine>;<openssl-key-id>[,<crtfile>[,<crtfile>[...]]]

#+begin_quote
  load private key by OpenSSL ENGINE interface; specify the name of
  engine (like with *-engine* params), the key specs (like with *-inkey*
  or *-key* params) and optionally certificates that verify this key
#+end_quote

*--crypto* <name>

#+begin_quote
  the name of the crypto engine to use from the following list: openssl,
  mscrypto, nss, gnutls, gcrypt (if no crypto engine is specified then
  the default one is used)
#+end_quote

*--crypto-config* <path>

#+begin_quote
  path to crypto engine configuration
#+end_quote

*--repeat* <number>

#+begin_quote
  repeat the operation <number> times
#+end_quote

*--disable-error-msgs*

#+begin_quote
  do not print xmlsec error messages
#+end_quote

*--print-crypto-error-msgs*

#+begin_quote
  print errors stack at the end
#+end_quote

*--help*

#+begin_quote
  print help information about the command
#+end_quote

*--xxe*

#+begin_quote
  enable External Entity resolution. WARNING: this may allow the reading
  of arbitrary files and URLs, controlled by the input XML document. Use
  with caution!
#+end_quote

*--url-map*:<url> <file>

#+begin_quote
  maps a given <url> to the given <file> for loading external resources
#+end_quote

* AUTHOR
Written by Aleksey Sanin <aleksey@aleksey.com>.

* REPORTING BUGS
Report bugs to http://www.aleksey.com/xmlsec/bugs.html

* COPYRIGHT
Copyright © 2002-2016 Aleksey Sanin <aleksey@aleksey.com>. All Rights
Reserved..\\
This is free software: see the source for copying information.
