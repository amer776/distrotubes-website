#+TITLE: Man1 - stl2gts.1
#+DESCRIPTION: Linux manpage for stl2gts.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stl2gts - convert an STL file to GTS format.

* SYNOPSIS
*stl2gts* [ /OPTIONS/ ] < /input.stl /> /output.gts/

* DESCRIPTION
This manual page documents briefly the *stl2gts* command.

* OPTIONS
These programs follow the usual GNU command line syntax, with long
options starting with two dashes (`-'). A summary of options is included
below.

- *-r*, *--revert* :: Revert face normals.

- *-n*, *--nomerge* :: Do not merge vertices.

- *-v*, *--verbose* :: Display surface statistics.

- *-h*, *--help* :: Display the help and exit.

* AUTHOR
stl2gts was written by Stephane Popinet <popinet@users.sourceforge.net>.

This manual page was written by Ruben Molina <rmolina@udea.edu.co>, for
the Debian project (but may be used by others).
