#+TITLE: Man1 - git-remote-bzr.1
#+DESCRIPTION: Linux manpage for git-remote-bzr.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-remote-bzr - Git remote support for Bazaar repositories

* SYNOPSIS
#+begin_example
  git clone bzr::<source-repo> [<destination>]
  git fetch bzr::<source-repo> [<destination>]
#+end_example

* DESCRIPTION
This command provides support for using /bzr/ repositories as Git
remotes, through the breezy-git plugin. At the moment it supports
cloning from, fetching from and pushing into Bazaar repositories. Fetch
support is still experimental, and may be slow.

* BUGS
Please report bugs at https://launchpad.net/brz/+filebug

* LICENSE
breezy-git and git-remote-bzr are licensed under the GNU GPL, version 2
or later.

* SEE ALSO
*git-remote-helpers*(1), *breezy*(1)

* BAZAAR
Part of the *breezy*(1) suite

* AUTHOR
breezy-git, git-remote-bzr and this manual page were written by Jelmer
Vernooĳ.
