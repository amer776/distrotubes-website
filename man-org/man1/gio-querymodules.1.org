#+TITLE: Man1 - gio-querymodules.1
#+DESCRIPTION: Linux manpage for gio-querymodules.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gio-querymodules - GIO module cache creation

* SYNOPSIS
*gio-querymodules* {DIRECTORY...}

* DESCRIPTION
*gio-querymodules* creates a giomodule.cache file in the listed
directories. This file lists the implemented extension points for each
module that has been found. It is used by GIO at runtime to avoid
opening all modules just to find out which extension points they are
implementing.

GIO modules are usually installed in the gio/modules subdirectory of
libdir.
