#+TITLE: Man1 - rofi-sensible-terminal.1
#+DESCRIPTION: Linux manpage for rofi-sensible-terminal.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*rofi-sensible-terminal* - launches $TERMINAL with fallbacks

* SYNOPSIS
rofi-sensible-terminal [arguments]

* DESCRIPTION
rofi-sensible-terminal is invoked in the rofi default config to start a
terminal. This wrapper script is necessary since there is no
distribution-independent terminal launcher (but for example Debian has
x-terminal-emulator). Distribution packagers are responsible for
shipping this script in a way which is appropriate for the distribution.

It tries to start one of the following (in that order):

#+begin_quote

  - =$TERMINAL= (this is a non-standard variable)

  - x-terminal-emulator

  - urxvt

  - rxvt

  - st

  - terminology

  - qterminal

  - Eterm

  - aterm

  - uxterm

  - xterm

  - roxterm

  - xfce4-terminal.wrapper

  - mate-terminal

  - lxterminal

  - konsole

  - alacritty

  - kitty
#+end_quote

* SEE ALSO
rofi(1)

* AUTHORS
Dave Davenport and contributors

Copied script from i3: Michael Stapelberg and contributors
