#+TITLE: Man1 - gvedit.1
#+DESCRIPTION: Linux manpage for gvedit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gvedit - simple graph editor and viewer

* SYNOPSIS
*gvedit* [ *-sv?* ] [ /file/ ]

* DESCRIPTION
*gvedit* provides a simple graph editor and viewer. It allows many
graphs to be viewed at the same time. The text of each graph is
displayed in its own text window.

The name of a Graphviz file can be specified on the command line. This
graph file will be automatically opened on start-up.

* OPTIONS
The following options are supported:

- *-s* :: By default, the layout algorithms that use initial positions
  given by a node's pos attribute (currently, fdp and neato) assume the
  coordinates are in inches. Frequently, the input graph has these in
  points, especially if the graph is output by a Graphviz layout
  algorithm. This flag can be used to scale the coordinates from points
  to inches.

- *-v* :: Verbose mode.

- *-?* :: Prints usage information and exit.

* AUTHOR
Arif Bilgin <arif@research.att.com>\\
Emden R. Gansner <erg@research.att.com>

* SEE ALSO
dot(1), dotty(1), lefty(1)
