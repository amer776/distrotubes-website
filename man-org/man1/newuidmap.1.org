#+TITLE: Man1 - newuidmap.1
#+DESCRIPTION: Linux manpage for newuidmap.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
newuidmap - set the uid mapping of a user namespace

* SYNOPSIS
*newuidmap* /pid/ /uid/ /loweruid/ /count/ [/uid/ /loweruid/ /count/ [
/.../ ]]

* DESCRIPTION
The *newuidmap* sets /proc/[pid]/uid_map based on its command line
arguments and the uids allowed in /etc/subuid. Note that the root user
is not exempted from the requirement for a valid /etc/subuid entry.

After the pid argument, *newuidmap* expects sets of 3 integers:

uid

#+begin_quote
  Beginning of the range of UIDs inside the user namespace.
#+end_quote

loweruid

#+begin_quote
  Beginning of the range of UIDs outside the user namespace.
#+end_quote

count

#+begin_quote
  Length of the ranges (both inside and outside the user namespace).
#+end_quote

*newuidmap* verifies that the caller is the owner of the process
indicated by *pid* and that for each of the above sets, each of the UIDs
in the range [loweruid, loweruid+count] is allowed to the caller
according to /etc/subuid before setting /proc/[pid]/uid_map.

Note that newuidmap may be used only once for a given process.

* OPTIONS
There currently are no options to the *newuidmap* command.

* FILES
/etc/subuid

#+begin_quote
  List of users subordinate user IDs.
#+end_quote

/proc/[pid]/uid_map

#+begin_quote
  Mapping of uids from one between user namespaces.
#+end_quote

* SEE ALSO
*login.defs*(5), *newusers*(8), *subuid*(5), *useradd*(8), *usermod*(8),
*userdel*(8).
