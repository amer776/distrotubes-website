#+TITLE: Man1 - rofi-theme-selector.1
#+DESCRIPTION: Linux manpage for rofi-theme-selector.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*rofi-theme-selector* - Preview and apply themes for *rofi*

* DESCRIPTION
*rofi-theme-selector* is a bash/rofi script to preview and apply themes
for *rofi*. It's part of any installation of *rofi*.

* USAGE
** Running rofi-theme-selector
*rofi-theme-selector* shows a list of all available themes in a *rofi*
window. It lets you preview each theme with the Enter key and apply the
theme to your *rofi* configuration file with Alt+a.

* Theme directories
*rofi-theme-selector* searches the following directories for themes:

#+begin_quote

  - ${PREFIX}/share/rofi/themes

  - $XDG_CONFIG_HOME/rofi/themes

  - $XDG_DATA_HOME/share/rofi/themes
#+end_quote

${PREFIX} reflects the install location of rofi. In most cases this will
be "/usr". $XDG_CONFIG_HOME is normally unset. Default path is
"$HOME/.config". $XDG_DATA_HOME is normally unset. Default path is
"$HOME/.local/share".

* SEE ALSO
rofi(1)

* AUTHORS
Qball Cow qball@gmpclient.org Rasmus Steinke rasi@xssn.at
