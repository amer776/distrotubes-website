#+TITLE: Man1 - doxywizard.1
#+DESCRIPTION: Linux manpage for doxywizard.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
doxywizard - a tool to configure and run doxygen on your source files

* SYNOPSIS
*doxywizard*

* DESCRIPTION
Doxywizard is an interactive frontend to the doxygen tool to configure
and run doxygen on your source files.

You can optionally pass the name of a configuration file as the first
argument to load it at startup.

* SEE ALSO
doxygen(1)
