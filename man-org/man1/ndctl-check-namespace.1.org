#+TITLE: Man1 - ndctl-check-namespace.1
#+DESCRIPTION: Linux manpage for ndctl-check-namespace.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndctl-check-namespace - check namespace metadata consistency

* SYNOPSIS
#+begin_example
  ndctl check-namespace <namespace> [<options>]
#+end_example

\\

* DESCRIPTION
A namespace in the /sector/ mode will have metadata on it to describe
the kernel BTT (Block Translation Table). The check-namespace command
can be used to check the consistency of this metadata, and optionally,
also attempt to repair it, if it has enough information to do so.

The namespace being checked has to be disabled before initiating a check
on it as a precautionary measure. The --force option can override this.

* EXAMPLES
Check a namespace (only report errors)

#+begin_example
  ndctl disable-namespace namespace0.0
  ndctl check-namespace namespace0.0
#+end_example

\\

Check a namespace, and perform repairs if possible

#+begin_example
  ndctl disable-namespace namespace0.0
  ndctl check-namespace --repair namespace0.0
#+end_example

\\

* OPTIONS
-R, --repair

#+begin_quote
  Perform metadata repairs if possible. Without this option, the raw
  namespace contents will not be touched.
#+end_quote

-L, --rewrite-log

#+begin_quote
  Regenerate the BTT log and write it to media. This can be used to
  convert from the old (pre 4.15) padding format that was incompatible
  with other BTT implementations to the updated format. This requires
  the --repair option to be provided.

  #+begin_quote
    #+begin_example
      WARNING: Do not interrupt this operation as it can potentially cause
      unrecoverable metadata corruption. It is highly recommended to create
      a backup of the raw namespace before attempting this.
    #+end_example
  #+end_quote
#+end_quote

-f, --force

#+begin_quote
  Unless this option is specified, a check-namespace operation will fail
  if the namespace is presently active. Specifying --force causes the
  namespace to be disabled before checking.
#+end_quote

-v, --verbose

#+begin_quote
  Emit debug messages for the namespace check process.
#+end_quote

-r, --region=

#+begin_quote
  A /regionX/ device name, or a region id number. Restrict the operation
  to the specified region(s). The keyword /all/ can be specified to
  indicate the lack of any restriction, however this is the same as not
  supplying a --region option at all.
#+end_quote

-b, --bus=

#+begin_quote
  A bus id number, or a provider string (e.g. "ACPI.NFIT"). Restrict the
  operation to the specified bus(es). The keyword /all/ can be specified
  to indicate the lack of any restriction, however this is the same as
  not supplying a --bus option at all.
#+end_quote

* COPYRIGHT
Copyright © 2016 - 2020, Intel Corporation. License GPLv2: GNU GPL
version 2 <http://gnu.org/licenses/gpl.html>. This is free software: you
are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.

* SEE ALSO
ndctl-disable-namespace(1), ndctl-enable-namespace(1), /UEFI NVDIMM
Label Protocol/
<http://www.uefi.org/sites/default/files/resources/UEFI_Spec_2_7.pdf>
