#+TITLE: Man1 - xpstops.1
#+DESCRIPTION: Linux manpage for xpstops.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xpstops - XPS to PostScript converter

* SYNOPSIS
*xpstops* [/OPTION/...] FILE [/OUTPUT FILE/]

* DESCRIPTION
*xpstops* converts XPS documents to PostScript format. *xpstops* reads
the XPS file, /FILE/, and writes a PostScript file, /OUTPUT FILE/. If
/OUTPUT FILE/ is not specified the output filename will be derived from
the /FILE/ filename.

* OPTIONS
*-?*, *--help*

#+begin_quote
  Show help options.
#+end_quote

*-d* /DOCUMENT/, *--document*=/DOCUMENT/

#+begin_quote
  The document inside the XPS file to convert. By default, the first
  document of the XPS file is used.
#+end_quote

*-f* /PAGE/, *--first*=/PAGE/

#+begin_quote
  The first page to convert.
#+end_quote

*-l* /PAGE/, *--last*=/PAGE/

#+begin_quote
  The last page to convert.
#+end_quote

*-o*, *--odd*

#+begin_quote
  Convert only odd pages.
#+end_quote

*-e*, *--even*

#+begin_quote
  Convert only even pages.
#+end_quote

*-r* /RESOLUTION/, *--resolution*=/RESOLUTION/

#+begin_quote
  Horizontal and vertical resolution in PPI (Pixels Per Inch). The
  default is 150 PPI.
#+end_quote

*--rx*=/RESOLUTION/

#+begin_quote
  Horizontal resolution in PPI (Pixels Per Inch). The default is 150
  PPI.
#+end_quote

*--ry*=/RESOLUTION/

#+begin_quote
  Vertical resolution in PPI (Pixels Per Inch). The default is 150 PPI.
#+end_quote

*-x* /X/, *--crop-x*=/X/

#+begin_quote
  The x-coordinate of the crop area top left corner.
#+end_quote

*-y* /Y/, *--crop-y*=/Y/

#+begin_quote
  The y-coordinate of the crop area top left corner.
#+end_quote

*-w* /WIDTH/, *--crop-width*=/WIDTH/

#+begin_quote
  The width of crop area.
#+end_quote

*-h* /HEIGHT/, *--crop-height*=/HEIGHT/

#+begin_quote
  The height of crop area.
#+end_quote

*--level2*

#+begin_quote
  Generate Level 2 PostScript. Level 2 supports color images and image
  compression.
#+end_quote

*--level3*

#+begin_quote
  Generate Level 3 PostScript. This enables all Level 2 features plus
  shading patterns and masked images. This is the default setting.
#+end_quote

*--eps*

#+begin_quote
  Generate an Encapsulated PostScript (EPS) file.
#+end_quote

*--paper*=/PAPER/

#+begin_quote
  Set the paper size to one of "A0", "A1", "A2", "A3", "A4", "A5", "B4",
  "B5", "Letter", "Tabloid", "Ledger", "Legal", "Statement",
  "Executive", "Folio", "Quarto", "10x14".
#+end_quote

*--duplex*

#+begin_quote
  Adds the %%IncludeFeature: *Duplex DuplexNoTumble DSC comment to the
  PostScript file. This tells the print manager to enable duplexing.
#+end_quote

*--paper-width*=/WIDTH/

#+begin_quote
  The paper width.
#+end_quote

*--paper-height*=/HEIGHT/

#+begin_quote
  The paper height.
#+end_quote

*--expand*

#+begin_quote
  Expand pages smaller than the paper to fill the paper. By default,
  pages are not scaled.
#+end_quote

*--no-shrink*

#+begin_quote
  Dont scale pages which are larger than the paper. By default, pages
  larger than the paper are shrunk to fit.
#+end_quote

*--no-center*

#+begin_quote
  Dont center on the paper pages smaller than the paper (after any
  scaling). By default, pages smaller than the paper are aligned to the
  lower-left corner.
#+end_quote

* BUGS
Please send bug reports to
*https://bugzilla.gnome.org/enter_bug.cgi?product=libgxps*.

* SEE ALSO
*xpstojpeg*(1) *xpstopng*(1) *xpstopdf*(1) *xpstosvg*(1)
