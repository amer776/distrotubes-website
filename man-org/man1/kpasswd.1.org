#+TITLE: Man1 - kpasswd.1
#+DESCRIPTION: Linux manpage for kpasswd.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kpasswd - change a user's Kerberos password

* SYNOPSIS
*kpasswd* [/principal/]

* DESCRIPTION
The kpasswd command is used to change a Kerberos principal's password.
kpasswd first prompts for the current Kerberos password, then prompts
the user twice for the new password, and the password is changed.

If the principal is governed by a policy that specifies the length
and/or number of character classes required in the new password, the new
password must conform to the policy. (The five character classes are
lower case, upper case, numbers, punctuation, and all other characters.)

* OPTIONS

#+begin_quote
  - */principal/* :: Change the password for the Kerberos principal
    principal. Otherwise, kpasswd uses the principal name from an
    existing ccache if there is one; if not, the principal is derived
    from the identity of the user invoking the kpasswd command.
#+end_quote

* ENVIRONMENT
See kerberos(7) for a description of Kerberos environment variables.

* SEE ALSO
kadmin(1), kadmind(8), kerberos(7)

* AUTHOR
MIT

* COPYRIGHT
1985-2021, MIT
