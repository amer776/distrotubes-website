#+TITLE: Man1 - cargo-new.1
#+DESCRIPTION: Linux manpage for cargo-new.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-new - Create a new Cargo package

* SYNOPSIS
*cargo new* [/options/] /path/

* DESCRIPTION
This command will create a new Cargo package in the given directory.
This includes a simple template with a *Cargo.toml* manifest, sample
source file, and a VCS ignore file. If the directory is not already in a
VCS repository, then a new repository is created (see *--vcs* below).

See *cargo-init*(1) for a similar command which will create a new
manifest in an existing directory.

* OPTIONS
** New Options
*--bin*

#+begin_quote
  Create a package with a binary target (*src/main.rs*). This is the
  default behavior.
#+end_quote

*--lib*

#+begin_quote
  Create a package with a library target (*src/lib.rs*).
#+end_quote

*--edition* /edition/

#+begin_quote
  Specify the Rust edition to use. Default is 2021. Possible values:
  2015, 2018, 2021
#+end_quote

*--name* /name/

#+begin_quote
  Set the package name. Defaults to the directory name.
#+end_quote

*--vcs* /vcs/

#+begin_quote
  Initialize a new VCS repository for the given version control system
  (git, hg, pijul, or fossil) or do not initialize any version control
  at all (none). If not specified, defaults to *git* or the
  configuration value *cargo-new.vcs*, or *none* if already inside a VCS
  repository.
#+end_quote

*--registry* /registry/

#+begin_quote
  This sets the *publish* field in *Cargo.toml* to the given registry
  name which will restrict publishing only to that registry.

  Registry names are defined in /Cargo config files/
  <https://doc.rust-lang.org/cargo/reference/config.html>. If not
  specified, the default registry defined by the *registry.default*
  config key is used. If the default registry is not set and
  *--registry* is not used, the *publish* field will not be set which
  means that publishing will not be restricted.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Create a binary Cargo package in the given directory:

  #+begin_quote
    #+begin_example
      cargo new foo
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-init*(1)
