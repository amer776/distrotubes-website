#+TITLE: Man1 - pbmtoascii.1
#+DESCRIPTION: Linux manpage for pbmtoascii.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtoascii - convert a PBM image to ASCII graphics

* SYNOPSIS
*pbmtoascii*

[*-1x2*|*-2x4*]

[/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtoascii* reads a PBM image as input and produces a somewhat crude
ASCII graphic image as output.

To convert back, use *asciitopgm*(1)

*ppmtoterm* does a similar thing for color images to be displayed on
color text terminals.

* OPTIONS
The *-1x2* and *-2x4* options give you two alternate ways for the pixels
to get mapped to characters. With *1x2*, the default, each character
represents a group of 1 pixel across by 2 pixels down. With *-2x4*, each
character represents 2 pixels across by 4 pixels down. With the 1x2 mode
you can see the individual pixels, so it's useful for previewing small
images on a non-graphics terminal. The 2x4 mode lets you display larger
images on a small display, but it obscures pixel-level details. 2x4 mode
is also good for displaying PGM images:

#+begin_example
  pamscale -width 158 | pnmnorm | pamditherbw -threshold | pbmtoascii -2x4
#+end_example

should give good results.

* SEE ALSO
*asciitopgm*(1)

*ppmtoascii*(1)

*ppmtoterm*(1)

*pbm*(5)

* AUTHOR
Copyright (C) 1988, 1992 by Jef Poskanzer.
