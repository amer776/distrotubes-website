#+TITLE: Man1 - git-init-db.1
#+DESCRIPTION: Linux manpage for git-init-db.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-init-db - Creates an empty Git repository

* SYNOPSIS
#+begin_example
  git init-db [-q | --quiet] [--bare] [--template=<template_directory>] [--separate-git-dir <git dir>] [--shared[=<permissions>]]
#+end_example

* DESCRIPTION
This is a synonym for *git-init*(1). Please refer to the documentation
of that command.

* GIT
Part of the *git*(1) suite
