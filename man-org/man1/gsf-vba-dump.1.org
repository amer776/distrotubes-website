#+TITLE: Man1 - gsf-vba-dump.1
#+DESCRIPTION: Linux manpage for gsf-vba-dump.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gsf-vba-dump - extract Visual Basic for Applications macros

* SYNOPSIS
*gsf-vba-dump* [/FILE/]...

* DESCRIPTION
This manual page briefly documents the *gsf-vba-dump* command.

Various Microsoft binary data formats, including Excel (.xls), Word
(.doc) and PowerPoint (.ppt) can embed macro code streams. These macro
streams are in P-code (intermediate language) compiled from Visual Basic
for Applications (VBA).

The *gsf-vba-dump* utility extracts these macro streams.

* BUGS
Macro extraction from Powerpoint (.ppt) files has not been implemented
yet.

* LICENSE
*gsf-vba-dump* is licensed under the terms of version 2.1 of the GNU
Lesser General Public License (LGPL) as published by the Free Software
Foundation. For information on this license look at the source code that
came with the software or see the GNU project page ⟨URL:
http://www.gnu.org ⟩.

* AUTHORS
*gsf-vba-dump*'s primary author is Jody Goldberg <jody@gnome.org>.

The initial version of this manpage was written by J.H.M. Dassen (Ray)
<jdassen@debian.org>.

* SEE ALSO
*gnumeric*(1)

The Gnumeric homepage ⟨URL: http://www.gnome.org/projects/gnumeric/ ⟩.

The GNOME project page ⟨URL: http://www.gnome.org/ ⟩.

Wikipedia, Visual Basic for Applications ⟨URL:
http://en.wikipedia.org/wiki/Visual_Basic_for_Applications ⟩.
