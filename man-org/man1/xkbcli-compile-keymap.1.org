#+TITLE: Man1 - xkbcli-compile-keymap.1
#+DESCRIPTION: Linux manpage for xkbcli-compile-keymap.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
compiles and prints a keymap based on the given options.

Print help and exit

Enable verbose debugging output

Print the full RMLVO with the defaults filled in for missing elements

Load the XKB file from stdin, ignore RMLVO options. This option must not
be used with

Add the given path to the include path list. This option is
order-dependent, include paths given first are searched first. If an
include path is given, the default include path list is not used. Use

to add the default include paths.

Add the default set of include directories. This option is
order-dependent, include paths given first are searched first.

The XKB ruleset

The XKB model

The XKB layout

The XKB layout variant

The XKB options
