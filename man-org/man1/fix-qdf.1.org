#+TITLE: Man1 - fix-qdf.1
#+DESCRIPTION: Linux manpage for fix-qdf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fix-qdf - repair PDF files in QDF form after editing

* SYNOPSIS
*fix-qdf* < /infilename/ > /outfilename/

* DESCRIPTION
The fix-qdf program is part of the qpdf package.

The fix-qdf program reads a PDF file in QDF form and writes out the same
file with stream lengths, cross-reference table entries, and object
stream offset tables regenerated.

For details about fix-qdf and about PDF files in QDF mode, please see
the qpdf manual, which can be found in
/usr/share/doc/qpdf/qpdf-manual.html or
/usr/share/doc/qpdf/qpdf-manual.pdf.
