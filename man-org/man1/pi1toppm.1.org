#+TITLE: Man1 - pi1toppm.1
#+DESCRIPTION: Linux manpage for pi1toppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pi1toppm - convert an Atari Degas .pi1 into a PPM image

* SYNOPSIS
*pi1toppm*

[/pi1file/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pi1toppm* reads an Atari Degas .pi1 file as input and produces a PPM
image as output.

* SEE ALSO
*ppmtopi1*(1) , *pc1toppm*(1) , *ppm*(5) , *pi3topbm*(1) , *pbmtopi3*(1)

* AUTHOR
Copyright (C) 1991 by Steve Belczyk (/seb3@gte.com/) and Jef Poskanzer.
