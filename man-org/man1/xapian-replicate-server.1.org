#+TITLE: Man1 - xapian-replicate-server.1
#+DESCRIPTION: Linux manpage for xapian-replicate-server.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xapian-replicate-server - Service database replication requests from
clients

* SYNOPSIS
*xapian-replicate-server* [/OPTIONS/] /DATABASE_PARENT_DIRECTORY/

* DESCRIPTION
xapian-replicate-server - Service database replication requests from
clients

* OPTIONS
- *-I*, *--interface*=/ADDR/ :: listen on interface ADDR

- *-p*, *--port*=/PORT/ :: port to listen on

- *-o*, *--one-shot* :: serve a single connection and exit

- *--help* :: display this help and exit

- *--version* :: output version information and exit
