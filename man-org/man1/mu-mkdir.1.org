#+TITLE: Man1 - mu-mkdir.1
#+DESCRIPTION: Linux manpage for mu-mkdir.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mu mkdir- create a new Maildir

* SYNOPSIS
*mu mkdir [options] <dir> [<dirs>]*

* DESCRIPTION
*mu mkdir* is the *mu* command for creating Maildirs. It does *not* use
the mu database. With the *mkdir* command, you can create new Maildirs
with permissions 0755. For example,

#+begin_example
     mu mkdir tom dick harry
#+end_example

creates three maildirs, /tom/, /dick/ and /harry/.

If creation fails for any reason, *no* attempt is made to remove any
parts that were created. This is for safety reasons.

* OPTIONS
- *--mode*=<mode> :: set the file access mode for the new maildir(s) as
  in *chmod(1)*.

* BUGS
Please report bugs if you find them: *https://github.com/djcb/mu/issues*

* AUTHOR
Dirk-Jan C. Binnema <djcb@djcbsoftware.nl>

* SEE ALSO
*maildir*(5), *mu*(1), *chmod*(1)
