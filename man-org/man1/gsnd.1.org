#+TITLE: Man1 - gsnd.1
#+DESCRIPTION: Linux manpage for gsnd.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gsnd - Run ghostscript (PostScript and PDF engine) without display

* SYNOPSIS
*gsnd* [ /options/ ] [ /files/ ] ...

* DESCRIPTION
This script simply invokes *gs*(1) with the *-NODISPLAY* flag, followed
by any other arguments from the command-line.

* SEE ALSO
gs(1)

* VERSION
This document was last revised for Ghostscript version 9.55.0.

* AUTHOR
Artifex Software, Inc. are the primary maintainers of Ghostscript. This
manpage by George Ferguson.
