#+TITLE: Man1 - xdg-icon-resource.1
#+DESCRIPTION: Linux manpage for xdg-icon-resource.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xdg-icon-resource - command line tool for (un)installing icon resources

* SYNOPSIS
*xdg-icon-resource* install [*--noupdate*] [*--novendor*] [*--theme
*/theme/] [*--context */context/] [*--mode */mode/] *--size */size/
/icon-file/ [/icon-name/]

*xdg-icon-resource* uninstall [*--noupdate*] [*--theme */theme/]
[*--context */context/] [*--mode */mode/] *--size */size/ /icon-name/

*xdg-icon-resource* forceupdate [*--theme */theme/] [*--mode */mode/]

*xdg-icon-resource* {*--help* | *--manual* | *--version*}

* DESCRIPTION
The xdg-icon-resource program can be used to install icon resources into
the desktop icon system in order to illustrate menu entries, to depict
desktop icons or to graphically represent file types.

The desktop icon system identifies icons by name. Depending on the
required size, the choice of icon theme and the context in which the
icon is used, the desktop icon system locates an appropriate icon
resource to depict an icon. Icon resources can be XPM files or PNG
files.

The desktop icon system works according to the XDG Icon Theme
Specification at
*http://www.freedesktop.org/wiki/Specifications/icon-theme-spec*

* COMMANDS
install

#+begin_quote
  Installs the icon file indicated by /icon-file/ to the desktop icon
  system under the name /icon-name/. Icon names do not have an
  extension. If /icon-name/ is not provided the name is derived from
  /icon-file/. The icon file must have /.png/ or /.xpm/ as extension. If
  a corresponding /.icon/ file exists in the same location as
  /icon-file/ it will be installed as well.
#+end_quote

uninstall

#+begin_quote
  Removes the icon indicated by /icon-name/ from the desktop icon
  system. Note that icon names do not have an extension.
#+end_quote

forceupdate

#+begin_quote
  Force an update of the desktop icon system. This is only useful if the
  last call to xdg-icon-resource included the *--noupdate* option.
#+end_quote

* OPTIONS
*--noupdate*

#+begin_quote
  Postpone updating the desktop icon system. If multiple icons are added
  in sequence this flag can be used to indicate that additional changes
  will follow and that it is not necessary to update the desktop icon
  system right away.
#+end_quote

*--novendor*

#+begin_quote
  Normally, xdg-icon-resource checks to ensure that an icon file to be
  installed in the /apps/ context has a proper vendor prefix. This
  option can be used to disable that check.

  A vendor prefix consists of alpha characters ([a-zA-Z]) and is
  terminated with a dash ("-"). Companies and organizations are
  encouraged to use a word or phrase, preferably the organizations name,
  for which they hold a trademark as their vendor prefix. The purpose of
  the vendor prefix is to prevent name conflicts.
#+end_quote

*--theme* /theme/

#+begin_quote
  Installs or removes the icon file as part of /theme/. If no theme is
  specified the icons will be installed as part of the default /hicolor/
  theme. Applications may install icons under multiple themes but should
  at least install icons for the default /hicolor/ theme.
#+end_quote

*--context* /context/

#+begin_quote
  Specifies the context for the icon. Icons to be used in the
  application menu and as desktop icon should use /apps/ as context
  which is the default context. Icons to be used as file icons should
  use /mimetypes/ as context. Other common contexts are /actions/,
  /devices/, /emblems/, /filesystems/ and /stock/.
#+end_quote

*--size* /size/

#+begin_quote
  Specifies the size of the icon. All icons must be square. Common sizes
  for icons in the apps context are: 16, 22, 32, 48, 64 and 128. Common
  sizes for icons in the mimetypes context are: 16, 22, 32, 48, 64 and
  128
#+end_quote

*--mode* /mode/

#+begin_quote
  /mode/ can be /user/ or /system/. In user mode the file is
  (un)installed for the current user only. In system mode the file is
  (un)installed for all users on the system. Usually only root is
  allowed to install in system mode.

  The default is to use system mode when called by root and to use user
  mode when called by a non-root user.
#+end_quote

*--help*

#+begin_quote
  Show command synopsis.
#+end_quote

*--manual*

#+begin_quote
  Show this manual page.
#+end_quote

*--version*

#+begin_quote
  Show the xdg-utils version information.
#+end_quote

* ENVIRONMENT VARIABLES
xdg-icon-resource honours the following environment variables:

XDG_UTILS_DEBUG_LEVEL

#+begin_quote
  Setting this environment variable to a non-zero numerical value makes
  xdg-icon-resource do more verbose reporting on stderr. Setting a
  higher value increases the verbosity.
#+end_quote

XDG_UTILS_INSTALL_MODE

#+begin_quote
  This environment variable can be used by the user or administrator to
  override the installation mode. Valid values are /user/ and /system/.
#+end_quote

* EXIT CODES
An exit code of 0 indicates success while a non-zero exit code indicates
failure. The following failure codes can be returned:

*1*

#+begin_quote
  Error in command line syntax.
#+end_quote

*2*

#+begin_quote
  One of the files passed on the command line did not exist.
#+end_quote

*3*

#+begin_quote
  A required tool could not be found.
#+end_quote

*4*

#+begin_quote
  The action failed.
#+end_quote

*5*

#+begin_quote
  No permission to read one of the files passed on the command line.
#+end_quote

* SEE ALSO
*xdg-desktop-icon*(1), *xdg-desktop-menu*(1), *xdg-mime*(1), *Icon theme
specification*[1]

* EXAMPLES
To install an icon resource to depict a launcher for the application
myfoobar, the company ShinyThings Inc. can use:

#+begin_quote
  #+begin_example
    xdg-icon-resource install --size 64 shinythings-myfoobar.png
  #+end_example
#+end_quote

To install an icon for a new application/x-foobar file type one can use:

#+begin_quote
  #+begin_example
    xdg-icon-resource install --context mimetypes --size 48 ./mime-foobar-48.png application-x-foobar
    xdg-icon-resource install --context mimetypes --size 64 ./mime-foobar-64.png application-x-foobar
  #+end_example
#+end_quote

This will install two icons with the name application-x-foobar but with
different sizes.

* AUTHORS
*Kevin Krammer*

#+begin_quote
  Author.
#+end_quote

*Jeremy White*

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2006\\

* NOTES
-  1. :: Icon theme specification

  http://www.freedesktop.org/wiki/Specifications/icon-theme-spec/
