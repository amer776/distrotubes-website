#+TITLE: Man1 - i3-msg.1
#+DESCRIPTION: Linux manpage for i3-msg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
i3-msg - send messages to i3 window manager

* SYNOPSIS
i3-msg [-q] [-v] [-h] [-s socket] [-t type] [-r] [message]

* OPTIONS
*-q, --quiet*

#+begin_quote
  Only send ipc message and suppress the output of the response.
#+end_quote

*-v, --version*

#+begin_quote
  Display version number and exit.
#+end_quote

*-h, --help*

#+begin_quote
  Display a short help-message and exit.
#+end_quote

*-s, --socket* /sock_path/

#+begin_quote
  i3-msg will use the environment variable I3SOCK or the socket path
  given here. If both fail, it will try to get the socket information
  from the root window and then try /tmp/i3-ipc.sock before exiting with
  an error.
#+end_quote

*-t* /type/

#+begin_quote
  Send ipc message, see below. This option defaults to "command".
#+end_quote

*-m*, *--monitor*

#+begin_quote
  Instead of exiting right after receiving the first subscribed event,
  wait indefinitely for all of them. Can only be used with "-t
  subscribe". See the "subscribe" IPC message type below for details.
#+end_quote

*-r, --raw*

#+begin_quote
  Display the raw JSON reply instead of pretty-printing errors (for
  commands) or displaying the top-level config file contents (for
  GET_CONFIG).
#+end_quote

*message*

#+begin_quote
  Send ipc message, see below.
#+end_quote

* IPC MESSAGE TYPES
command

#+begin_quote
  The payload of the message is a command for i3 (like the commands you
  can bind to keys in the configuration file) and will be executed
  directly after receiving it.
#+end_quote

get_workspaces

#+begin_quote
  Gets the current workspaces. The reply will be a JSON-encoded list of
  workspaces.
#+end_quote

get_outputs

#+begin_quote
  Gets the current outputs. The reply will be a JSON-encoded list of
  outputs (see the reply section of docs/ipc, e.g. at
  *https://i3wm.org/docs/ipc.html#_receiving_replies_from_i3*).
#+end_quote

get_tree

#+begin_quote
  Gets the layout tree. i3 uses a tree as data structure which includes
  every container. The reply will be the JSON-encoded tree.
#+end_quote

get_marks

#+begin_quote
  Gets a list of marks (identifiers for containers to easily jump to
  them later). The reply will be a JSON-encoded list of window marks.
#+end_quote

get_bar_config

#+begin_quote
  Gets the configuration (as JSON map) of the workspace bar with the
  given ID. If no ID is provided, an array with all configured bar IDs
  is returned instead.
#+end_quote

get_binding_modes

#+begin_quote
  Gets a list of configured binding modes.
#+end_quote

get_version

#+begin_quote
  Gets the version of i3. The reply will be a JSON-encoded dictionary
  with the major, minor, patch and human-readable version.
#+end_quote

get_config

#+begin_quote
  Gets the currently loaded i3 configuration.
#+end_quote

send_tick

#+begin_quote
  Sends a tick to all IPC connections which subscribe to tick events.
#+end_quote

subscribe

#+begin_quote
  The payload of the message describes the events to subscribe to. Upon
  reception, each event will be dumped as a JSON-encoded object. See the
  -m option for continuous monitoring.
#+end_quote

* DESCRIPTION
i3-msg is a sample implementation for a client using the unix socket IPC
interface to i3.

** Exit status:
0: if OK, 1: if invalid syntax or unable to connect to ipc-socket 2: if
i3 returned an error processing your command(s)

* EXAMPLES

#+begin_quote
  #+begin_example
    # Use 1-px border for current client
    i3-msg "border 1pixel"

    # You can leave out the quotes
    i3-msg border normal

    # Dump the layout tree
    i3-msg -t get_tree

    # Monitor window changes
    i3-msg -t subscribe -m [ "window" ]
  #+end_example
#+end_quote

* ENVIRONMENT
** I3SOCK
If no ipc-socket is specified on the commandline, this variable is used
to determine the path, at which the unix domain socket is expected, on
which to connect to i3.

* SEE ALSO
i3(1)

* AUTHOR
Michael Stapelberg and contributors
