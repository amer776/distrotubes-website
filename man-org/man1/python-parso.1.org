#+TITLE: Man1 - python-parso.1
#+DESCRIPTION: Linux manpage for python-parso.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
parso - parso Documentation

Release v0.8.2. (Installation)

Parso is a Python parser that supports error recovery and round-trip
parsing for different Python versions (in multiple Python versions).
Parso is also able to list multiple syntax errors in your python file.

Parso has been battle-tested by /jedi/. It was pulled out of jedi to be
useful for other projects as well.

Parso consists of a small API to parse Python and analyse the syntax
tree.

A simple example:

#+begin_example
  >>> import parso
  >>> module = parso.parse('hello + 1', version="3.9")
  >>> expr = module.children[0]
  >>> expr
  PythonNode(arith_expr, [<Name: hello@1,0>, <Operator: +>, <Number: 1>])
  >>> print(expr.get_code())
  hello + 1
  >>> name = expr.children[0]
  >>> name
  <Name: hello@1,0>
  >>> name.end_pos
  (1, 5)
  >>> expr.end_pos
  (1, 9)
#+end_example

To list multiple issues:

#+begin_example
  >>> grammar = parso.load_grammar()
  >>> module = grammar.parse('foo +\nbar\ncontinue')
  >>> error1, error2 = grammar.iter_errors(module)
  >>> error1.message
  'SyntaxError: invalid syntax'
  >>> error2.message
  "SyntaxError: 'continue' not properly in loop"
#+end_example

* DOCS
** Installation and Configuration
** The preferred way (pip)
On any system you can install /parso/ directly from the Python package
index using pip:

#+begin_quote

  #+begin_quote
    #+begin_example
      sudo pip install parso
    #+end_example
  #+end_quote
#+end_quote

** From git
If you want to install the current development version (master branch):

#+begin_quote

  #+begin_quote
    #+begin_example
      sudo pip install -e git://github.com/davidhalter/parso.git#egg=parso
    #+end_example
  #+end_quote
#+end_quote

** Manual installation from a downloaded package (not recommended)
If you prefer not to use an automated package installer, you can
/download/ a current copy of /parso/ and install it manually.

To install it, navigate to the directory containing /setup.py/ on your
console and type:

#+begin_quote

  #+begin_quote
    #+begin_example
      sudo python setup.py install
    #+end_example
  #+end_quote
#+end_quote

** Usage
/parso/ works around grammars. You can simply create Python grammars by
calling /parso.load_grammar()/. Grammars (with a custom tokenizer and
custom parser trees) can also be created by directly instantiating
/parso.Grammar()/. More information about the resulting objects can be
found in the parser tree documentation.

The simplest way of using parso is without even loading a grammar
(/parso.parse()/):

#+begin_quote

  #+begin_quote
    #+begin_example
      >>> import parso
      >>> parso.parse('foo + bar')
      <Module: @1-1>
    #+end_example
  #+end_quote
#+end_quote

** Loading a Grammar
Typically if you want to work with one specific Python version, use:

#+begin_quote
  - *parso.load_grammar(*, version: Optional[/str/] = None, path:
    Optional[/str/] = None)* :: Loads a /parso.Grammar/. The default
    version is the current Python version.

    - *Parameters* :: 

      - *version* (/str/) -- A python version string, e.g.
        *version='3.8'*.

      - *path* (/str/) -- A path to a grammar file
#+end_quote

** Grammar methods
You will get back a grammar object that you can use to parse code and
find issues in it:

#+begin_quote
  - *class parso.Grammar(text: str, *, tokenizer, parser=<class
    'parso.parser.BaseParser'>,
    diff_parser=None)* :: /parso.load_grammar()/ returns instances of
    this class.

  Creating custom none-python grammars by calling this is not supported,
  yet.

  #+begin_quote
    - *Parameters* :: *text* -- A BNF representation of your grammar.
  #+end_quote

  #+begin_quote
    - *parse(code: Optional[Union[/str/, /bytes/]] = None, *,
      error_recovery=True, path: Optional[Union[/os.PathLike/, /str/]] =
      None, start_symbol: Optional[/str/] = None, cache=False,
      diff_cache=False, cache_path: Optional[Union[/os.PathLike/,
      /str/]] = None, file_io: Optional[parso.file_io.FileIO] = None) ->
      parso.grammar._NodeT* :: If you want to parse a Python file you
      want to start here, most likely.

    If you need finer grained control over the parsed instance, there
    will be other ways to access it.

    #+begin_quote
      - *Parameters* :: 

        - *code* (/str/) -- A unicode or bytes string. When it's not
          possible to decode bytes to a string, returns a
          /UnicodeDecodeError/.

        - *error_recovery* (/bool/) -- If enabled, any code will be
          returned. If it is invalid, it will be returned as an error
          node. If disabled, you will get a ParseError when encountering
          syntax errors in your code.

        - *start_symbol* (/str/) -- The grammar rule (nonterminal) that
          you want to parse. Only allowed to be used when error_recovery
          is False.

        - *path* (/str/) -- The path to the file you want to open. Only
          needed for caching.

        - *cache* (/bool/) -- Keeps a copy of the parser tree in RAM and
          on disk if a path is given. Returns the cached trees if the
          corresponding files on disk have not changed. Note that this
          stores pickle files on your file system (e.g. for Linux in
          *~/.cache/parso/*).

        - *diff_cache* (/bool/) -- Diffs the cached python module
          against the new code and tries to parse only the parts that
          have changed. Returns the same (changed) module that is found
          in cache. Using this option requires you to not do anything
          anymore with the cached modules under that path, because the
          contents of it might change. This option is still somewhat
          experimental. If you want stability, please don't use it.

        - *cache_path* (/bool/) -- If given saves the parso cache in
          this directory. If not given, defaults to the default cache
          places on each platform.

      - *Returns* :: A subclass of *parso.tree.NodeOrLeaf*. Typically a
        *parso.python.tree.Module*.
    #+end_quote
  #+end_quote

  #+begin_quote
    - *iter_errors(node)* :: Given a *parso.tree.NodeOrLeaf* returns a
      generator of /parso.normalizer.Issue/ objects. For Python this is
      a list of syntax/indentation errors.
  #+end_quote

  #+begin_quote
    - *refactor(base_node, node_to_str_map)* :: 
  #+end_quote
#+end_quote

** Error Retrieval
/parso/ is able to find multiple errors in your source code. Iterating
through those errors yields the following instances:

#+begin_quote
  - *class parso.normalizer.Issue(node, code, message)* :: 

    - *code* :: An integer code that stands for the type of error.

  #+begin_quote
    - *message* :: A message (string) for the issue.
  #+end_quote

  #+begin_quote
    - *start_pos* :: The start position position of the error as a tuple
      (line, column). As always in /parso/ the first line is 1 and the
      first column 0.
  #+end_quote
#+end_quote

** Utility
/parso/ also offers some utility functions that can be really useful:

#+begin_quote
  - *parso.parse(code=None, **kwargs)* :: A utility function to avoid
    loading grammars. Params are documented in /parso.Grammar.parse()/.

    - *Parameters* :: *version* (/str/) -- The version used by
      /parso.load_grammar()/.
#+end_quote

#+begin_quote
  - *parso.split_lines(string: /str/, keepends: /bool/ = False) ->
    Sequence[/str/]* :: Intended for Python code. In contrast to
    Python's /str.splitlines()/, looks at form feeds and other special
    characters as normal text. Just splits *\n* and *\r\n*. Also
    different: Returns *[""]* for an empty string input.

  In Python 2.7 form feeds are used as normal characters when using
  str.splitlines. However in Python 3 somewhere there was a decision to
  split also on form feeds.
#+end_quote

#+begin_quote
  - *parso.python_bytes_to_unicode(source: Union[/str/, /bytes/],
    encoding: /str/ = 'utf-8', errors: /str/ = 'strict') ->
    /str/* :: Checks for unicode BOMs and PEP 263 encoding declarations.
    Then returns a unicode object like in /bytes.decode()/.

    - *Parameters* :: 

      - *encoding* -- See /bytes.decode()/ documentation.

      - *errors* -- See /bytes.decode()/ documentation. *errors* can be
        *'strict'*, *'replace'* or *'ignore'*.
#+end_quote

** Used By

#+begin_quote

  - /jedi/ (which is used by IPython and a lot of editor plugins).

  - /mutmut/ (mutation tester)
#+end_quote

** Parser Tree
The parser tree is returned by calling *parso.Grammar.parse()*.

*NOTE:*

#+begin_quote

  #+begin_quote
    Note that parso positions are always 1 based for lines and zero
    based for columns. This means the first position in a file is (1,
    0).
  #+end_quote
#+end_quote

** Parser Tree Base Classes
Generally there are two types of classes you will deal with:
/parso.tree.Leaf/ and /parso.tree.BaseNode/.

#+begin_quote
  - *class parso.tree.BaseNode(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.tree.NodeOrLeaf/

  The super class for all nodes. A node has children, a type and
  possibly a parent node.

  #+begin_quote
    - *children* :: A list of /NodeOrLeaf/ child nodes.
  #+end_quote

  #+begin_quote
    - *parent: Optional[parso.tree.BaseNode]* :: The parent /BaseNode/
      of this leaf. None if this is the root node.
  #+end_quote

  #+begin_quote
    - *property start_pos: Tuple[/int/, /int/]* :: Returns the starting
      position of the prefix as a tuple, e.g. /(3, 4)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *get_start_pos_of_prefix()* :: Returns the start_pos of the
      prefix. This means basically it returns the end_pos of the last
      prefix. The /get_start_pos_of_prefix()/ of the prefix /+/ in /2 +
      1/ would be /(1, 1)/, while the start_pos is /(1, 2)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *property end_pos: Tuple[/int/, /int/]* :: Returns the end
      position of the prefix as a tuple, e.g. /(3, 4)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *get_code(include_prefix=True)* :: Returns the code that was the
      input for the parser for this node.

      - *Parameters* :: *include_prefix* -- Removes the prefix
        (whitespace and comments) of e.g. a statement.
  #+end_quote

  #+begin_quote
    - *get_leaf_for_position(position, include_prefixes=False)* :: Get
      the /parso.tree.Leaf/ at *position*

      - *Parameters* :: 

        - *position* (/tuple/) -- A position tuple, row, column. Rows
          start from 1

        - *include_prefixes* (/bool/) -- If *False*, *None* will be
          returned if *position* falls on whitespace or comments before
          a leaf

      - *Returns* :: /parso.tree.Leaf/ at *position*, or *None*
  #+end_quote

  #+begin_quote
    - *get_first_leaf()* :: Returns the first leaf of a node or itself
      if this is a leaf.
  #+end_quote

  #+begin_quote
    - *get_last_leaf()* :: Returns the last leaf of a node or itself if
      this is a leaf.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.tree.Leaf(value: /str/, start_pos: Tuple[/int/, /int/],
    prefix: /str/ = '')* :: Bases: /parso.tree.NodeOrLeaf/

  Leafs are basically tokens with a better API. Leafs exactly know where
  they were defined and what text preceeds them.

  #+begin_quote
    - *value* :: *str()* The value of the current token.
  #+end_quote

  #+begin_quote
    - *prefix* :: *str()* Typically a mixture of whitespace and
      comments. Stuff that is syntactically irrelevant for the syntax
      tree.
  #+end_quote

  #+begin_quote
    - *parent: Optional[parso.tree.BaseNode]* :: The parent /BaseNode/
      of this leaf.
  #+end_quote

  #+begin_quote
    - *property start_pos: Tuple[/int/, /int/]* :: Returns the starting
      position of the prefix as a tuple, e.g. /(3, 4)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *get_start_pos_of_prefix()* :: Returns the start_pos of the
      prefix. This means basically it returns the end_pos of the last
      prefix. The /get_start_pos_of_prefix()/ of the prefix /+/ in /2 +
      1/ would be /(1, 1)/, while the start_pos is /(1, 2)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *get_first_leaf()* :: Returns the first leaf of a node or itself
      if this is a leaf.
  #+end_quote

  #+begin_quote
    - *get_last_leaf()* :: Returns the last leaf of a node or itself if
      this is a leaf.
  #+end_quote

  #+begin_quote
    - *get_code(include_prefix=True)* :: Returns the code that was the
      input for the parser for this node.

      - *Parameters* :: *include_prefix* -- Removes the prefix
        (whitespace and comments) of e.g. a statement.
  #+end_quote

  #+begin_quote
    - *property end_pos: Tuple[/int/, /int/]* :: Returns the end
      position of the prefix as a tuple, e.g. /(3, 4)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote
#+end_quote

All nodes and leaves have these methods/properties:

#+begin_quote
  - *class parso.tree.NodeOrLeaf* :: Bases: /object/

  The base class for nodes and leaves.

  #+begin_quote
    - *type: /str/* :: The type is a string that typically matches the
      types of the grammar file.
  #+end_quote

  #+begin_quote
    - *get_root_node()* :: Returns the root node of a parser tree. The
      returned node doesn't have a parent node like all the other
      nodes/leaves.
  #+end_quote

  #+begin_quote
    - *get_next_sibling()* :: Returns the node immediately following
      this node in this parent's children list. If this node does not
      have a next sibling, it is None
  #+end_quote

  #+begin_quote
    - *get_previous_sibling()* :: Returns the node immediately preceding
      this node in this parent's children list. If this node does not
      have a previous sibling, it is None.
  #+end_quote

  #+begin_quote
    - *get_previous_leaf()* :: Returns the previous leaf in the parser
      tree. Returns /None/ if this is the first element in the parser
      tree.
  #+end_quote

  #+begin_quote
    - *get_next_leaf()* :: Returns the next leaf in the parser tree.
      Returns None if this is the last element in the parser tree.
  #+end_quote

  #+begin_quote
    - *abstract property start_pos: Tuple[/int/, /int/]* :: Returns the
      starting position of the prefix as a tuple, e.g. /(3, 4)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *abstract property end_pos: Tuple[/int/, /int/]* :: Returns the
      end position of the prefix as a tuple, e.g. /(3, 4)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *abstract get_start_pos_of_prefix()* :: Returns the start_pos of
      the prefix. This means basically it returns the end_pos of the
      last prefix. The /get_start_pos_of_prefix()/ of the prefix /+/ in
      /2 + 1/ would be /(1, 1)/, while the start_pos is /(1, 2)/.

      - *Return tuple of int* :: (line, column)
  #+end_quote

  #+begin_quote
    - *abstract get_first_leaf()* :: Returns the first leaf of a node or
      itself if this is a leaf.
  #+end_quote

  #+begin_quote
    - *abstract get_last_leaf()* :: Returns the last leaf of a node or
      itself if this is a leaf.
  #+end_quote

  #+begin_quote
    - *abstract get_code(include_prefix=True)* :: Returns the code that
      was the input for the parser for this node.

      - *Parameters* :: *include_prefix* -- Removes the prefix
        (whitespace and comments) of e.g. a statement.
  #+end_quote
#+end_quote

** Python Parser Tree
This is the syntax tree for Python 3 syntaxes. The classes represent
syntax elements like functions and imports.

All of the nodes can be traced back to the /Python grammar file/. If you
want to know how a tree is structured, just analyse that file (for each
Python version it's a bit different).

There's a lot of logic here that makes it easier for Jedi (and other
libraries) to deal with a Python syntax tree.

By using /parso.tree.NodeOrLeaf.get_code()/ on a module, you can get
back the 1-to-1 representation of the input given to the parser. This is
important if you want to refactor a parser tree.

#+begin_example
  >>> from parso import parse
  >>> parser = parse('import os')
  >>> module = parser.get_root_node()
  >>> module
  <Module: @1-1>
#+end_example

Any subclasses of /Scope/, including /Module/ has an attribute
/iter_imports/:

#+begin_example
  >>> list(module.iter_imports())
  [<ImportName: import os@1,0>]
#+end_example

** Changes to the Python Grammar
A few things have changed when looking at Python grammar files:

#+begin_quote

  - /Param/ does not exist in Python grammar files. It is essentially a
    part of a *parameters* node. /parso/ splits it up to make it easier
    to analyse parameters. However this just makes it easier to deal
    with the syntax tree, it doesn't actually change the valid syntax.

  - A few nodes like /lambdef/ and /lambdef_nocond/ have been merged in
    the syntax tree to make it easier to do deal with them.
#+end_quote

** Parser Tree Classes

#+begin_quote
  - *class parso.python.tree.DocstringMixin* :: Bases: /object/

    - *get_doc_node()* :: Returns the string leaf of a docstring. e.g.
      *r'''foo'''*.
#+end_quote

#+begin_quote
  - *class parso.python.tree.PythonMixin* :: Bases: /object/

  Some Python specific utilities.

  #+begin_quote
    - *get_name_of_position(position)* :: Given a (line, column) tuple,
      returns a /Name/ or *None* if there is no name at that position.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.PythonLeaf(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.PythonMixin/, /parso.tree.Leaf/

    - *get_start_pos_of_prefix()* :: Basically calls
      /parso.tree.NodeOrLeaf.get_start_pos_of_prefix()/.
#+end_quote

#+begin_quote
  - *class parso.python.tree.PythonBaseNode(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonMixin/, /parso.tree.BaseNode/
#+end_quote

#+begin_quote
  - *class parso.python.tree.PythonNode(type, children)* :: Bases:
    /parso.python.tree.PythonMixin/, *parso.tree.Node*
#+end_quote

#+begin_quote
  - *class parso.python.tree.PythonErrorNode(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonMixin/, *parso.tree.ErrorNode*
#+end_quote

#+begin_quote
  - *class parso.python.tree.PythonErrorLeaf(token_type, value,
    start_pos, prefix='')* :: Bases: *parso.tree.ErrorLeaf*,
    /parso.python.tree.PythonLeaf/
#+end_quote

#+begin_quote
  - *class parso.python.tree.EndMarker(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    *parso.python.tree._LeafWithoutNewlines*

    - *type: /str/ = 'endmarker'* :: The type is a string that typically
      matches the types of the grammar file.
#+end_quote

#+begin_quote
  - *class parso.python.tree.Newline(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.PythonLeaf/

  Contains NEWLINE and ENDMARKER tokens.

  #+begin_quote
    - *type: /str/ = 'newline'* :: The type is a string that typically
      matches the types of the grammar file.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Name(value: /str/, start_pos: Tuple[/int/,
    /int/], prefix: /str/ = '')* :: Bases:
    *parso.python.tree._LeafWithoutNewlines*

  A string. Sometimes it is important to know if the string belongs to a
  name or not.

  #+begin_quote
    - *type: /str/ = 'name'* :: The type is a string that typically
      matches the types of the grammar file.
  #+end_quote

  #+begin_quote
    - *is_definition(include_setitem=False)* :: Returns True if the name
      is being defined.
  #+end_quote

  #+begin_quote
    - *get_definition(import_name_always=False,
      include_setitem=False)* :: Returns None if there's no definition
      for a name.

      - *Parameters* :: *import_name_always* -- Specifies if an import
        name is always a definition. Normally foo in /from foo import
        bar/ is not a definition.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Literal(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.PythonLeaf/
#+end_quote

#+begin_quote
  - *class parso.python.tree.Number(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.Literal/

    - *type: /str/ = 'number'* :: The type is a string that typically
      matches the types of the grammar file.
#+end_quote

#+begin_quote
  - *class parso.python.tree.String(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.Literal/

    - *type: /str/ = 'string'* :: The type is a string that typically
      matches the types of the grammar file.

  #+begin_quote
    - *property string_prefix* :: 
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.FStringString(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.PythonLeaf/

  f-strings contain f-string expressions and normal python strings.
  These are the string parts of f-strings.

  #+begin_quote
    - *type: /str/ = 'fstring_string'* :: The type is a string that
      typically matches the types of the grammar file.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.FStringStart(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.PythonLeaf/

  f-strings contain f-string expressions and normal python strings.
  These are the string parts of f-strings.

  #+begin_quote
    - *type: /str/ = 'fstring_start'* :: The type is a string that
      typically matches the types of the grammar file.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.FStringEnd(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    /parso.python.tree.PythonLeaf/

  f-strings contain f-string expressions and normal python strings.
  These are the string parts of f-strings.

  #+begin_quote
    - *type: /str/ = 'fstring_end'* :: The type is a string that
      typically matches the types of the grammar file.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Operator(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    *parso.python.tree._LeafWithoutNewlines*,
    *parso.python.tree._StringComparisonMixin*

    - *type: /str/ = 'operator'* :: The type is a string that typically
      matches the types of the grammar file.
#+end_quote

#+begin_quote
  - *class parso.python.tree.Keyword(value: /str/, start_pos:
    Tuple[/int/, /int/], prefix: /str/ = '')* :: Bases:
    *parso.python.tree._LeafWithoutNewlines*,
    *parso.python.tree._StringComparisonMixin*

    - *type: /str/ = 'keyword'* :: The type is a string that typically
      matches the types of the grammar file.
#+end_quote

#+begin_quote
  - *class parso.python.tree.Scope(children)* :: Bases:
    /parso.python.tree.PythonBaseNode/,
    /parso.python.tree.DocstringMixin/

  Super class for the parser tree, which represents the state of a
  python text file. A Scope is either a function, class or lambda.

  #+begin_quote
    - *iter_funcdefs()* :: Returns a generator of /funcdef/ nodes.
  #+end_quote

  #+begin_quote
    - *iter_classdefs()* :: Returns a generator of /classdef/ nodes.
  #+end_quote

  #+begin_quote
    - *iter_imports()* :: Returns a generator of /import_name/ and
      /import_from/ nodes.
  #+end_quote

  #+begin_quote
    - *get_suite()* :: Returns the part that is executed by the
      function.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Module(children)* :: Bases:
    /parso.python.tree.Scope/

  The top scope, which is always a module. Depending on the underlying
  parser this may be a full module or just a part of a module.

  #+begin_quote
    - *type: /str/ = 'file_input'* :: The type is a string that
      typically matches the types of the grammar file.
  #+end_quote

  #+begin_quote
    - *get_used_names()* :: Returns all the /Name/ leafs that exist in
      this module. This includes both definitions and references of
      names.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Decorator(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/

    - *type: /str/ = 'decorator'* :: The type is a string that typically
      matches the types of the grammar file.
#+end_quote

#+begin_quote
  - *class parso.python.tree.ClassOrFunc(children)* :: Bases:
    /parso.python.tree.Scope/

    - *property name* :: Returns the /Name/ leaf that defines the
      function or class name.

  #+begin_quote
    - *get_decorators()* :: 

      - *Return type* :: list of /Decorator/
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Class(children)* :: Bases:
    /parso.python.tree.ClassOrFunc/

  Used to store the parsed contents of a python class.

  #+begin_quote
    - *type: /str/ = 'classdef'* :: The type is a string that typically
      matches the types of the grammar file.
  #+end_quote

  #+begin_quote
    - *get_super_arglist()* :: Returns the /arglist/ node that defines
      the super classes. It returns None if there are no arguments.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Function(children)* :: Bases:
    /parso.python.tree.ClassOrFunc/

  Used to store the parsed contents of a python function.

  Children:

  #+begin_quote

    #+begin_quote
      #+begin_example
        0. <Keyword: def>
        1. <Name>
        2. parameter list (including open-paren and close-paren <Operator>s)
        3. or 5. <Operator: :>
        4. or 6. Node() representing function body
        3. -> (if annotation is also present)
        4. annotation (if present)
      #+end_example
    #+end_quote
  #+end_quote

  #+begin_quote
    - *type: /str/ = 'funcdef'* :: The type is a string that typically
      matches the types of the grammar file.
  #+end_quote

  #+begin_quote
    - *get_params()* :: Returns a list of /Param()/.
  #+end_quote

  #+begin_quote
    - *property name* :: Returns the /Name/ leaf that defines the
      function or class name.
  #+end_quote

  #+begin_quote
    - *iter_yield_exprs()* :: Returns a generator of /yield_expr/.
  #+end_quote

  #+begin_quote
    - *iter_return_stmts()* :: Returns a generator of /return_stmt/.
  #+end_quote

  #+begin_quote
    - *iter_raise_stmts()* :: Returns a generator of /raise_stmt/.
      Includes raise statements inside try-except blocks
  #+end_quote

  #+begin_quote
    - *is_generator()* :: 

      - *Return bool* :: Checks if a function is a generator or not.
  #+end_quote

  #+begin_quote
    - *property annotation* :: Returns the test node after /->/ or
      /None/ if there is no annotation.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Lambda(children)* :: Bases:
    /parso.python.tree.Function/

  Lambdas are basically trimmed functions, so give it the same
  interface.

  Children:

  #+begin_quote

    #+begin_quote
      #+begin_example
         0. <Keyword: lambda>
         *. <Param x> for each argument x
        -2. <Operator: :>
        -1. Node() representing body
      #+end_example
    #+end_quote
  #+end_quote

  #+begin_quote
    - *type: /str/ = 'lambdef'* :: The type is a string that typically
      matches the types of the grammar file.
  #+end_quote

  #+begin_quote
    - *property name* :: Raises an AttributeError. Lambdas don't have a
      defined name.
  #+end_quote

  #+begin_quote
    - *property annotation* :: Returns /None/, lambdas don't have
      annotations.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Flow(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/
#+end_quote

#+begin_quote
  - *class parso.python.tree.IfStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.python.tree.Flow/

    - *type: /str/ = 'if_stmt'* :: The type is a string that typically
      matches the types of the grammar file.

  #+begin_quote
    - *get_test_nodes()* :: E.g. returns all the /test/ nodes that are
      named as x, below:

      #+begin_quote

        #+begin_quote
          - *if x:* :: pass

          - *elif x:* :: pass
        #+end_quote
      #+end_quote
  #+end_quote

  #+begin_quote
    - *get_corresponding_test_node(node)* :: Searches for the branch in
      which the node is and returns the corresponding test node (see
      function above). However if the node is in the test node itself
      and not in the suite return None.
  #+end_quote

  #+begin_quote
    - *is_node_after_else(node)* :: Checks if a node is defined after
      /else/.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.WhileStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.python.tree.Flow/

    - *type: /str/ = 'while_stmt'* :: The type is a string that
      typically matches the types of the grammar file.
#+end_quote

#+begin_quote
  - *class parso.python.tree.ForStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.python.tree.Flow/

    - *type: /str/ = 'for_stmt'* :: The type is a string that typically
      matches the types of the grammar file.

  #+begin_quote
    - *get_testlist()* :: Returns the input node *y* from: *for x in
      y:*.
  #+end_quote

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: 
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.TryStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.python.tree.Flow/

    - *type: /str/ = 'try_stmt'* :: The type is a string that typically
      matches the types of the grammar file.

  #+begin_quote
    - *get_except_clause_tests()* :: Returns the *test* nodes found in
      *except_clause* nodes. Returns *[None]* for except clauses without
      an exception given.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.WithStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.python.tree.Flow/

    - *type: /str/ = 'with_stmt'* :: The type is a string that typically
      matches the types of the grammar file.

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: Returns the a list
      of /Name/ that the with statement defines. The defined names are
      set after /as/.
  #+end_quote

  #+begin_quote
    - *get_test_node_from_name(name)* :: 
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Import(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/

    - *get_path_for_name(name)* :: The path is the list of names that
      leads to the searched name.

      - *Return list of Name* :: 

  #+begin_quote
    - *is_nested()* :: 
  #+end_quote

  #+begin_quote
    - *is_star_import()* :: 
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.ImportFrom(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.python.tree.Import/

    - *type: /str/ = 'import_from'* :: The type is a string that
      typically matches the types of the grammar file.

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: Returns the a list
      of /Name/ that the import defines. The defined names are set after
      /import/ or in case an alias - /as/ - is present that name is
      returned.
  #+end_quote

  #+begin_quote
    - *get_from_names()* :: 
  #+end_quote

  #+begin_quote
    - *property level* :: The level parameter of *__import__*.
  #+end_quote

  #+begin_quote
    - *get_paths()* :: The import paths defined in an import statement.
      Typically an array like this: *[<Name: datetime>, <Name: date>]*.

      - *Return list of list of Name* :: 
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.ImportName(children:
    List[parso.tree.NodeOrLeaf])* :: Bases: /parso.python.tree.Import/

  For *import_name* nodes. Covers normal imports without *from*.

  #+begin_quote
    - *type: /str/ = 'import_name'* :: The type is a string that
      typically matches the types of the grammar file.
  #+end_quote

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: Returns the a list
      of /Name/ that the import defines. The defined names is always the
      first name after /import/ or in case an alias - /as/ - is present
      that name is returned.
  #+end_quote

  #+begin_quote
    - *property level* :: The level parameter of *__import__*.
  #+end_quote

  #+begin_quote
    - *get_paths()* :: 
  #+end_quote

  #+begin_quote
    - *is_nested()* :: This checks for the special case of nested
      imports, without aliases and from statement:

      #+begin_quote
        #+begin_example
          import foo.bar
        #+end_example
      #+end_quote
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.KeywordStatement(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/

  For the following statements: /assert/, /del/, /global/, /nonlocal/,
  /raise/, /return/, /yield/.

  /pass/, /continue/ and /break/ are not in there, because they are just
  simple keywords and the parser reduces it to a keyword.

  #+begin_quote
    - *property type* :: Keyword statements start with the keyword and
      end with /_stmt/. You can crosscheck this with the Python grammar.
  #+end_quote

  #+begin_quote
    - *property keyword* :: 
  #+end_quote

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: 
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.AssertStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.KeywordStatement/

    - *property assertion* :: 
#+end_quote

#+begin_quote
  - *class parso.python.tree.GlobalStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.KeywordStatement/

    - *get_global_names()* :: 
#+end_quote

#+begin_quote
  - *class parso.python.tree.ReturnStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.KeywordStatement/
#+end_quote

#+begin_quote
  - *class parso.python.tree.YieldExpr(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/

    - *type: /str/ = 'yield_expr'* :: The type is a string that
      typically matches the types of the grammar file.
#+end_quote

#+begin_quote
  - *class parso.python.tree.ExprStmt(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/,
    /parso.python.tree.DocstringMixin/

    - *type: /str/ = 'expr_stmt'* :: The type is a string that typically
      matches the types of the grammar file.

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: Returns a list of
      /Name/ defined before the /=/ sign.
  #+end_quote

  #+begin_quote
    - *get_rhs()* :: Returns the right-hand-side of the equals.
  #+end_quote

  #+begin_quote
    - *yield_operators()* :: Returns a generator of /+=/, /=/, etc. or
      None if there is no operation.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.NamedExpr(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/

    - *type: /str/ = 'namedexpr_test'* :: The type is a string that
      typically matches the types of the grammar file.

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: 
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.Param(children, parent)* :: Bases:
    /parso.python.tree.PythonBaseNode/

  It's a helper class that makes business logic with params much easier.
  The Python grammar defines no *param* node. It defines it in a
  different way that is not really suited to working with parameters.

  #+begin_quote
    - *type: /str/ = 'param'* :: The type is a string that typically
      matches the types of the grammar file.
  #+end_quote

  #+begin_quote
    - *property star_count* :: Is /0/ in case of /foo/, /1/ in case of
      /*foo/ or /2/ in case of /**foo/.
  #+end_quote

  #+begin_quote
    - *property default* :: The default is the test node that appears
      after the /=/. Is /None/ in case no default is present.
  #+end_quote

  #+begin_quote
    - *property annotation* :: The default is the test node that appears
      after /:/. Is /None/ in case no annotation is present.
  #+end_quote

  #+begin_quote
    - *property name* :: The /Name/ leaf of the param.
  #+end_quote

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: 
  #+end_quote

  #+begin_quote
    - *property position_index* :: Property for the positional index of
      a paramter.
  #+end_quote

  #+begin_quote
    - *get_parent_function()* :: Returns the function/lambda of a
      parameter.
  #+end_quote

  #+begin_quote
    - *get_code(include_prefix=True, include_comma=True)* :: Like all
      the other get_code functions, but includes the param
      /include_comma/.

      - *Parameters* :: *bool* (/include_comma/) -- If enabled includes
        the comma in the string output.
  #+end_quote
#+end_quote

#+begin_quote
  - *class parso.python.tree.SyncCompFor(children:
    List[parso.tree.NodeOrLeaf])* :: Bases:
    /parso.python.tree.PythonBaseNode/

    - *type: /str/ = 'sync_comp_for'* :: The type is a string that
      typically matches the types of the grammar file.

  #+begin_quote
    - *get_defined_names(include_setitem=False)* :: Returns the a list
      of /Name/ that the comprehension defines.
  #+end_quote
#+end_quote

#+begin_quote
  - *parso.python.tree.CompFor* :: alias of
    /parso.python.tree.SyncCompFor/
#+end_quote

#+begin_quote
  - *class parso.python.tree.UsedNamesMapping(dct)* :: Bases:
    /collections.abc.Mapping/

  This class exists for the sole purpose of creating an immutable dict.
#+end_quote

** Utility

#+begin_quote
  - *parso.tree.search_ancestor(node, *node_types)* :: Recursively looks
    at the parents of a node and returns the first found node that
    matches node_types. Returns *None* if no matching node is found.

    - *Parameters* :: 

      - *node* -- The ancestors of this node will be checked.

      - *node_types* (/tuple of str/) -- type names that are searched
        for.
#+end_quote

** Development
If you want to contribute anything to /parso/, just open an issue or
pull request to discuss it. We welcome changes! Please check the
*CONTRIBUTING.md* file in the repository, first.

** Deprecations Process
The deprecation process is as follows:

#+begin_quote

  1. A deprecation is announced in the next major/minor release.

  2. We wait either at least a year & at least two minor releases until
     we remove the deprecated functionality.
#+end_quote

** Testing
The test suite depends on *pytest*:

#+begin_quote

  #+begin_quote
    #+begin_example
      pip install pytest
    #+end_example
  #+end_quote
#+end_quote

To run the tests use the following:

#+begin_quote

  #+begin_quote
    #+begin_example
      pytest
    #+end_example
  #+end_quote
#+end_quote

If you want to test only a specific Python version (e.g. Python 3.9),
it's as easy as:

#+begin_quote

  #+begin_quote
    #+begin_example
      python3.9 -m pytest
    #+end_example
  #+end_quote
#+end_quote

Tests are also run automatically on /Travis CI/.

* RESOURCES

#+begin_quote

  - /Source Code on Github/

  - /Travis Testing/

  - /Python Package Index/
#+end_quote

* AUTHOR
parso contributors

* COPYRIGHT
parso contributors
