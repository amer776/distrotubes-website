#+TITLE: Man1 - xdg-user-dirs-update.1
#+DESCRIPTION: Linux manpage for xdg-user-dirs-update.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xdg-user-dirs-update - Update XDG user dir configuration

* SYNOPSIS
*xdg-user-dirs-update* [OPTION...] [--set /NAME/ /PATH/...]

* DESCRIPTION
*xdg-user-dirs-update* updates the current state of the users
user-dirs.dir. If none existed before then one is created based on the
system default values, or falling back to the old non-translated
filenames if such directories exists. The list of old directories used
are: ~/Desktop, ~/Templates and ~/Public.

If an old configuration exists it is updated with any new default
directories. Additionally, any configured directories that point to
non-existing locations are reset by pointing then to the users home
directory. This typically happens when the users removed the directory,
so they likely dont want to use it anymore.

On the first run a user-dirs.locale file is created containing the
locale that was used for the translation. This is used later by GUI
tools like *xdg-user-dirs-gtk-update* to detect if the locale was
changed, letting you to migrate from the old names.

*xdg-user-dirs-update* is normally run automatically at the start of a
user session to update the XDG user dirs according to the users locale.

* OPTIONS
The following options are understood:

*--help*

#+begin_quote
  Print help output and exit.
#+end_quote

*--force*

#+begin_quote
  Update existing user-dirs.dir, but force a full reset. This means:
  Dont reset nonexisting directories to HOME, rather recreate the
  directory. Never use backwards compatible non-translated names. Always
  recreate user-dirs.locale.
#+end_quote

*--dummy-output */PATH/

#+begin_quote
  Write the configuration to /PATH/ instead of the default configuration
  file. Also, no directories are created.
#+end_quote

*--set */NAME/* */PATH/

#+begin_quote
  Sets the XDG user dir with the given name.

  /NAME/ should be one of the following:

  #+begin_quote
    DESKTOP
  #+end_quote

  #+begin_quote
    DOWNLOAD
  #+end_quote

  #+begin_quote
    TEMPLATES
  #+end_quote

  #+begin_quote
    PUBLICSHARE
  #+end_quote

  #+begin_quote
    DOCUMENTS
  #+end_quote

  #+begin_quote
    MUSIC
  #+end_quote

  #+begin_quote
    PICTURES
  #+end_quote

  #+begin_quote
    VIDEOS
  #+end_quote

  /PATH/ must be an absolute path, e.g. $HOME/Some/Directory.
#+end_quote

* FILES
The XDG user dirs configuration is stored in the user-dirs.dir file in
the location pointed to by the *XDG_CONFIG_HOME* environment variable.

* ENVIRONMENT
The *XDG_CONFIG_HOME* environment variable determines where the
user-dirs.dirs file is located.

* SEE ALSO
*xdg-user-dir*(1), *user-dirs.dirs*(5), *user-dirs.defaults*(5),
*user-dirs.conf*(5).
