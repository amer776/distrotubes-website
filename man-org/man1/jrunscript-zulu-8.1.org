#+TITLE: Man1 - jrunscript-zulu-8.1
#+DESCRIPTION: Linux manpage for jrunscript-zulu-8.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jrunscript - Runs a command-line script shell that supports interactive
and batch modes. This command is experimental and unsupported.

* SYNOPSIS
#+begin_example

  jrunscript [options] [arguments]
#+end_example

- /options/ :: The command-line options. See Options.

- /arguments/ :: Arguments, when used, follow immediately after options
  or the command name. See Arguments.

* DESCRIPTION
The jrunscript command is a language-independent command-line script
shell. The jrunscript command supports both an interactive
(read-eval-print) mode and a batch (-f option) mode of script execution.
By default, JavaScript is the language used, but the -l option can be
used to specify a different language. By using Java to scripting
language communication, the jrunscript command supports an exploratory
programming style.

* OPTIONS
- -classpath /path/ :: \\
  Indicate where any class files are that the script needs to access.

- -cp /path/ :: \\
  Same as -classpathpath.

- -D/name/=/value/ :: \\
  Sets a Java system property.

- -J/flag/ :: \\
  Passes flag directly to the Java Virtual Machine where the jrunscript
  command is running.

- -I /language/ :: \\
  Uses the specified scripting language. By default, JavaScript is used.
  To use other scripting languages, you must specify the corresponding
  script engine's JAR file with the -cp or -classpath option.

- -e /script/ :: \\
  Evaluates the specified script. This option can be used to run
  one-line scripts that are specified completely on the command line.

- -encoding /encoding/ :: \\
  Specifies the character encoding used to read script files.

- -f /script-file/ :: \\
  Evaluates the specified script file (batch mode).

- -f - :: \\
  Reads and evaluates a script from standard input (interactive mode).

- -help :: \\
  Displays a help message and exits.

- -? :: \\
  Displays a help message and exits.

- -q :: \\
  Lists all script engines available and exits.

* ARGUMENTS
If arguments are present and if no -e or -f option is used, then the
first argument is the script file and the rest of the arguments, if any,
are passed to the script. If arguments and -e or the -f option are used,
then all arguments are passed to the script. If arguments, -e and -f are
missing, then interactive mode is used. Script arguments are available
to a script in an engine variable named arguments of type String array.

* EXAMPLES
** EXECUTE INLINE SCRIPTS
#+begin_example
  jrunscript -e "print('hello world')"
#+end_example

#+begin_example
  jrunscript -e "cat('http://www.example.com')"
#+end_example

#+begin_example
#+end_example

** USE SPECIFIED LANGUAGE AND EVALUATE THE SCRIPT FILE
#+begin_example
  jrunscript -l js -f test.js
#+end_example

#+begin_example
#+end_example

** INTERACTIVE MODE
#+begin_example
  jrunscript
#+end_example

#+begin_example
  js> print('Hello World\n');
#+end_example

#+begin_example
  Hello World
#+end_example

#+begin_example
  js> 34 + 55
#+end_example

#+begin_example
  89.0
#+end_example

#+begin_example
  js> t = new java.lang.Thread(function() { print('Hello World\n'); })
#+end_example

#+begin_example
  Thread[Thread-0,5,main]
#+end_example

#+begin_example
  js> t.start()
#+end_example

#+begin_example
  js> Hello World
#+end_example

#+begin_example
#+end_example

#+begin_example
  js>
#+end_example

#+begin_example
#+end_example

** RUN SCRIPT FILE WITH SCRIPT ARGUMENTS
The test.js file is the script file. The arg1, arg2 and arg3 arguments
are passed to the script. The script can access these arguments with an
arguments array.

#+begin_example
  jrunscript test.js arg1 arg2 arg3
#+end_example

#+begin_example
#+end_example

* SEE ALSO
If JavaScript is used, then before it evaluates a user defined script,
the jrunscript command initializes certain built-in functions and
objects. These JavaScript built-ins are documented in JsDoc-Toolkit at
http://code.google.com/p/jsdoc-toolkit/

\\
