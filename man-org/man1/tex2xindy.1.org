#+TITLE: Man1 - tex2xindy.1
#+DESCRIPTION: Linux manpage for tex2xindy.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
*\$1*

#+begin_example
#+end_example

\\

* NAME
tex2xindy - a preprocessor of the xindy index processor

* SYNOPSIS
tex2xindy [-o] [attr_file]

* DESCRIPTION
*tex2xindy* transforms a LaTeX index file =.idx= (or an =.aux= file)
into a *xindy* raw index file.

It is a filter that reads from /stdin/ a file in the input format of
LaTeX's raw index file, i.e., with =\indexentry= tags. It outputs on
/stdout/ a *xindy* raw index file, i.e., with =indexentry= clauses.

If the option *-o* is not specified, *tex2xindy* handles ^^-notation of
TeX and outputs the octet that is represented: =^^ab= in the input gets
output as the octet 0xab. If =^^^^abcd= or =^^^^^^^^abcdefab= are
detected, they are output as is.

If the option *-o* is specified, *tex2xindy* operates in /Omega/ mode
and handles its ^^-notation: Then =^^ab=, =^^^^abcd=, and
=^^^^^^^^abcdefab= represent Unicode characters with code points 0xab,
0xabcd, and 0xabcdefab respectively. They are output in UTF-8 encoding.

If the optional argument /attr_file/ is specified, *tex2xindy* writes
all index key attributes into this file.

* DEFICITS
This program was written since it was not easily possible to extract the
parser from the old makeindex system. Therefore it does not find all
errors in the input as the /makeindex/ (1) version.

Additionally it uses only the default input specifiers of
/makeindex/ (1). If other input specifiers (cf. manual page of
/makeindex/ (1)) are needed, the input specifiers (starting from the
pattern =KEYWORD=, see below) must be changed and the program must be
recompiled.

The particular missing feature is configuration of the quote and the
actual characters, maybe also the escape, subitem (level), and encap
characters. Argument and range delimiters seem to be less of a problem.

In fact, input markup handling (and thus *tex2xindy*) should be
incorporated into the *xindy* kernel, to be able to specify
configuration in xindy style files.

* SEE ALSO
/texindy/ (1), /xindy/ (1), /makeindex/ (1)

* AUTHOR
Roger Kehr, Institut fuer Theoretische Informatik, TU Darmstadt

* COPYRIGHT AND LICENSE
Copyright (c) 1996,1997 Roger Kehr. Copyright (c) 2006 Joachim Schrod.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
675 Mass Ave, Cambridge, MA 02139, USA.
