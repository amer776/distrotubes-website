#+TITLE: Man1 - grab_color_image.1
#+DESCRIPTION: Linux manpage for grab_color_image.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grab_color_image - grab an image using libdc1394

* SYNOPSIS
*grab_color_image [/--guid=/dev/video1394/x/] [filename.ppm]*

* DESCRIPTION
Get one image using libdc1394 and store it as a portable pix map (ppm)
image.

* OPTIONS
- *--guid* :: specifies camera to use. default is the first camera found
  in the system. *--help* print help message

* AUTHOR
This manual page was written by Peter De Schrijver <p2@debian.org> for
the Debian GNU/Linux system (but may be used by others) and added to
upstream by Peter Antoniac <theseinfeld@users.sf.net>.
