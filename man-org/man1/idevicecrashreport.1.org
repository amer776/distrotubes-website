#+TITLE: Man1 - idevicecrashreport.1
#+DESCRIPTION: Linux manpage for idevicecrashreport.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idevicecrashreport - Retrieve crash reports from a device.

* SYNOPSIS
*idevicecrashreport* [OPTIONS] DIRECTORY

* DESCRIPTION
Simple utility to move crash reports from a device to a local directory.

The utility outputs lines prefixed with either "Link:", "Copy:" or
"Move:" depending on whether a symlink was created, a file was copied or
moved from the device to the target DIRECTORY.

* OPTIONS
- *-u, --udid UDID* :: target specific device by UDID.

- *-n, --network* :: connect to network device.

- *-e, --extract* :: extract raw crash report into separate '.crash'
  files.

- *-k, --keep* :: copy but do not remove crash reports from device.

- *-d, --debug* :: enable communication debugging.

- *-h, --help* :: prints usage information.

- *-v, --version* :: prints version information.

* AUTHOR
Martin Szulecki

Nikias Bassen

* ON THE WEB
https://libimobiledevice.org

https://github.com/libimobiledevice/libimobiledevice
