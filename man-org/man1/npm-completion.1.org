#+TITLE: Man1 - npm-completion.1
#+DESCRIPTION: Linux manpage for npm-completion.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*npm-completion* - Tab Completion for npm

** Synopsis

#+begin_quote
  #+begin_example
    source <(npm completion)
  #+end_example
#+end_quote

Note: This command is unaware of workspaces.

** Description
Enables tab-completion in all npm commands.

The synopsis above loads the completions into your current shell. Adding
it to your ~/.bashrc or ~/.zshrc will make the completions available
everywhere:

#+begin_quote
  #+begin_example
    npm completion >> ~/.bashrc
    npm completion >> ~/.zshrc
  #+end_example
#+end_quote

You may of course also pipe the output of *npm completion* to a file
such as */usr/local/etc/bash_completion.d/npm* or
*/etc/bash_completion.d/npm* if you have a system that will read that
file for you.

When *COMP_CWORD*, *COMP_LINE*, and *COMP_POINT* are defined in the
environment, *npm completion* acts in "plumbing mode", and outputs
completions based on the arguments.

** See Also

#+begin_quote

  - npm help developers

  - npm help npm
#+end_quote
