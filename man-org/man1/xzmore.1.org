#+TITLE: Man1 - xzmore.1
#+DESCRIPTION: Linux manpage for xzmore.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xzmore, lzmore - view xz or lzma compressed (text) files

* SYNOPSIS
*xzmore* [/file.../]\\
*lzmore* [/file.../]

* DESCRIPTION
*xzmore* is a filter which allows examination of *xz*(1) or *lzma*(1)
compressed text files one screenful at a time on a soft-copy terminal.

To use a pager other than the default *more,* set environment variable
*PAGER* to the name of the desired program. The name *lzmore* is
provided for backward compatibility with LZMA Utils.

- *e* or *q* :: When the prompt --More--(Next file: /file/) is printed,
  this command causes *xzmore* to exit.

- *s* :: When the prompt --More--(Next file: /file/) is printed, this
  command causes *xzmore* to skip the next file and continue.

For list of keyboard commands supported while actually viewing the
content of a file, refer to manual of the pager you use, usually
*more*(1).

* SEE ALSO
*more*(1), *xz*(1), *xzless*(1), *zmore*(1)
