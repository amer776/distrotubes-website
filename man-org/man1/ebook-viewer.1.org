#+TITLE: Man1 - ebook-viewer.1
#+DESCRIPTION: Linux manpage for ebook-viewer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ebook-viewer - ebook-viewer

#+begin_quote

  #+begin_quote
    #+begin_example
      ebook-viewer [options] file
    #+end_example
  #+end_quote
#+end_quote

View an e-book.

Whenever you pass arguments to *ebook-viewer* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--continue* :: Continue reading the last opened book
#+end_quote

#+begin_quote
  - *--detach* :: Detach from the controlling terminal, if any (Linux
    only)
#+end_quote

#+begin_quote
  - *--force-reload* :: Force reload of all opened books
#+end_quote

#+begin_quote
  - *--full-screen, --fullscreen, -f* :: If specified, the E-book viewer
    window will try to open full screen when started.
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--open-at* :: The position at which to open the specified book. The
    position is a location or position you can get by using the Go
    to->Location action in the viewer controls. Alternately, you can use
    the form toc:something and it will open at the location of the first
    Table of Contents entry that contains the string *"*something*"*.
    The form toc-href:something will match the href (internal link
    destination) of toc nodes. The matching is exact. If you want to
    match a substring, use the form toc-href-contains:something. The
    form ref:something will use Reference mode references.
#+end_quote

#+begin_quote
  - *--raise-window* :: If specified, the E-book viewer window will try
    to come to the front when started.
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
