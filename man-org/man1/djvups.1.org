#+TITLE: Man1 - djvups.1
#+DESCRIPTION: Linux manpage for djvups.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
djvups - Convert DjVu documents to PostScript.

* SYNOPSIS
*djvups [*/options/*] [*/djvufile/*] [*/outputfile/*]*

* DESCRIPTION
This program decodes DjVu file /djvufile/, and generates a PostScript
file named /outputfile/. The DjVu data is read from the standard input
when argument /djvufile/ is not specified or when it is equal to a
single dash. Similarly, the output data is written to the standard
output when argument /outputfile/ is not specified or equal to a single
dash.

PostScript printers have various capabilities. Investigate options
*-level* and *-gray* for obtaining the best results.

* OPTIONS
- *-help* :: Prints the list of recognized options.

- *-verbose* :: Displays a progress bar.

- *-page=*/pagespec/ :: Specify the document pages to be converted. The
  page specification /pagespec/ contains one or more comma-separated
  page ranges. A page range is either a page number, or two page numbers
  separated by a dash. Specification *1-10*, for instance, prints pages
  1 to 10. Specification *1,3,99999-4* prints pages 1 and 3, followed by
  all the document pages in reverse order up to page 4.

- *-format=ps* :: Produce a PostScript file. This is the default.

- *-format=eps* :: Produce an Encapsulated PostScript file. Encapsulated
  PostScript files are suitable for embedding images into other
  documents. Encapsulated PostScript file can only contain a single
  page. Setting this option overrides the options *-copies*,
  *-orientation*, *-zoom*, *-cropmarks*, and *-booklet*.

- *-copies=*/n/ :: Specify the number of copies to print.

- *-orientation=*/orient/ :: Specify whether pages should be printed
  using the *auto*, *portrait*, or *landscape* orientation.

- *-mode=*/modespec/ :: Specify how pages should be decoded. The default
  mode, *color*, renders all the layers of the DjVu documents. Mode
  *black* only renders the foreground layer mask. This mode does not
  work with DjVuPhoto images because these files have no foreground
  layer mask. Modes *foreground* and *background* only render the
  foreground layer or the background layer of a DjVuDocument image.

- *-zoom=*/zoomspec/ :: Specify a zoom factor /zoomspec/. The default
  zoom factor, *auto*, scales the image to fit the page. Argument
  /zoomspec/ also can be a number in range *25* to *2400* representing a
  magnification percentage relative to the original size of the
  document.

- *-frame=*/yesno/ :: Specifying *yes* causes the generation of a thin
  gray border representing the boundaries of the document pages. The
  default is *no*.

- *-cropmarks=*/yesno/ :: Specifying *yes* causes the generation of crop
  marks indicating where pages should be cut. The default is *no*.

- *-level=*/languagelevel/ :: Select the language level of the generated
  PostScript. /languagelevel/. Valid language levels are *1*, *2*, and
  *3*. Level *3* produces the most compact and fast printing PostScript
  files. Some of these files however require a very modern printer.
  Level *2* is the default value. The generated PostScript files are
  almost as compact and work with all but the oldest PostScript
  printers. Level *1* can be used as a last resort option.

- *-color=*/yesno/ :: The default value, *yes*, generates a color
  PostScript file. Specifying value *no* converts the image to gray
  scale. The resulting PostScript file is smaller and marginally more
  portable.

- *-gray* :: This option is equivalent to option *-color=no* and is
  provided for convenience.

- *-colormatch=*/yesno/ :: The default value, *yes*, generates a
  PostScript file using device independent colors in compliance with the

specification. Modern printers then produce colors that match the
original as well as possible. Specifying value *no* generates a
PostScript file using device dependent colors. This is sometimes useful
with older printers. You can then use option *-gamma* to tune the output
colors.

- *-gamma=*/gammaspec/ :: Specify a gamma correction factor for the
  device dependent PostScript colors. Argument /gammaspec/ must be in
  range *0.3* to *5.0*. Gamma correction normally pertains to cathodic
  screens only. It gets meaningful for printers because several models
  interpret device dependent RGB colors by emulating the color response
  of a cathodic tube.

- *-booklet=*/opt/ :: Turns the booklet printing mode on. The booklet
  mode prints two pages on each side in a way suitable for making a
  booklet by folding the sheets. Option /opt/ can take values *no* for
  disabling the booklet mode, *yes* for enabling the recto/verso booklet
  mode, and *recto* or *verso* to print only one side of each sheet.

- *-bookletmax=*/max/ :: Specifies the maximal number of pages per
  booklet. A single printout might then be composed of several booklets.
  Argument /max/ is rounded up to the next multiple of 4. Specifying *0*
  sets no maximal number of pages and ensures that the printout will
  produce a single booklet. This is the default.

- *-bookletalign=*/align/ :: Specifies a positive or negative offset
  applied to the verso of each sheet. Argument /align/ is expressed in
  points (one point is 1/72th of an inch, or 0.352 millimeter) This is
  useful with certain printers to ensure that both recto and verso are
  properly aligned. The default value is of course *0*.

- *-bookletfold=*/base[+incr]/ :: Specifies the extra margin left
  between both pages on a single sheet. The base value /base/ is
  expressed in points (one point is 1/72th of an inch, or 0.352
  millimeter). This margin is incremented for each outer sheet by value
  /incr/ expressed in millipoints. The default value is *18+200*.

* CREDITS
This program was written by Léon Bottou <leonb@users.sourceforge.net>,
Andrei Erofeev <andrew_erofeev@yahoo.com>, and Florin Nicsa.

* SEE ALSO
*djvu*(1), *ddjvu*(1). *djview*(1)
