#+TITLE: Man1 - pgmtoppm.1
#+DESCRIPTION: Linux manpage for pgmtoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pgmtoppm - colorize a PGM (grayscale) image into a PPM (color) image

* SYNOPSIS
*pgmtoppm* /colorspec/ [/pgmfile/] *pgmtoppm*
/colorspec1/*-*/colorspec2/ [/pgmfile/] *pgmtoppm* *-map=*/mapfile/
[/pgmfile/]

Minimum unique abbreviation of option is acceptable. You may use double
hyphens instead of single hyphen to denote options. You may use white
space in place of the equals sign to separate an option name from its
value.

* DESCRIPTION
This program is part of *Netpbm*(1)

*pgmtoppm* reads a PGM as input and produces a PPM file as output with a
specific color assigned to each gray value in the input.

If you specify one color argument, black in the pgm file stays black and
white in the pgm file turns into the specified color in the ppm file.
Gray values in between are linearly mapped to differing intensities of
the specified color.

If you specify two color arguments (separated by a hyphen), then black
gets mapped to the first color and white gets mapped to the second and
gray values in between get mapped linearly (across a three dimensional
space) to colors in between.

Specify the color (/color/) as described for the
[[file:libppm.html#colorname][argument of the *ppm_parsecolor()* library
routine]] .

Also, you can specify an entire colormap with the *-map* option. The
mapfile is just a *ppm* file; it can be any shape, all that matters is
the colors in it and their order. In this case, black gets mapped into
the first color in the map file, and white gets mapped to the last and
gray values in between are mapped linearly onto the sequence of colors
in between. The maxval of the output image is the maxval of the map
image.

A more direct way to specify a particular color to replace each
particular gray level is to use *pamlookup*. You make an index file that
explicitly associates a color with each possible gray level.

* NOTE - MAXVAL
When you don't use *-map*, the 'maxval,' or depth, of the output image
is the same as that of the input image. The maxval affects the color
resolution, which may cause quantization errors you don't anticipate in
your output. For example, you have a simple black and white image as a
PGM with maxval 1. Run this image through *pgmtoppm 0f/00/00* to try to
make the image black and faint red. Because the output image will also
have maxval 1, there is no such thing as faint red. It has to be either
full-on red or black. *pgmtoppm* rounds the color 0f/00/00 down to
black, and you get an output image that is nothing but black.

The fix is easy: Pass the input through *pamdepth* on the way into
*pgmtoppm* to increase its depth to something that would give you the
resolution you need to get your desired color. In this case, *pamdepth
16* would do it. Or spare yourself the unnecessary thinking and just say
*pamdepth 255*.

PBM input is a special case. While you might think this would be
equivalent to a PGM with maxval 1 since only two gray levels are
necessary to represent a PBM image, *pgmtoppm*, like all Netpbm
programs, in fact treats it as a maxval of 255.

* SEE ALSO
*pamdepth*(1) , *rgb3toppm*(1) , *ppmtopgm*(1) , *ppmtorgb3*(1) ,
*ppm*(5) , *pgm*(5)

* AUTHOR
Copyright (C) 1991 by Jef Poskanzer.
