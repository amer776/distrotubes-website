#+TITLE: Man1 - ndctl-write-labels.1
#+DESCRIPTION: Linux manpage for ndctl-write-labels.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndctl-write-labels - write data to the label area on a dimm

* SYNOPSIS
#+begin_example
  ndctl write-labels <nmem> [-i <filename>]
#+end_example

\\

* DESCRIPTION
The namespace label area is a small persistent partition of capacity
available on some NVDIMM devices. The label area is used to resolve
aliasing between /pmem/ and /blk/ capacity by delineating namespace
boundaries. Read data from the input filename, or stdin, and write it to
the given <nmem> device. Note that the device must not be active in any
region, otherwise the kernel will not allow write access to the device's
label data area.

* OPTIONS
<memory device(s)>

#+begin_quote
  A /nmemX/ device name, or a dimm id number. Restrict the operation to
  the specified dimm(s). The keyword /all/ can be specified to indicate
  the lack of any restriction, however this is the same as not supplying
  a --dimm option at all.
#+end_quote

-s, --size=

#+begin_quote
  Limit the operation to the given number of bytes. A size of 0
  indicates to operate over the entire label capacity.
#+end_quote

-O, --offset=

#+begin_quote
  Begin the operation at the given offset into the label area.
#+end_quote

-b, --bus=

#+begin_quote
  A bus id number, or a provider string (e.g. "ACPI.NFIT"). Restrict the
  operation to the specified bus(es). The keyword /all/ can be specified
  to indicate the lack of any restriction, however this is the same as
  not supplying a --bus option at all.
#+end_quote

-v

#+begin_quote
  Turn on verbose debug messages in the library (if ndctl was built with
  logging and debug enabled).
#+end_quote

-i, --input

#+begin_quote
  input file
#+end_quote

* SEE ALSO
/UEFI NVDIMM Label Protocol/
<http://www.uefi.org/sites/default/files/resources/UEFI_Spec_2_7.pdf>
