#+TITLE: Man1 - gobject-query.1
#+DESCRIPTION: Linux manpage for gobject-query.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gobject-query - display a tree of types

* SYNOPSIS
*gobject-query* froots [OPTION...]

*gobject-query* tree [OPTION...]

* DESCRIPTION
*gobject-query* is a small utility that draws a tree of types.

*gobject-query* takes a mandatory argument that specifies whether it
should iterate over the fundamental types or print a type tree.

* COMMANDS
*froots*

#+begin_quote
  iterate over fundamental roots
#+end_quote

*tree*

#+begin_quote
  print type tree
#+end_quote

* OPTIONS
*-r* /TYPE/

#+begin_quote
  specify the root type
#+end_quote

*-n*

#+begin_quote
  dont descend type tree
#+end_quote

*-b* /STRING/

#+begin_quote
  specify indent string
#+end_quote

*-i* /STRING/

#+begin_quote
  specify incremental indent string
#+end_quote

*-s* /NUMBER/

#+begin_quote
  specify line spacing
#+end_quote

*-h*, *--help*

#+begin_quote
  Print brief help and exit.
#+end_quote

*-v*, *--version*

#+begin_quote
  Print version and exit.
#+end_quote
