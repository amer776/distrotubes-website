#+TITLE: Man1 - mrftopbm.1
#+DESCRIPTION: Linux manpage for mrftopbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
mrftopbm - convert an MRF image to PBM format

* SYNOPSIS
*mrftopbm* [ *-a* ] [ /input.mrf/ ]

* DESCRIPTION
This program is part of *Netpbm*(1)

*mrftopbm* converts an MRF image to PBM format.

*mrftopbm* takes the MRF image from the file named by the /input.mrf/
argument, or Standard Input if you don't specify /input.mrf/. The output
goes to Standard Output.

For more information about mrf, see *the*MRF specification (1)

* OPTIONS
- *-a* :: causes *mrftopbm* to include the edges, if any, in the output
  PBM. This may help when debugging a compressor's edge optimization.

* AUTHOR
Russell Marks.

* SEE ALSO
*pbmtomrf*(1) , *pbm*(5) , *mrf*(1)
