#+TITLE: Man1 - git-merge-one-file.1
#+DESCRIPTION: Linux manpage for git-merge-one-file.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-merge-one-file - The standard helper program to use with
git-merge-index

* SYNOPSIS
#+begin_example
  git merge-one-file
#+end_example

* DESCRIPTION
This is the standard helper program to use with /git merge-index/ to
resolve a merge after the trivial merge done with /git read-tree -m/.

* GIT
Part of the *git*(1) suite
