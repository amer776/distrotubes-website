#+TITLE: Man1 - amstex.1
#+DESCRIPTION: Linux manpage for amstex.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
amstex - structured text formatting and typesetting

* SYNOPSIS
*amstex* [/first-line/]

* DESCRIPTION
This manual page is not meant to be exhaustive. The complete
documentation for this version of TeX can be found in the info file or
manual /Web2C: A TeX implementation/.

The AmSTeX language is described in the book /The Joy of TeX/. AmSTeX is
a TeX macro package, not a modification to the TeX source program, so
all the capabilities described in *tex*(1) are present.

The AmSTeX macros encourage writers to think about the content of their
documents, rather than the form. The ideal, not always realized, is to
have no formatting commands (like “switch to italic” or “skip 2 picas”)
in the document at all; instead, everything is done by specific markup
instructions: “emphasize”, “start a section”.

For authors more familiar with LaTeX, an AMS-supported package called
/amsmath.sty/ is available, among others.

* FILES
- /amsguide.tex/ :: Documentation for AmSTeX, typesettable by TeX.

- /amfndoc.tex/ :: Documentation for AMS fonts, typesettable by TeX.

* SEE ALSO
*latex*(1), *tex*(1), *slitex*(1).\\
Michael Spivak, /The Joy of TeX/, 2nd edition, American Mathematical
Society, 1990, ISBN 0-8218-2997-1.
