#+TITLE: Man1 - kdenlive.1
#+DESCRIPTION: Linux manpage for kdenlive.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kdenlive - An open source non-linear video editor.

* SYNOPSIS
kdenlive [Qt-options] [KDE-options] [options] [file]

* DESCRIPTION
Kdenlive is an open source non-linear video editor which supports a
large number of formats for editing. It relies on several other open
source projects, such as FFmpeg and MLT video framework.

* OPTIONS
** 
** Arguments:
- *file* :: Document to open

** Options:
- *--mlt-path <path>* :: Set the path for MLT environment

- *--mlt-log <verbose/debug>* :: Set MLT log level

** 
** Generic options:
- *--help* :: Show help about options

- *--help-qt* :: Show Qt specific options

- *--help-kde* :: Show KDE specific options

- *--help-all* :: Show all options

- *--author* :: Show author information

- *-v, --version* :: Show version information

- *--license* :: Show license information

- *--* :: End of options

** 
** KDE options:
- *--qwindowtitle <caption>* :: Use 'caption' as name in the titlebar

- *--icon <icon>* :: Use 'icon' as the application icon

- *--config <filename>* :: Use alternative configuration file

- *--nocrashhandler* :: Disable crash handler, to get core dumps

- *--waitforwm* :: Waits for a WM_NET compatible windowmanager

- *--style <style>* :: sets the application GUI style

- *--geometry <geometry>* :: sets the client geometry of the main
  widget - see man X for the argument format

** 
** Qt options:
- *--display <displayname>* :: Use the X-server display 'displayname'

- *--session <sessionId>* :: Restore the application for the given
  'sessionId'

- *--cmap* :: Causes the application to install a private color map on
  an 8-bit display

- *--ncols <count>* :: Limits the number of colors allocated in the
  color cube on an 8-bit display, if the application is using the
  QApplication::ManyColor color specification

- *--nograb* :: tells Qt to never grab the mouse or the keyboard

- *--dograb* :: running under a debugger can cause an implicit -nograb,
  use -dograb to override

- *--sync* :: switches to synchronous mode for debugging

- *--fn, --font <fontname>* :: defines the application font

- *--bg, --background <color>* :: sets the default background color and
  an application palette (light and dark shades are calculated)

- *--fg, --foreground <color>* :: sets the default foreground color

- *--btn, --button <color>* :: sets the default button color

- *--name <name>* :: sets the application name

- *--title <title>* :: sets the application title (caption)

- *--visual TrueColor* :: forces the application to use a TrueColor
  visual on an 8-bit display

- *--inputstyle <inputstyle>* :: sets XIM (X Input Method) input style.
  Possible values are onthespot, overthespot, offthespot and root

- *--im <XIM server>* :: set XIM server

- *--noxim* :: disable XIM

- *--reverse* :: mirrors the whole layout of widgets

- *--stylesheet* :: <file.qss> applies the Qt stylesheet to the
  application widgets

** 
* ENVIRONMENT
- *KDENLIVE_RENDER_LOG* :: If $*KDENLIVE_RENDER_LOG* is set, a log will
  be written to /tmp/kdenlive_render.log.XXXXXXXX, that contains
  information about the result of calling melt, ffmpeg, etc. This can be
  used for debugging.

* SEE ALSO
*kdenlive_render*(1). Please see the homepage at
*https://www.kdenlive.org/*\\

* AUTHORS
#+begin_example
  Jean-Baptiste Mardelle <jb@kdenlive.org>

#+end_example
