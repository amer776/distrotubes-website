#+TITLE: Man1 - libmikmod-config.1
#+DESCRIPTION: Linux manpage for libmikmod-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libmikmod-config - script to get information about the installed version
of libmikmod.

* SYNOPSIS
*libmikmod-config [--prefix/[=DIR]/] [--exec-prefix/[=DIR]/]*
[--version] [--libs] [--cflags] [--ldadd]

* DESCRIPTION
/libmikmod-config/ is a tool that is used to determine the compiler and
linker flags that should be used to compile and link programs that use
/libmikmod/. It is also used internally by the libmikmod.m4 macro for
GNU autoconf that is included with /libmikmod/.

* OPTIONS
/libmikmod-config/ accepts the following options:

- *--version* :: Print the currently installed version of /libmikmod/ on
  the standard output.

- *--libs* :: Print the linker flags that are necessary to link a
  /libmikmod/ program.

- *--ldadd* :: Print the extra linker flags that are necessary to link a
  /libmikmod/ program if it was compiled with the GNU C compiler.

- *--cflags* :: Print the compiler flags that are necessary to compile a
  /libmikmod/ program.

- *--prefix=PREFIX* :: If specified, use PREFIX instead of the
  installation prefix that /libmikmod/ was built with when computing the
  output for the --cflags and --libs options. This option is also used
  for the exec prefix if --exec-prefix was not specified. This option
  must be specified before any --libs or --cflags options.

- *--exec-prefix=PREFIX* :: If specified, use PREFIX instead of the
  installation exec prefix that /libmikmod/ was built with when
  computing the output for the --cflags and --libs options. This option
  must be specified before any --libs or --cflags options.

* AUTHORS
/libmikmod/ is the result of the work of many people, including:
Jean-Paul Mikkers, Jake Stine, Miodrag Vallat, Steve McIntyre, Peter
Amstutz, and many others.

A full list of people having worked on libmikmod can be found in the
libmikmod source package.

This man page was inspired by the gtk-config man page written by Owen
Taylor.
