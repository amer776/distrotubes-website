#+TITLE: Man1 - wvtag.1
#+DESCRIPTION: Linux manpage for wvtag.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wvtag - metadata utility for wavpack files

* SYNOPSIS
*wvtag* [/-options/] /INFILE/...

* DESCRIPTION
*wvtag* applies the specified metadata operation(s) to each of the
specified WavPack source file(s) in this order: clean, import, delete,
write, extract, list. Note that WavPack metadata is stored in APEv2
tags, and this tool will automatically import from an ID3v1 tag if it is
the only tag present on the source file, and that ID3v1 tag will be
deleted and replaced with an APEv2 tag if an edit is requested.

* OPTIONS
*--allow-huge-tags*

#+begin_quote
  allow tag data up to 16 MB (embedding > 1 MB is not recommended for
  portable devices and may not work with some programs including WavPack
  pre-4.70)
#+end_quote

*-c*

#+begin_quote
  extract cuesheet only to stdout (equivalent to *-x “cuesheet”*)
#+end_quote

*-cc*

#+begin_quote
  extract cuesheet to source-name.cue file in same directory as source
  file (equivalent to *-xx “cuesheet=%a.cue”*)
#+end_quote

*--clean*, *--clear*

#+begin_quote
  clean all items from tag (done first)
#+end_quote

*-d “*/Field/”

#+begin_quote
  delete specified metadata item (text or binary)
#+end_quote

*-h*, *--help*

#+begin_quote
  this help display
#+end_quote

*--import-id3*

#+begin_quote
  import applicable tag items from ID3v2.3 tag present in DSF (and
  other) files into APEv2 tag (if there are > 1 MB cover images present
  add *--allow-huge-tags* to include them)
#+end_quote

*-l*, *--list*

#+begin_quote
  list all tag items (done last)
#+end_quote

*--no-utf8-convert*

#+begin_quote
  dont recode passed tags from local encoding to UTF-8, assume they are
  in UTF-8 already
#+end_quote

*-q*

#+begin_quote
  quiet (keep console output to a minimum)
#+end_quote

*-v*, *--version*

#+begin_quote
  write program version to stdout
#+end_quote

*-w “*/Field/=”

#+begin_quote
  delete specified metadata item (text or binary)
#+end_quote

*-w “*/Field/=/Value/”

#+begin_quote
  write specified text metadata to APEv2 tag
#+end_quote

*-w “*/Field/=@/file.ext/”

#+begin_quote
  write specified text metadata from file to APEv2 tag, normally used
  for embedded cuesheets and logs (field names “Cuesheet” and “Log”)
#+end_quote

*--write-binary-tag “*/Field/=@/file.ext/”

#+begin_quote
  write the specified binary metadata file to APEv2 tag, normally used
  for cover art with the specified field name “Cover Art (Front)”
#+end_quote

*-x “*/Field/”

#+begin_quote
  extract the specified tag field only to stdout
#+end_quote

*-xx “*/Field/[=/file/]”

#+begin_quote
  extract the specified tag field to named file in same directory as
  source file; optional filename specification may contain *%a* which is
  replaced with the audio file base name, *%t* replaced with the tag
  field name (note: comes from data for binary tags) and *%e* replaced
  with the extension from the binary tag source file (or “txt” for text
  tag).
#+end_quote

*-y*

#+begin_quote
  yes to all warnings (use with caution!)
#+end_quote

* SEE ALSO
*wavpack*(1), *wvunpack*(1), *wvgain*(1)

Please visit www.wavpack.com for more information

* COPYRIGHT
This manual page was written by David Bryant <david@wavpack.com>.
Permission is granted to copy, distribute and/or modify this document
under the terms of the BSD License.

* AUTHOR
*David Bryant* <david@wavpack.com>

#+begin_quote
  Author
#+end_quote

* COPYRIGHT
\\
Copyright © 2021 David Bryant\\
