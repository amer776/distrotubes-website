#+TITLE: Man1 - ppmdcfont.1
#+DESCRIPTION: Linux manpage for ppmdcfont.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmdcfont - Turn a Ppmdfont file into C source for a builtin font

* SYNOPSIS
*ppmdcfont*

* DESCRIPTION
This program is part of *Netpbm*(1)

(There are no arguments or options)

*ppmdcfont* creates a C source file that you can compile into a built-in
font for use with the Netpbm PPM drawing facilities. It reads a Ppmdfont
file on Standard Input and writes the C source code to Standard Output.

The output C source code has the font object's name hardcoded as
*ppmd_standardfont*, which you will definitely want to edit, because
that is the name of the font built in to *libnetpbm*. If you don't
change it, it will conflict both cognitively and in program linking.
There should obviously be an option on *ppmdcfont* to choose this, but
the development effort has not been justified so far.

See *Libnetpbm*PPM*Drawing*Function Manual (1) for details on Ppmdfont
files.

* SEE ALSO
*ppmdraw*(1) , *ppmddumpfont*(1) , *ppmdcfont*(1) ,
*Libnetpbm*PPM*Drawing*Function*Manual*(1)
