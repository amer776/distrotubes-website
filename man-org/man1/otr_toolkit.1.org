#+TITLE: Man1 - otr_toolkit.1
#+DESCRIPTION: Linux manpage for otr_toolkit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
otr_parse, otr_sesskeys, otr_mackey, otr_readforge, otr_modify,
otr_remac - Process Off-the-Record Messaging transcripts

* SYNOPSIS
*otr_parse*\\
*otr_sesskeys* /our_privkey their_pubkey/\\
*otr_mackey* /aes_enc_key/\\
*otr_readforge* /aes_enc_key [newmsg]/\\
*otr_modify* /mackey old_text new_text offset/\\
*otr_remac* /mackey sender_instance receiver_instance flags snd_keyid
rcv_keyid pubkey counter encdata revealed_mackeys/

* DESCRIPTION
Off-the-Record (OTR) Messaging allows you to have private conversations
over IM by providing: - Encryption - No one else can read your instant
messages. - Authentication - You are assured the correspondent is who
you think it is. - Deniability - The messages you send do /not/ have
digital signatures that are checkable by a third party. Anyone can forge
messages after a conversation to make them look like they came from you.
However, /during/ a conversation, your correspondent is assured the
messages he sees are authentic and unmodified. - Perfect forward
secrecy - If you lose control of your private keys, no previous
conversation is compromised.

The OTR Toolkit is useful for analyzing and/or forging OTR messages. Why
do we offer this? Primarily, to make absolutely sure that transcripts of
OTR conversations are really easy to forge after the fact. [Note that
/during/ an OTR conversation, messages can't be forged without real-time
access to the secret keys on the participants' computers, and in that
case, all security has already been lost.] Easily-forgeable transcripts
help us provide the "Deniability" property: if someone claims you said
something over OTR, they'll have no proof, as anyone at all can modify a
transcript to make it say whatever they like, and still have all the
verification come out correctly.

Here are the six programs in the toolkit:

- otr_parse - Parse OTR messages given on stdin, showing the values of
all the fields in OTR protocol messages.

- otr_sesskeys our_privkey their_pubkey - Shows our public key, the
session id, two AES and two MAC keys derived from the given
Diffie-Hellman keys (one private, one public).

- otr_mackey aes_enc_key - Shows the MAC key derived from the given AES
key.

- otr_readforge aes_enc_key [newmsg] - Decrypts an OTR Data message
using the given AES key, and displays the message. - If newmsg is given,
replace the message with that one, encrypt and MAC it properly, and
output the resulting OTR Data Message. This works even if the given key
was not correct for the original message, so as to enable complete
forgeries.

- otr_modify mackey old_text new_text offset - Even if you can't read
the data because you don't know either the AES key or the Diffie-Hellman
private key, but you can make a good guess that the substring "old_text"
appears at the given offset in the message, replace the old_text with
the new_text (which must be of the same length), recalculate the MAC
with the given mackey, and output the resulting Data message. - Note
that, even if you don't know any text in an existing message, you can
still forge messages of your choice using the otr_readforge command,
above.

- otr_remac mackey sender_instance receiver_instance flags snd_keyid
rcv_keyid pubkey counter encdata revealed_mackeys - Make a new OTR
protocol version 3 Data Message, with the given pieces (note that the
data part is already encrypted). MAC it with the given mackey.

* SEE ALSO
*Off-the-Record Messaging*, at [[https://www.cypherpunks.ca/otr/]]

* AUTHOR
otr_toolkit was written by the OTR Dev Team <otr@cypherpunks.ca>.
