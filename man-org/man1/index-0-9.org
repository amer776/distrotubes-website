#+TITLE: Man1 - 0-9
#+DESCRIPTION: Man1 - 0-9
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* 0-9
No results for '0-9'.

#+begin_src bash :exports results
readarray -t starts_with_num < <(find . -type f -iname "[0-9]*" | sort)

for x in "${starts_with_num[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./411toppm.1.org][411toppm.1]] |
| [[file:./7z.1.org][7z.1]]       |
| [[file:./7za.1.org][7za.1]]      |
| [[file:./7zr.1.org][7zr.1]]      |
