#+TITLE: Man1 - B
#+DESCRIPTION: Man1 - B
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* B
#+begin_src bash :exports results
readarray -t starts_with_b < <(find . -type f -iname "b*" | sort)

for x in "${starts_with_b[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./b2sum.1.org][b2sum.1]]       |
| [[file:./base32.1.org][base32.1]]      |
| [[file:./base64.1.org][base64.1]]      |
| [[file:./basename.1.org][basename.1]]    |
| [[file:./basenc.1.org][basenc.1]]      |
| [[file:./bash.1.org][bash.1]]        |
| [[file:./bashbug.1.org][bashbug.1]]     |
| [[file:./bat.1.org][bat.1]]         |
| [[file:./bbox.1.org][bbox.1]]        |
| [[file:./bc.1.org][bc.1]]          |
| [[file:./bcomps.1.org][bcomps.1]]      |
| [[file:./bdftopcf.1.org][bdftopcf.1]]    |
| [[file:./bg5conv.1.org][bg5conv.1]]     |
| [[file:./bibtex.1.org][bibtex.1]]      |
| [[file:./bibtex8.1.org][bibtex8.1]]     |
| [[file:./bibtexu.1.org][bibtexu.1]]     |
| [[file:./bioradtopgm.1.org][bioradtopgm.1]] |
| [[file:./bison.1.org][bison.1]]       |
| [[file:./blender.1.org][blender.1]]     |
| [[file:./bmptopnm.1.org][bmptopnm.1]]    |
| [[file:./bmptoppm.1.org][bmptoppm.1]]    |
| [[file:./bombadillo.1.org][bombadillo.1]]  |
| [[file:./bond2team.1.org][bond2team.1]]   |
| [[file:./bootctl.1.org][bootctl.1]]     |
| [[file:./brltty.1.org][brltty.1]]      |
| [[file:./broadwayd.1.org][broadwayd.1]]   |
| [[file:./broot.1.org][broot.1]]       |
| [[file:./brotli.1.org][brotli.1]]      |
| [[file:./brushtopbm.1.org][brushtopbm.1]]  |
| [[file:./brz.1.org][brz.1]]         |
| [[file:./bscalc.1.org][bscalc.1]]      |
| [[file:./bsdcat.1.org][bsdcat.1]]      |
| [[file:./bsdcpio.1.org][bsdcpio.1]]     |
| [[file:./bsdtar.1.org][bsdtar.1]]      |
| [[file:./bspwm.1.org][bspwm.1]]       |
| [[file:./bssh.1.org][bssh.1]]        |
| [[file:./btcflash.1.org][btcflash.1]]    |
| [[file:./bugpoint.1.org][bugpoint.1]]    |
| [[file:./busctl.1.org][busctl.1]]      |
| [[file:./bwrap.1.org][bwrap.1]]       |
| [[file:./bzip2.1.org][bzip2.1]]       |
