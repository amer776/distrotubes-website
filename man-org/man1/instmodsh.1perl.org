#+TITLE: Man1 - instmodsh.1perl
#+DESCRIPTION: Linux manpage for instmodsh.1perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
instmodsh - A shell to examine installed modules

* SYNOPSIS
instmodsh

* DESCRIPTION
A little interface to ExtUtils::Installed to examine installed modules,
validate your packlists and even create a tarball from an installed
module.

* SEE ALSO
ExtUtils::Installed
