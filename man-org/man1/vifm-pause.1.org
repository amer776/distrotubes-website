#+TITLE: Man1 - vifm-pause.1
#+DESCRIPTION: Linux manpage for vifm-pause.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vifm-pause - a helper script for vifm

* SYNOPSIS
Only called automagically from within vifm

vifm-pause

* DESCRIPTION
A helper script for vifm, not for standalone use.

Prints "Press return ..." message and waits until Enter key is pressed.

* SEE ALSO
*vifm*(1)

* AUTHOR
This manual page was initially written by Edelhard Becker
<becker@edelhard.de>, for the Debian GNU/Linux system (but may be used
by others).\\
Then it was adjusted by Hendrik Jaeger <deb@henk.geekmail.org> and
updated by xaizek <xaizek@posteo.net>.
