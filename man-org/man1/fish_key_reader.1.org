#+TITLE: Man1 - fish_key_reader.1
#+DESCRIPTION: Linux manpage for fish_key_reader.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fish_key_reader - explore what characters keyboard keys send

* SYNOPSIS

#+begin_quote

  #+begin_quote
    #+begin_example
      fish_key_reader [OPTIONS]
    #+end_example
  #+end_quote
#+end_quote

* DESCRIPTION
*fish_key_reader* is used to study input received from the terminal and
can help with key binds. The program is interactive and works on
standard input. Individual characters themselves and their hexadecimal
values are displayed.

The tool will write an example bind command matching the character
sequence captured to stdout. If the character sequence matches a special
key name (see *bind --key-names*), both *bind CHARS ...* and *bind -k
KEYNAME ...* usage will be shown. Additional details about the
characters received, such as the delay between chars, are written to
stderr.

The following options are available:

#+begin_quote

  - *-c* or *--continuous* begins a session where multiple key sequences
    can be inspected. By default the program exits after capturing a
    single key sequence.

  - *-h* or *--help* prints usage information.

  - *-v* or *--version* prints fish_key_reader's version and exits.
#+end_quote

* USAGE NOTES
The delay in milliseconds since the previous character was received is
included in the diagnostic information written to stderr. This
information may be useful to determine the optimal
*fish_escape_delay_ms* setting or learn the amount of lag introduced by
tools like *ssh*, *mosh* or *tmux*.

*fish_key_reader* intentionally disables handling of many signals. To
terminate *fish_key_reader* in *--continuous* mode do:

#+begin_quote

  - press *Control*+*C* twice, or

  - press *Control*+*D* twice, or

  - type *exit*, or

  - type *quit*
#+end_quote

* COPYRIGHT
2021, fish-shell developers
