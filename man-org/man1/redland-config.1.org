#+TITLE: Man1 - redland-config.1
#+DESCRIPTION: Linux manpage for redland-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
redland-config - script to get information about the installed version
of Redland

* SYNOPSIS
*redland-config* [--cflags] [--help] [--libs] [--libtool-libs]
[--prefix/[=DIR]/] [--private-libs] [--version-decimal] [--version]

* DESCRIPTION
/redland-config/ is a tool that is used to determine the compile and
linker flags that should be used to compile and link programs that use
the Redland RDF library.

* OPTIONS
/redland-config/ accepts the following options:

- *--version* :: Print the currently installed version of redland on the
  standard output.

- *--version-decimal* :: Print the currently installed version of
  redland as a decimal integer.

- *--libs* :: Print the linker flags that are necessary to link a
  redland program. This excludes linker arguments used to build the
  librdf shared library.

- *--libtool-libs* :: Print the flags that are necessary to link a
  redland program with libtool.

- *--private-libs* :: Print the linker flags that are necessary to build
  the librdf shared library. This option is not usually needed because
  the librdf shared library has already been dynamically linked against
  these libs.

- *--cflags* :: Print the compiler flags that are necessary to compile a
  redland program.

- *--prefix=PREFIX* :: If specified, use PREFIX instead of the
  installation prefix that redland was built with when computing the
  output for the --cflags and --libs options. This option must be
  specified before any --libs or --cflags options.

* SEE ALSO
*redland*(3),

* AUTHOR
Dave Beckett - [[http://www.dajobe.org/]]
