#+TITLE: Man1 - pip-download.1
#+DESCRIPTION: Linux manpage for pip-download.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pip-download - description of pip download command

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know why you
    came to this page and what on it helped you and what did not. (/Read
    more about this research/)
  #+end_quote
#+end_quote

* DESCRIPTION
Download packages from:

#+begin_quote

  - PyPI (and other indexes) using requirement specifiers.

  - VCS project urls.

  - Local project directories.

  - Local or remote source archives.
#+end_quote

pip also supports downloading from "requirements files", which provide
an easy way to specify a whole environment to be downloaded.

* USAGE

#+begin_quote

  #+begin_quote
    #+begin_example
      python -m pip download [options] <requirement specifier> [package-index-options] ...
      python -m pip download [options] -r <requirements file> [package-index-options] ...
      python -m pip download [options] <vcs project url> ...
      python -m pip download [options] <local project path> ...
      python -m pip download [options] <archive url/path> ...
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *-c, --constraint <file>* :: Constrain versions using the given
    constraints file. This option can be used multiple times.
#+end_quote

#+begin_quote
  - *-r, --requirement <file>* :: Install from the given requirements
    file. This option can be used multiple times.
#+end_quote

#+begin_quote
  - *--no-deps* :: Don't install package dependencies.
#+end_quote

#+begin_quote
  - *--global-option <options>* :: Extra global options to be supplied
    to the setup.py call before the install command.
#+end_quote

#+begin_quote
  - *--no-binary <format_control>* :: Do not use binary packages. Can be
    supplied multiple times, and each time adds to the existing value.
    Accepts either ":all:" to disable all binary packages, ":none:" to
    empty the set (notice the colons), or one or more package names with
    commas between them (no colons). Note that some packages are tricky
    to compile and may fail to install when this option is used on them.
#+end_quote

#+begin_quote
  - *--only-binary <format_control>* :: Do not use source packages. Can
    be supplied multiple times, and each time adds to the existing
    value. Accepts either ":all:" to disable all source packages,
    ":none:" to empty the set, or one or more package names with commas
    between them. Packages without binary distributions will fail to
    install when this option is used on them.
#+end_quote

#+begin_quote
  - *--prefer-binary* :: Prefer older binary packages over newer source
    packages.
#+end_quote

#+begin_quote
  - *--src <dir>* :: Directory to check out editable projects into. The
    default in a virtualenv is "<venv path>/src". The default for global
    installs is "<current dir>/src".
#+end_quote

#+begin_quote
  - *--pre* :: Include pre-release and development versions. By default,
    pip only finds stable versions.
#+end_quote

#+begin_quote
  - *--require-hashes* :: Require a hash to check each requirement
    against, for repeatable installs. This option is implied when any
    package in a requirements file has a --hash option.
#+end_quote

#+begin_quote
  - *--progress-bar <progress_bar>* :: Specify type of progress to be
    displayed [off|on|ascii|pretty|emoji] (default: on)
#+end_quote

#+begin_quote
  - *--no-build-isolation* :: Disable isolation when building a modern
    source distribution. Build dependencies specified by PEP 518 must be
    already installed if this option is used.
#+end_quote

#+begin_quote
  - *--use-pep517* :: Use PEP 517 for building source distributions (use
    --no-use-pep517 to force legacy behaviour).
#+end_quote

#+begin_quote
  - *-d, --dest <dir>* :: Download packages into <dir>.
#+end_quote

#+begin_quote
  - *--platform <platform>* :: Only use wheels compatible with
    <platform>. Defaults to the platform of the running system. Use this
    option multiple times to specify multiple platforms supported by the
    target interpreter.
#+end_quote

#+begin_quote
  - *--python-version <python_version>* :: The Python interpreter
    version to use for wheel and "Requires-Python" compatibility checks.
    Defaults to a version derived from the running interpreter. The
    version can be specified using up to three dot-separated integers
    (e.g. "3" for 3.0.0, "3.7" for 3.7.0, or "3.7.3"). A major-minor
    version can also be given as a string without dots (e.g. "37" for
    3.7.0).
#+end_quote

#+begin_quote
  - *--implementation <implementation>* :: Only use wheels compatible
    with Python implementation <implementation>, e.g. 'pp', 'jy', 'cp',
    or 'ip'. If not specified, then the current interpreter
    implementation is used. Use 'py' to force implementation-agnostic
    wheels.
#+end_quote

#+begin_quote
  - *--abi <abi>* :: Only use wheels compatible with Python abi <abi>,
    e.g. 'pypy_41'. If not specified, then the current interpreter abi
    tag is used. Use this option multiple times to specify multiple abis
    supported by the target interpreter. Generally you will need to
    specify --implementation, --platform, and --python-version when
    using this option.
#+end_quote

#+begin_quote
  - *--no-clean* :: Don't clean up build directories.
#+end_quote

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know:

    #+begin_quote

      1. What problem were you trying to solve when you came to this
         page?

      2. What content was useful?

      3. What content was not useful?
    #+end_quote
  #+end_quote
#+end_quote

* AUTHOR
pip developers

* COPYRIGHT
2008-2021, PyPA
