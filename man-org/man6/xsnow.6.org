#+TITLE: Manpages - xsnow.6
#+DESCRIPTION: Linux manpage for xsnow.6
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xsnow - Snow and Santa on your desktop

* SYNOPSIS
*xsnow* [/OPTION/]...

* DESCRIPTION
Xsnow shows an animation of Santa and snow on your desktop. Xsnow can
also run in one or more windows, see options -xwininfo, -id . (These
options only work satisfactorily in an X11 environment.) Xsnow depends
on an X11 environment. This is forced by setting the environment
variable GDK_BACKEND=x11 before initializing the GTK. Hopefully, this
will ensure that xsnow also runs in a Wayland environment for some time.

If xsnow is misbehaving, try to remove the file $HOME/.xsnowrc.

** "General options:
Below:\\
<n> denotes an unsigned decimal (e.g 123) or octal (e.g. 017) or hex
(e.g. 0x50009) number.\\
<c> denotes a string like "red" or "#123456".\\
<f> denotes a file name, like "/home/rick/Pictures/background.jpg".

- *-h, -help* :: print this text.

- *-H, -manpage* :: print man page.

- *-v, -version* :: prints version of xsnow.

- *-changelog* :: prints ChangeLog.

- *-selfrep* :: put tar ball on stdout, so you can do:\\
  xsnow -selfrep > xsnow.tar.gz

- *-display name* :: Drop the snowflakes on the given display. Make sure
  the display is nearby, so you can hear them enjoy...

- *-vintage* :: Run xsnow in vintage settings.

- *-defaults* :: Do not read config file (see FILES).

- *-noconfig* :: Do not read or write config file (see FILES).

- *-hidemenu* :: Start with hidden interactive menu.

- *-nomenu* :: Do not show interactive menu.

- *-scale <n>* :: Apply scalefactor (default: 100).

- *-doublebuffer <n>* :: 1: use double buffering; 0: do not use double
  buffering (default: 1). Only effective with '-root' or '-id' or
  '-xwininfo'.

- *-transparency <n>* :: Transparency in % (default: 0)

- *-theme <n>* :: 1: use xsnow theme for menu; 0: use system theme
  (default: 1)

- *-checkgtk <n>* :: 0: Do not check gtk version before starting the
  user interface. 1: Check gtk version before starting the user
  interface. (default: 1).

- *-id <n>, -window-id <n>* :: Snow in window with id (for example from
  xwininfo).

- *-desktop* :: Act as if window is a desktop.

- *-allworkspaces <n>* :: 0: use one desktop for snow, 1: use all
  desktops (default: 1).

- *-above* :: Snow above your windows. Default is to snow below your
  windows. NOTE: in some environments this results in an un-clickable
  desktop.

- *-xwininfo * :: Use a cursor to point at the window you want the snow
  to be fallen in.

- *-stopafter <n>* :: Stop xsnow after so many seconds.

- *-root * :: Force to paint on (virtual) root window.\\
  Use this for xscreensaver:\\
  Make sure xscreensaver is running, either as a start-up application\\
  or from the command line, e.g:\\
  nohup xscreensaver &\\
  or\\
  nohup xscreensaver -no-capture-stderr &\\
  Run the program xscreensaver-demo to create the file ~/.xscreensaver\\
  In the file ~.xscreensaver add after the line 'programs:' this line:\\
  xsnow -root\\
  Use the program xscreensaver-demo to select xsnow as screensaver.\\
  You probably want to select: Mode: Only One Screen Saver.

- *-bg <f> * :: file to be used as background when running under
  xscreensaver.

- *-noisy * :: Write extra info about some mouse clicks, X errors etc,
  to stdout.

- *-cpuload <n>* :: How busy is your system with xsnow: the higher, the
  more load on the system (default: 100).

** "Snow options:
- *-snowflakes <n>* :: The higher, the more snowflakes are generated per
  second. Default: 100.

- *-blowsnow* :: (Default) Animate blow-off snow.

- *-noblowsnow* :: Do not animate blowing snow from trees or windows

- *-sc <c> * :: Use the given string as color for the flakes (default:
  #fffafa).

- *-snowspeedfactor <n>* :: Multiply the speed of snow with this
  number/100 (default: 100).

- *-snowsize <n>* :: Set size of (non-vintage) snow flakes (default: 8).

- *-snow * :: (Default) Show snow.

- *-nosnow -nosnowflakes* :: Do not show snow.

- *-flakecountmax <n>* :: Maximum number of active flakes (default:
  300).

- *-blowofffactor <n>* :: The higher, the more snow is generated in
  blow-off scenarios (default: 40).

** "Tree options:
- *-treetype <n>[,<n> ...]* :: Choose tree types: minimum 0, maximum 7
  (default: 1,2,3,4,5,6,7,). Thanks to Carla Vermin for numbers >=3!
  Credits: Image by b0red on Pixabay.

- *-treetype all* :: (Default) Use all non-vintage available tree types.

- *-tc <c>* :: Use the given string as the color for the vintage tree
  (default: #7fff00). Works only for treetype 0.

- *-notrees* :: Do not display the trees.

- *-showtrees* :: (Default) Display the trees.

- *-trees <n>* :: Desired number of trees. Default 10.

- *-treefill <n>* :: Region in percents of the height of the window
  where trees grow (default: 30).

- *-treeoverlap* :: Allow scenery items to overlap each other (default).

- *-notreeoverlap* :: Do not allow scenery items to overlap each other.

** "Santa options:
- *-showsanta* :: (Default) Display Santa running all over the screen.

- *-nosanta* :: Do not display Santa running all over the screen.

- *-showrudolph* :: (Default) With Rudolph.

- *-norudolph* :: No Rudolph.

- *-santa <n>* :: The minimum size of Santa is 0, the maximum size is 4.
  Default is 3. Thanks to Thomas Linder for the (big) Santa 2! Santa 3
  is derived from Santa 2, and shows the required eight reindeer. The
  appearance of Santa 4 may be a surprise, thanks to Carla Vermin for
  this one.

- *-santaspeedfactor <n>* :: The speed Santa should not be excessive if
  he doesn't want to get fined. The appropriate speed for the Santa
  chosen will be multiplied by santaspeedfactor/100 (default: 100).

** "Celestial options:
- *-wind * :: (Default) It will get windy now and then.

- *-nowind * :: By default it gets windy now and then. If you prefer
  quiet weather specify -nowind.

- *-whirlfactor <n>* :: This sets the whirl factor, i.e. the maximum
  adjustment of the horizontal speed. The default value is 100.

- *-windtimer <n>* :: With -windtimer you can specify how often it gets
  windy. It's sort of a period in seconds, default value is 30.

- *-stars <n>* :: The number of stars (default: 20).

- *-meteorites* :: (Default) Show meteorites.

- *-nometeorites* :: Do not show meteorites.

- *-moon <n>* :: 1: show moon, 0: do not show moon (default: 1).\\
  Picture of moon thanks to Pedro Lasta on Unsplash.\\
  https://unsplash.com/photos/wCujVcf0JDw

- *-moonspeed <n>* :: Speed of moon in pixels/minute (default: 120).

- *-moonsize <n>* :: Realtive size of moon (default: 100).

- *-halo <n>* :: 1: show halo around moon, 0: do not show halo (default:
  1).

- *-halobrightness <n>* :: Brightness of halo (default: 25).

** "Fallen snow options:
- *-wsnowdepth <n>* :: Maximum thickness of snow on top of windows
  (default: 30).

- *-ssnowdepth <n>* :: Maximum thickness of snow at the bottom of the
  screen (default: 50).

- *-maxontrees <n>* :: Maximum number of flakes on trees. Default 200.

- *-keepsnowonwindows* :: (Default) Keep snow on top of the windows.

- *-nokeepsnowonwindows* :: Do not keep snow on top of the windows.

- *-keepsnowonscreen* :: (Default) Keep snow at the bottom of the
  screen.

- *-nokeepsnowonscreen* :: Do not keep snow at the bottom of the screen.

- *-keepsnowontrees* :: (Default) Keep snow on trees.

- *-nokeepsnowontrees* :: Do not keep snow on trees.

- *-keepsnow* :: (Default) Have snow sticking anywhere.

- *-nokeepsnow* :: Do not have snow sticking anywhere.

- *-fluffy* :: (Default) Create fluff on fallen snow.

- *-nofluffy* :: Do not create fluff on fallen snow.

- *-offsetx <n>* :: Correction for window-manager provided x-coordinate
  of window. Default 4.

- *-offsety <n>* :: Correction for window-manager provided y-coordinate
  of window. Default 0.

- *-offsetw <n>* :: Correction for window-manager provided width of
  window. Default -8.

- *-offsets <n>* :: Correction for bottom coordinate of your screen. A
  negative value lifts the xsnow screen up. Default 0.

- *-ignoretop <n>* :: Do not collect snow on window > 0.8*width of
  screen and closer than <n> pixels from the top. Sometimes an hidden
  window is sitting there, but treated as a normal window by xsnow.
  Default 0.

- *-ignorebottom <n>* :: Analog to -ignoretop, but now for the bottom.
  Default 0.

** "Birds options:
- *-anarchy <n>* :: Anarchy factor ( 0..100 default: 50).

- *-birdscolor <c> * :: Use the given string as color for the birds
  (default: #361A07).

- *-birdsonly <n>* :: Show only birds ( 0/1 default: 0).

- *-birdsspeed <n>* :: Speed of birds ( 0..300 default: 100).

- *-disweight <n>* :: Eagerness to keep desired distance ( 0..100
  default: 20).

- *-focuscentre <n>* :: Eagerness to fly to the focus ( 0..300 default:
  100).

- *-followneighbours <n>* :: Eagerness to follow neighbours ( 0..100
  default: 30).

- *-nbirds <n>* :: Number of birds ( 0..400 default: 70).

- *-neighbours <n>* :: Number of neighbours to watch ( 0..20 default:
  7).

- *-prefdistance <n>* :: Preferred distance to neighbours ( 0..100
  default: 40).

- *-showbirds <n>* :: Show birds ( 0/1 default: 1).

- *-showattr <n>* :: Show attraction point ( 0/1 default: 0).

- *-followsanta <n>* :: Birds like Santa ( 0/1 default: 0).

- *-viewingdistance <n>* :: Viewing distance ( 0..95 default: 40).

- *-birdsscale <n>* :: Scalefactor used painting the birds (default:
  100).

** "FILES
\\

- *$HOME/.xsnowrc* :: Settings are read from and written to this file.
  See flags -noconfig and -defaults how to influence this behaviour.\\
  NOTE: the following settings are not read or written:\\
  -above -defaults -desktop -fullscreen -noconfig -id\\
  -nomenu -stopafter -xwininfo -display -noisy -checkgtk

- *$HOME/xsnow/pixmaps/tree.xpm* :: If present, xsnow will try this file
  for displaying the trees. The format must be xpm (X PixMap) format,
  see https://en.wikipedia.org/wiki/X_PixMap .\\
  NOTE: when this file is present, no menu will appear.

- *$HOME/xsnow/pixmaps/santa<n>.xpm* :: where <n> = 1,2,3,4. If present,
  xsnow will try this files (4 of them) for displaying Santa. The format
  must be xpm (X PixMap) format, see
  https://en.wikipedia.org/wiki/X_PixMap .\\
  NOTE: when these files are present, no menu will appear.

** "EXAMPLES
\\
\\
$ xsnow -defaults # run with defaults.\\
$ xsnow # run using values from the config file.\\
$ xsnow -treetype 1,2 # use tree types 1 and 2.

** "BUGS
\\
\\
- Xsnow needs a complete rewrite: the code is a mess.\\
- The flags are not consistent, caused by trying to be compatible with
older versions.\\
- Xsnow stresses the Xserver too much.\\
- Xsnow does run in Wayland, but will not snow on all windows.\\
- Xsnow tries to create a click-through window. This is not successful
in for example FVWM/xcompmgr. In that case, xsnow tries to keep the snow
window below all others, resulting in a transient effect when you click
on the desktop. Sadly, no FVWM menu will appear...\\
- Remnants of fluffy snow can persist after removing the fallen snow.
These will gradually disappear, so no big deal.\\
- Remnants of meteorites can persist after passage of Santa. These will
eventually be wiped out by snow or Santa.\\
- Xsnow tries to adapt its snowing window if the display settings are
changed while xsnow is running. This does not function always well.\\
- Xsnow does not play well with 'xcompmgr -a'. In some environments
(Raspberry 64 bit) xcompmgr is started with the flag '-a', resulting in
a black snow window. Remedy: In a terminal window type: killall xcompmgr
nohup xcompmgr -n & and try again.\\
- In XFCE, compositing must be enabled for xsnow. Settings -> Window
Manager Tweaks -> Compositor -> Enable display compositing\\
- In multi-screen environments, it depends on the display settings if it
is snowing on all screens. Experiment!

* COPYRIGHT
\\
This is free software; see the source for copying conditions. There is
NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.
