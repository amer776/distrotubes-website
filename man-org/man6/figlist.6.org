#+TITLE: Manpages - figlist.6
#+DESCRIPTION: Linux manpage for figlist.6
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
figlist - lists figlet fonts and control files

* SYNOPSIS
*figlist* [ *-d* /directory/ ]

* DESCRIPTION
Lists all fonts and control files in figlet's default font directory.
Replaces "figlet -F", which was removed from figlet version 2.1.

* EXAMPLES
To use *figlist* with its default settings, simply type

#+begin_quote
  *example% figlist*
#+end_quote

To list all the font and control files in /usr/share/fonts/figlet

#+begin_quote
  *example% figlist -d /usr/share/fonts/figlet*
#+end_quote

* AUTHORS
figlist was written by Glenn Chappell <ggc@uiuc.edu>

This manual page was written by Jonathon Abbott for the Debian Project.

* SEE ALSO
*figlet*(6), *chkfont*(6), *showfigfonts*(6)
