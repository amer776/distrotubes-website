#!/usr/bin/env bash

### man1p ###
# An array of all files in the 'mandb' directory
readarray -t manfiles < <(find ../mandb/man1p -iname "*.*")

# For each file, convert to from 'man' to 'org'.
for file in "${manfiles[@]}"; do
   name=$(echo "$file" | awk -F / '{print $NF}')
   pandoc -o ../man-org/man1p/"$name".org -f man -t org "$file" && \
      echo "$file.org created." || echo "Error converting $file."
done

cd ../man-org/man1p/ || exit
yes | rm ./*.gz
